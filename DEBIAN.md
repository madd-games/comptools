# Debian Repository for CompTools

CompTools is available in the Madd Games Debian repository. The repository can be added using:

```
wget http://madd-games.org/deb/KEY.gpg
sudo apt-key add KEY.gpg
sudo add-apt-repository 'deb http://madd-games.org/deb/ /'
```

The binaries can then be installed using the following:

```
sudo apt-get install comptools
```

Development headers can be installed using the following:

```
sudo apt-get install comptools-dev
```
