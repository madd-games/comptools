/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stddef.h>

#include <comptools/cc/cc-ast.h>

#include "cc.h"

#include CC_ARCH_CONF_HEADER
#include CC_PLATFORM_CONF_HEADER

#ifndef CC_PLATFORM_PTRDIFF
#error "CC_PLATFORM_PTRDIFF not defined!"
#endif

#ifndef CC_PLATFORM_VA_LIST
#define	CC_PLATFORM_VA_LIST	"typedef struct { void *__va_next; } va_list;"
#endif

#define	XSTR(...) #__VA_ARGS__
#define	STR(...) XSTR(__VA_ARGS__)

/**
 * List of internal headers. These are headers from the free-standing environment,
 * guaranteed to exist even if libc does not provide them. With CompTools, we use
 * the libc headers if they exist, and internal headers otherwise.
 */
InternalHeader ccInternalHeaders[] = {
	{"stddef.h", "\
#ifndef __STDDEF_H\n\
#define	__STDDEF_H\n\
\n\
#define	NULL	((void*)0)\n\
#define	offsetof	__builtin_offsetof\n\
\n\
typedef " CC_PLATFORM_PTRDIFF " ptrdiff_t;\n\
typedef " CC_PLATFORM_SIZE " size_t;\n\
typedef " CC_PLATFORM_MAX_ALIGN " max_align_t;\n\
typedef long wchar_t;\n\
\n\
#endif"},

	{"stdarg.h", "\
#ifndef __STDARG_H\n\
#define	__STDARG_H\n\
\n\
" CC_PLATFORM_VA_LIST "\n\
typedef va_list __gnuc_va_list;		/* to stop glibc complaining */\n\
\n\
#define	va_start(ap, parmN)	__builtin_va_start(ap, parmN)\n\
#define	va_arg(ap, type)	__builtin_va_arg(ap, type)\n\
#define	va_end(ap)		__builtin_va_end(ap)\n\
#define	va_copy(dest, src)	__builtin_va_copy(dest, src)\n\
\n\
#endif"},
	
	{"limits.h", "\
#ifndef __LIMITS_H\n\
#define	__LIMIT_H_\n\
\n\
#define	CHAR_BITS		8\n\
#define	SCHAR_MIN		-128\n\
#define	SCHAR_MAX		127\n\
#define	UCHAR_MAX		255\n\
#define	CHAR_MIN		-128\n\
#define	CHAR_MAX		127\n\
#define	SHRT_MIN		" STR(-(1ULL << (8*CC_SIZEOF_SHORT-1))) "\n\
#define	SHRT_MAX		" STR((1ULL << (8*CC_SIZEOF_SHORT-1))-1) "\n\
#define	USHRT_MAX		" STR((1ULL << (8*CC_SIZEOF_SHORT))-1) "\n\
#define	INT_MIN			" STR(-(1ULL << (8*CC_SIZEOF_INT-1))) "\n\
#define	INT_MAX			" STR((1ULL << (8*CC_SIZEOF_INT-1))-1) "\n\
#define	UINT_MAX		" STR((1ULL << (8*CC_SIZEOF_INT))-1) "\n\
#define	LONG_MIN		" STR(-(1ULL << (8*CC_SIZEOF_LONG-1))) "\n\
#define	LONG_MAX		" STR((1ULL << (8*CC_SIZEOF_LONG-1))-1) "\n\
#define	ULONG_MAX		" STR((1ULL << (8*CC_SIZEOF_LONG)-1)) "\n\
#define	LLONG_MIN		" STR(-(1ULL << (8*CC_SIZEOF_LONG_LONG-1))) "\n\
#define	LLONG_MAX		" STR((1ULL << (8*CC_SIZEOF_LONG_LONG-1))-1) "\n\
#define	ULLONG_MAX		" STR((1ULL << (8*CC_SIZEOF_LONG_LONG)-1)) "\n\
\n\
#endif"},
	// LIST TERMINATOR
	{NULL, NULL}
};
