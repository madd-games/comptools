/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>
#include <errno.h>

#include <comptools/cc/cpp.h>
#include <comptools/cc/cc-parse.h>
#include <comptools/cc/cc-treedump.h>
#include <comptools/cc/cc-ast.h>
#include <comptools/cc/cc-emit.h>
#include <comptools/ctir/ctir.h>
#include <comptools/ctir/analysis.h>
#include <comptools/obj.h>
#include <comptools/opt.h>
#include <comptools/console.h>
#include <comptools/search.h>
#include <comptools/codegen.h>
#include <comptools/ld.h>

#include "cc.h"

#define	COMPTOOLS_CC_VERSION			"1"

#define CC_DEFINE_SCRIPTS
#include CC_ARCH_CONF_HEADER
#include CC_PLATFORM_CONF_HEADER
#undef CC_DEFINE_SCRIPTS

#ifndef CC_ARCH_DEFINES
#define	CC_ARCH_DEFINES
#endif

#ifndef CC_PLATFORM_DEFINES
#define	CC_PLATFORM_DEFINES
#endif

#ifndef CC_ARCH_INCLUDES
#define	CC_ARCH_INCLUDES
#endif

#ifndef CC_PLATFORM_INCLUDES
#define	CC_PLATFORM_INCLUDES
#endif

#ifndef CC_ARCH_LIBDIRS
#define	CC_ARCH_LIBDIRS
#endif

#ifndef CC_PLATFORM_LIBDIRS
#define CC_PLATFORM_LIBDIRS
#endif

#ifndef CC_DATA_MODEL
#error "No data model specified for the target!"
#endif

#ifndef CC_CODEGEN
#error "No code generator specified for the target!"
#endif

#ifndef LD_LIB_PATTERN
#define	LD_LIB_PATTERN		"%s/lib%s.so"
#endif

#ifndef LD_SONAME_PATTERN
#define	LD_SONAME_PATTERN	"lib%s.so"
#endif

const char **quoteIncDirs;
int numQuoteIncDirs;
const char **incDirs;
int numIncDirs;
const char **inlineMacros;
int numInlineMacros;
int stopAtCPP;
int stopAtCPPDump;
int stopAtTree;
int stopAtAST;
int stopAtCTIR;
int stopAtASM;
int stopAtObject;
int stopAtDeps;
int freestanding;
const char *outFile;
const char *depTarget;

int verbose = 0;
int linkShared = 0;
int linkReloc = 0;
const char **libDirs;
const char **libNames;
int numLibDirs;
int numLibNames;
const char *scriptFile;
const char *soname;
int nostartfiles;
int nostdlib;

ObjDriver *objdrv;

void addQuoteIncDir(const char *dirname)
{
	int index = numQuoteIncDirs++;
	quoteIncDirs = (const char**) realloc(quoteIncDirs, sizeof(void*) * numQuoteIncDirs);
	quoteIncDirs[index] = dirname;
};

void addIncDir(const char *dirname)
{
	int index = numIncDirs++;
	incDirs = (const char**) realloc(incDirs, sizeof(void*) * numIncDirs);
	incDirs[index] = dirname;
};

void addMacro(const char *spec)
{
	int index = numInlineMacros++;
	inlineMacros = (const char**) realloc(inlineMacros, sizeof(void*) * numInlineMacros);
	inlineMacros[index] = spec;
};

void addLibDir(const char *dirname)
{
	int index = numLibDirs++;
	libDirs = (const char**) realloc(libDirs, sizeof(void*) * numLibDirs);
	libDirs[index] = dirname;
};

void addLibName(const char *libname)
{
	int index = numLibNames++;
	libNames = (const char**) realloc(libNames, sizeof(void*) * numLibNames);
	libNames[index] = libname;
};

char* readWholeFile(FILE *fp)
{
	char *result = strdup("");
	char linebuf[2048];
	char *line;
	
	while ((line = fgets(linebuf, 2048, fp)) != NULL)
	{
		char *newBuffer = (char*) malloc(strlen(result) + strlen(line) + 1);
		sprintf(newBuffer, "%s%s", result, line);
		free(result);
		result = newBuffer;
	};
	
	return result;
};

Error ccIncluder(const char *headerName, const char **data, const char **filename)
{
	char *actualName = strdup(headerName+1);
	actualName[strlen(actualName)-1] = 0;
	
	char *path;
	if (headerName[0] == '"')
	{
		path = searchFile("%s/%s", quoteIncDirs, actualName);
		if (path != NULL)
		{
			FILE *fp = fopen(path, "r");
			if (fp == NULL)
			{
				free(path);
				return ERR_IO;
			};
			
			*filename = path;
			*data = readWholeFile(fp);
			
			fclose(fp);
			return ERR_OK;
		};
	};
	
	path = searchFile("%s/%s", incDirs, actualName);
	
	if (path == NULL)
	{
		InternalHeader *scan;
		for (scan=ccInternalHeaders; scan->name!=NULL; scan++)
		{
			if (strcmp(scan->name, actualName) == 0)
			{
				*filename = strdup(headerName);
				*data = scan->contents;
				free(actualName);
				return ERR_OK;
			};
		};
		
		free(actualName);
		return ERR_IO;
	};
		
	FILE *fp = fopen(path, "r");
	if (fp == NULL)
	{
		free(path);
		return ERR_IO;
	};
	
	*filename = path;
	*data = readWholeFile(fp);
	
	fclose(fp);
	return ERR_OK;
};

void addStartFiles(Linker *ld, const char **files)
{
	for (; *files!=NULL; files++)
	{
		char *path = searchFile("%s/%s", libDirs, *files);
		if (path == NULL)
		{
			fprintf(stderr, "CRITICAL: Cannot find startup file `%s'\n", *files);
			exit(1);
		};
		
		Object *obj = objRead(objdrv, path);
		if (obj == NULL)
		{
			fprintf(stderr, "CRITICAL: failed to load startup file `%s'\n", path);
			exit(1);
		};
		
		ldAddInput(ld, obj);
		free(path);
	};
};

int main(int argc, char *argv[])
{
	AsDriver *asdrv = asGetDriver(CC_CODEGEN);
	if (asdrv == NULL)
	{
		fprintf(stderr, "%s: failed to load assembler driver `%s'\n", argv[0], CC_CODEGEN);
		return 1;
	};

	objdrv = objGetDriver(CC_DEFAULT_OBJ);
	if (objdrv == NULL)
	{
		fprintf(stderr, "%s: CRITICAL: could not find object file format driver `%s'\n",
			argv[0], CC_DEFAULT_OBJ);
		return 1;
	};

	CodeGenDriver *cgdrv = codegenGetDriver(CC_CODEGEN);
	if (cgdrv == NULL)
	{
		fprintf(stderr, "%s: failed to load code generator `%s'!\n", argv[0], CC_CODEGEN);
		abort();
		return 1;
	};

	int dummy;
	OptionList optlist;
	optInit(&optlist);
	ctirAddOptions(&optlist);
	if (cgdrv->genoptions != NULL) cgdrv->genoptions(&optlist);
	if (objdrv->drvOpts != NULL) objdrv->drvOpts(&optlist);
	optAdd(&optlist, "-E", "",
		"Stop at the pre-processing stage, and dump processed tokens to standard output.",
		OPT_FLAG, &stopAtCPP);
	optAdd(&optlist, "-P", "",
		"Stop at the pre-processing stage, and dump an equivalent C file to standard output.",
		OPT_FLAG, &stopAtCPPDump);
	optAdd(&optlist, "-I", "<DIRECTORY>", "Add a directory to the list of header search directories.",
		OPT_LIST, addIncDir);
	optAdd(&optlist, "-D", "<MACRO>[=<DEFINITION>]", "Define a pre-processor macro on the command line.",
		OPT_LIST, addMacro);
	optAdd(&optlist, "-ffreestanding", "", "Compile in free-standing mode.",
		OPT_FLAG, &freestanding);
	optAdd(&optlist, "-fdump-parse-tree", "", "Stop at the parse tree stage, and dump the parse tree to standard output.",
		OPT_FLAG, &stopAtTree);
	optAdd(&optlist, "-fdump-ast", "", "Stop at the AST stage, and dump the AST to standard output.",
		OPT_FLAG, &stopAtAST);
	optAdd(&optlist, "-fdump-ctir", "", "Stop at the CTIR generation stage, and dump the CTIR unit.",
		OPT_FLAG, &stopAtCTIR);
	optAdd(&optlist, "-S", "", "Stop at the assembly stage, and output the assembly to the output file.",
		OPT_FLAG, &stopAtASM);
	optAdd(&optlist, "-c", "", "Stop at the object code stage.",
		OPT_FLAG, &stopAtObject);
	optAdd(&optlist, "-o", "<OUTPUT-FILE>", "Specify the compiler output file.",
		OPT_SHORT_STRING, &outFile);
	optAdd(&optlist, "-t", "<linker-script>", "Specify the linker script to use.",
		OPT_SHORT_STRING, &scriptFile);
	optAdd(&optlist, "-shared", "", "Link as a shared library instead of executable.",
		OPT_FLAG, &linkShared);
	optAdd(&optlist, "-soname", "<SONAME>", "Specify the canonical name of a shared library.",
		OPT_STRING, &soname);
	optAdd(&optlist, "-L", "<DIRECTORY>", "Add a directory to the list of library search directories.",
		OPT_LIST, addLibDir);
	optAdd(&optlist, "-l", "<LIBRARY>", "Link the output against the specified library.",
		OPT_LIST, addLibName);
	optAdd(&optlist, "-r", "", "Link as a further relocatable file.",
		OPT_FLAG, &linkReloc);
	optAdd(&optlist, "-nostartfiles", "", "Do not link agasint CRT startup files.",
		OPT_FLAG, &nostartfiles);
	optAdd(&optlist, "-nostdlib", "", "Do not link against standard libraries.",
		OPT_FLAG, &nostdlib);
	optAdd(&optlist, "-fPIC", "", "Emit position-independent code with a `large' offset table. The semantics of what constitutes `large' depends "
					"on the target, and some targets may emit PIC by default.",
		OPT_FLAG, &dummy);
	optAdd(&optlist, "-fpic", "", "Emit position-independent code with a `small' offset table. The semantics of what constitutes `small' depends "
					"on the target, and some targets may emit PIC by default.",
		OPT_FLAG, &dummy);
	optAdd(&optlist, "-M", "", "Write a `make' rule to the output file, specifying the dependencies of the input file.",
		OPT_FLAG, &stopAtDeps);
	optAdd(&optlist, "-MM", "", "The same as `-M'", OPT_FLAG, &stopAtDeps);
	optAdd(&optlist, "-MT", "<TARGET>", "Specify the `make' target name for `-M'.", OPT_SHORT_STRING, &depTarget);
	optAdd(&optlist, "-Wall", "", "TODO", OPT_FLAG, &dummy);
	optAdd(&optlist, "-Werror", "", "Treat all warnings as errors.", OPT_FLAG, &conWerror);
	
	char **files = optParse(argc, argv, &optlist);
	if (files == NULL)
	{
		return 1;
	};
	
	if (files[0] == NULL)
	{
		fprintf(stderr, "%s: no input files\n", argv[0]);
		fprintf(stderr, "Use `%s --help' to get help.\n", argv[0]);
		return 1;
	};

	addQuoteIncDir(".");
	int i;
	for (i=0; files[i] != NULL; i++)
	{
		char *copy = strdup(files[i]);
		char *slashPos = strrchr(copy, '/');
		if (slashPos != NULL)
		{
			*slashPos = 0;
			addQuoteIncDir(copy);
		}
		else
		{
			free(copy);
		};
	};
	
	if (!freestanding)
	{
		addIncDir(CC_SYSROOT "/usr/include");
		addIncDir(CC_SYSROOT "/usr/include/" CC_TARGET);
		addIncDir(CC_SYSROOT "/usr/local/include");
		addIncDir(CC_SYSROOT "/usr/local/include/" CC_TARGET);
		
		addLibDir(CC_SYSROOT "/usr/lib");
		addLibDir(CC_SYSROOT "/usr/lib/" CC_TARGET);
		addLibDir(CC_SYSROOT "/lib");
		addLibDir(CC_SYSROOT "/lib/" CC_TARGET);
		addLibDir(CC_SYSROOT "/usr/local/lib");
		addLibDir(CC_SYSROOT "/usr/local/lib/" CC_TARGET);
	};
	CC_ARCH_INCLUDES;
	CC_PLATFORM_INCLUDES;
	CC_ARCH_LIBDIRS;
	CC_PLATFORM_LIBDIRS;
	
	quoteIncDirs = (const char**) realloc(quoteIncDirs, sizeof(void*) * (numQuoteIncDirs+1));
	quoteIncDirs[numQuoteIncDirs] = NULL;
	incDirs = (const char**) realloc(incDirs, sizeof(void*) * (numIncDirs+1));
	incDirs[numIncDirs] = NULL;

	libDirs = (const char**) realloc(libDirs, sizeof(void*) * (numLibDirs+1));
	libNames = (const char**) realloc(libNames, sizeof(void*) * (numLibNames+1));
	libDirs[numLibDirs] = NULL;
	libNames[numLibNames] = NULL;

	Linker *ld = NULL;
	if (stopAtCPP || stopAtCPPDump || stopAtTree || stopAtAST || stopAtCTIR || stopAtASM || stopAtObject || stopAtDeps)
	{
		if (files[1] != NULL)
		{
			fprintf(stderr, "%s: multiple files specified, but I'm not linking!\n", argv[0]);
			return 1;
		};
	}
	else
	{
		const char *script;
		if (scriptFile == NULL)
		{
			if (freestanding)
			{
				fprintf(stderr, "%s: no linker script specified whilst in free-standing mode (-ffreestanding)\n", argv[0]);
				return 1;
			};
			
			scriptFile = "<internal>";
			if (linkShared)
			{
				script = ccSharedLDS;
			}
			else if (linkReloc)
			{
				script = "";
			}
			else
			{
				script = ccExecLDS;
			};
		};
		
		ld = ldCreate(script, scriptFile);
		
		if (!nostartfiles && !freestanding)
		{
			if (linkShared)
			{
				addStartFiles(ld, ccSharedPrefixFiles);
			}
			else if (!linkReloc)
			{
				addStartFiles(ld, ccExecPrefixFiles);
			};
		};
	};
	
	for (i=0; files[i] != NULL; i++)
	{
		const char *sourceFile = files[i];
		
		if (ld != NULL && strlen(sourceFile) > 2 && strcmp(&sourceFile[strlen(sourceFile)-2], ".o") == 0)
		{
			Object *obj = objRead(objdrv, sourceFile);
			if (obj == NULL)
			{
				fprintf(stderr, "%s: failed to load object file `%s'\n", argv[0], sourceFile);
				return 1;
			};
			
			ldAddInput(ld, obj);
			continue;
		};
		
		FILE *fp = fopen(sourceFile, "r");
		if (fp == NULL)
		{
			fprintf(stderr, "%s: could not open %s: %s\n", argv[0], sourceFile, strerror(errno));
			return 1;
		};
		
		char *data = readWholeFile(fp);
		fclose(fp);
		
		CPP *cpp = cppNew();
		cpp->includer = ccIncluder;
		
		// standard #defines
		cppDefine(cpp, "__STDC__", "1");
		if (!freestanding) cppDefine(cpp, "__STDC_HOSTED__", "1");
		else cppDefine(cpp, "__STDC_HOSTED__", "0");
		cppDefine(cpp, "__STDC_VERSION__", "201710L");		/* C18 */
		
		// CompTools-specific #defines
		cppDefine(cpp, "__COMPTOOLS__", COMPTOOLS_CC_VERSION);
		
		// platform specific #defines, as given by the platform configuration
		// header
		CC_ARCH_DEFINES;
		CC_PLATFORM_DEFINES;
		
		// inline macros (defined with -D)
		int j;
		for (j=0; j<numInlineMacros; j++)
		{
			char *spec = strdup(inlineMacros[j]);
			
			const char *name = spec;
			const char *value = "";
			
			char *equalsPos = strchr(spec, '=');
			if (equalsPos != NULL)
			{
				*equalsPos = 0;
				value = equalsPos+1;
			};
			
			cppDefine(cpp, name, value);
			if (conGetError() != 0) return 1;
		};
		
		Token *toklist = cppRun(cpp, data, sourceFile);
		if (conGetError() != 0) return 1;
		
		if (stopAtDeps)
		{
			if (depTarget == NULL)
			{
				char *copy = strdup(sourceFile);
				copy[strlen(copy)-1] = 'o';
				depTarget = copy;
			};
			
			FILE *fp;
			if (outFile == NULL)
			{
				fp = stdout;
			}
			else
			{
				fp = fopen(outFile, "w");
			};
			
			fprintf(fp, "%s:", depTarget);
			
			int j;
			for (j=0; j<cpp->numDeps; j++)
			{
				fprintf(fp, " %s", cpp->deps[j]);
			};
			
			fprintf(fp, "\n");
			return 0;
		};
		
		if (stopAtCPP)
		{
			lexDumpTokenList(toklist);
			return 0;
		};
		
		if (stopAtCPPDump)
		{
			Token *tok;
			for (tok=toklist; tok->type!=TOK_END; tok=tok->next)
			{
				if (tok->prevType == TOK_CC_NEWLINE)
				{
					printf("\n");
				}
				else if (tok->prevType == TOK_WHITESPACE)
				{
					printf(" ");
				};
				
				printf("%s", tok->value);
			};
			
			printf("\n");
			return 0;
		};
		
		CC_Context *ctx = ccParse(toklist);
		if (conGetError() != 0) return 1;
		
		if (stopAtTree)
		{
			ccDumpTree(ctx);
			return 0;
		};
		
		CC_DataModel dataModel = CC_DATA_MODEL;
		CC_Compiler *cc = ccNew(&dataModel);
		CC_Unit *unit = ccCompile(cc, ctx);
		
		if (unit == NULL)
		{
			return 1;
		};
		
		if (stopAtAST)
		{
			ccDumpUnit(unit);
			return 0;
		};
		
		CTIR_Unit *ctir = ccEmit(cc, unit);
		if (ctir == NULL)
		{
			return 1;
		};
		
		ctirOptimize(ctir);
		if (conGetError() != 0) return 1;
		
		if (stopAtCTIR)
		{
			ctirDump(ctir);
			return 0;
		};
		
		As *as;
		Object *obj;
		if (stopAtASM)
		{
			FILE *fp;
			if (outFile == NULL)
			{
				fp = stdout;
			}
			else
			{
				fp = fopen(outFile, "w");
				if (fp == NULL)
				{
					fprintf(stderr, "%s: cannot open output file `%s': %s\n", argv[0], outFile, strerror(errno));
					return 1;
				};
			};
			
			as = asPseudo(fp);
		}
		else
		{
			obj = objNew(objdrv);
			if (obj == NULL)
			{
				fprintf(stderr, "%s: failed to create object\n", argv[0]);
				return 1;
			};
			
			as = asNew(obj, asdrv);
			if (as == NULL)
			{
				fprintf(stderr, "%s: failed to create assembler session\n", argv[0]);
				return 1;
			};
		};
		
		if (codegen(cgdrv, as, ctir) != 0)
		{
			return 1;
		};
		
		if (stopAtASM) return 0;
		
		if (stopAtObject)
		{
			if (outFile == NULL) outFile = "a.out";
			
			Error err = objWrite(obj, outFile);
			if (err != ERR_OK)
			{
				fprintf(stderr, "%s: failed to write object `%s': error 0x%04X\n",
						argv[0], outFile, err);
				return 1;
			};
		};
		
		if (conGetError()) return 1;
		ldAddInput(ld, obj);
	};
	
	if (!nostartfiles && !freestanding)
	{
		if (linkShared)
		{
			addStartFiles(ld, ccSharedSuffixFiles);
		}
		else if (!linkReloc)
		{
			addStartFiles(ld, ccExecSuffixFiles);
		};
	};

	const char **lscan;
	for (lscan=libNames; *lscan!=NULL; lscan++)
	{
		const char *libname = *lscan;
		char *path = searchFile(LD_LIB_PATTERN, libDirs, libname);
		
		if (path == NULL)
		{
			fprintf(stderr, "%s: cannot find -l%s\n", argv[0], libname);
			return 1;
		};
		
		if (verbose)
		{
			fprintf(stderr, "%s: found -l%s at %s\n", argv[0], libname, path);
		};
		
		Object *obj = objRead(objdrv, path);
		if (obj == NULL)
		{
			fprintf(stderr, "%s: failed to load object file `%s'\n", argv[0], path);
			return 1;
		};
		
		if (obj->soname == NULL)
		{
			char *soname = (char*) malloc(strlen(LD_SONAME_PATTERN) + strlen(libname) + 1);
			sprintf(soname, LD_SONAME_PATTERN, libname);
			obj->soname = soname;
		};
		
		ldAddInput(ld, obj);
	};
	
	lscan = NULL;
	if (!freestanding)
	{
		if (linkShared)
		{
			lscan = ccSharedLibs;
		}
		else if (!linkReloc)
		{
			lscan = ccExecLibs;
		};
	};
	
	if (lscan != NULL)
	{
		for (; *lscan!=NULL; lscan++)
		{
			const char *libname = *lscan;
			char *path = searchFile("%s/%s", libDirs, libname);
			
			if (path == NULL)
			{
				fprintf(stderr, "%s: cannot find standard library `%s'\n", argv[0], libname);
				return 1;
			};
			
			if (verbose)
			{
				fprintf(stderr, "%s: found `%s' at %s\n", argv[0], libname, path);
			};
			
			Object *obj = objRead(objdrv, path);
			if (obj == NULL)
			{
				fprintf(stderr, "%s: failed to load object file `%s'\n", argv[0], path);
				return 1;
			};
			
			if (obj->soname == NULL)
			{
				char *soname = (char*) malloc(strlen(LD_SONAME_PATTERN) + strlen(libname) + 1);
				sprintf(soname, LD_SONAME_PATTERN, libname);
				obj->soname = soname;
			};
			
			ldAddInput(ld, obj);
		};
	};

	if (outFile == NULL)
	{
		outFile = "a.out";
	};
	
	if (soname == NULL)
	{
		ldSetSoname(ld, outFile);
	}
	else
	{
		ldSetSoname(ld, soname);
	};
	
	if (linkShared)
	{
		ldOutputType(ld, CC_SHARED_OBJTYPE);
	}
	else if (linkReloc)
	{
		ldOutputType(ld, OBJTYPE_RELOC);
	}
	else
	{
		ldOutputType(ld, CC_EXEC_OBJTYPE);
	};

	Object *result = ldRun(ld, objdrv);
	if (result == NULL)
	{
		return 1;
	};
	
	if (verbose) fprintf(stderr, "%s: writing object file `%s'\n", argv[0], outFile);
	
	Error err = objWrite(result, outFile);
	if (err != ERR_OK)
	{
		fprintf(stderr, "%s: failed to write output file `%s': error 0x%04X\n",
				argv[0], outFile, err);
		return 1;
	};

	return 0;
};
