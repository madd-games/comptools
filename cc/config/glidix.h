/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define	CC_TARGET_GLIDIX

#define	GLIDIX_XSTR(...) #__VA_ARGS__
#define	GLIDIX_STR(...) GLIDIX_XSTR(__VA_ARGS__)

/**
 * Configuration file for Glidix.
 */
#define	CC_PLATFORM_DEFINES \
	cppDefine(cpp, "__glidix__", ""); \
	cppDefine(cpp, "__unix__", ""); \
	cppDefine(cpp, "__posix__", ""); \
	cppDefine(cpp, "__GLIDIX__", ""); \
	cppDefine(cpp, "__UNIX__", ""); \
	cppDefine(cpp, "__POSIX__", "");

/**
 * Default to a base address of 0 if unknown.
 */
#ifndef CC_DEFAULT_BASE_ADDR
#define	CC_DEFAULT_BASE_ADDR	0
#endif

/**
 * Linker scripts for executables and shared libraries.
 */
#ifdef CC_DEFINE_SCRIPTS
const char *ccExecLDS = GLIDIX_STR(
	. = CC_DEFAULT_BASE_ADDR;

	/**
	 * Glidix executables start at the symbol `_start', which is defined in `_start.o'.
	 */
	entry _start;

	/**
	 * The .text section of an executable is laid out as follows:
	 * First, the init proloug (.init_p) is merged with the .init section,
	 * then with its epilog (.init_e). Same thing for .fini. This way, _start
	 * can call the global constructors and destructors by calling _init()
	 * and _fini() functions which it defines locally. We don't emit .init
	 * or .fini sections into executables, and instead allow _start to invoke them,
	 * so that these can be executed immediately before and after main().
	 */
	section .text
	{
		load .init_p;
		load .init;
		load .init_e;
		
		load .fini_p;
		load .fini;
		load .fini_e;
		
		load .text;
	};

	/**
	 * Other sections are as expected.
	 */
	align 0x1000;
	section .rodata
	{
		load .rodata;
	};

	align 0x1000;
	section .data
	{
		load .data;
	};

	align 0x1000;
	section .bss
	{
		load .bss;
	};
);

const char *ccSharedLDS = GLIDIX_STR(
	. = CC_DEFAULT_BASE_ADDR;
	
	/**
	 * For shared libraries, we need `.init' and `.fini' sections, so that the dynamic linker
	 * can run global constructors and destructors. The file `_crtlib.o' contains sections
	 * `.init_p' (init prolog) and `.init_e' (init epilog) and the same for `.fini'; these must
	 * be put before and after the constructor/destructor code. Then all other sections are as
	 * expected.
	 */
	section .init
	{
		load .init_p;
		load .init;
		load .init_e;
	};
	
	align 0x1000;
	section .fini
	{
		load .fini_p;
		load .fini;
		load .fini_e;
	};
	
	align 0x1000;
	section .text
	{
		load .text;
	};
	
	align 0x1000;
	section .rodata
	{
		load .rodata;
	};
	
	align 0x1000;
	section .data
	{
		load .data;
	};
	
	align 0x1000;
	section .bss
	{
		load .bss;
	};
);

/**
 * Names of object files to link before and after the files specified to the compiler. These are omitted if the
 * `-nostartfiles' command-line switch is passed. For Glidix, we must link in `_start.o' when building an
 * executable, and `_crtlib.o' when linking a shared library. Nothing needs to be linked in at the end.
 * The files are searched in the library directories.
 */
const char *ccExecPrefixFiles[] = {"_start.o", NULL};
const char *ccExecSuffixFiles[] = {NULL};
const char *ccSharedPrefixFiles[] = {"_crtlib.o", NULL};
const char *ccSharedSuffixFiles[] = {NULL};

/**
 * Names of libraries to link against by default. These are skipped if `-nostdlib' is given. For Glidix, we link
 * against the C library by default.
 */
const char *ccExecLibs[] = {"libc.so", NULL};
const char *ccSharedLibs[] = {"libc.so", NULL};

#endif	/* CC_DEFINE_SCRIPTS */

/**
 * What object file types to use when linking executables and shared libraries.
 */
#define	CC_EXEC_OBJTYPE		OBJTYPE_EXEC
#define	CC_SHARED_OBJTYPE	OBJTYPE_SHARED
