/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define	CC_TARGET_X86_64

/**
 * Configuration file for x86_64. That means the x86_64 (AMD64) architecture, with LP64;
 * x32 is defined elsewhere.
 */

#define	CC_CODEGEN "x86_64"

#define	CC_ARCH_DEFINES	\
	cppDefine(cpp, "__x86_64__", ""); \
	cppDefine(cpp, "__X86_64__", ""); \
	cppDefine(cpp, "__LP64__", "");

/**
 * Default type specifications on x86_64. Platform configuration headers may #undef them
 * and replace with their own if necessary.
 */
#define	CC_PLATFORM_PTRDIFF		"long"
#define	CC_PLATFORM_SIZE		"unsigned long"
#define	CC_PLATFORM_MAX_ALIGN		"unsigned long"

/**
 * Default data model for x86_64. Platform configuration headers may change this.
 */
#define	CC_SIZEOF_SHORT			2
#define	CC_SIZEOF_INT			4
#define	CC_SIZEOF_LONG			8
#define	CC_SIZEOF_LONG_LONG		8

#define	CC_DATA_MODEL	{\
	.sizeofShort = CC_SIZEOF_SHORT,\
	.sizeofInt = CC_SIZEOF_INT,\
	.sizeofLong = CC_SIZEOF_LONG,\
	.sizeofLongLong = CC_SIZEOF_LONG_LONG,\
	.sizeofFloat = 4,\
	.sizeofDouble = 8,\
	.sizeofLongDouble = 8,\
	.sizeofFloatComplex = 8,\
	.sizeofDoubleComplex = 16,\
	.sizeofLongDoubleComplex = 16,\
	.sizeofBool = 1,\
	.sizeofPtr = 8\
};

/**
 * Default base address for executables. This is just a hint for the platform config header.
 */
#define	CC_DEFAULT_BASE_ADDR		0x400000

/**
 * Page size.
 */
#define	CC_PAGE_SIZE			0x1000
