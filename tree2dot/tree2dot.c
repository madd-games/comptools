/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define	MAX_CHILD_NODES				64

/**
 * A very simple tool which converts the XML-formatted trees from CompTools into DOT
 * descriptions, to help them be visualized. Reads from standard input, writes to
 * standard output. This tool is very simple and only correctly recognises trees
 * output by CompTools, using additional assumptions - it is NOT a generic XML
 * parser!
 */

int nodeCounter = 0;

typedef struct Node_
{
	struct Node_ *parent;
	const char *label;
	int nodeNumber;
	int numChildNodes;
	struct Node_ *children[MAX_CHILD_NODES];
} Node;

void outLabels(Node *node)
{
	printf("\t%d [label=\"%s\"];\n", node->nodeNumber, node->label);
	
	int i;
	for (i=0; i<node->numChildNodes; i++)
	{
		outLabels(node->children[i]);
	};
};

void dumpGraph(Node *node)
{
	// draw arrow from parent to me if i have one
	if (node->parent != NULL)
	{
		printf("\t%d -> %d;\n", node->parent->nodeNumber, node->nodeNumber);
	};
	
	int i;
	for (i=0; i<node->numChildNodes; i++)
	{
		dumpGraph(node->children[i]);
	};
};

int main()
{
	Node *topNode = (Node*) calloc(1, sizeof(Node));
	topNode->label = "<root>";
	topNode->nodeNumber = nodeCounter++;
	Node *currentNode = topNode;
	
	char linebuf[2048];
	
	while (fgets(linebuf, 2048, stdin))
	{
		char *line = linebuf;
		char *newline = strchr(line, '\n');
		if (newline != NULL) *newline = 0;
		
		while (*line == '\t') line++;
		if (*line == 0) continue;
		
		assert(*line == '<');
		line++;
		
		if (*line == '/')
		{
			currentNode = currentNode->parent;
			continue;
		};
		
		char *label = line;
		line = strchr(line, '>');
		assert(line != NULL);
		*line = 0;
		line++;
		
		char *endpos = strstr(line, "</");
		if (endpos != NULL)
		{
			*endpos = 0;
			char *newLabel = (char*) malloc(strlen(label) + strlen(line) + 4);
			sprintf(newLabel, "%s : %s", label, line);
			
			Node *newNode = (Node*) calloc(1, sizeof(Node));
			newNode->parent = currentNode;
			newNode->label = newLabel;
			newNode->nodeNumber = nodeCounter++;
			
			currentNode->children[currentNode->numChildNodes++] = newNode;
		}
		else
		{
			Node *newNode = (Node*) calloc(1, sizeof(Node));
			newNode->parent = currentNode;
			newNode->label = strdup(label);
			newNode->nodeNumber = nodeCounter++;
			
			currentNode->children[currentNode->numChildNodes++] = newNode;
			currentNode = newNode;
		};
	};
	
	assert(topNode->numChildNodes == 1);
	topNode = topNode->children[0];
	topNode->parent = NULL;
	
	printf("digraph tree {\n");
	
	// assign labels first
	outLabels(topNode);
	
	// now output the connections
	dumpGraph(topNode);
	
	printf("}\n");
	return 0;
};
