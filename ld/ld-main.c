/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>
#include <errno.h>

#include <comptools/ld.h>
#include <comptools/obj.h>
#include <comptools/opt.h>
#include <comptools/console.h>
#include <comptools/search.h>

#ifndef LD_LIB_PATTERN
#define	LD_LIB_PATTERN		"%s/lib%s.so"
#endif

#ifndef LD_SONAME_PATTERN
#define	LD_SONAME_PATTERN	"lib%s.so"
#endif

const char *inputFormat = LD_DEFAULT_INPUT_FORMAT;
const char *outputFormat = LD_DEFAULT_OUTPUT_FORMAT;
const char *scriptFile = NULL;
const char *outFile = "a.out";
const char *soname = NULL;
int verbose = 0;
int linkShared = 0;
int linkReloc = 0;
const char **libDirs;
const char **libNames;
int numLibDirs;
int numLibNames;

void addLibDir(const char *dirname)
{
	int index = numLibDirs++;
	libDirs = (const char**) realloc(libDirs, sizeof(void*) * numLibDirs);
	libDirs[index] = dirname;
};

void addLibName(const char *libname)
{
	int index = numLibNames++;
	libNames = (const char**) realloc(libNames, sizeof(void*) * numLibNames);
	libNames[index] = libname;
};

char* readWholeFile(FILE *fp)
{
	char *result = strdup("");
	char linebuf[2048];
	char *line;
	
	while ((line = fgets(linebuf, 2048, fp)) != NULL)
	{
		char *newBuffer = (char*) malloc(strlen(result) + strlen(line) + 1);
		sprintf(newBuffer, "%s%s", result, line);
		free(result);
		result = newBuffer;
	};
	
	return result;
};

int main(int argc, char *argv[])
{
	ObjDriver* drvInput = objGetDriver(inputFormat);
	if (drvInput == NULL)
	{
		fprintf(stderr, "%s: failed to load object file format driver for input format `%s'\n",
				argv[0], inputFormat);
		return 1;
	};
	
	ObjDriver* drvOutput = objGetDriver(outputFormat);
	if (drvOutput == NULL)
	{
		fprintf(stderr, "%s: failed to load object file format driver for output format `%s'\n",
				argv[0], outputFormat);
		return 1;
	};

	OptionList optlist;
	optInit(&optlist);
	if (drvOutput->drvOpts != NULL) drvOutput->drvOpts(&optlist);
	optAdd(&optlist, "-v", "", "Be verbose: print additional debugging information.",
		OPT_FLAG, &verbose);
	optAdd(&optlist, "-t", "<linker-script>", "Specify the linker script to use.",
		OPT_SHORT_STRING, &scriptFile);
	optAdd(&optlist, "-o", "<output-file>", "Specify the output file to use.",
		OPT_SHORT_STRING, &outFile);
	optAdd(&optlist, "-shared", "", "Link as a shared library instead of executable.",
		OPT_FLAG, &linkShared);
	optAdd(&optlist, "-soname", "<SONAME>", "Specify the canonical name of a shared library.",
		OPT_STRING, &soname);
	optAdd(&optlist, "-L", "<DIRECTORY>", "Add a directory to the list of library search directories.",
		OPT_LIST, addLibDir);
	optAdd(&optlist, "-l", "<LIBRARY>", "Link the output against the specified library.",
		OPT_LIST, addLibName);
	optAdd(&optlist, "-r", "", "Link as a further relocatable file.",
		OPT_FLAG, &linkReloc);
		
	char **files = optParse(argc, argv, &optlist);
	if (files == NULL)
	{
		return 1;
	};
	
	if (files[0] == NULL)
	{
		fprintf(stderr, "%s: no input files\n", argv[0]);
		fprintf(stderr, "Use `%s --help' to get help.\n", argv[0]);
		return 1;
	};
	
	if (verbose)
	{
		fprintf(stderr, "%s: using input file format driver: %s\n", argv[0], inputFormat);
		fprintf(stderr, "%s: using output file format driver: %s\n", argv[0], outputFormat);
		fprintf(stderr, "%s: using linker script: %s\n", argv[0], scriptFile);
	};
	
	if (soname == NULL)
	{
		soname = outFile;
	};
	
	libDirs = (const char**) realloc(libDirs, sizeof(void*) * (numLibDirs+1));
	libNames = (const char**) realloc(libNames, sizeof(void*) * (numLibNames+1));
	libDirs[numLibDirs] = NULL;
	libNames[numLibNames] = NULL;
	
	if (scriptFile == NULL)
	{
		fprintf(stderr, "%s: no linker script specified\n", argv[0]);
		return 1;
	};
	
	FILE *fp = fopen(scriptFile, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "%s: cannot open linker script `%s': %s\n",
			argv[0], scriptFile, strerror(errno));
		return 1;
	};
	
	char* scriptSource = readWholeFile(fp);
	fclose(fp);
	
	if (verbose)
	{
		fprintf(stderr, "%s: dump of linker script in use:\n === BEGIN === \n", argv[0]);
		fprintf(stderr, "%s", scriptSource);
		fprintf(stderr, " === END ===\n");
	};
	
	Linker *ld = ldCreate(scriptSource, scriptFile);
	if (ld == NULL)
	{
		return 1;
	};
	
	char **fscan;
	for (fscan=files; *fscan!=NULL; fscan++)
	{
		const char *filename = *fscan;
		
		if (verbose)
		{
			fprintf(stderr, "%s: loading object `%s'\n", argv[0], filename);
		};
		
		Object *obj = objRead(drvInput, filename);
		if (obj == NULL)
		{
			fprintf(stderr, "%s: failed to load object file `%s'\n", argv[0], filename);
			return 1;
		};
		
		ldAddInput(ld, obj);
	};
	
	const char **lscan;
	for (lscan=libNames; *lscan!=NULL; lscan++)
	{
		const char *libname = *lscan;
		char *path = searchFile(LD_LIB_PATTERN, libDirs, libname);
		
		if (path == NULL)
		{
			fprintf(stderr, "%s: cannot find -l%s\n", argv[0], libname);
			return 1;
		};
		
		if (verbose)
		{
			fprintf(stderr, "%s: found -l%s at %s\n", argv[0], libname, path);
		};
		
		Object *obj = objRead(drvInput, path);
		if (obj == NULL)
		{
			fprintf(stderr, "%s: failed to load object file `%s'\n", argv[0], path);
			return 1;
		};
		
		if (obj->soname == NULL)
		{
			char *soname = (char*) malloc(strlen(LD_SONAME_PATTERN) + strlen(libname) + 1);
			sprintf(soname, LD_SONAME_PATTERN, libname);
			obj->soname = soname;
		};
		
		ldAddInput(ld, obj);
	};
	
	if (verbose)
	{
		fprintf(stderr, "%s: linking\n", argv[0]);
	};
	
	ldSetSoname(ld, soname);
	
	ldOutputType(ld, OBJTYPE_EXEC);
	if (linkShared)
	{
		ldOutputType(ld, OBJTYPE_SHARED);
	}
	else if (linkReloc)
	{
		ldOutputType(ld, OBJTYPE_RELOC);
	};
	
	Object *result = ldRun(ld, drvOutput);
	if (result == NULL)
	{
		return 1;
	};
	
	if (verbose) fprintf(stderr, "%s: writing object file `%s'\n", argv[0], outFile);
	
	Error err = objWrite(result, outFile);
	if (err != ERR_OK)
	{
		fprintf(stderr, "%s: failed to write output file `%s': error 0x%04X\n",
				argv[0], outFile, err);
		return 1;
	};
	
	return 0;
};
