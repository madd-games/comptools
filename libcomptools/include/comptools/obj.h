/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_OBJ_H_
#define COMPTOOLS_OBJ_H_

#include <stdio.h>

#include <comptools/types.h>
#include <comptools/error.h>
#include <comptools/opt.h>

/**
 * Types of sections.
 */
#define	SECTYPE_NOBITS				0
#define	SECTYPE_PROGBITS			1

/**
 * Section flags.
 */
#define	SEC_READ				(1 << 0)
#define	SEC_WRITE				(1 << 1)
#define	SEC_EXEC				(1 << 2)

/**
 * Symbol bindings.
 */
#define	SYMB_LOCAL				0
#define	SYMB_GLOBAL				1
#define	SYMB_WEAK				2

/**
 * Symbol types.
 */
#define	SYMT_NONE				0
#define	SYMT_OBJECT				1
#define	SYMT_FUNC				2
#define	SYMT_UNDEF				3
#define	SYMT_SECTION				4
#define	SYMT_COMMON				5

/**
 * Object types.
 */
#define	OBJTYPE_RELOC				0
#define	OBJTYPE_EXEC				1
#define	OBJTYPE_SHARED				2

/**
 * GxoAnnot node types.
 */
#define	ANNOT_Function				0x0001
#define	ANNOT_Address				0x0002
#define	ANNOT_Size				0x0003
#define	ANNOT_Name				0x0004
#define	ANNOT_Code				0x0005
#define	ANNOT_AcmPop				0x0006
#define	ANNOT_AcmPushConst			0x0007
#define	ANNOT_AcmPushStateEntry			0x0008
#define	ANNOT_AcmAdd				0x0009
#define	ANNOT_AcmSub				0x000A
#define	ANNOT_AcmMemoryFetch			0x000B
#define	ANNOT_Unwind				0x000C
#define	ANNOT_Offset				0x000D
#define	ANNOT_StateEntry			0x000E
#define	ANNOT_InsnPtr				0x000F
#define	ANNOT_AcmUndefined			0x0010
#define	ANNOT_LineInfo				0x0011
#define	ANNOT_File				0x0012
#define	ANNOT_Line				0x0013

/**
 * Declare all structures.
 */
struct Symbol_;
struct Reloc_;
struct Section_;
struct Object_;
struct ObjDriver_;
struct Annot_;

/**
 * Relocation type description.
 */
typedef struct
{
	/**
	 * Resolve the relocation. Store the result at 'ptr'. Return ERR_OK on success,
	 * or an error number on error. 'symval' is the value of the indicated symbol,
	 * and 'addend' is the addend.
	 */
	Error (*resolve)(void *ptr, uint64_t symval, int64_t addend, struct Reloc_ *reloc);
	
	/**
	 * Name of this relocation.
	 */
	const char *name;
	
	/**
	 * Number of bytes occupied by the relocation result; typically used to make sure
	 * that relocation cannot cause a buffer overflow when invalid.
	 */
	unsigned int bytes;
} RelocType;

/**
 * Description of a symbol.
 */
typedef struct Symbol_
{
	/**
	 * Link.
	 */
	struct Symbol_*				next;
	
	/**
	 * Symbol name.
	 */
	char*					name;
	
	/**
	 * Type of symbol (SYMT_*).
	 */
	int					type;

	/**
	 * Symbol binding (SYMB_*).
	 */
	int					binding;
	
	/**
	 * The section referenced by this symbol. NULL means no section. The exact
	 * meaning is dependent on the symbol type.
	 */
	struct Section_*			sect;
	
	/**
	 * Symbol value and size. Meaning depends on symbol type.
	 */
	uint64_t				value;
	uint64_t				size;
	
	/**
	 * Symbol alignment (if applicable). Only taken into account for common symbols.
	 */
	uint64_t				align;
	
	/**
	 * Driver-specific auxilliary data.
	 */
	void*					aux;
} Symbol;

/**
 * Description of a relocation.
 */
typedef struct Reloc_
{
	/**
	 * Link.
	 */
	struct Reloc_*				next;
	
	/**
	 * Relocation type.
	 */
	RelocType*				type;
	
	/**
	 * Section and offset into it, where the relocation result shall be
	 * stored.
	 */
	struct Section_*			sect;
	uint64_t				offset;
	
	/**
	 * Symbol against which we are relocating.
	 */
	Symbol*					sym;
	
	/**
	 * Addend (type-specific value).
	 */
	int64_t					addend;
	
	/**
	 * Which property of the symbol we are relocating against. If this is NULL,
	 * we are relocating against its address. Other values are object-format-dependent.
	 */
	char*					prop;
} Reloc;

/**
 * Description of a section.
 */
typedef struct Section_
{
	/**
	 * Link.
	 */
	struct Section_*			next;
	
	/**
	 * The object inside which this section is defined.
	 */
	struct Object_*				obj;
	
	/**
	 * Name of the section.
	 */
	char*					name;
	
	/**
	 * Relocations in this section.
	 */
	Reloc*					relocs;
	
	/**
	 * Section type (SECTYPE_*).
	 */
	int					type;
	
	/**
	 * Section flags (SEC_*).
	 */
	int					flags;
	
	/**
	 * Base address of the section, and its size.
	 */
	uint64_t				addr;
	uint64_t				size;
	
	/**
	 * Section alignment.
	 */
	uint64_t				align;
	
	/**
	 * Points to the section data in memory. This is always NULL for
	 * SECTYPE_NOBITS sections.
	 */
	void*					data;
	
	/**
	 * Symbol for this section.
	 */
	Symbol*					sym;
	
	/**
	 * Helper field, used for any purpose needed by the driver.
	 */
	void*					aux;
	
	/**
	 * Set to 1 when the section becomes loaded in an output object.
	 */
	int					loaded;
	
	/**
	 * If 'loaded', which output section and offset?
	 */
	struct Section_*			outsect;
	uint64_t				outoff;
	
	/**
	 * List of GXO annotations (only for GXO).
	 */
	struct Annot_*				annot;
} Section;

/**
 * Child of a <Function> node.
 */
typedef struct AnnotChild_
{
	/**
	 * Next child.
	 */
	struct AnnotChild_*			next;
	
	/**
	 * Node type.
	 */
	uint16_t				type;
	
	/**
	 * Buffer and size.
	 */
	void*					data;
	size_t					size;
} AnnotChild;

/**
 * GXO annotation for a function.
 */
typedef struct Annot_
{
	/**
	 * Next annotation.
	 */
	struct Annot_*				next;
	
	/**
	 * Function name.
	 */
	const char*				name;
	
	/**
	 * Offset into the section.
	 */
	uint64_t				sectOffset;
	
	/**
	 * Size of the function in bytes.
	 */
	uint64_t				size;
	
	/**
	 * Child nodes.
	 */
	AnnotChild*				children;
} Annot;

/**
 * Description of an object.
 */
typedef struct Object_
{
	/**
	 * List of sections in this object.
	 */
	Section*				sects;
	
	/**
	 * List of symbols defined by this object.
	 */
	Symbol*					syms;
	
	/**
	 * Entry symbol; NULL is unspecified.
	 */
	Symbol*					entry;
	
	/**
	 * Object file format driver.
	 */
	struct ObjDriver_*			driver;

	/**
	 * Driver-specific data.
	 */
	void*					drvdata;
	
	/**
	 * Object type (OBJTYPE_*). Default is OBJTYPE_RELOC.
	 */
	int					type;
	
	/**
	 * Shared object name (soname) or NULL if not defined.
	 */
	char*					soname;
	
	/**
	 * Dependencies of this object. The entries don't have to be full: they may
	 * for example miss sections if not known. But they must at the very least
	 * contain a soname.
	 */
	struct Object_**			depends;
	int					numDepends;
} Object;

/**
 * Describes an object file format driver.
 */
typedef struct ObjDriver_
{
	/**
	 * Name of the driver.
	 */
	const char *name;
	
	/**
	 * Option structure to be passed to init().
	 */
	void *opt;
	
	/**
	 * Initialize the driver on the specified object.
	 */
	void (*init)(Object *obj, const void *opt);
	
	/**
	 * Write the object to a file. Return ERR_OK if successful, or an error
	 * number on error.
	 */
	Error (*write)(Object *obj, FILE *fp);
	
	/**
	 * Read an object from a file. 'obj' is an object freshly created by objNew();
	 * this function must read from 'fp' and update 'obj' accordingly. Return
	 * ERR_OK if successful, or an error number on error.
	 */
	Error (*read)(Object *obj, FILE *fp);
	
	/**
	 * Called by the linker just before relocations are applied on executable and
	 * libraries. Return ERR_OK if everything went well, otherwise an error number.
	 *
	 * The object type was already set in obj->type.
	 */
	Error (*preReloc)(Object *obj);
	
	/**
	 * Called by the linker after all relocations have been performed. Return ERR_OK
	 * if everything went well, otherwise an error number.
	 */
	Error (*postReloc)(Object *obj);
	
	/**
	 * Add driver-specific options to the option list.
	 */
	void (*drvOpts)(OptionList *optlist);
} ObjDriver;

/**
 * Create a new object. Initially it contains no sections or symbols, and its
 * type is OBJTYPE_RELOC.
 */
Object* objNew(ObjDriver *driver);

/**
 * Get the named section from the object file. Returns the section on success, NULL
 * if not found.
 */
Section* objGetSection(Object *obj, const char *name);

/**
 * Create a new section with the specified attributes. You must first make sure the section
 * does not already exist, using objGetSection(); otherwise the results are undefined!
 */
Section* objCreateSection(Object *obj, const char *name, int type, int flags);

/**
 * Append data to a section. The section type must be SECTYPE_PROGBITS. Return ERR_OK on success,
 * or an error number on error.
 */
Error objSectionAppend(Section *sect, const void *data, size_t size);

/**
 * Reserve space inside a NOBITS section.
 */
Error objSectionResv(Section *sect, size_t size);

/**
 * Get a symbol with the specified name from the specified object. If it does not exist, then if
 * 'make' is 0, NULL is returned; if 'make' is nonzero, the symbol is created as an undefined symbol.
 */
Symbol* objGetSymbol(Object *obj, const char *name, int make);

/**
 * Append a relocation to a section. A relocation table entry is added, pointing at the end of the
 * section, and referencing the specified symbol; and zeroes are appended to the section data,
 * according to the size of the relocation type.
 *
 * Returns ERR_OK if successful, ERR_INVALID_PARAM if the section is not SECTYPE_PROGBITS.
 */
Error objSectionReloc(Section *sect, RelocType *type, Symbol *sym, int64_t addend, const char *prop);

/**
 * Resolve local references in an object. This converts all relocations against defined local symbols
 * to relocations against section symbols. Always returns ERR_OK.
 */
Error objResolveLocal(Object *obj);

/**
 * Write an object to a file. This process is implemented by the file format driver. Returns
 * ERR_OK if successful, or an error number on error.
 */
Error objWrite(Object *obj, const char *filename);

/**
 * Get an object file format driver by name, such as "flat" or "elf64-x86_64". Returns NULL if not found.
 */
ObjDriver* objGetDriver(const char *name);

/**
 * Add a new symbol at the end of the specified section in the specified object. Returns ERR_OK
 * on success; ERR_MULTIPLE_DEF if already defined.
 */
Error objAddSymbol(Object *obj, Section *sect, const char *name);
Error objAddSymbolWithType(Object *obj, Section *sect, const char *name, int type);

/**
 * Create a new function annotation node at the end of the specified section, giving the function the
 * specified name. Returns the annotation pointer.
 */
Annot* objAddAnnot(Section *sect, const char *name);

/**
 * Create a new annotation child, and add it to 'parent' if it's not NULL.
 */
AnnotChild* objCreateAnnotChild(Annot *parent);

/**
 * Indicate that the specified symbol ends at the end of the specified section.
 */
Error objEndSymbol(Object *obj, const char *name);

/**
 * Set the binding of a symbol. Cannot fail.
 */
void objSymbolBinding(Object *obj, const char *name, int binding);

/**
 * Define an absolute symbol. Returns ERR_OK if successful; ERR_MULTIPLE_DEF if already defined.
 */
Error objAbsoluteSymbol(Object *obj, const char *name, uint64_t value);

/**
 * Define a common symbol. Returns ERR_OK if successful; ERR_MULTIPLE_DEF if already defined
 * (with different properties).
 */
Error objCommonSymbol(Object *obj, const char *name, uint64_t align, uint64_t size);

/**
 * Read an object from a file. Returns the object on success, NULL on error (invalid file format).
 */
Object* objRead(ObjDriver *driver, const char *filename);

/**
 * Set the soname of an object.
 */
void objSetSoname(Object *obj, const char *soname);

/**
 * Add a dependency to 'obj'. 'dep' must be a shared object. Returns ERR_OK on success, or an
 * error number on error.
 */
Error objAddDepend(Object *obj, Object *dep);

#endif
