/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_CODEGEN_H_
#define COMPTOOLS_CODEGEN_H_

#include <stdio.h>
#include <stdarg.h>

#include <comptools/error.h>
#include <comptools/as.h>
#include <comptools/ctir/ctir.h>
#include <comptools/hashtab.h>
#include <comptools/opt.h>

struct CodeGen_;

/**
 * Represents an assembly instruction.
 */
typedef struct CodeGenInsn_
{
	/**
	 * Driver-specific data.
	 */
	void *drvdata;
	
	/**
	 * A "debug string" displayed on a dump of a dependency graph.
	 */
	char *debugstr;
	
	/**
	 * Number of CPU cycles used by this instruction.
	 */
	int cycles;
	
	/**
	 * Bitwise-OR of the CPU units consumed by this instruction.
	 */
	uint32_t units;
	
	/**
	 * List of instructions which must run before this one.
	 */
	int numDeps;
	struct CodeGenInsn_ **deps;
	
	/**
	 * Set to '1' when the instruction has been scheduled.
	 */
	int schedDone;
	
	/**
	 * If not NULL, forces the specified instruction to be emitted immediately
	 * after this one.
	 */
	struct CodeGenInsn_ *schedNext;
} CodeGenInsn;

/**
 * Represents a basic block of instructions.
 */
typedef struct
{
	/**
	 * Label to emit in front of the block.
	 */
	char *label;
	
	/**
	 * List of instructions in this block, in no particular order.
	 */
	int numInsn;
	CodeGenInsn **insn;
} CodeGenBlock;

/**
 * Represents an instruction graph for a function.
 */
typedef struct
{
	/**
	 * Blocks in this function.
	 */
	int numBlocks;
	CodeGenBlock **blocks;
	
	/**
	 * Bitset indicating which registers are nonvolatile.
	 */
	uint64_t presvRegs;
} CodeGenGraph;

/**
 * Represents the schedule generated for a basic block.
 */
typedef struct
{
	const char *label;
	int numInsn;
	CodeGenInsn **insn;
} CodeGenSched;

/**
 * Represents information about a CTIR register, used prior to allocation.
 */
typedef struct CodeGenRegInfo_
{
	/**
	 * CTIR type of the register.
	 */
	int type;
	
	/**
	 * Bitset of which CPU registers a "preffered" for this CTIR register.
	 * The allocator will try its hardest to allocate one from this set.
	 */
	uint64_t prefRegs;
	
	/**
	 * Bitset of which CPU registers are allowed for this CTIR register.
	 * The allocator will never allocate outside this set; it will spill
	 * instead of it has to.
	 */
	uint64_t allowedRegs;
	
	/**
	 * Live range.
	 */
	uint64_t bornAt;			// point of first use
	uint64_t deadAt;			// point of LAST USE (still needed at this point)
	
	/**
	 * Register (currently) allocated to the variable.
	 * 0 = no allocation done yet (live range didn't begin perhaps)
	 * -1 = register had to be spilled.
	 */
	int cpureg;
	
	/**
	 * Pre-color for this register.
	 */
	int precolor;
	
	/**
	 * Alternative location, if the register had to be spilled. The backend
	 * decides the meaning of this field.
	 */
	int location;
	
	/**
	 * Overlapping registers.
	 */
	int numOverlaps;
	struct CodeGenRegInfo_ **overlaps;
	
	/**
	 * Used by the allocator to "mark" registers.
	 */
	int marked;
} CodeGenRegInfo;

/**
 * Represents a code generator driver.
 */
typedef struct
{
	/**
	 * Name of this driver.
	 */
	const char *name;
	
	/**
	 * Arbitrary options.
	 */
	void *opt;
	
	/**
	 * Array of names for registers.
	 */
	const char* regNames[64];
	
	/**
	 * Initialize the code generator. Return ERR_OK on success, or an error number on
	 * error.
	 */
	Error (*init)(struct CodeGen_ *codegen, const void *opt);
	
	/**
	 * Generate an instruction graph for the specified function.
	 * Return NULL on error, report errors to the console.
	 */
	CodeGenGraph* (*genfunc)(struct CodeGen_ *codegen, CTIR_Func *func);
	
	/**
	 * Provide information about registers. The 'regs' array is filled with the
	 * following information:
	 * 	- The type of each register;
	 * 	- The live range of each register;
	 * 	- The set of preferred registers;
	 * 	- The set of allowed registers.
	 * Furthermore, if any register is pre-allocated (for example, arguments), their
	 * locations must be specified too.
	 * This function is NOT expected to find overlapping registers, so leave these
	 * fields alone. The generic generator does it automatically.
	 */
	void (*genregs)(struct CodeGen_ *codegen, CTIR_Func *func, int numScheds, CodeGenSched **scheds, CodeGenRegInfo *info);
	
	/**
	 * Get a new spill location for the specified function.
	 */
	int (*genspill)(CTIR_Func *func);
	
	/**
	 * Emit a function prolog.
	 */
	void (*genprolog)(struct CodeGen_ *codegen, CTIR_Func *func, CodeGenRegInfo *regs);
	
	/**
	 * Emit a function epilog.
	 */
	void (*genepilog)(struct CodeGen_ *codegen, CTIR_Func *func, CodeGenRegInfo *regs);
	
	/**
	 * Emit a block.
	 */
	void (*genblock)(struct CodeGen_ *codegen, CTIR_Func *func, CodeGenRegInfo *regs, CodeGenSched *sched, int blockIndex);
	
	/**
	 * Output a line of assembly which emits CTIR data.
	 */
	void (*gendata)(struct CodeGen_ *codegen, CTIR_Data *data);
	
	/**
	 * Add driver-specific options to the given option list. Please note that all options must start with `-m'.
	 * This function pointer may be NULL, in which case no options are added.
	 */
	void (*genoptions)(OptionList *optlist);
} CodeGenDriver;

/**
 * Describes an inline constant.
 */
typedef struct
{
	/**
	 * Required alignment.
	 */
	size_t align;
	
	/**
	 * A line of assembly which outputs the constnat data.
	 */
	const char *defline;
} CodeGenInlineConst;

/**
 * Describes a code generator.
 */
typedef struct CodeGen_
{
	/**
	 * The driver.
	 */
	CodeGenDriver *driver;
	
	/**
	 * Driver-specific data.
	 */
	void *drvdata;
	
	/**
	 * The assembler in use.
	 */
	As *as;
	
	/**
	 * The CTIR unit being processed.
	 */
	CTIR_Unit *unit;
	
	/**
	 * Next label number.
	 */
	int nextLabel;

	/**
	 * Inline constant table. Maps a label to a CodeGenInlineConst. Used for constants
	 * which the code generator must emit to memory. The labels are placed in the
	 * `.rodata' section.
	 */
	HashTable *inlineConst;
} CodeGen;

/**
 * Create a new inline constant.
 */
void codegenInlineConst(CodeGen *gen, const char *label, size_t align, const char *defline);

/**
 * Create a new graph with the specified number of blocks.
 */
CodeGenGraph* codegenNewGraph(int numBlocks);

/**
 * Get a code generator driver given a name (e.g. 'x86_64'). Returns NULL if not found.
 */
CodeGenDriver* codegenGetDriver(const char *name);

/**
 * Generate a new label, and return it as a string on the heap.
 */
char* codegenLabel(CodeGen *gen);

/**
 * Perform code generation. Return 0 on success, -1 on error. Errors are reported to the console.
 */
int codegen(CodeGenDriver *drv, As *as, CTIR_Unit *unit);

/**
 * Format a string and return it.
 */
int codegenFormatAP(char **strp, const char *fmt, va_list ap);
int codegenFormat(char **strp, const char *fmt, ...);

#endif
