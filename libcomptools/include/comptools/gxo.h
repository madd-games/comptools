/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_GXO_H_
#define COMPTOOLS_GXO_H_

#include <comptools/types.h>

/**
 * GXO header magic number.
 */
#define	GXO_MAGIC				0x40AABBCC01234567

/**
 * Header generic flags.
 */
#define	GXO_GEN_EXEC				(1 << 0)
#define	GXO_GEN_PIC				(1 << 1)
#define	GXO_GEN_DYNLD				(1 << 2)
#define	GXO_GEN_REL				(1 << 3)
#define	GXO_GEN_TLS				(1 << 4)

/**
 * Section generic flags.
 */
#define	SECT_GEN_EXEC				(1 << 0)
#define	SECT_GEN_WRITE				(1 << 1)
#define	SECT_GEN_READ				(1 << 2)
#define	SECT_GEN_NOALLOC			(1 << 3)
#define	SECT_GEN_RELRO				(1 << 4)

/**
 * Symbol scopes.
 */
#define	SYM_SCOPE_LOCAL				0
#define	SYM_SCOPE_WEAK				1
#define	SYM_SCOPE_GLOBAL			2

/**
 * Symbol types.
 */
#define	SYM_TYPE_UNDEF				0
#define	SYM_TYPE_OBJECT				1
#define	SYM_TYPE_FUNC				2
#define	SYM_TYPE_ABS				3
#define	SYM_TYPE_TLS				4
#define	SYM_TYPE_LABEL				5

/**
 * Generic relocation types.
 */
#define	RT_GEN_NONE				0
#define	RT_GEN_U8				0x8
#define	RT_GEN_S8				0x9
#define	RT_GEN_U16				0x10
#define	RT_GEN_S16				0x11
#define	RT_GEN_U32				0x20
#define	RT_GEN_S32				0x21
#define	RT_GEN_U64				0x40
#define	RT_GEN_S64				0x41

/**
 * Type indicating 8-character ASCII strings aliased as 64-bit integers.
 */
typedef	uint64_t				gxostr8_t;

/**
 * GXO data types.
 */
typedef	CT_NEWTYPE(uint8_t)			gxo8_t;
typedef CT_NEWTYPE(uint16_t)			gxo16_t;
typedef CT_NEWTYPE(uint32_t)			gxo32_t;
typedef CT_NEWTYPE(uint64_t)			gxo64_t;

/**
 * GXO header.
 */
typedef struct
{
	gxo64_t					gxoMagic;
	gxostr8_t				gxoArch;
	gxostr8_t				gxoSystem;
	gxo64_t					gxoArchFlags;
	gxo64_t					gxoSystemFlags;
	gxo64_t					gxoGenericFlags;
	gxo64_t					gxoEntryAddr;
	gxo32_t					gxoHeaderSize;
	gxo32_t					gxoSectionTableOffset;
	gxo32_t					gxoSectionCount;
	gxo32_t					gxoSectionEntrySize;
} GXO_Header;

/**
 * GXO section table entry.
 */
typedef struct
{
	gxostr8_t				sectName;
	gxo64_t					sectAddr;
	gxo64_t					sectSize;
	gxo32_t					sectOffset;
	gxo8_t					sectArchFlags;
	gxo8_t					sectSystemFlags;
	gxo8_t					sectGenericFlags;
	gxo8_t					sectAlignLog2;
} GXO_Section;

/**
 * GXO symbol table entry.
 */
typedef struct
{
	gxo64_t					symName;
	gxo64_t					symValue;
	gxo32_t					symSize;
	gxo8_t					symScope;
	gxo8_t					symType;
	gxo8_t					symAlignLog2;
	gxo8_t					symBucket;
} GXO_Symbol;

/**
 * Relocation table entry.
 */
typedef struct
{
	gxo64_t					relPosition;
	gxo64_t					relSymbol;
	gxo64_t					relType;
	gxo64_t					relParam;
} GXO_Reloc;

#endif
