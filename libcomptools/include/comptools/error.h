/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_ERROR_H_
#define COMPTOOLS_ERROR_H_

/**
 * An enumeration of errors defined by comptools. Please assign explicit values to all
 * enums. All values should be 4 hex digits, the leftmost one being the category, and the
 * rightmost three being the error number in that category.
 */
typedef enum
{
	/**
	 * The value zero indicates no error occured; everything is OK!
	 */
	ERR_OK = 0,
	
	/**
	 * Category 1: Generic errors
	 */
	ERR_INVALID_PARAM = 0x1000,	/* invalid parameter */
	ERR_IO = 0x1001,		/* I/O error */
	
	/**
	 * Category 2: Object file and linker errors.
	 */
	ERR_RELOC_TRUNC = 0x2000,	/* relocation truncated */
	ERR_UNDEF_REF = 0x2001,		/* undefined reference */
	ERR_MULTIPLE_DEF = 0x2002,	/* multiple definition */
} Error;

#endif
