/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef CTIR_OPCODE_DEF
#	error "Define CTIR_OPCODE_DEF before including ctir-opcodes.h"
#endif

/**
 * This file defines CTIR opcodes. Each definition is an invocation of CTIR_OPCODE_DEF()
 * followed by a comma. The macro CTIR_OPCODE_DEF takes one parameter: the opcode name.
 * A definition is provided before including this file, and this allows us to do two things:
 * in ctir.h, this generates an 'enum' list, and in ctir.c, this generates the opcode name
 * table.
 */

CTIR_OPCODE_DEF(nop),			/* do nothing */
CTIR_OPCODE_DEF(i8),
CTIR_OPCODE_DEF(i16),
CTIR_OPCODE_DEF(i32),
CTIR_OPCODE_DEF(i64),
CTIR_OPCODE_DEF(u8),
CTIR_OPCODE_DEF(u16),
CTIR_OPCODE_DEF(u32),
CTIR_OPCODE_DEF(u64),
CTIR_OPCODE_DEF(f32),
CTIR_OPCODE_DEF(f64),
CTIR_OPCODE_DEF(alloc),			/* alloc <align>, <size> : allocate 'size' bytes on stack */
CTIR_OPCODE_DEF(store),			/* store <location>, <value> : store value at location */
CTIR_OPCODE_DEF(debug_var),		/* debug_var <name>, <reg> */
CTIR_OPCODE_DEF(ret),			/* ret <reg> : return the value in 'reg' */
CTIR_OPCODE_DEF(load),			/* load <location>, <type> : load a type from a location */
CTIR_OPCODE_DEF(addrof),		/* addrof <location> : get the address of <location> */
CTIR_OPCODE_DEF(add),			/* add <a>, <b> : calculate the sum of two values */
CTIR_OPCODE_DEF(soft_fence),		/* soft_fence : soft fence */
CTIR_OPCODE_DEF(hard_fence),		/* hard_fence : hard fence */
CTIR_OPCODE_DEF(mul),			/* mul <a>, <b> : calculate the product of two values */
CTIR_OPCODE_DEF(div),			/* div <a>, <b> : get the quotient of division of a by b */
CTIR_OPCODE_DEF(mod),			/* mod <a>, <b> : get a modulo b */
CTIR_OPCODE_DEF(sub),			/* sub <a>, <b> : subtract */
CTIR_OPCODE_DEF(call),			/* call <type>, <func>, ... : call a function */
CTIR_OPCODE_DEF(jmp),			/* jmp <block> : jump to a block */
CTIR_OPCODE_DEF(label),			/* label <label> : temporary label */
CTIR_OPCODE_DEF(select),		/* select <subject>, <a>, <b> : jump to <a> if <subject> is nonzero, else to <b> */
CTIR_OPCODE_DEF(phi),			/* phi ... : phi-selection */
CTIR_OPCODE_DEF(crash),			/* crash : abort the program */
CTIR_OPCODE_DEF(switch),		/* switch <ctrl>, <defaultTargetBlock>, <value1>, <target1>, ... : switch */
CTIR_OPCODE_DEF(eq),			/* eq <a>, <b> : evaluate to true if a == b */
CTIR_OPCODE_DEF(neq),			/* neq <a>, <b> : not equal */
CTIR_OPCODE_DEF(less),			/* less <a>, <b> : is (a < b) ? */
CTIR_OPCODE_DEF(less_eq),		/* less_eq <a>, <b> : is (a <= b) ? */
CTIR_OPCODE_DEF(greater),		/* greater <a>, <b> : is (a > b) ? */
CTIR_OPCODE_DEF(greater_eq),		/* greater_eq <a>, <b> : is (a >= b) ? */
CTIR_OPCODE_DEF(shl),			/* shl <a>, <b> : shift <a> left by <b> bits */
CTIR_OPCODE_DEF(shr),			/* shr <a>, <b> : shift <a> right by <b> bits */
CTIR_OPCODE_DEF(sar),			/* sar <a>, <b> : shift <a> arithmetically right by <b> bits */

/**
 * Undefine CTIR_OPCODE_DEF at the end.
 */
#undef CTIR_OPCODE_DEF
