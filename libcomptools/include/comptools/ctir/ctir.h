/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_CTIR_CTIR_H_
#define COMPTOOLS_CTIR_CTIR_H_

#include <comptools/lex.h>
#include <inttypes.h>

#define	CTIR_OPCODE_DEF(name)	CTIR_##name
typedef enum
{
#include <comptools/ctir/ctir-opcodes.h>
	CTIR_OPCODE_COUNT,
} CTIR_Opcode;

/**
 * Function attributes.
 */
#define	CTIR_FA_NORETURN			(1 << 0)		/* function never returns */
#define	CTIR_FA_RETVAL				(1 << 1)		/* function returns a value */

/**
 * CTIR fundamental data types.
 */
enum
{
	CTIR_VOID,
	CTIR_U8,
	CTIR_U16,
	CTIR_U32,
	CTIR_U64,
	CTIR_I8,
	CTIR_I16,
	CTIR_I32,
	CTIR_I64,
	CTIR_F32,
	CTIR_F64,
	CTIR_STACK,
	
	CTIR_NUM_TYPES
};

/**
 * Kinds of operands.
 */
enum
{
	CTIR_KIND_CONST,
	CTIR_KIND_TYPESPEC,
	CTIR_KIND_REG,
	CTIR_KIND_BLOCKREF,
	CTIR_KIND_SYMREF,
	CTIR_KIND_TUPLE,
	CTIR_KIND_TMPLABEL,
};

/**
 * Symbol visiblity.
 */
enum
{
	CTIR_VIS_LOCAL,
	CTIR_VIS_WEAK,
	CTIR_VIS_GLOBAL
};

/**
 * Description of an operand.
 */
typedef struct CTIR_Operand_
{
	int kind;
	union
	{
		union
		{
			uint64_t constVal;
			double doubleVal;
		};
		
		int typeSpec;
		int regNo;
		int blockIndex;
		
		struct
		{
			const char *symbolName;
			int isGlobal;
		};
		
		struct
		{
			int numSubs;
			struct CTIR_Operand_ *subs;
		};
	};
} CTIR_Operand;

/**
 * Represents a CTIR instruction.
 */
typedef struct
{
	/**
	 * Token associated with the instruction.
	 */
	Token *tok;
	
	/**
	 * Result register (or 0 if none).
	 */
	int resultReg;
	
	/**
	 * Opcode.
	 */
	int opcode;
	
	/**
	 * Operands.
	 */
	int numOps;
	CTIR_Operand *ops;
} CTIR_Insn;

/**
 * Represents a block.
 */
typedef struct
{
	/**
	 * Index of the block within the function description.
	 */
	int index;
	
	/**
	 * Arbitrary working data for code generator or analyser.
	 */
	void *cgdata;
	
	/**
	 * Label assigned by the code generator to the block.
	 */
	char *label;
	
	/**
	 * Instructions in this block.
	 */
	int numCode;
	CTIR_Insn *code;
} CTIR_Block;

/**
 * Function definition.
 */
typedef struct CTIR_Func_
{
	struct CTIR_Func_ *next;
	
	/**
	 * Visibility of the function (CTIR_VIS_*).
	 */
	int vis;
	
	/**
	 * Name of the function.
	 */
	const char *name;
	
	/**
	 * Function attributes (bitwise-OR of CTIR_FA_*).
	 */
	int attr;
	
	/**
	 * Function argument list. This is a tuple containing type specifiers
	 * and optionally sub-tuples. The code generator shall scan the list and
	 * generate the prolog as follows:
	 * 	- For every scalar type specifier, it shall assign the value of the
	 * 	  argument to the next available register (starting $1).
	 * 	- For every tuple, it shall be treated as a struct, which is spilled
	 * 	  to the stack, and the STACK object assigned to the next available
	 * 	  register.
	 */
	CTIR_Operand args;
	
	/**
	 * Function return type. Either a single type specifier, or a tuple (representing
	 * structs or unions).
	 */
	CTIR_Operand ret;
	
	/**
	 * List of blocks within this function.
	 */
	int numBlocks;
	CTIR_Block **blocks;
	
	/**
	 * Temporary field for use by a code generator.
	 */
	void *cgdata;
	
	/**
	 * Number of CTIR registers used by this function. This is zero until the code generator
	 * stage.
	 */
	int numRegsUsed;
} CTIR_Func;

/**
 * Data descriptor.
 */
typedef struct
{
	/**
	 * Type (CTIR_*).
	 */
	int type;
	
	/**
	 * Data value.
	 */
	uint64_t val;
} CTIR_Data;

/**
 * Global variable definition.
 */
typedef struct CTIR_Glob_
{
	struct CTIR_Glob_ *next;
	
	/**
	 * Visibility (CTIR_VIS_*).
	 */
	int vis;
	
	/**
	 * Name of the symbol.
	 */
	const char *name;
	
	/**
	 * Section to put the symbol in.
	 */
	const char *section;
	
	/**
	 * Required alignment.
	 */
	size_t align;
	
	/**
	 * List of data descriptors.
	 */
	int numData;
	CTIR_Data *data;
	
	/**
	 * If numData is zero but this is not, it is the size to reserve.
	 */
	size_t size;
} CTIR_Glob;

/**
 * Common variable definition.
 */
typedef struct CTIR_Comm_
{
	struct CTIR_Comm_ *next;
	
	/**
	 * Name of the symbol.
	 */
	const char *name;
	
	/**
	 * Size and alignment.
	 */
	size_t size, align;
} CTIR_Comm;

/**
 * CTIR unit.
 */
typedef struct
{
	/**
	 * Linked list of global variables.
	 */
	CTIR_Glob *globs;
	
	/**
	 * Linked list of common variables.
	 */
	CTIR_Comm *comms;
	
	/**
	 * Linked list of functions.
	 */
	CTIR_Func *funcs;
} CTIR_Unit;

/**
 * Get a the name of a CTIR opcode as a string. If the opcode is invalid,
 * returns the string "<invalid>".
 */
const char* ctirGetOpcodeName(CTIR_Opcode opc);

/**
 * Create a new unit.
 */
CTIR_Unit* ctirNewUnit();

/**
 * Create a new CTIR common variable.
 */
CTIR_Comm* ctirNewComm(CTIR_Unit *unit, const char *name, size_t size, size_t align);

/**
 * Create a new CTIR global variable.
 */
CTIR_Glob* ctirNewGlob(CTIR_Unit *unit, int vis, const char *section, const char *name, size_t align, int numData, CTIR_Data *data);

/**
 * Create a new function.
 */
CTIR_Func* ctirNewFunc(CTIR_Unit *unit, int vis, int attr, const char *name);

/**
 * Create a new block inside a function.
 */
CTIR_Block* ctirNewBlock(CTIR_Func *func);

/**
 * Form a constant operand.
 */
CTIR_Operand ctirConst(uint64_t val);

/**
 * Form a double constant operand.
 */
CTIR_Operand ctirConstDouble(double val);

/**
 * Form a type specifier operand.
 */
CTIR_Operand ctirTypeSpec(int type);

/**
 * Form a register operand.
 */
CTIR_Operand ctirReg(int num);

/**
 * Form a block reference operand.
 */
CTIR_Operand ctirBlockRef(int num);

/**
 * Form a symbol reference operand.
 */
CTIR_Operand ctirSymbolRef(const char *name, int isGlobal);

/**
 * Form a tuple operand.
 */
CTIR_Operand ctirTuple(int count, CTIR_Operand *ops);

/**
 * Form a temporary label operand.
 */
CTIR_Operand ctirTmpLabel(const char *lbl);

/**
 * Form a list of operands.
 */
CTIR_Operand* ctirOperandList(int count, ...);

/**
 * Add a new instruction to an instruction buffer.
 */
void ctirInsnBuffer(int *counter, CTIR_Insn **buffer, Token *tok, int resultReg, int opcode, int numOps, CTIR_Operand *ops);

/**
 * Append a list of instructions to an instruction buffer.
 */
void ctirAppendBuffer(int *counter, CTIR_Insn **buffer, int numCode, CTIR_Insn *code);

/**
 * Add a new instruction to a block.
 */
void ctirInsn(CTIR_Block *block, Token *tok, int resultReg, int opcode, int numOps, CTIR_Operand *ops);

/**
 * Append a list of instructions to a block.
 */
void ctirAppend(CTIR_Block *block, int numCode, CTIR_Insn *code);

/**
 * Dump a description of the unit to standard output.
 */
void ctirDump(CTIR_Unit *unit);

#endif
