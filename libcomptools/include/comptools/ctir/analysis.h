/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_CTIR_ANALYSIS_H_
#define COMPTOOLS_CTIR_ANALYSIS_H_

#include <comptools/ctir/ctir.h>
#include <comptools/opt.h>

/**
 * Describes an optimisation or static analysis step.
 */
typedef struct
{
	/**
	 * Name of the step, used for command-line switches.
	 */
	const char *name;
	
	/**
	 * Description of the step, for `--help'.
	 */
	const char *help;

	/**
	 * Which optimisation level the step is enabled on.
	 */
	int level;

	/**
	 * Implementation of the step. Return 0 if changes were made to the code
	 * (including adding tags), -1 if nothing was done.
	 */
	int (*apply)(CTIR_Unit *unit);
	
	/**
	 * Enable/disable switches.
	 */
	int enable;
	int disable;
} CTIR_Analysis;

/**
 * Add optimisation-related options to the option list.
 */
void ctirAddOptions(OptionList *optList);

/**
 * Perform optimisations on the specified CTIR unit.
 */
void ctirOptimize(CTIR_Unit *unit);

/**
 * Declare all optimisation functions here.
 */
int ctirOptDeadCode(CTIR_Unit *unit);
int ctirOptVars(CTIR_Unit *unit);

#endif
