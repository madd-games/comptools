/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_AS_H_
#define COMPTOOLS_AS_H_

#include <comptools/obj.h>
#include <comptools/error.h>

/**
 * Declare all the structures.
 */
struct As_;

/**
 * Represents an assembler driver.
 */
typedef struct
{
	/**
	 * Name of this driver.
	 */
	const char *name;
	
	/**
	 * Arbitrary options.
	 */
	void *opt;
	
	/**
	 * Comment string.
	 */
	const char *comment;
	
	/**
	 * Initialize the assembler driver. Returns ERR_OK if successful, error number otherwise.
	 * This is called after 'as' itself was initialized. 'opt' is the option pointer taken from
	 * the driver description.
	 */
	Error (*init)(struct As_ *as, const void *opt);
	
	/**
	 * Parse an assembly line. Return NULL on success, or an error string on the heap on error
	 * (this is later passed to free()).
	 */
	char* (*line)(struct As_ *as, const char *line);
	
	/**
	 * Return the GXO machine state entry number for a given register name. Return -1 if not valid.
	 */
	int (*getGxoRegNum)(struct As_ *as, const char *regname);
	
	/**
	 * Make a value in the endianness of the target.
	 */
	void (*make)(uint64_t value, void *buffer, int count);
} AsDriver;

/**
 * Describes an assembler session.
 */
typedef struct As_
{
	/**
	 * The assembler driver.
	 */
	AsDriver *driver;
	
	/**
	 * The object to which we are writing.
	 */
	Object *obj;
	
	/**
	 * Current section (might be NULL).
	 */
	Section *sect;
	
	/**
	 * Driver-specific data.
	 */
	void *drvdata;
	
	/**
	 * Current annotation we are working on.
	 */
	Annot *annot;
} As;

/**
 * Create a new assembler session, writing to the specified object using the
 * specified assembler driver.
 */
As* asNew(Object *obj, AsDriver *driver);

/**
 * Get the current section in the assembler. This is the section which subsequent instructions
 * will be written to. If no section is currently selected, then a text section is created and
 * used.
 */
Section* asGetSection(As *as);

/**
 * Feed a line into the assembler. Returns NULL on success; otherwise, returns an error string
 * on the heap (you must later pass it to free() ).
 */
char* asLine(As *as, const char *line);

/**
 * Get an assembler driver given a name (such as 'x86_64'). Returns NULL if not found.
 */
AsDriver* asGetDriver(const char *name);

/**
 * Create a pseudo-assembler, which prints the assembly into a file.
 */
As* asPseudo(FILE *fp);

#endif
