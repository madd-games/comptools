/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_LD_H_
#define COMPTOOLS_LD_H_

#include <comptools/obj.h>
#include <comptools/ld-script.h>

/**
 * Describes a linker session.
 */
typedef struct
{
	/**
	 * The linker script parse tree.
	 */
	LDS_Script *script;
	
	/**
	 * Array of input objects.
	 */
	Object** inputs;
	int numInputs;
	
	/**
	 * Current position (base address of next section).
	 */
	uint64_t pos;
	
	/**
	 * Output object.
	 */
	Object *out;
	
	/**
	 * Desired output object type.
	 */
	int outType;
	
	/**
	 * Shared object name or NULL.
	 */
	char *soname;
	
	/**
	 * Dependencies (shared libraries).
	 */
	Object **depends;
	int numDepends;
} Linker;

/**
 * Create a new linker session, using the specified script. 'filename' is the name of the file
 * from which the script was read, and is used only for diagnostics.
 * 
 * Returns the linker on success, or NULL on error (invalid script). If an error does occur,
 * then an appropriate error message will be printed to the console (and the console error
 * flag will be set).
 */
Linker* ldCreate(const char *script, const char *filename);

/**
 * Set the shared object name.
 */
void ldSetSoname(Linker *ld, const char *soname);

/**
 * Add a new input object to the linker.
 */
void ldAddInput(Linker *ld, Object *in);

/**
 * Set the output type.
 */
void ldOutputType(Linker *ld, int type);

/**
 * Perform the final link, and output the resulting object in the specified format. If an error
 * occurs, prints the errors to the console and returns NULL.
 */
Object* ldRun(Linker *ld, ObjDriver* drv);

#endif
