/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_CC_EMIT_H_
#define COMPTOOLS_CC_EMIT_H_

#include <comptools/cc/cc-ast.h>
#include <comptools/ctir/ctir.h>

/**
 * Information about a variable in a context.
 */
typedef struct
{
	/**
	 * CTIR register number containing this variable, or 0 if not available.
	 */
	int valueReg;
	
	/**
	 * CTIR register number containing the stack allocation for this
	 * variable, or 0 if no stack space was allocated explicitly.
	 */
	int stackReg;
	
	/**
	 * The type.
	 */
	CC_Type *type;
	
	/**
	 * The token which defined this variable.
	 */
	Token *tok;
} CC_VarInfo;

/**
 * Information about a label seen in the C code.
 */
typedef struct
{
	/**
	 * The associated block. The label points to the beginning of it. The block
	 * is created as soon as the label is seen (whether it be in a definition or
	 * in a use such as a 'goto' statement).
	 */
	CTIR_Block *block;
	
	/**
	 * Whether or not the label was defined.
	 */
	int haveDef;
	
	/**
	 * Token associated with the last use. Mostly used to generate a diagnostic
	 * if an undefined label was used.
	 */
	Token *tok;
} CC_Label;

/**
 * Emitter global context.
 */
typedef struct
{
	/**
	 * Next free CTIR register.
	 */
	int nextReg;
	
	/**
	 * The unit being compiled.
	 */
	CC_Unit *unit;
	
	/**
	 * The unit being emitted.
	 */
	CTIR_Unit *out;
	
	/**
	 * A number for the next tmp label to be generated.
	 */
	int nextTmpNum;
} CC_GlobalContext;

/**
 * C flow context.
 */
typedef struct
{
	/**
	 * The function into which we are emitting code.
	 */
	CTIR_Func *func;
	
	/**
	 * Block to which we are currently emitting code.
	 */
	CTIR_Block *block;
	
	/**
	 * The compiler.
	 */
	CC_Compiler *cc;
	
	/**
	 * Target blocks for break/continue statements. 0 indicates there is
	 * no target (since break/continue never jump to block 0 anyway).
	 */
	int breakTarget;
	int continueTarget;
	
	/**
	 * If not NULL, then the current context is a "switch body", and this
	 * describes the CTIR_switch instruction.
	 */
	CTIR_Insn *currentSwitch;
	
	/**
	 * Return type of function being compiled.
	 */
	CC_Type *retType;
	
	/**
	 * Hash table mapping variable names to their information.
	 */
	HashTable *vars;
	
	/**
	 * Hash table of labels. This is changes once per FUNCTION, not for every block,
	 * so in all context within the same function, this is the same pointer. Values
	 * are of type CC_Label*.
	 */
	HashTable *labels;
	
	/**
	 * Global context.
	 */
	CC_GlobalContext *glob;
} CC_FlowContext;

/**
 * Representation of expression values.
 */
typedef struct
{
	/**
	 * Type of value. If this is NULL, it means that there is no value because an
	 * error occured during translation.
	 */
	CC_Type *type;
	
	/**
	 * CTIR code which calculates the value and stores it in some register.
	 */
	int numCode;
	CTIR_Insn *code;

	/**
	 * Which register contains the value. This is set to 0 for structs.
	 */
	int reg;
	
	/**
	 * CTIR code which calculates the address of the value, if applicable.
	 */
	int numAddrCode;
	CTIR_Insn *addrCode;
	
	/**
	 * Which register contains the address of the value. This is 0 for non-lvalues.
	 */
	int addrReg;
	
	/**
	 * Token associated with the value.
	 */
	Token *tok;
	
	/**
	 * Whether a constant value is known or not, and if so, the value itself, various types.
	 */
	int hasConst;
	int8_t vi8;
	int16_t vi16;
	int32_t vi32;
	int64_t vi64;
	uint8_t vu8;
	uint16_t vu16;
	uint32_t vu32;
	uint64_t vu64;
	float vf;
	double vd;
	
	/**
	 * An operand that would need to be passed to 'store' to store a value in this object, if it's an lvalue.
	 * If CTIR_KIND_CONST (0), then no operand will do it. If an operand CAN do it, then none of the code from this
	 * value is considered.
	 */
	CTIR_Operand storeOp;
	
	/**
	 * Whether or not the expression has side effects, such assignment or function calls.
	 */
	int sideEffects;
} CC_ExprValue;

/**
 * Generate a CTIR unit from a C AST. Returns NULL on error and reports errors to the
 * console.
 */
CTIR_Unit* ccEmit(CC_Compiler *cc, CC_Unit *input);

#endif
