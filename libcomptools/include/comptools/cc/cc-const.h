/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_CC_CONST_H_
#define COMPTOOLS_CC_CONST_H_

#include <comptools/cc/cc-parse.h>
#include <comptools/cc/cc-ast.h>

/**
 * Constant expression types.
 */
enum
{
	CC_CONST_INT,
	CC_CONST_STRING,
	CC_CONST_FLOAT,
	CC_CONST_ERROR,
};

/**
 * Represents the result of a constant expression in C.
 */
typedef struct
{
	/**
	 * Resulting type of expression (CC_CONST_*).
	 */
	int type;
	
	/**
	 * Token meaningfully associated with the result. Use for error reporting.
	 */
	Token *tok;
	
	/**
	 * Value (field in use depends on 'type').
	 */
	union
	{
		int64_t i;			// if CC_CONST_INT
		const char *str;		// if CC_CONST_STRING
		double f;			// if CC_CONST_FLOAT
		const char *err;		// if CC_CONST_ERR		(error message)
	};
} CC_Const;

/**
 * Returns nonzero if the specified CC_Const value is nonzero.
 */
int ccIsTrue(CC_Const value);

/**
 * Calculate the value of a constant expression. Returns a CC_Const struct, which indicates
 * either a value, or an error.
 * 
 * There are multiple entry points because depending on the exact production of grammr being
 * in use, the parse tree node may be different, but the expression is required to be constant,
 * or an error is returned.
 */
CC_Const ccEvalConstantExpression(CC_Compiler *cc, CC_ConstantExpression *expr);
CC_Const ccEvalAssignmentExpression(CC_Compiler *cc, CC_AssignmentExpression *expr);
CC_Const ccEvalExpression(CC_Compiler *cc, CC_Expression *expr);

#endif
