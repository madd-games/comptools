/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef CC_DEF_NODE_TYPE
#	error "Please define CC_DEF_NODE_TYPE before including cc-nodetypes.h"
#endif

/**
 * This file lists C AST node types. Each line must be an invocation of the
 * CC_DEF_NODE_TYPE() macro, specifying a name, and ending with a comma. There
 * are 2 definitions used: in cc-ast.h, it is defined such that it populates an
 * enumeration list, and in cc-ast.c, it is defined such that it populates the array
 * of node types names.
 */

/**
 * Constant values of different types.
 */
CC_DEF_NODE_TYPE(CC_int),
CC_DEF_NODE_TYPE(CC_uint),
CC_DEF_NODE_TYPE(CC_long),
CC_DEF_NODE_TYPE(CC_ulong),
CC_DEF_NODE_TYPE(CC_longlong),
CC_DEF_NODE_TYPE(CC_ulonglong),
CC_DEF_NODE_TYPE(CC_double),

/**
 * String literal, contained in 'str'.
 */
CC_DEF_NODE_TYPE(CC_string),

/**
 * CC_block(statements...)
 * Execute statements in order, in a new scope.
 */
CC_DEF_NODE_TYPE(CC_block),

/**
 * CC_alloc_local([init])
 * Create a new local variable named 'str', with type 'cval.valType'. If 'init' is given,
 * it is an expression evaluating to the initial value of the variable.
 */
CC_DEF_NODE_TYPE(CC_alloc_local),

/**
 * CC_expr_seq(left, right)
 * Evaluate expression 'left', discard the result, then evaluate 'right' and return the result.
 */
CC_DEF_NODE_TYPE(CC_expr_seq),

/**
 * CC_assign_*(left, right)
 * 'left' names an lvalue, 'right' is an expression for a value to store in 'left'.
 */
CC_DEF_NODE_TYPE(CC_assign),				/* = */
CC_DEF_NODE_TYPE(CC_assign_mul),			/* *= */
CC_DEF_NODE_TYPE(CC_assign_div),			/* /= */
CC_DEF_NODE_TYPE(CC_assign_mod),			/* %= */
CC_DEF_NODE_TYPE(CC_assign_add),			/* += */
CC_DEF_NODE_TYPE(CC_assign_sub),			/* -= */
CC_DEF_NODE_TYPE(CC_assign_shl),			/* <<= */
CC_DEF_NODE_TYPE(CC_assign_shr),			/* >>= */
CC_DEF_NODE_TYPE(CC_assign_and),			/* &= */
CC_DEF_NODE_TYPE(CC_assign_xor),			/* ^= */
CC_DEF_NODE_TYPE(CC_assign_or),				/* |= */

/**
 * CC_cond(subject, trueExpr, falseExpr)
 * If 'subject' evaluates to a true value, evaluate to 'trueExpr', else evaluate to 'falseExpr'.
 */
CC_DEF_NODE_TYPE(CC_cond),

/**
 * CC_logical_or(a, b)
 * CC_logical_and(a, b)
 * Logical OR/AND, between 'a' and 'b'.
 */
CC_DEF_NODE_TYPE(CC_logical_or),
CC_DEF_NODE_TYPE(CC_logical_and),

/**
 * CC_bitwise_or(a, b)
 * CC_bitwise_xor(a, b)
 * CC_bitwise_and(a, b)
 * Bitwise OR/XOR/AND between 'a' and 'b'.
 */
CC_DEF_NODE_TYPE(CC_bitwise_or),
CC_DEF_NODE_TYPE(CC_bitwise_xor),
CC_DEF_NODE_TYPE(CC_bitwise_and),

/**
 * CC_eq(a, b)
 * CC_neq(a, b)
 * Returns true if either a equals (eq) b, or if it does not (neq).
 */
CC_DEF_NODE_TYPE(CC_eq),				/* == */
CC_DEF_NODE_TYPE(CC_neq),				/* != */

/**
 * CC_less(a, b)
 * CC_greater(a, b)
 * CC_less_eq(a, b)
 * CC_greater_eq(a, b)
 * Implements relational operations on 'a' and 'b'.
 */
CC_DEF_NODE_TYPE(CC_less),				/* a < b */
CC_DEF_NODE_TYPE(CC_greater),				/* a > b */
CC_DEF_NODE_TYPE(CC_less_eq),				/* a <= b */
CC_DEF_NODE_TYPE(CC_greater_eq),			/* a >= b */

/**
 * CC_shl(a, b)
 * CC_shr(a, b)
 * Bit shifts.
 */
CC_DEF_NODE_TYPE(CC_shl),				/* a << b */
CC_DEF_NODE_TYPE(CC_shr),				/* a >> b */

/**
 * CC_add(a, b)
 * CC_sub(a, b)
 * Addition and subtraction.
 */
CC_DEF_NODE_TYPE(CC_add),
CC_DEF_NODE_TYPE(CC_sub),

/**
 * CC_mul(a, b)
 * CC_div(a, b)
 * CC_mod(a, b)
 * Multiplication, division and modulo.
 */
CC_DEF_NODE_TYPE(CC_mul),
CC_DEF_NODE_TYPE(CC_div),
CC_DEF_NODE_TYPE(CC_mod),

/**
 * CC_cast(expr)
 * Casts 'expr' to the type given in 'cval.valType'.
 */
CC_DEF_NODE_TYPE(CC_cast),

/**
 * CC_pre_inc(expr)
 * CC_post_inc(expr)
 * CC_pre_dec(expr)
 * CC_post_dec(expr)
 * Pre-/post- increment/decrement 'expr' (which is an lvalue).
 */
CC_DEF_NODE_TYPE(CC_pre_inc),				/* ++expr */
CC_DEF_NODE_TYPE(CC_post_inc),				/* expr++ */
CC_DEF_NODE_TYPE(CC_pre_dec),				/* --expr */
CC_DEF_NODE_TYPE(CC_post_dec),				/* expr-- */

/**
 * CC_ptr(expr)
 * Get a pointer to 'expr'.
 */
CC_DEF_NODE_TYPE(CC_ptr),

/**
 * CC_deref(expr)
 * Dereference pointer 'expr'.
 */
CC_DEF_NODE_TYPE(CC_deref),

/**
 * CC_promote(expr)
 * Perform integer promotions of 'expr' and evaluate to the result.
 * (Implements the unary '+' operator).
 */
CC_DEF_NODE_TYPE(CC_promote),

/**
 * CC_negate(expr)
 * Return the arithmetic negative of 'expr'.
 * (Implements the unary '-' operator).
 */
CC_DEF_NODE_TYPE(CC_negate),

/**
 * CC_bitwise_not(expr)
 * Returns the bitwise NOT of 'expr'.
 */
CC_DEF_NODE_TYPE(CC_bitwise_not),

/**
 * Returns the logical NOT of 'expr'.
 */
CC_DEF_NODE_TYPE(CC_logical_not),

/**
 * Returns the size of the value of 'expr'.
 */
CC_DEF_NODE_TYPE(CC_sizeof),

/**
 * CC_subscript(subject, index)
 * Evaluate to the index'th member of 'subject'.
 */
CC_DEF_NODE_TYPE(CC_subscript),

/**
 * CC_call(func, ...)
 * Call the function named by 'func' (either a function or function pointer) with
 * the specified arguments.
 */
CC_DEF_NODE_TYPE(CC_call),

/**
 * CC_member(obj)
 * Get the member named 'str' from 'obj'. 'obj' must be a struct or union.
 */
CC_DEF_NODE_TYPE(CC_member),

/**
 * CC_id()
 * Evaluate the identifier named by 'str'.
 */
CC_DEF_NODE_TYPE(CC_id),

/**
 * CC_compound_const(expr)
 * 'expr' is a CC_init_list. 'valType' specifies which type the list shall be applied to, and
 * this node evaluates to the initialized object.
 */
CC_DEF_NODE_TYPE(CC_compound_const),

/**
 * CC_init_list(...)
 * List of initializers (CC_init).
 */
CC_DEF_NODE_TYPE(CC_init_list),

/**
 * CC_init(expr, [desig])
 * Initialize the designator 'desig' to 'expr'. Used as part of initializer list.
 */
CC_DEF_NODE_TYPE(CC_init),

/**
 * CC_desig([next])
 * A designator. If 'str' is not NULL, it contains a member name. Otherwise, 'valI'
 * is an index.
 */
CC_DEF_NODE_TYPE(CC_desig),

/**
 * CC_ret([expr])
 * Return the specified expression.
 */
CC_DEF_NODE_TYPE(CC_ret),

/**
 * CC_label()
 * Emits a standard label named 'str'.
 */
CC_DEF_NODE_TYPE(CC_label),

/**
 * CC_case()
 * Emits a label for a 'case', of 'valI'.
 */
CC_DEF_NODE_TYPE(CC_case),

/**
 * CC_default()
 * Emits a label for a 'default' case.
 */
CC_DEF_NODE_TYPE(CC_default),

/**
 * CC_break()
 * Break out of teh current loop.
 */
CC_DEF_NODE_TYPE(CC_break),

/**
 * CC_continue()
 * Continue the current loop.
 */
CC_DEF_NODE_TYPE(CC_continue),

/**
 * CC_goto()
 * Jump to label 'str'.
 */
CC_DEF_NODE_TYPE(CC_goto),

/**
 * CC_if(expr, trueAction, [falseAction])
 * If 'expr' evaluates to nonzero, evaluate 'trueAction', else evaluate 'falseAction'.
 */
CC_DEF_NODE_TYPE(CC_if),

/**
 * CC_switch(expr, st)
 * Evaluate 'expr', and jump to the correct label within 'st'.
 */
CC_DEF_NODE_TYPE(CC_switch),

/**
 * CC_while(expr, st)
 * Repeat 'st' until 'expr' becomes false.
 */
CC_DEF_NODE_TYPE(CC_while),

/**
 * CC_do_while(expr, st)
 * Run 'st' once, then repeat until 'expr' becomes false.
 */
CC_DEF_NODE_TYPE(CC_do_while),

/**
 * CC_for(init, cond, iter, body)
 * Basically a 'for' loop.
 */
CC_DEF_NODE_TYPE(CC_for),

/**
 * Do nothing. Essentially evaluates to a void without side effects.
 */
CC_DEF_NODE_TYPE(CC_nop),

/**
 * At the very end, undefine CC_DEF_NODE_TYPE.
 */
#undef CC_DEF_NODE_TYPE
