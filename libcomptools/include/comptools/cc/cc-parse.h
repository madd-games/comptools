/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_CC_PARSE_H_
#define	COMPTOOLS_CC_PARSE_H_

#include <setjmp.h>

#include <comptools/cc/cpp.h>
#include <comptools/lex.h>
#include <comptools/hashtab.h>

/**
 * Name scope.
 */
typedef struct CC_NameScope_
{
	/**
	 * Parent context.
	 */
	struct CC_NameScope_ *prev;
	
	/**
	 * Typedef names in this scope. All values are set to NULL, and its only
	 * the presence or lack of a key which determines whether an identifier
	 * is a typedef name.
	 *
	 * Note that each context inherits a full copy of the previous context's
	 * typedef name set, so that the names can be shadowed if necessary.
	 */
	HashTable *typedefNames;
} CC_NameScope;

/**
 * StaticAssert ::= '_Static_assert' '(' ConstantExpression ',' StringLiteral ')' ';'
 */
typedef struct
{
	Token *op;			/* '_Static_assert' */
	struct CC_ConstantExpression_ *expr;
	Token *str;
} CC_StaticAssert;

/**
 * Expression ::= AssignmentExpression [',' Expression]
 */
typedef struct CC_Expression_
{
	struct CC_AssignmentExpression_ *assign;
	Token *op;					/* ',' */
	struct CC_Expression_ *next;
} CC_Expression;

/**
 * ArgumentExpressionList ::= AssignmentExpression [',' ArgumentExpressionList]
 */
typedef struct CC_ArgumentExpressionList_
{
	struct CC_AssignmentExpression_ *assign;
	struct CC_ArgumentExpressionList_ *next;
} CC_ArgumentExpressionList;

/**
 * DirectAbstractDeclarator ::= ['(' AbstractDeclarator ')'] [DeclaratorTail]
 */
typedef struct CC_DirectDeclarator_ CC_DirectAbstractDeclarator;

/**
 * AbstractDeclarator ::= [Pointer] [DirectAbstractDeclarator]
 */
typedef struct CC_Declarator_ CC_AbstractDeclarator;

/**
 * SpecifierQualifierList ::= TypeSpecifier [SpecifierQualifierList]
 * 				| TypeQualifier [SpecifierQualifierList]
 * 				| AlignmentSpecifier [SpecifierQualifierList]
 * 				| AttributeSpecifier [SpecifierQualiferList]
 * NOTE: Must be compatible with CC_DeclarationSpecifiers!
 */
typedef struct CC_SpecifierQualifierList_
{
	struct CC_TypeSpecifier_ *typeSpecifier;
	Token *typeQualifier;
	struct CC_AlignmentSpecifier_ *alignSpecifier;
	struct CC_AttributeSpecifier_ *attr;
	struct CC_SpecifierQualifierList_ *next;
} CC_SpecifierQualifierList;

/**
 * TypeName ::= SpecifierQualifierList [AbstractDeclarator]
 */
typedef struct
{
	Token *tok;
	CC_SpecifierQualifierList *specs;
	CC_AbstractDeclarator *decl;
} CC_TypeName;

/**
 * AtomicTypeSpecifier ::= '_Atomic' '(' TypeName ')'
 */
typedef struct
{
	Token *keyword;		/* '_Atomic' */
	CC_TypeName *typeName;
} CC_AtomicTypeSpecifier;

/**
 * EnumeratorList ::= Identifier ['=' ConstantExpression] [',' [EnumeratorList]]
 */
typedef struct CC_EnumeratorList_
{
	Token *id;
	struct CC_ConstantExpression_ *expr;
	struct CC_EnumeratorList_ *next;
} CC_EnumeratorList;

/**
 * EnumSpecifier ::= 'enum' [Identifier] ['{' EnumeratorList '}']
 */
typedef struct
{
	Token *keyword;		/* 'enum' */
	Token *id;
	CC_EnumeratorList *list;
} CC_EnumSpecifier;

/**
 * StructDeclaratorList ::=
 * 		Declarator [',' StructDeclaratorList]
 * 		[Declarator] ':' ConstantExpressipn [',' StructDeclaratorList]
 */
typedef struct CC_StructDeclaratorList_
{
	struct CC_Declarator_ *decl;
	struct CC_ConstantExpression_ *bits;
	struct CC_StructDeclaratorList_ *next;
} CC_StructDeclaratorList;

/**
 * StructDeclarationList ::= SpecifierQualifierList [StructDeclaratorList] ';'
 * 			| StaticAssert
 */
typedef struct CC_StructDeclarationList_
{
	Token *tok;
	CC_SpecifierQualifierList *qualifiers;
	CC_StructDeclaratorList *decls;
	CC_StaticAssert *staticAssert;
	struct CC_StructDeclarationList_ *next;
} CC_StructDeclarationList;

/**
 * StructOrUnionSpecifier ::= StructOrUnion [Identifier] ['{' StructDeclarationList '}']
 */
typedef struct
{
	Token *keyword;			/* "struct" or "union" */
	Token *id;
	CC_StructDeclarationList *decls;
} CC_StructOrUnionSpecifier;

/**
 * TypeSpecifier ::= <type-specifier-token>
 * 		| AtomicTypeSpecifier
 * 		| EnumSpecifier
 * 		| StructOrUnionSpecifier
 */
typedef struct CC_TypeSpecifier_
{
	Token *token;
	CC_AtomicTypeSpecifier* atomicSpecifier;
	CC_EnumSpecifier *enumSpecifier;
	CC_StructOrUnionSpecifier *compSpecifier;
} CC_TypeSpecifier;

/**
 * ConstantExpression ::= ConditionalExpression
 */
typedef struct CC_ConstantExpression_
{
	struct CC_ConditionalExpression_ *cond;
} CC_ConstantExpression;

/**
 * AlignmentSpecifier ::= '_Alignas' '(' TypeName ')'
 * 			| '_Alignas' '(' ConstantExpression ')'
 */
typedef struct CC_AlignmentSpecifier_
{
	CC_TypeName *typeName;
	CC_ConstantExpression *expr;
} CC_AlignmentSpecifier;

/**
 * AttributeSpecifier ::= '__attribute__' '(' '(' Identifier ') ')'
 */
typedef struct CC_AttributeSpecifier_
{
	Token *id;
} CC_AttributeSpecifier;

/**
 * DeclarationSpecifiers ::=
 *	StorageClassSpecifier [DeclarationSpecifiers]
 * 	TypeSpecifier [DeclarationSpecifiers]
 * 	TypeQualifier [DeclarationSpecifiers]
 * 	FunctionSpecifier [DeclarationSpecifiers]
 * 	AlignmentSpecifier [DeclarationSpecifiers]
 * 	AttributeSpecifier [DeclarationSpecifiers]
 * 
 * NOTE: The first few fields MUST match the layout of CC_SpecifierQualifierList,
 *       so that when necessary, this structure can be cast to it!
 */
typedef struct CC_DeclarationSpecifiers_
{
	CC_TypeSpecifier *typeSpecifier;
	Token *typeQualifier;
	CC_AlignmentSpecifier *alignSpecifier;
	CC_AttributeSpecifier *attrSpecifier;
	struct CC_DeclarationSpecifiers_ *next;
	Token *storageClass;
	Token *funcSpecifier;
} CC_DeclarationSpecifiers;

/**
 * TypeQualifierList ::= TypeQualifier [TypeQualifierList]
 */
typedef struct CC_TypeQualifierList_
{
	Token *qualifier;
	struct CC_TypeQualifierList_ *next;
} CC_TypeQualifierList;

/**
 * Pointer ::= '*' [TypeQualifierList] [Pointer]
 */
typedef struct CC_Pointer_
{
	CC_TypeQualifierList *qualifs;
	struct CC_Pointer_ *next;
} CC_Pointer;

/**
 * GenericAssocList ::=
 * 	'default' ':' AssignmentExpression [',' GenericAssocList]
 * 	TypeName ':' AssignmentExpression [',' GenericAssocList]
 */
typedef struct CC_GenericAssocList_
{
	CC_TypeName *typeName;			// NULL if 'default'
	struct CC_AssignmentExpression_ *expr;
	struct CC_GenericAssocList_ *next;
} CC_GenericAssocList;

/**
 * GenericSelection ::= '_Generic' '(' AssignmentExpression ',' [GenericAssocList] ')'
 */
typedef struct
{
	Token *tok;
	struct CC_AssignmentExpression_ *expr;
	CC_GenericAssocList *assocs;
} CC_GenericSelection;

/**
 * PrimaryExpression ::=
 * 	Identifier
 * 	Constant
 * 	StringLiteral
 * 	'(' Expression ')'
 * 	GenericSelection
 */
typedef struct CC_PrimaryExpression_
{
	Token *tok;
	CC_GenericSelection *generic;
	struct CC_Expression_ *expr;
} CC_PrimaryExpression;

/**
 * SubscriptTail ::= '[' Expression ']'
 */
typedef struct
{
	struct CC_Expression_ *expr;
} CC_SubscriptTail;

/**
 * CallTail ::= '(' [ArgumentExpressionList] ')'
 */
typedef struct
{
	CC_ArgumentExpressionList *list;
} CC_CallTail;

/**
 * MemberTail ::=
 * 	'->' Identifier
 * 	'.' Identifier
 */
typedef struct
{
	Token *op;
	Token *id;
} CC_MemberTail;

/**
 * PostfixTail ::=
 * 	SubscriptTail
 * 	CallTail
 * 	MemberTail
 * 	PostfixOperator
 * PostfixOperator ::= '++' | '--'	
 */
typedef struct CC_PostfixTail_
{
	Token *tok;
	CC_SubscriptTail *subscript;
	CC_CallTail *call;
	CC_MemberTail *member;
	Token *op;
	struct CC_PostfixTail_ *next;
} CC_PostfixTail;

/**
 * PostfixExpression ::=
 * 	PrimaryExpression [PostfixTail]
 * 	'(' TypeName ')' '{' [InitializerList] '}'
 */
typedef struct CC_PostfixExpression_
{
	CC_PrimaryExpression *primary;
	CC_TypeName *typeName;
	struct CC_InitializerList_ *initList;
	CC_PostfixTail *tail;
} CC_PostfixExpression;

/**
 * UnaryExpression ::=
 * 	'++' UnaryExpression
 * 	'--' UnaryExpression
 * 	UnaryOperator CastExpression
 * 	'sizeof' UnaryExpression
 * 	'sizeof' '(' TypeName ')'
 * 	'_Alignof' '(' TypeName ')'
 * 	PostfixExpression
 * UnaryOperator ::= '&' | '*' | '+' | '-' | '~' | '!'
 */
typedef struct CC_UnaryExpression_
{
	Token *op;				/* ++, --, UnaryOperator, sizeof, _Alignof, NULL */
	struct CC_UnaryExpression_ *unary;
	CC_TypeName *typeName;
	CC_PostfixExpression *postfix;
	struct CC_CastExpression_ *cast;
} CC_UnaryExpression;

/**
 * CastExpression ::= UnaryExpression | '(' TypeName ')' CastExpression
 */
typedef struct CC_CastExpression_
{
	// either this...
	CC_UnaryExpression *unary;
	
	// ...or this
	CC_TypeName *typeName;
	struct CC_CastExpression_ *cast;
} CC_CastExpression;

/**
 * MultiplicativeExpression ::= CastExpression [MultiplicativeOperator MultiplicativeExpression]
 * MultiplicativeOperator ::= '*' | '/' | '%'
 */
typedef struct CC_MultiplicativeExpression_
{
	CC_CastExpression *cast;
	Token *op;
	struct CC_MultiplicativeExpression_ *next;
} CC_MultiplicativeExpression;

/**
 * AdditiveExpression ::= MultiplicativeExpression [AdditiveOperator AdditiveExpression]
 * AdditiveOperator ::= '+' | '-'
 */
typedef struct CC_AdditiveExpression_
{
	CC_MultiplicativeExpression *mul;
	Token *op;
	struct CC_AdditiveExpression_ *next;
} CC_AdditiveExpression;

/**
 * ShiftExpression ::= AdditiveExpression [ShiftOperator ShiftExpression]
 * ShiftOperator ::= '<<' | '>>'
 */
typedef struct CC_ShiftExpression_
{
	CC_AdditiveExpression *add;
	Token *op;
	struct CC_ShiftExpression_ *next;
} CC_ShiftExpression;

/**
 * RelationalExpression ::= ShiftExpression [RelationalOperator RelationalExpression]
 * RelationalOperator ::= '<' | '>' | '<=' | '>='
 */
typedef struct CC_RelationalExpression_
{
	CC_ShiftExpression *shift;
	Token *op;
	struct CC_RelationalExpression_ *next;
} CC_RelationalExpression;

/**
 * EqualityExpression ::= RelationalExpression [EqualityOperator EqualityExpression]
 * EqualityOperator ::= '==' | '!='
 */
typedef struct CC_EqualityExpression_
{
	CC_RelationalExpression *rel;
	Token *op;
	struct CC_EqualityExpression_ *next;
} CC_EqualityExpression;

/**
 * BitwiseANDExpression ::= EqualityExpression ['&' BitwiseANDExpression]
 */
typedef struct CC_BitwiseANDExpression_
{
	CC_EqualityExpression *equality;
	Token *op;				/* '&' */
	struct CC_BitwiseANDExpression_ *next;
} CC_BitwiseANDExpression;

/**
 * BitwiseXORExpression ::= BitwiseANDExpression ['^' BitwiseXORExpression]
 */
typedef struct CC_BitwiseXORExpression_
{
	CC_BitwiseANDExpression *bitwiseAND;
	Token *op;				/* '^' */
	struct CC_BitwiseXORExpression_ *next;
} CC_BitwiseXORExpression;

/**
 * BitwiseORExpression ::= BitwiseXORExpression ['|' BitwiseORExpression]
 */
typedef struct CC_BitwiseORExpression_
{
	CC_BitwiseXORExpression *bitwiseXOR;
	Token *op;				/* '|' */
	struct CC_BitwiseORExpression_ *next;
} CC_BitwiseORExpression;

/**
 * LogicalANDExpression ::= BitwiseORExpression ['&&' LogicalANDExpression]
 */
typedef struct CC_LogicalANDExpression_
{
	CC_BitwiseORExpression *bitwiseOR;
	Token *op;				/* '&&' */
	struct CC_LogicalANDExpression_ *next;
} CC_LogicalANDExpression;

/**
 * LogicalORExpression ::= LogicalANDExpression ['||' LogicalORExpression]
 */
typedef struct CC_LogicalORExpression_
{
	CC_LogicalANDExpression *logicalAND;
	Token *op;				/* '||' */
	struct CC_LogicalORExpression_ *next;
} CC_LogicalORExpression;

/**
 * ConditionalExpression ::= LogicalORExpression ['?' Expression ':' ConditionalExpression]
 */
typedef struct CC_ConditionalExpression_
{
	CC_LogicalORExpression *left;
	Token *op;					/* '?' */
	CC_Expression *trueExpr;
	struct CC_ConditionalExpression_ *falseExpr;
} CC_ConditionalExpression;

/**
 * AssignmentExpression ::= ConditionalExpression
 * 				| UnaryExpression AssignmentOperator AssignmentExpression
 */
typedef struct CC_AssignmentExpression_
{
	// either this...
	CC_ConditionalExpression *cond;
	
	// ...or this
	CC_UnaryExpression *left;
	Token *op;
	struct CC_AssignmentExpression_ *right;
} CC_AssignmentExpression;

/**
 * ParameterTypeList ::= DeclarationSpecifiers [Declarator | AbstractDeclarator]
 * 				[',' ('...' | ParameterTypeList)]
 */
typedef struct CC_ParameterTypeList_
{
	// if all fields are set to NULL, it means that this is a variable argument list.
	// that is, the last node will have everything set to NULL for a valist.
	CC_DeclarationSpecifiers *declSpecs;
	struct CC_Declarator_ *decl;
	CC_AbstractDeclarator *absDecl;
	struct CC_ParameterTypeList_ *next;
} CC_ParameterTypeList;

/**
 * DeclaratorTail ::= '[' [TypeQualifierList] [AssignmentExpression] ']' [DeclaratorTail]
 * 			| '[' 'static' [TypeQualifierList] AssignmentExpression ']' [DeclaratorTail]
 *			| '[' TypeQualifierList 'static' AssignmentExpression ']' [DeclaratorTail]
 * 			| '[' [TypeQualifierList] '*' ']' [DeclaratorTail]
 * 			| '(' [ParameterTypeList] ')'
 */
typedef struct CC_DeclaratorTail_
{
	Token *tok;
	
	// if 'array' is set to 1, then this is an array tail (begins with '[')
	int array;
	Token *isStatic;
	CC_TypeQualifierList *qualifs;
	CC_AssignmentExpression *expr;
	int vla;				/* '*' was given as the size */
	
	// if 'array' is 0, then this is an argument tail (begins with '(')
	// so one of the following will be non-NULL
	CC_ParameterTypeList *paramList;
	
	// applies to both
	struct CC_DeclaratorTail_ *next;
} CC_DeclaratorTail;

/**
 * DirectDeclarator ::= Identifier [DeclaratorTail]
 *			| '(' Declarator ')' [DeclaratorTail]
 */
typedef struct CC_DirectDeclarator_
{
	Token *id;
	struct CC_Declarator_ *decl;
	CC_DeclaratorTail *tail;
} CC_DirectDeclarator;

/**
 * Declarator ::= [Pointer] DirectDeclarator
 */
typedef struct CC_Declarator_
{
	CC_Pointer *ptr;
	CC_DirectDeclarator *direct;
} CC_Declarator;

/**
 * DesignatorList ::=
 * 	'[' ConstantExpression ']' [DesignatorList]
 * 	'.' Identifier [DesignatorList]
 */
typedef struct CC_DesignatorList_
{
	CC_ConstantExpression *expr;
	Token *id;
	struct CC_DesignatorList_ *next;
} CC_DesignatorList;

/**
 * Designation ::= DesignatorList '='
 */
typedef struct
{
	CC_DesignatorList *list;
} CC_Designation;

/**
 * InitializerList ::= [Designation] Initializer [',' [InitializerList]]
 */
typedef struct CC_InitializerList_
{
	Token *tok;
	CC_Designation *designation;
	struct CC_Initializer_ *init;
	struct CC_InitializerList_ *next;
} CC_InitializerList;

/**
 * Initializer ::=
 * 	AssignmentExpression
 * 	'{' [InitializerList] '}'
 */
typedef struct CC_Initializer_
{
	struct CC_AssignmentExpression_* assign;
	CC_InitializerList *initList;
} CC_Initializer;

/**
 * InitDeclaratorList ::= Declarator ['=' Initializer] [',' InitDeclaratorList]
 */
typedef struct CC_InitDeclaratorList_
{
	CC_Declarator *decl;
	CC_Initializer *init;
	struct CC_InitDeclaratorList_ *next;
} CC_InitDeclaratorList;

/**
 * Declaration ::= DeclarationSpecifiers [InitDeclaratorList]
 * 		| StaticAssert
 */
typedef struct
{
	CC_DeclarationSpecifiers *declSpecs;
	CC_InitDeclaratorList *initDeclList;
	CC_StaticAssert *staticAssert;
} CC_Declaration;

/**
 * LabeledStatement ::=
 * 	Identifier ':' Statement
 * 	'case' ConstantExpression ':' Statement
 * 	'default' ':' Statement
 */
typedef struct
{
	Token *id;			// if "Identifier" form is used
	Token *keyword;			// if either 'case' or 'default' form is used
	CC_ConstantExpression *expr;	// if 'case' form is used
	struct CC_Statement_ *st;	// in all cases
} CC_LabeledStatement;

/**
 * IfStatement ::= 'if' '(' Expression ')' Statement ['else' Statement]
 */
typedef struct
{
	Token *op;			// 'if'
	CC_Expression *expr;
	struct CC_Statement_ *trueAction;
	struct CC_Statement_ *falseAction;
} CC_IfStatement;

/**
 * SwitchStatement ::= 'switch' '(' Expression ')' Statement
 */
typedef struct
{
	Token *op;			/* "switch" */
	CC_Expression *expr;
	struct CC_Statement_ *st;
} CC_SwitchStatement;

/**
 * WhileStatement ::= 'while' '(' Expression ')' Statement
 */
typedef struct
{
	Token *op;			/* "while" */
	CC_Expression *expr;
	struct CC_Statement_ *st;
} CC_WhileStatement;

/**
 * DoWhileStatement ::= 'do' Statement 'while' '(' Expression ')' ';'
 */
typedef struct
{
	Token *op;			/* "do" */
	CC_Expression *expr;
	struct CC_Statement_ *st;
} CC_DoWhileStatement;

/**
 * ForStatement ::=
 * 	'for' '(' Declaration [Expression] ';' [Expression] ')' Statement
 * 	'for' '(' [Expression] ';' [Expression] ';' [Expression] ')' Statement
 */
typedef struct
{
	Token *op;			/* "for" */
	CC_Declaration *decl;
	CC_Expression *init;
	CC_Expression *cond;
	CC_Expression *iter;
	struct CC_Statement_ *st;
} CC_ForStatement;

/**
 * JumpStatement ::=
 * 	'goto' Identifier ';'
 * 	'continue' ';'
 * 	'break' ';'
 * 	'return' [Expression] ';'
 */
typedef struct
{
	Token *keyword;		// "goto", "continue", "break", or "return"
	Token *id;		// if "goto"
	CC_Expression *expr;	// if "return" with optional expression
} CC_JumpStatement;

/**
 * Statement ::=
 * 	LabeledStatement
 * 	CompoundStatement
 * 	[Expression] ';'
 * 	IfStatement
 * 	SwitchStatement
 * 	WhileStatement
 * 	DoWhileStatement
 * 	ForStatement
 * 	JumpStatement
 */
typedef struct CC_Statement_
{
	Token *tok;
	CC_LabeledStatement *label;
	struct CC_CompoundStatement_ *comp;
	CC_Expression *expr;
	CC_IfStatement *ifs;
	CC_SwitchStatement *sw;
	CC_WhileStatement *whiles;
	CC_DoWhileStatement *doWhile;
	CC_ForStatement *fors;
	CC_JumpStatement *jmp;
} CC_Statement;

/**
 * BlockItemList ::=
 * 	Declaration [BlockItemList]
 * 	Statement [BlockItemList]
 */
typedef struct CC_BlockItemList_
{
	CC_Declaration *decl;
	CC_Statement *st;
	struct CC_BlockItemList_ *next;
} CC_BlockItemList;

/**
 * CompoundStatement ::= '{' [BlockItemList] '}'
 */
typedef struct CC_CompoundStatement_
{
	Token *tok;
	CC_BlockItemList *items;
} CC_CompoundStatement;

/**
 * FunctionDefinition ::= DeclarationSpecifiers Declarator CompoundStatement
 */
typedef struct
{
	CC_DeclarationSpecifiers *specs;
	CC_Declarator *decl;
	CC_CompoundStatement *body;
} CC_FunctionDefinition;

/**
 * TranslationUnit ::= ExternalDeclaration [TranslationUnit]
 * 
 * ExternalDeclaration ::= FunctionDefinition
 * 				| Declaration
 */
typedef struct CC_ExternalDeclaration_
{
	CC_Declaration *decl;
	CC_FunctionDefinition *funcDef;
	struct CC_ExternalDeclaration_ *next;
} CC_ExternalDeclaration;

/**
 * Compiler context, stores information needed by the parser.
 */
typedef struct
{
	/**
	 * Current scope.
	 */
	CC_NameScope *scope;
	
	/**
	 * Currently-selected token.
	 */
	Token *token;
	
	/**
	 * The token on which the last error was reported.
	 */
	Token *errToken;
	
	/**
	 * Latest reported error.
	 */
	char *lastError;
	
	/**
	 * jmp_buf to allow the parser to abort.
	 */
	jmp_buf retbuf;
	
	/**
	 * Linked list of external declarations.
	 */
	CC_ExternalDeclaration *declHead;
} CC_Context;

/**
 * Parse C code and return the resulting CC_Context if successful. If failed,
 * returns NULL after reporting errors to the console.
 *
 * 'toklist' is the pre-processed list of tokens.
 */
CC_Context* ccParse(Token *toklist);

#endif
