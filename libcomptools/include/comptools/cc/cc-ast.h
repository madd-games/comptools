/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_CC_AST_H_
#define COMPTOOLS_CC_AST_H_

#include <comptools/cc/cc-parse.h>

/**
 * Type qualifiers.
 */
#define	CC_TQ_CONST			(1 << 0)
#define	CC_TQ_RESTRICT			(1 << 1)
#define	CC_TQ_VOLATILE			(1 << 2)
#define	CC_TQ_ATOMIC			(1 << 3)

/**
 * Function flags.
 */
#define	CC_FUNC_INLINE			(1 << 0)
#define	CC_FUNC_NORETURN		(1 << 1)

/**
 * Storage classes.
 */
enum
{
	CC_STOR_NONE,
	CC_STOR_TYPEDEF,
	CC_STOR_EXTERN,
	CC_STOR_STATIC,
	CC_STOR_THREAD_LOCAL,
	CC_STOR_AUTO,
	CC_STOR_REGISTER,
	CC_STOR_STATIC_THREAD_LOCAL,
	CC_STOR_EXTERN_THREAD_LOCAL
};

/**
 * C types.
 */
enum
{
	CC_TYPE_VOID,
	CC_TYPE_CHAR,
	CC_TYPE_SIGNED_CHAR,
	CC_TYPE_UNSIGNED_CHAR,
	CC_TYPE_SIGNED_SHORT,
	CC_TYPE_UNSIGNED_SHORT,
	CC_TYPE_SIGNED_INT,
	CC_TYPE_UNSIGNED_INT,
	CC_TYPE_SIGNED_LONG,
	CC_TYPE_UNSIGNED_LONG,
	CC_TYPE_SIGNED_LONG_LONG,
	CC_TYPE_UNSIGNED_LONG_LONG,
	CC_TYPE_FLOAT,
	CC_TYPE_DOUBLE,
	CC_TYPE_LONG_DOUBLE,
	CC_TYPE_BOOL,
	CC_TYPE_FLOAT_COMPLEX,
	CC_TYPE_DOUBLE_COMPLEX,
	CC_TYPE_LONG_DOUBLE_COMPLEX,
	CC_TYPE_STRUCT_OR_UNION,
	CC_TYPE_ARRAY,
	CC_TYPE_PTR,
	CC_TYPE_FUNC,
};

#define	CC_DEF_NODE_TYPE(name)	name
enum
{
#include <comptools/cc/cc-nodetypes.h>
	CC_NUM_NODE_TYPES
};

/**
 * Type description.
 */
typedef struct CC_Type_
{
	/**
	 * Bitwise-OR of type qualifiers (CC_TQ_*), or 0.
	 */
	int qualifs;
	
	/**
	 * Type-kind (CC_TYPE_*).
	 */
	int kind;
	
	/**
	 * If kind is either CC_TYPE_ARRAY or CC_TYPE_PTR, this specifies the base type.
	 * If kind is CC_TYPE_FUNC, it specifies the return type.
	 */
	struct CC_Type_ *base;
	
	/**
	 * If kind is CC_TYPE_ARRAY, this specifies the number of elements, except that:
	 * 	-1 means VLA (no constant size is known)
	 * 	0 means incomplete type (no size specified)
	 */
	int64_t numElements;
	
	/**
	 * If kind is CC_TYPE_STRUCT_OR_UNION, the tag name. If a struct or union without
	 * a tag is defined, the tag is auto-generated, in the form <anonymous struct X>.
	 */
	const char *tag;
	
	/**
	 * If kind is CC_TYPE_FUNC, specifies the types of arguments.
	 */
	struct CC_Type_ **argTypes;
	const char **argNames;
	Token **argTokens;
	int numArgs;
	
	/**
	 * If kind is CC_TYPE_FUNC, specifies whether the function has a variable argument
	 * list.
	 */
	int valist;
} CC_Type;

/**
 * Describes a constant value in an AST.
 */
typedef struct
{
	union
	{
		int64_t valI;
		uint64_t valU;
	};
	double valDouble;
	CC_Type *valType;
} CC_NodeConst;

/**
 * Describes a node of the AST for C.
 */
typedef struct CC_Node_
{
	/**
	 * Token associated with this node.
	 */
	Token *tok;
	
	/**
	 * Type of node.
	 */
	int type;
	
	/**
	 * Arbitrary string for argument, or NULL.
	 */
	const char *str;
	
	/**
	 * Constant value (if applicable).
	 */
	CC_NodeConst cval;
	
	/**
	 * Child nodes.
	 */
	int numChildren;
	struct CC_Node_* children[];
} CC_Node;

/**
 * Describes a function definition.
 */
typedef struct CC_Func_
{
	struct CC_Func_ *next;
	
	/**
	 * Storage class.
	 */
	int storageClass;
	
	/**
	 * Bitwise-OR of function flags (CC_FUNC_*).
	 */
	int flags;
	
	/**
	 * Function type.
	 */
	CC_Type *type;
	
	/**
	 * Name of the function.
	 */
	const char *name;
	
	/**
	 * Token associated with the function definition.
	 */
	Token *tok;
	
	/**
	 * Body of the function: must be a CC_block node.
	 */
	CC_Node *body;
} CC_Func;

/**
 * Describes a global declaration.
 */
typedef struct CC_GlobalVar_
{
	/**
	 * Storage class.
	 */
	int storageClass;
	
	/**
	 * Type of variable.
	 */
	CC_Type *type;
	
	/**
	 * Variable name.
	 */
	const char *name;
	
	/**
	 * Initializer AST, if applicable, or NULL if uninitialized.
	 */
	CC_Node *init;
	
	/**
	 * Token associated with the declaration, for diagnostic purposes.
	 */
	Token *tok;
	
	/**
	 * Set to 1 if this is a local variable which was already initialized.
	 */
	int initDone;
} CC_Decl;

/**
 * Describes a structure/union member.
 */
typedef struct CC_StructMember_
{
	/**
	 * Next member.
	 */
	struct CC_StructMember_ *next;
	
	/**
	 * Name of this member.
	 */
	const char *name;
	
	/**
	 * Size, alignment, offset (in bytes).
	 */
	size_t size;
	size_t align;
	size_t offset;
	
	/**
	 * Type.
	 */
	CC_Type *type;
	
	/**
	 * Token associated with this member.
	 */
	Token *tok;
	
	/**
	 * If this member is a bit-field, these specify the bit offset and bit
	 * length. If the bit length is zero, then this is not a bit-field.
	 * When bitfields are present in a single unit, they are defined as
	 * separate members, but with the same offset.
	 */
	size_t bitOff;
	size_t bitLen;
	
	/**
	 * If this is 1, it instructs the compiler that no more bit-fields
	 * shall be packed into this unit.
	 */
	int nopack;
} CC_StructMember;

/**
 * Describes a structure or union definition.
 */
typedef struct
{
	/**
	 * True if this is a union.
	 */
	int isUnion;
	
	/**
	 * Size and alignment requirement of the struct/union.
	 */
	size_t size;
	size_t align;
	
	/**
	 * Linked list of members.
	 */
	CC_StructMember *memberList;
	
	/**
	 * Hash table mapping member names to entries in the member list.
	 */
	HashTable *memberTable;
} CC_StructDef;

/**
 * Definition of an enumeration value.
 */
typedef struct
{
	/**
	 * The token at which the enumeration was defined.
	 */
	Token *tok;
	
	/**
	 * The value itself.
	 */
	int value;
} CC_EnumVal;

/**
 * Describes a data model. Each field specifies the size of a fundamental type.
 * sizeof(char) is always 1.
 */
typedef struct
{
	size_t sizeofShort;
	size_t sizeofInt;
	size_t sizeofLong;
	size_t sizeofLongLong;
	size_t sizeofFloat;
	size_t sizeofDouble;
	size_t sizeofLongDouble;
	size_t sizeofFloatComplex;
	size_t sizeofDoubleComplex;
	size_t sizeofLongDoubleComplex;
	size_t sizeofBool;
	size_t sizeofPtr;
} CC_DataModel;

/**
 * Describes a translation unit.
 */
typedef struct
{
	/**
	 * Hash table of global declarations (value type CC_Decl*).
	 */
	HashTable *globals;
	
	/**
	 * Hash table of struct/union tags (value type CC_StructDef*).
	 */
	HashTable *structs;
	
	/**
	 * Hash table of enum values (value type CC_EnumVal*).
	 */
	HashTable *enums;
	
	/**
	 * Linked list of functions.
	 */
	CC_Func *funcs;
	
	/**
	 * Data model used by this unit.
	 */
	CC_DataModel *dataModel;
} CC_Unit;

/**
 * Describes scope for declarations.
 */
typedef struct CC_Scope_
{
	struct CC_Scope_ *prev;
	
	/**
	 * Hash table mapping variable names to CC_Decl* for this scope.
	 */
	HashTable *vars;
} CC_Scope;

/**
 * Describes a compiler state.
 */
typedef struct
{
	/**
	 * The unit being worked on.
	 */
	CC_Unit *unit;
	
	/**
	 * Data model in use.
	 */
	CC_DataModel *dataModel;
	
	/**
	 * Current scope (NULL means global scope).
	 */
	CC_Scope *scope;
	
	/**
	 * Next tag number to be generated by the compiler.
	 */
	int nextTagNum;
} CC_Compiler;

/**
 * Get a node type name given its number. Returns "<invalid>" if invalid.
 */
const char* ccGetNodeTypeName(int nodeType);

/**
 * Create a new compiler object with the specified data model.
 */
CC_Compiler* ccNew(CC_DataModel *dataModel);

/**
 * Return the type of variable 'name' in the current context, or NULL if no such variable exists.
 */
CC_Type* ccGetVarType(CC_Compiler *cc, const char *name);

/**
 * Compile a C parse tree into a CC_Unit. Return the unit on success, or NULL on error.
 * Errors are reported to the console.
 */
CC_Unit* ccCompile(CC_Compiler *cc, CC_Context *ctx);

/**
 * Get the base type corresponding to a specifier-qualifer-list. Returns NULL on error,
 * and reports errors to the console.
 */
CC_Type* ccCompileSpecifierQualifierList(CC_Compiler *cc, CC_SpecifierQualifierList *list);

/**
 * Get the type and identifier (if given) corresponding to a declarator or abstract declarator
 * with the given base type. Return 0 on success, -1 on error. 
 */
int ccCompileDeclarator(CC_Compiler *cc, CC_Type *baseType, CC_Declarator *decl, CC_Type **outType, const char **outName, Token **outTok);

/**
 * Compile a type name into a type specification. Returns NULL on error, and reports errors to
 * the console.
 */
CC_Type* ccCompileTypeName(CC_Compiler *cc, CC_TypeName *typeName);

/**
 * Get the size of the specified type. Returns 0 if it cannot be computed.
 */
size_t ccGetTypeSize(CC_Compiler *cc, CC_Type *type);

/**
 * Get the alignment requirement of the specified type. Returns 0 if it cannot be computed.
 */
size_t ccGetTypeAlign(CC_Compiler *cc, CC_Type *type);

/**
 * Check if the specified types are compatible.
 */
int ccIsTypeCompatible(CC_Type *a, CC_Type *b);

/**
 * Dump a description of a unit to standard output, as an XML tree.
 */
void ccDumpUnit(CC_Unit *unit);

/**
 * Make a string representing the specified type, and return it. The string is on the heap, and may
 * later be released by a call to free().
 */
char* ccGetTypeName(CC_Type *type);

/**
 * Dump type information to stdout.
 */
void ccDumpType(CC_Type *type);

#endif
