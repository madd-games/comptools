/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_CC_CPP_H_
#define COMPTOOLS_CC_CPP_H_

#include <comptools/lex.h>
#include <comptools/hashtab.h>
#include <comptools/error.h>

/**
 * C token types.
 */
enum
{
	/* basic tokens */
	TOK_CC_DIR = TOK_USER_BASE,		/* pre-processor directive */
	TOK_CC_INCLUDE,				/* #include + header name */
	TOK_CC_NEWLINE,				/* non-espcaped newline (preproc only) */
	TOK_CC_ID,				/* identifier */
	TOK_CC_ICONST,				/* integer constant */
	TOK_CC_FCONST,				/* float constant */
	TOK_CC_CHARCONST,			/* character constant */
	TOK_CC_STRING,				/* string literal */
	
	/* reserved keywords */
	TOK_CC_AUTO,
	TOK_CC_BREAK,
	TOK_CC_CASE,
	TOK_CC_CHAR,
	TOK_CC_CONST,
	TOK_CC_CONTINUE,
	TOK_CC_DEFAULT,
	TOK_CC_DO,
	TOK_CC_DOUBLE,
	TOK_CC_ELSE,
	TOK_CC_ENUM,
	TOK_CC_EXTERN,
	TOK_CC_FLOAT,
	TOK_CC_FOR,
	TOK_CC_GOTO,
	TOK_CC_IF,
	TOK_CC_INLINE,
	TOK_CC_INT,
	TOK_CC_LONG,
	TOK_CC_REGISTER,
	TOK_CC_RESTRICT,
	TOK_CC_RETURN,
	TOK_CC_SHORT,
	TOK_CC_SIGNED,
	TOK_CC_SIZEOF,
	TOK_CC_STATIC,
	TOK_CC_STRUCT,
	TOK_CC_SWITCH,
	TOK_CC_TYPEDEF,
	TOK_CC_UNION,
	TOK_CC_UNSIGNED,
	TOK_CC_VOID,
	TOK_CC_VOLATILE,
	TOK_CC_WHILE,
	TOK_CC_ALIGNAS,
	TOK_CC_ALIGNOF,
	TOK_CC_ATOMIC,
	TOK_CC_BOOL,
	TOK_CC_COMPLEX,
	TOK_CC_GENERIC,
	TOK_CC_IMAGINARY,
	TOK_CC_NORETURN,
	TOK_CC_STATIC_ASSERT,
	TOK_CC_THREAD_LOCAL,
	
	/* CompTools-specific keywords */
	TOK_CC_ATTRIBUTE,
	
	/* punctuators */
	TOK_CC_LSQPAREN,			/* [ */
	TOK_CC_RSQPAREN,			/* ] */
	TOK_CC_LPAREN,				/* ( */
	TOK_CC_RPAREN,				/* ) */
	TOK_CC_LBRACE,				/* { */
	TOK_CC_RBRACE,				/* } */
	TOK_CC_DOT,				/* . */
	TOK_CC_MARROW,				/* -> ('member arrow') */
	TOK_CC_INC,				/* ++ */
	TOK_CC_DEC,				/* -- */
	TOK_CC_BAND,				/* & ('bitwise and') */
	TOK_CC_AST,				/* * ('asterisk') */
	TOK_CC_PLUS,				/* + */
	TOK_CC_MINUS,				/* - */
	TOK_CC_TILDE,				/* ~ */
	TOK_CC_EXCL,				/* ! */
	TOK_CC_SLASH,				/* / */
	TOK_CC_MODULO,				/* % */
	TOK_CC_LSHIFT,				/* << */
	TOK_CC_RSHIFT,				/* >> */
	TOK_CC_LARROW,				/* < */
	TOK_CC_RARROW,				/* > */
	TOK_CC_LARROW_EQ,			/* <= */
	TOK_CC_RARROW_EQ,			/* >= */
	TOK_CC_EQ,				/* == */
	TOK_CC_NEQ,				/* != */
	TOK_CC_XOR,				/* ^ */
	TOK_CC_BOR,				/* | ('bitwise or') */
	TOK_CC_LAND,				/* && ('logical and') */
	TOK_CC_LOR,				/* || ('logical or') */
	TOK_CC_QUEST,				/* ? */
	TOK_CC_COLON,				/* : */
	TOK_CC_SEMICOLON,			/* ; */
	TOK_CC_ELLIPSIS,			/* ... */
	TOK_CC_ASSIGN,				/* = */
	TOK_CC_ASSIGN_AST,			/* *= */
	TOK_CC_ASSIGN_SLASH,			/* /= */
	TOK_CC_ASSIGN_MODULO,			/* %= */
	TOK_CC_ASSIGN_PLUS,			/* += */
	TOK_CC_ASSIGN_MINUS,			/* -= */
	TOK_CC_ASSIGN_LSHIFT,			/* <<= */
	TOK_CC_ASSIGN_RSHIFT,			/* >>= */
	TOK_CC_ASSIGN_AND,			/* &= */
	TOK_CC_ASSIGN_XOR,			/* ^= */
	TOK_CC_ASSIGN_OR,			/* |= */
	TOK_CC_COMMA,				/* , */
	TOK_CC_HASH,				/* # */
	TOK_CC_DOUBLE_HASH,			/* ## */
	TOK_CC_BACKSLASH,			/* \ */
};

/**
 * Macro definition.
 */
typedef struct
{
	/**
	 * Name of this macro.
	 */
	const char *name;
	
	/**
	 * Names of arguments. For example, for:
	 * #define MY_MACRO(x, y) ...
	 * This array will be {"x", "y"}.
	 * The array itself, and all the elements, are allocated on the heap.
	 */
	char **argNames;
	int numArgs;
	
	/**
	 * Set to 1 if this is a function-like macro.
	 */
	int func;
	
	/**
	 * Set to 1 if this is a function-like macro which takes a variable argument list.
	 */
	int vargs;
	
	/**
	 * List of tokens forming the definition (NULL means empty).
	 */
	Token* tokens;
} MacroDef;

/**
 * Describes a C pre-processor. This can be created using cppNew(), then options can be
 * set, and a file can be imported to be pre-processed, before being passed to the main
 * C parser.
 */
typedef struct
{
	/**
	 * Hash table mapping macro names to their definitions (MacroDef).
	 * Empty macros have value 'NULL'.
	 */
	HashTable* macros;
	
	/**
	 * First and last token in the post-processing list.
	 */
	Token *first;
	Token *last;
	
	/**
	 * "Inclusion engine". If this is set to NULL, then the pre-processor will refuse
	 * to #include any files. If this function pointer is set, then the function is responsible
	 * for finding header files. 'headerName' is a string either of the form "\"header.h\"" or
	 * "<header.h>". If the header is found, the function must set the *data and *filename
	 * pointers to contain the file contents and the file name, respectively (for diagnostic
	 * purposes), and return ERR_OK. Otherwise, it must return an error number (typically ERR_IO)
	 * and not modify the pointers.
	 */
	Error (*includer)(const char *headerName, const char **data, const char **filename);
	
	/**
	 * List of dependencies (headers included).
	 */
	char **deps;
	int numDeps;
} CPP;

/**
 * Tokenize C input. This returns tokens prior to pre-processing, so they include things
 * like pre-processor directives. They also include newlines, but comments and other
 * whitespace is removed.
 *
 * The token list is returned on success (which may be NULL for empty files). On error,
 * a console error is printed, and NULL is returned.
 */
Token* cppTokenize(const char *data, const char *filename);

/**
 * Create a new pre-processor context. No macros are initially defined, and there is no
 * include file provider.
 */
CPP* cppNew();

/**
 * Set the value of a macro to the contents of 'str'. 'str' may be an empty string. 'str'
 * is tokenized as normal C code and set as the macro value; a tokenization error (reported
 * via console) occurs if the syntax of the string is not valid. An error is also reported
 * if the macro is already defined.
 */
void cppDefine(CPP *cpp, const char *name, const char *str);

/**
 * Run the pre-processor on the specified file contents. Returns the post-processed tokens
 * (which may be NULL for an effectively empty file). If an error occurs, it is reported
 * via the console (check conGetError()).
 */
Token* cppRun(CPP *cpp, const char *data, const char *name);

#endif
