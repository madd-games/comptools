/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_TYPES_H_
#define COMPTOOLS_TYPES_H_

/**
 * <comptools/types.h>
 * Define some generic data types.
 */

#include <stdint.h>

/**
 * Define a new type which is just a union containing a field called 'value' of the
 * specified type, and an array called 'bytes' that can be used to access individual
 * bytes of it.
 */
#define	CT_NEWTYPE(parent) union {parent value; char bytes[sizeof(parent)];}

/**
 * Unsigned little-endian types.
 */
typedef CT_NEWTYPE(uint8_t) ule8_t;
typedef CT_NEWTYPE(uint16_t) ule16_t;
typedef CT_NEWTYPE(uint32_t) ule32_t;
typedef CT_NEWTYPE(uint64_t) ule64_t;

/**
 * Signed little-endian types.
 */
typedef CT_NEWTYPE(int8_t) sle8_t;
typedef CT_NEWTYPE(int16_t) sle16_t;
typedef CT_NEWTYPE(int32_t) sle32_t;
typedef CT_NEWTYPE(int64_t) sle64_t;

/**
 * Unsigned big-endian types.
 */
typedef CT_NEWTYPE(uint8_t) ube8_t;
typedef CT_NEWTYPE(uint16_t) ube16_t;
typedef CT_NEWTYPE(uint32_t) ube32_t;
typedef CT_NEWTYPE(uint64_t) ube64_t;

/**
 * Signed big-endian types.
 */
typedef CT_NEWTYPE(int8_t) sbe8_t;
typedef CT_NEWTYPE(int16_t) sbe16_t;
typedef CT_NEWTYPE(int32_t) sbe32_t;
typedef CT_NEWTYPE(int64_t) sbe64_t;

/**
 * Generic functions for making either a little-endian or big-endian value.
 * 'val' is the value in host byte order, up to 64 bits. 'buffer' points to
 * where the bytes will be stored; 'count' is the number of bytes.
 */
void ctMakeLE(uint64_t value, void *buffer, int count);
void ctMakeBE(uint64_t value, void *buffer, int count);

/**
 * Generic functions for getting either little-endian or big-endian values converted
 * to host byte order. The returned value is converted to 64 bits, unsigned.
 */
uint64_t ctGetLE(const void *buffer, int count);
uint64_t ctGetBE(const void *buffer, int count);

/**
 * Makers for unsigned little-endian values. Converts host byte order to little-endian.
 */
ule8_t ule8(uint8_t value);
ule16_t ule16(uint16_t value);
ule32_t ule32(uint32_t value);
ule64_t ule64(uint64_t value);

/**
 * Makers for signed little-endian values. Converts host byte order to little-endian.
 */
sle8_t sle8(int8_t value);
sle16_t sle16(int16_t value);
sle32_t sle32(int32_t value);
sle64_t sle64(int64_t value);

/**
 * Makers for unsigned big-endian values. Converts host byte order to big-endian.
 */
ube8_t ube8(uint8_t value);
ube16_t ube16(uint16_t value);
ube32_t ube32(uint32_t value);
ube64_t ube64(uint64_t value);

/**
 * Makers for signed big-endian values. Converts host byte order to big-endian.
 */
sbe8_t sbe8(int8_t value);
sbe16_t sbe16(int16_t value);
sbe32_t sbe32(int32_t value);
sbe64_t sbe64(int64_t value);

/**
 * Getters for unsigned little-endian values. Converts little-endian to host byte order.
 */
uint8_t ule8_get(ule8_t val);
uint16_t ule16_get(ule16_t val);
uint32_t ule32_get(ule32_t val);
uint64_t ule64_get(ule64_t val);

/**
 * Getters for signed little-endian values. converts little-endian to host byte order.
 */
int8_t sle8_get(sle8_t val);
int16_t sle16_get(sle16_t val);
int32_t sle32_get(sle32_t val);
int64_t sle64_get(sle64_t val);

/**
 * Getters for unsigned big-endian values. Converts big-endian to host byte order.
 */
uint8_t ube8_get(ube8_t val);
uint16_t ube16_get(ube16_t val);
uint32_t ube32_get(ube32_t val);
uint64_t ube64_get(ube64_t val);

/**
 * Getters for signed big-endian values. Converts big-endian to host byte order.
 */
int8_t sbe8_get(sbe8_t val);
int16_t sbe16_get(sbe16_t val);
int32_t sbe32_get(sbe32_t val);
int64_t sbe64_get(sbe64_t val);

#endif
