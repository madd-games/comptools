/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_LD_SCRIPT_H_
#define COMPTOOLS_LD_SCRIPT_H_

#include <comptools/lex.h>

/**
 * Tokens used by .lds files.
 */
enum
{
	TOK_LDS_SECTION = TOK_USER_BASE,		/* "section" keyword */
	TOK_LDS_ID,					/* identifier */
	TOK_LDS_CONST,					/* integer constant */
	TOK_LDS_ADD,					/* '+' */
	TOK_LDS_SUB,					/* '-' */
	TOK_LDS_SEMICOLON,				/* ';' */
	TOK_LDS_EQUALS,					/* '=' */
	TOK_LDS_OPEN_BRACE,				/* '{' */
	TOK_LDS_CLOSE_BRACE,				/* '}' */
	TOK_LDS_SECTION_TYPE,				/* section type keyword */
	TOK_LDS_SECTION_FLAGS,				/* section flags */
	TOK_LDS_LOAD,					/* "load" keyword */
	TOK_LDS_ENTRY,					/* "entry" keyword */
	TOK_LDS_ALIGN,					/* "align" keyword */
};

/**
 * LDS_PrimaryExpr ::= LDS_Const | LDS_ID
 */
typedef struct
{
	const char *value;
	const char *symname;
	Token *srctok;
} LDS_PrimaryExpr;

/**
 * LDS_Sum ::= LDS_PrimaryExpr ( "+" | "-" ) LDS_Sum | LDS_PrimaryExpr
 */
typedef struct LDS_Sum_
{
	LDS_PrimaryExpr *expr;
	int op;						/* 0, TOK_LDS_ADD or TOK_LDS_SUB */
	struct LDS_Sum_ *next;
} LDS_Sum;

/**
 * LDS_Assignment ::= <name> "=" LDS_Sum ";"
 */
typedef struct
{
	const char *name;
	LDS_Sum* sum;
} LDS_Assignment;

/**
 * LDS_Load ::= "load" <name> ";"
 */
typedef struct
{
	const char *name;
} LDS_Load;

/**
 * LDS_SectionStatement ::= LDS_Assignment | LDS_Load
 */
typedef struct
{
	LDS_Assignment *assign;
	LDS_Load *load;
	Token *srctok;
} LDS_SectionStatement;

/**
 * LDS_SectionBody ::= LDS_SectionStatement [LDS_SectionBody]
 */
typedef struct LDS_SectionBody_
{
	LDS_SectionStatement *st;
	struct LDS_SectionBody_ *next;
} LDS_SectionBody;

/**
 * LDS_SectionDef ::= "section" <name> [LDS_SectionType] [LDS_SectionFlags] "{" LDS_SectionBody "}"
 */
typedef struct
{
	const char *name;
	int type;			// SECTYPE_PROGIBTS or SECTYPE_NOBITS
	int flags;			// section flags
	LDS_SectionBody *body;
	Token *srctok;
	Token *nametok;
} LDS_SectionDef;

/**
 * LDS_Entry ::= "entry" <name> ";"
 */
typedef struct
{
	const char *symname;
	Token *nametok;
} LDS_Entry;

/**
 * LDS_Align ::= "align" <alignment> ";"
 */
typedef struct
{
	const char *value;
	Token *srctok;
} LDS_Align;

/**
 * LDS_TopLevelStatement ::= LDS_Assignment | LDS_SectionDef | LDS_Entry | LDS_Align
 */
typedef struct
{
	LDS_Assignment *assign;
	LDS_SectionDef *secdef;
	LDS_Entry *entry;
	LDS_Align *align;
	Token *srctok;
} LDS_TopLevelStatement;

/**
 * LDS_Script ::= LDS_TopLevelStatement LDS_Script | LDS_TopLevelStatement
 */
typedef struct LDS_Script_
{
	LDS_TopLevelStatement *st;
	struct LDS_Script_ *next;
} LDS_Script;

/**
 * Parse a .lds file. If a parse error occurs, the console error flag will be set. NULL is
 * a valid return value: when the script is empty.
 */
LDS_Script* ldsParse(const char *script, const char *filename);

/**
 * Dump the parse tree to standard output. For debugging purposes.
 */
void ldsDump(LDS_Script *script);

#endif
