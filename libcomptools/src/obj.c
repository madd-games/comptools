/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifndef _WIN32
#include <sys/stat.h>
#include <dlfcn.h>
#else
#include <windows.h>
#endif

#include <comptools/obj.h>

extern ObjDriver objDriver_flat;

#ifdef OBJ_ENABLE_ELF64_X86_64
extern ObjDriver objDriver_elf64__x86_64;
#endif

#ifdef OBJ_ENABLE_GXO_X86_64_GLIDIX
extern ObjDriver objDriver_gxo__x86_64__glidix;
#endif

typedef struct
{
	const char *name;
	ObjDriver *drv;
} ObjDriverMapping;

static ObjDriverMapping objDriverList[] = {
	{"flat", &objDriver_flat},

#ifdef OBJ_ENABLE_ELF64_X86_64
	{"elf64-x86_64", &objDriver_elf64__x86_64},
#endif

#ifdef OBJ_ENABLE_GXO_X86_64_GLIDIX
	{"gxo-x86_64-glidix", &objDriver_gxo__x86_64__glidix},
#endif
	
	// LIST TERMINATOR
	{NULL, NULL}
};

Object* objNew(ObjDriver *driver)
{
	Object *obj = (Object*) malloc(sizeof(Object));
	memset(obj, 0, sizeof(Object));
	obj->driver = driver;
	
	if (driver->init != NULL) driver->init(obj, driver->opt);
	return obj;
};

Section* objGetSection(Object *obj, const char *name)
{
	Section *sect;
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		if (strcmp(sect->name, name) == 0)
		{
			return sect;
		};
	};
	
	return NULL;
};

Section* objCreateSection(Object *obj, const char *name, int type, int flags)
{
	Section *sect = (Section*) malloc(sizeof(Section));
	memset(sect, 0, sizeof(Section));
	sect->name = strdup(name);
	sect->type = type;
	sect->flags = flags;
	sect->align = 1;
	sect->obj = obj;
	
	uint64_t highestAddr = 0;
	Section *scan;
	for (scan=obj->sects; scan!=NULL; scan=scan->next)
	{
		uint64_t end = scan->addr + scan->size;
		if (end > highestAddr) highestAddr = end;
	};
	
	sect->addr = highestAddr;
	sect->next = obj->sects;
	obj->sects = sect;
	
	sect->sym = objGetSymbol(obj, name, 1);
	sect->sym->type = SYMT_SECTION;
	sect->sym->sect = sect;
	
	return sect;
};

Error objSectionAppend(Section *sect, const void *data, size_t size)
{
	if (sect->type != SECTYPE_PROGBITS)
	{
		return ERR_INVALID_PARAM;
	};
	
	sect->data = realloc(sect->data, sect->size + size);
	memcpy((char*)sect->data + sect->size, data, size);
	sect->size += size;
	
	return ERR_OK;
};

Error objSectionResv(Section *sect, size_t size)
{
	if (sect->type != SECTYPE_NOBITS)
	{
		return ERR_INVALID_PARAM;
	};

	sect->size += size;
	
	return ERR_OK;
};

Symbol* objGetSymbol(Object *obj, const char *name, int make)
{
	Symbol *sym;
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		if (strcmp(sym->name, name) == 0)
		{
			return sym;
		};
	};
	
	if (!make) return NULL;
	
	sym = (Symbol*) malloc(sizeof(Symbol));
	memset(sym, 0, sizeof(Symbol));
	
	sym->name = strdup(name);
	sym->type = SYMT_UNDEF;
	sym->binding = SYMB_LOCAL;
	
	sym->next = obj->syms;
	obj->syms = sym;
	
	return sym;
};

Error objSectionReloc(Section *sect, RelocType *type, Symbol *sym, int64_t addend, const char *prop)
{
	if (sect->type != SECTYPE_PROGBITS)
	{
		return ERR_INVALID_PARAM;
	};
	
	Reloc *reloc = (Reloc*) malloc(sizeof(Reloc));
	memset(reloc, 0, sizeof(Reloc));
	
	reloc->type = type;
	reloc->sect = sect;
	reloc->offset = sect->size;
	reloc->sym = sym;
	reloc->addend = addend;
	
	reloc->next = sect->relocs;
	sect->relocs = reloc;
	
	sect->data = realloc(sect->data, sect->size + type->bytes);
	memset((char*)sect->data + sect->size, 0, type->bytes);
	sect->size += type->bytes;
	
	if (prop != NULL) reloc->prop = strdup(prop);
	
	return ERR_OK;
};

Error objResolveLocal(Object *obj)
{
	Section *sect;
	Reloc *reloc;
	
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		for (reloc=sect->relocs; reloc!=NULL; reloc=reloc->next)
		{
			switch (reloc->sym->type)
			{
			case SYMT_NONE:
			case SYMT_OBJECT:
			case SYMT_FUNC:
				if (reloc->sym->sect != NULL && reloc->sym->binding == SYMB_LOCAL)
				{
					reloc->addend += reloc->sym->value;
					reloc->sym = reloc->sym->sect->sym;
				};
			};
		};
	};
	
	// delete all zero-sized sections
	while (obj->sects != NULL && obj->sects->size == 0)
	{
		obj->sects = obj->sects->next;
	};
	
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		while (sect->next != NULL && sect->next->size == 0)
		{
			sect->next = sect->next->next;
		};
	};
	
	// delete all symbols which belong to empty sections
	while (obj->syms != NULL && obj->syms->sect != NULL && obj->syms->sect->size == 0)
	{
		obj->syms = obj->syms->next;
	};
	
	Symbol *sym;
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		while (sym->next != NULL && sym->next->sect != NULL && sym->next->sect->size == 0)
		{
			sym->next = sym->next->next;
		};
	};
	
	return ERR_OK;
};

Error objWrite(Object *obj, const char *filename)
{
	objResolveLocal(obj);
	
	FILE *fp = fopen(filename, "wb");
	if (fp == NULL)
	{
		return ERR_IO;
	};
	
	Error err = obj->driver->write(obj, fp);
	if (err != ERR_OK)
	{
		// writing failed, so close and delete the file
		fclose(fp);
		remove(filename);
		return err;
	};
	
	// if the object file is an executable, and we are on a POSIX system,
	// mark it as executable
#ifndef _WIN32
	if (obj->type == OBJTYPE_EXEC)
	{
		int fd = fileno(fp);
		if (fd == -1)
		{
			perror("objWrite: fileno");
		}
		else if (fchmod(fileno(fp), 0755) != 0)
		{
			perror("objWrite: fchmod executable");
		};
	};
#endif

	fclose(fp);
	return ERR_OK;
};

ObjDriver* objGetDriver(const char *name)
{
	ObjDriverMapping *map;
	for (map=objDriverList; map->name!=NULL; map++)
	{
		if (strcmp(map->name, name) == 0)
		{
			return map->drv;
		};
	};
	
	return NULL;
};

Error objAddSymbolWithType(Object *obj, Section *sect, const char *name, int type)
{
	Symbol *sym = objGetSymbol(obj, name, 1);
	if (sym->sect != NULL || sym->value != 0)
	{
		return ERR_MULTIPLE_DEF;
	};
	
	if (sym->type == SYMT_UNDEF)
	{
		sym->type = SYMT_NONE;
	};
	
	sym->sect = sect;
	sym->value = sect->size;
	sym->type = type;
	
	return ERR_OK;
};

Error objAddSymbol(Object *obj, Section *sect, const char *name)
{
	return objAddSymbolWithType(obj, sect, name, SYMT_NONE);
};

Annot* objAddAnnot(Section *sect, const char *name)
{
	Annot *annot = (Annot*) calloc(1, sizeof(Annot));
	annot->name = strdup(name);
	annot->sectOffset = sect->size;
	
	annot->next = sect->annot;
	sect->annot = annot;
	
	return annot;
};

AnnotChild* objCreateAnnotChild(Annot *parent)
{
	AnnotChild *child = (AnnotChild*) calloc(1, sizeof(Annot));
	if (parent != NULL)
	{
		child->next = parent->children;
		parent->children = child;
	};
	
	return child;
};

Error objEndSymbol(Object *obj, const char *name)
{
	Symbol *sym = objGetSymbol(obj, name, 0);
	if (sym == NULL || sym->sect == NULL) return ERR_UNDEF_REF;
	sym->size = sym->sect->size - sym->value;
	return ERR_OK;
};

void objSymbolBinding(Object *obj, const char *name, int binding)
{
	Symbol *sym = objGetSymbol(obj, name, 1);
	sym->binding = binding;
};

Error objAbsoluteSymbol(Object *obj, const char *name, uint64_t value)
{
	Symbol *sym = objGetSymbol(obj, name, 1);
	if (sym->sect != NULL || sym->value != 0)
	{
		return ERR_MULTIPLE_DEF;
	};
	
	if (sym->type == SYMT_UNDEF)
	{
		sym->type = SYMT_NONE;
	};
	
	sym->sect = NULL;
	sym->value = value;
	
	return ERR_OK;
};

Error objCommonSymbol(Object *obj, const char *name, uint64_t align, uint64_t size)
{
	Symbol *sym = objGetSymbol(obj, name, 1);
	if (sym->sect != NULL || sym->value != 0)
	{
		return ERR_MULTIPLE_DEF;
	};
	
	if (sym->type == SYMT_COMMON)
	{
		if (sym->align == align && sym->size == size) return ERR_OK;
	};
	
	if (sym->type != SYMT_UNDEF)
	{
		return ERR_MULTIPLE_DEF;
	};
	
	sym->type = SYMT_COMMON;
	sym->align = align;
	sym->size = size;
	sym->binding = SYMB_GLOBAL;
	
	return ERR_OK;
};

Object* objRead(ObjDriver *driver, const char *filename)
{
	if (driver->read == NULL) return NULL;
	
	FILE *fp = fopen(filename, "rb");
	if (fp == NULL) return NULL;

	Object *obj = objNew(driver);
	if (obj == NULL)
	{
		fclose(fp);
		return NULL;
	};
	
	Error err = driver->read(obj, fp);
	if (err != ERR_OK) return NULL;
	
	return obj;
};

void objSetSoname(Object *obj, const char *soname)
{
	free(obj->soname);
	obj->soname = strdup(soname);
};

Error objAddDepend(Object *obj, Object *dep)
{
	if (dep->type != OBJTYPE_SHARED)
	{
		return ERR_INVALID_PARAM;
	};
	
	if (dep->soname == NULL)
	{
		return ERR_INVALID_PARAM;
	};
	
	int index = obj->numDepends++;
	obj->depends = (Object**) realloc(obj->depends, sizeof(void*) * obj->numDepends);
	obj->depends[index] = dep;
	
	return ERR_OK;
};
