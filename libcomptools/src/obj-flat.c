/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <comptools/obj-flat.h>

static Error objFlatWrite(Object *obj, FILE *fp)
{
	// TODO: some way to specify the origin
	uint64_t origin = 0;
	
	Section *sect;
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		if (sect->type == SECTYPE_PROGBITS)
		{
			fseek(fp, sect->addr-origin, SEEK_SET);
			fwrite(sect->data, 1, sect->size, fp);
		};
		
		// calculate relocations
		Reloc *reloc;
		for (reloc=sect->relocs; reloc!=NULL; reloc=reloc->next)
		{
			if (reloc->sym->type == SYMT_UNDEF || reloc->sym->type == SYMT_COMMON)
			{
				fprintf(stderr, "objFlatWrite: warning: undefined reference to `%s'\n",
					reloc->sym->name);
			};
			
			uint64_t symval = reloc->sym->value;
			if (reloc->sym->sect != NULL) symval += reloc->sym->sect->addr;
			
			char buf[reloc->type->bytes];
			reloc->type->resolve(buf, symval, reloc->addend, reloc);
			
			fseek(fp, sect->addr-origin+reloc->offset, SEEK_SET);
			fwrite(buf, 1, reloc->type->bytes, fp);
		};
	};
	
	return ERR_OK;
};

ObjDriver objDriver_flat = {
	.name = "flat",
	.write = objFlatWrite
};
