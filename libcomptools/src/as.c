/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <comptools/as.h>
#include <comptools/lex.h>
#include <comptools/obj.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <dlfcn.h>
#endif

#define	ACM_pop					(((uint64_t)ANNOT_AcmPop) << 48)
#define	ACM_const				((((uint64_t)ANNOT_AcmPushConst) << 48) | 8)
#define	ACM_reg					((((uint64_t)ANNOT_AcmPushStateEntry) << 48) | 1)
#define	ACM_add					(((uint64_t)ANNOT_AcmAdd) << 48)
#define	ACM_sub					(((uint64_t)ANNOT_AcmSub) << 48)
#define	ACM_fetch				(((uint64_t)ANNOT_AcmMemoryFetch) << 48)
#define	ACM_undef				(((uint64_t)ANNOT_AcmUndefined) << 48)

#ifdef AS_ENABLE_X86_64
extern AsDriver asDriver_x86_64;
#endif

typedef struct
{
	const char *name;
	AsDriver *drv;
} AsDriverMapping;

static AsDriverMapping asDriverList[] = {
#ifdef AS_ENABLE_X86_64
	{"x86_64", &asDriver_x86_64},
#endif

	// LIST TERMINATOR
	{NULL, NULL}
};

As* asNew(Object *obj, AsDriver *driver)
{
	As *as = (As*) malloc(sizeof(As));
	memset(as, 0, sizeof(As));
	
	as->driver = driver;
	as->obj = obj;
	as->sect = NULL;
	
	driver->init(as, driver->opt);
	return as;
};

Section* asGetSection(As *as)
{
	if (as->sect == NULL)
	{
		as->sect = objCreateSection(as->obj, ".text", SECTYPE_PROGBITS, SEC_READ | SEC_EXEC);
	};
	
	return as->sect;
};

static char* asCompileAcm(As *as, void **out, size_t *outSize)
{
	char *buffer = NULL;
	size_t size = 0;
	
	char *insn;
	for (insn=strtok(NULL, " \t\n"); insn!=NULL; insn=strtok(NULL, " \t\n"))
	{
		if (strcmp(insn, "pop") == 0)
		{
			buffer = (char*) realloc(buffer, size + 8);
			size += 8;
			as->driver->make(ACM_pop, buffer + size - 8, 8);
		}
		else if (memcmp(insn, "const:", 6) == 0)
		{
			char *endptr;
			long value = strtol(insn + 6, &endptr, 0);
			if (*endptr != 0)
			{
				free(buffer);
				return strdup("parameter to `const:' must be an integer constant");
			};
			
			buffer = (char*) realloc(buffer, size + 16);
			size += 16;
			
			as->driver->make(ACM_const, buffer + size - 16, 8);
			as->driver->make(value, buffer + size - 8, 8);
		}
		else if (memcmp(insn, "reg:", 4) == 0)
		{
			int regnum = as->driver->getGxoRegNum(as, insn + 4);
			if (regnum == -1)
			{
				free(buffer);
				char *error = (char*) malloc(strlen(insn + 4) + 256);
				sprintf(error, "unknown register name `%s'", insn + 4);
				return error;
			};
			
			buffer = (char*) realloc(buffer, size + 9);
			size += 9;
			
			as->driver->make(ACM_reg, buffer + size - 9, 8);
			buffer[size-1] = regnum;
		}
		else if (strcmp(insn, "add") == 0)
		{
			buffer = (char*) realloc(buffer, size + 8);
			size += 8;
			as->driver->make(ACM_add, buffer + size - 8, 8);
		}
		else if (strcmp(insn, "sub") == 0)
		{
			buffer = (char*) realloc(buffer, size + 8);
			size += 8;
			as->driver->make(ACM_sub, buffer + size - 8, 8);
		}
		else if (strcmp(insn, "fetch") == 0)
		{
			buffer = (char*) realloc(buffer, size + 8);
			size += 8;
			as->driver->make(ACM_fetch, buffer + size - 8, 8);
		}
		else if (strcmp(insn, "undef") == 0)
		{
			buffer = (char*) realloc(buffer, size + 8);
			size += 8;
			as->driver->make(ACM_undef, buffer + size - 8, 8);
		}
		else
		{
			free(buffer);
			
			char *err = (char*) malloc(strlen(insn) + 256);
			sprintf(err, "unrecognised ACM opcode: `%s'", insn);
			return err;
		};
	};
	
	*out = buffer;
	*outSize = size;
	return NULL;
};

static char* asProcessDirective(As *as, char *dir)
{
	char *cmd = strtok(dir, " \t\n");
	if (cmd == NULL)
	{
		return NULL;
	}
	else if (strcmp(cmd, ".globl") == 0)
	{
		char *symname = strtok(NULL, " \t\n");
		if (symname == NULL)
		{
			return strdup("'.globl' directive expects a parameter");
		};
		
		objSymbolBinding(as->obj, symname, SYMB_GLOBAL);
		return NULL;
	}
	else if (strcmp(cmd, ".line") == 0)
	{
		char *filename = strtok(NULL, ":");
		if (filename == NULL)
		{
			return strdup("`.line' directive expects a parameter");
		};
		
		char *linespec = strtok(NULL, "");
		if (linespec == NULL)
		{
			return strdup("`.line' directive expects a line number");
		};
		
		char *endptr;
		unsigned long lineno = strtoul(linespec, &endptr, 0);
		if (*endptr != 0)
		{
			return strdup("second parameter to '.line' must be an integer constant");
		};
		
		if (as->annot == NULL)
		{
			return strdup("`.line' directive can only be used inside a function");
		};
		
		AnnotChild *node = objCreateAnnotChild(as->annot);
		node->type = ANNOT_LineInfo;
		
		// <LineInfo>
		node->size = 0
			+ 8 + 8				// <Offset>?</Offset>
			+ 8 + 4				// <Line>?</Line>
			+ 8 + strlen(filename)		// <File>?</File>
		;
		// </LineInfo>
		
		node->data = malloc(node->size);
		char *put = (char*) node->data;
		
		// <LineInfo>
		
		//	<Offset>
		as->driver->make((((uint64_t) ANNOT_Offset) << 48) | 8, put + 0, 8);
		as->driver->make(asGetSection(as)->size - as->annot->sectOffset, put + 8, 8);
		//	</Offset>
		
		//	<Line>
		as->driver->make((((uint64_t) ANNOT_Line) << 48) | 4, put + 16, 8);
		as->driver->make(lineno, put + 24, 4);
		//	</Line>
		
		//	<File>
		as->driver->make((((uint64_t) ANNOT_File) << 48) + strlen(filename), put + 28, 8);
		memcpy(put + 36, filename, strlen(filename));
		//	</File>
		
		// </LineInfo>
		return NULL;
	}
	else if (strcmp(cmd, ".weak") == 0)
	{
		char *symname = strtok(NULL, " \t\n");
		if (symname == NULL)
		{
			return strdup("'.weak' directive expects a parameter");
		};
		
		objSymbolBinding(as->obj, symname, SYMB_WEAK);
		return NULL;
	}
	else if (strcmp(cmd, ".equ") == 0)
	{
		char *symname = strtok(NULL, " \t\n");
		char *valstr = strtok(NULL, " \t\n");
		
		if (symname == NULL || valstr == NULL)
		{
			return strdup("'.equ' directive expects 2 parameters");
		};
		
		char *endptr;
		unsigned long value = strtoul(valstr, &endptr, 0);
		if (*endptr != 0)
		{
			return strdup("second parameter to '.equ' must be an integer constant");
		};
		
		objAbsoluteSymbol(as->obj, symname, value);
		return NULL;
	}
	else if (strcmp(cmd, ".comm") == 0)
	{
		char *name = strtok(NULL, " \t\n");
		char *alignstr = strtok(NULL, " \t\n");
		char *sizestr = strtok(NULL, " \t\n");
		
		if (name == NULL || alignstr == NULL || sizestr == NULL)
		{
			return strdup("'.comm' directive expects a parameter");
		};
		
		char *endptr;
		unsigned long align = strtoul(alignstr, &endptr, 0);
		if (*endptr != 0)
		{
			return strdup("the second argument to '.comm' must be an integer");
		};
		
		unsigned long size = strtoul(sizestr, &endptr, 0);
		if (*endptr != 0)
		{
			return strdup("the third argument to '.comm' must be an integer");
		};
		
		objCommonSymbol(as->obj, name, align, size);
		return NULL;
	}
	else if (strcmp(cmd, ".section") == 0)
	{
		char *name = strtok(NULL, " \t\n");
		if (name ==  NULL)
		{
			return strdup("'.section' requires a name");
		};
		
		int sectype = -1;
		int flags = -1;
		
		char *typename = strtok(NULL, " \t\n");
		if (typename != NULL)
		{
			if (strcmp(typename, "progbits") == 0)
			{
				sectype = SECTYPE_PROGBITS;
			}
			else if (strcmp(typename, "nobits") == 0)
			{
				sectype = SECTYPE_NOBITS;
			}
			else
			{
				const char *format = "invalid section type '%s' (expected 'progbits' or 'nobits'";
				char *buffer = (char*) malloc(strlen(format) + strlen(typename) + 1);
				sprintf(buffer, format, typename);
				return buffer;
			};
			
			char *flagspec = strtok(NULL, " \t\n");
			if (flagspec != NULL)
			{
				flags = 0;
				
				const char *format = "unrecognised flag `%c'";
				char *buffer;
				
				for (; *flagspec!=0; flagspec++)
				{
					switch (*flagspec)
					{
					case 'r':
						flags |= SEC_READ;
						break;
					case 'w':
						flags |= SEC_WRITE;
						break;
					case 'x':
						flags |= SEC_EXEC;
						break;
					default:
						buffer = (char*) malloc(strlen(format));
						sprintf(buffer, format, *flagspec);
						return buffer;
					};
				};
			};
		};
		
		if (sectype == -1)
		{
			if (strcmp(name, ".bss") == 0)
			{
				sectype = SECTYPE_NOBITS;
			}
			else
			{
				sectype = SECTYPE_PROGBITS;
			};
		};
		
		if (flags == -1)
		{
			if (strcmp(name, ".text") == 0)
			{
				flags = SEC_READ | SEC_EXEC;
			}
			else if (strcmp(name, ".init") == 0)
			{
				flags = SEC_READ | SEC_EXEC;
			}
			else if (strcmp(name, ".fini") == 0)
			{
				flags = SEC_READ | SEC_EXEC;
			}
			else if (strcmp(name, ".rodata") == 0)
			{
				flags = SEC_READ;
			}
			else
			{
				flags = SEC_READ | SEC_WRITE;
			};
		};
		
		as->sect = objGetSection(as->obj, name);
		if (as->sect == NULL)
		{
			as->sect = objCreateSection(as->obj, name, sectype, flags);
		};
		
		return NULL;
	}
	else if (strcmp(cmd, ".string") == 0)
	{
		char *token = strtok(NULL, "");
		if (token ==  NULL)
		{
			return strdup("'.string' requires a parameter");
		};
		
		char *buffer = (char*) malloc(strlen(token)+1);
		char result = lexParseString(token, buffer);
		if (result == -1)
		{
			free(buffer);
			const char *format = "invalid string token: %s";
			char *ebuffer = (char*) malloc(strlen(format) + strlen(token));
			sprintf(ebuffer, format, token);
			return ebuffer;
		}
		else if (result != 0)
		{
			const char *format = "invalid escape sequence: \\%c";
			char *ebuffer = (char*) malloc(strlen(format));
			sprintf(ebuffer, format, result);
			free(buffer);
			return ebuffer;
		}
		else
		{
			objSectionAppend(asGetSection(as), buffer, strlen(buffer)+1);
		};
		
		free(buffer);
		return NULL;
	}
	else if (strcmp(cmd, ".object") == 0)
	{
		char *name = strtok(NULL, "");
		if (name == NULL)
		{
			return strdup("`.object' requires the object name as a parameter");
		};
		
		Error err = objAddSymbolWithType(as->obj, asGetSection(as), name, SYMT_OBJECT);
		if (err == ERR_OK)
		{
			return NULL;
		}
		else if (err == ERR_MULTIPLE_DEF)
		{
			static const char *format = "multiple definition of symbol `%s'";
			char *buffer = (char*) malloc(strlen(format) + strlen(name));
			sprintf(buffer, format, name);
			return buffer;
		}
		else
		{
			static const char *format = "unknown error while adding symbol `%s'";
			char *buffer = (char*) malloc(strlen(format) + strlen(name));
			sprintf(buffer, format, name);
			return buffer;
		};
	}
	else if (strcmp(cmd, ".func") == 0)
	{
		char *name = strtok(NULL, "");
		if (name == NULL)
		{
			return strdup("`.func' requires the function name as a parameter");
		};
		
		Error err = objAddSymbolWithType(as->obj, asGetSection(as), name, SYMT_FUNC);
		if (err == ERR_OK)
		{
			as->annot = objAddAnnot(asGetSection(as), name);
			return NULL;
		}
		else if (err == ERR_MULTIPLE_DEF)
		{
			static const char *format = "multiple definition of symbol `%s'";
			char *buffer = (char*) malloc(strlen(format) + strlen(name));
			sprintf(buffer, format, name);
			return buffer;
		}
		else
		{
			static const char *format = "unknown error while adding symbol `%s'";
			char *buffer = (char*) malloc(strlen(format) + strlen(name));
			sprintf(buffer, format, name);
			return buffer;
		};
	}
	else if (strcmp(cmd, ".unwind") == 0)
	{
		if (as->annot == NULL)
		{
			return strdup("`.unwind' can only appear inside a function");
		};
		
		char *regname = strtok(NULL, " \t\n");
		if (regname == NULL)
		{
			return strdup("`.unwind' requires a register name and ACM code");
		};
		
		int regnum = as->driver->getGxoRegNum(as, regname);
		if (regnum == -1)
		{
			char *error = (char*) malloc(strlen(regname) + 256);
			sprintf(error, "`%s' is not a valid register name on this target", regname);
			return error;
		};
		
		void *acm;
		size_t size;
		
		char *error = asCompileAcm(as, &acm, &size);
		if (error != NULL) return error;
		
		AnnotChild *node = objCreateAnnotChild(as->annot);
		node->type = ANNOT_Unwind;
		
		// <Unwind>
		node->size = 0
			+ 8 + 8			// <Offset>?</Offset>
			+ 8 + 1			// <StateEntry>?</StateEntry>
			+ 8 + size		// <Code>?</Code>
		;
		// </Unwind>
		
		node->data = malloc(node->size);
		
		char *put = (char*) node->data;
		
		// <Unwind>
		
		//	<Offset>
		as->driver->make(((uint64_t)ANNOT_Offset << 48) | 8, put + 0, 8);
		as->driver->make(asGetSection(as)->size - as->annot->sectOffset, put + 8, 8);
		//	</Offset>
		
		// 	<StateEntry>
		as->driver->make(((uint64_t)ANNOT_StateEntry << 48) | 1, put + 16, 8);
		put[24] = regnum;
		// 	</StateEntry>
		
		// 	<Code>
		as->driver->make(((uint64_t)ANNOT_Code << 48) + size, put + 25, 8);
		memcpy(put + 33, acm, size);
		// 	</Code>
		
		// </Unwind>
		free(acm);
		return NULL;
	}
	else if (strcmp(cmd, ".end") == 0)
	{
		char *name = strtok(NULL, "");
		if (name == NULL)
		{
			return strdup("`.end' requires a symbol name as a parameter");
		};
		
		Error err = objEndSymbol(as->obj, name);
		if (err != ERR_OK)
		{
			return strdup("undefined symbol passed to `.end'");
		};
		
		if (as->annot != NULL)
		{
			as->annot->size = asGetSection(as)->size - as->annot->sectOffset;
			as->annot = NULL;
		};
		
		return NULL;
	}
	else if (strcmp(cmd, ".align") == 0)
	{
		char *val = strtok(NULL, "");
		if (val == NULL)
		{
			return strdup("`.align' required a parameter");
		};

		char *endptr;
		unsigned long align = strtoul(val, &endptr, 0);
		if (*endptr != 0)
		{
			return strdup("the argument to `.align' must be an integer");
		};
		
		if (align == 0 || (align & (align - 1)))
		{
			return strdup("alignment must be a power of two");
		};
		
		Section *sect = asGetSection(as);
		if (sect->align < align)
		{
			sect->align = align;
		};
		
		char zeroes[align];
		memset(zeroes, 0, align);
		
		unsigned long bytesLeft = (align - (sect->size % align)) % align;
		if (sect->type == SECTYPE_NOBITS)
		{
			objSectionResv(sect, bytesLeft);
		}
		else
		{
			objSectionAppend(sect, zeroes, bytesLeft);
		};
		
		return NULL;
	}
	else if (strcmp(cmd, ".resv") == 0)
	{
		char *val = strtok(NULL, "");
		if (val == NULL)
		{
			return strdup("`.resv' required a parameter");
		};

		char *endptr;
		unsigned long bytes = strtoul(val, &endptr, 0);
		if (*endptr != 0)
		{
			return strdup("the argument to `.resv' must be an integer");
		};
		
		if (objSectionResv(asGetSection(as), bytes) != ERR_OK)
		{
			return strdup("`.resv' cannot be used inside PROGBITS section");
		};
		
		return NULL;
	}
	else
	{
		const char *format = "invalid assembler directive `%s'";
		char *buffer = (char*) malloc(strlen(format) + strlen(cmd));
		sprintf(buffer, format, cmd);
		return buffer;
	};
};

char* asLine(As *as, const char *rawline)
{
	if (as->driver == NULL)
	{
		FILE *fp = (FILE*) as->drvdata;
		fprintf(fp, "%s\n", rawline);
		return NULL;
	};
	
	char *line = strdup(rawline);
	char *orgline = line;
	
	char *newline = strchr(line, '\n');
	if (newline != NULL) *newline = 0;
	newline = strchr(line, '\r');
	if (newline != NULL) *newline = 0;
	
	while ((strchr(" \t", *line) != NULL) && (*line != 0)) line++;
	
	if (line[0] == 0)
	{
		return NULL;
	};
	
	char *compos = strstr(line, "//");
	if (compos != NULL) *compos = 0;
	
	compos = strstr(line, as->driver->comment);
	if (compos != NULL) *compos = 0;
	
	if (line[0] == '.')
	{
		return asProcessDirective(as, line);
	}
	else if (line[strlen(line)-1] == ':')
	{
		line[strlen(line)-1] = 0;
		Error err = objAddSymbol(as->obj, asGetSection(as), line);
		free(line);
		
		if (err == ERR_OK)
		{
			return NULL;
		}
		else if (err == ERR_MULTIPLE_DEF)
		{
			static const char *format = "multiple definition of symbol `%s'";
			char *buffer = (char*) malloc(strlen(format) + strlen(line));
			sprintf(buffer, format, line);
			return buffer;
		}
		else
		{
			static const char *format = "unknown error while adding symbol `%s'";
			char *buffer = (char*) malloc(strlen(format) + strlen(line));
			sprintf(buffer, format, line);
			return buffer;
		};
	}
	else
	{
		char *error = as->driver->line(as, line);
		free(orgline);
		return error;
	};
};

AsDriver* asGetDriver(const char *name)
{
	AsDriverMapping *map;
	for (map=asDriverList; map->name!=NULL; map++)
	{
		if (strcmp(map->name, name) == 0)
		{
			return map->drv;
		};
	};
	
	return NULL;
};

As* asPseudo(FILE *fp)
{
	As *as = (As*) calloc(1, sizeof(As));
	as->drvdata = fp;
	return as;
};
