/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <string.h>

#include <comptools/ld-script.h>
#include <comptools/console.h>
#include <comptools/obj.h>

/**
 * Token specifications for .lds files.
 */
static TokenSpec ldsTokens[] = {
	{TOK_LDS_SECTION,		"section"},
	{TOK_LDS_SECTION_TYPE,		"(progbits|nobits)"},
	{TOK_LDS_LOAD,			"load"},
	{TOK_LDS_ENTRY,			"entry"},
	{TOK_LDS_ALIGN,			"align"},
	{TOK_LDS_ID,			"[._a-zA-Z][._a-zA-Z0-9]*"},
	{TOK_LDS_CONST,			"([1-9][0-9]*|0x[0-9a-fA-F]+|0[0-7]*)"},
	{TOK_LDS_ADD,			"\\+"},
	{TOK_LDS_SUB,			"\\-"},
	{TOK_LDS_SEMICOLON,		"\\;"},
	{TOK_LDS_EQUALS,		"\\="},
	{TOK_LDS_OPEN_BRACE,		"\\{"},
	{TOK_LDS_CLOSE_BRACE,		"\\}"},
	{TOK_LDS_SECTION_FLAGS,		"\\:[rwx]*"},
	
	// WHITESPACE
	{TOK_WHITESPACE,		"$+"},
	{TOK_WHITESPACE,		"\\/\\/.*?(\n|%)"},		/* line comment */
	{TOK_WHITESPACE,		"\\/\\*.*?(\\*\\/)"},		/* block comment */
	
	// LIST TERMINATOR
	{TOK_ENDLIST},
};

static LDS_PrimaryExpr* ldsPrimaryExpr(Token *token, Token **nextTok)
{
	if (token->type == TOK_LDS_CONST)
	{
		LDS_PrimaryExpr *expr = (LDS_PrimaryExpr*) malloc(sizeof(LDS_PrimaryExpr));
		memset(expr, 0, sizeof(LDS_PrimaryExpr));
		expr->value = token->value;
		expr->srctok = token;
		
		*nextTok = token->next;
		return expr;
	}
	else if (token->type == TOK_LDS_ID)
	{
		LDS_PrimaryExpr *expr = (LDS_PrimaryExpr*) malloc(sizeof(LDS_PrimaryExpr));
		memset(expr, 0, sizeof(LDS_PrimaryExpr));
		expr->symname = token->value;
		expr->srctok = token;
		
		*nextTok = token->next;
		return expr;
	}
	else
	{
		return NULL;
	};
};

static LDS_Sum* ldsSum(Token *token, Token **nextTok)
{
	// LDS_Sum ::= LDS_PrimaryExpr ( "+" | "-" ) LDS_Sum | LDS_PrimaryExpr
	LDS_PrimaryExpr *expr = ldsPrimaryExpr(token, nextTok);
	if (expr == NULL) return NULL;
	
	LDS_Sum *sum = (LDS_Sum*) malloc(sizeof(LDS_Sum));
	memset(sum, 0, sizeof(LDS_Sum));
	sum->expr = expr;
	
	token = *nextTok;
	if (token->type == TOK_LDS_ADD || token->type == TOK_LDS_SUB)
	{
		int op = token->type;
		*nextTok = token->next;
		token = token->next;

		sum->next = ldsSum(token, nextTok);
		if (sum->next == NULL)
		{
			lexDiag(token, CON_ERROR, "expected an expression, not `%s'", token->value);
		}
		else
		{
			sum->op = op;
		};
	};
	
	return sum;
};

static LDS_Assignment* ldsAssignment(Token *token, Token **nextTok)
{
	// LDS_Assignment ::= <name> "=" LDS_Sum ";"
	if (token->type != TOK_LDS_ID) return NULL;
	
	Token *tokName = token;
	token = token->next;
	
	if (token->type != TOK_LDS_EQUALS)
	{
		return NULL;
	};
	
	token = token->next;
	LDS_Sum *sum = ldsSum(token, nextTok);
	if (sum == NULL)
	{
		lexDiag(token, CON_ERROR, "expected an expression, not `%s'", token->value);
		*nextTok = token;
		return NULL;
	};
	
	LDS_Assignment *assign = (LDS_Assignment*) malloc(sizeof(LDS_Assignment));
	memset(assign, 0, sizeof(LDS_Assignment));
	assign->name = tokName->value;
	assign->sum = sum;

	token = *nextTok;
	if (token->type != TOK_LDS_SEMICOLON)
	{
		lexDiag(token, CON_ERROR, "epxected `;' or an operator, not `%s'", token->value);
	}
	else
	{
		*nextTok = token->next;
	};
	
	return assign;
};

static LDS_Load* ldsLoad(Token *token, Token **nextTok)
{
	// LDS_Load ::= "load" <name> ";"
	if (token->type != TOK_LDS_LOAD) return NULL;
	token = token->next;
	
	if (token->type != TOK_LDS_ID)
	{
		lexDiag(token, CON_ERROR, "expected section name, not `%s'", token->value);
		return NULL;
	};
	
	const char *name = token->value;
	token = token->next;
	
	if (token->type != TOK_LDS_SEMICOLON)
	{
		lexDiag(token, CON_ERROR, "expected `;', not `%s'", token->value);
	}
	else
	{
		token = token->next;
	};
	
	*nextTok = token;
	
	LDS_Load *load = (LDS_Load*) malloc(sizeof(LDS_Load));
	load->name = name;
	return load;
};

static LDS_SectionStatement* ldsSectionStatement(Token *token, Token **nextTok)
{
	// LDS_SectionStatement ::= LDS_Assignment | LDS_Load
	LDS_Assignment *assign = ldsAssignment(token, nextTok);
	LDS_Load *load = NULL;
	if (assign == NULL)
	{
		load = ldsLoad(token, nextTok);
		if (load == NULL) return NULL;
	};
	
	LDS_SectionStatement *st = (LDS_SectionStatement*) malloc(sizeof(LDS_SectionStatement));
	memset(st, 0, sizeof(LDS_SectionStatement));
	st->assign = assign;
	st->load = load;
	st->srctok = token;
	return st;
};

static LDS_SectionBody* ldsSectionBody(Token *token, Token **nextTok)
{
	// LDS_SectionBody ::= LDS_SectionStatement [LDS_SectionBody]
	LDS_SectionBody *first = NULL;
	LDS_SectionBody *last = NULL;
	
	while (token->type != TOK_LDS_CLOSE_BRACE)
	{
		if (token->type == TOK_LDS_SEMICOLON)
		{
			token = token->next;
			continue;
		};
		
		LDS_SectionStatement *st = ldsSectionStatement(token, nextTok);
		if (st == NULL)
		{
			lexDiag(token, CON_ERROR, "expected a statement, not `%s'", token->value);
			break;
		};
		
		LDS_SectionBody *body = (LDS_SectionBody*) malloc(sizeof(LDS_SectionBody));
		memset(body, 0, sizeof(LDS_SectionBody));
		body->st = st;
		
		if (first == NULL)
		{
			first = last = body;
		}
		else
		{
			last->next = body;
			last = body;
		};
		
		token = *nextTok;
	};
	
	*nextTok = token->next;
	return first;
};

static LDS_SectionDef* ldsSectionDef(Token *token, Token **nextTok)
{
	// LDS_SectionDef ::= "section" <name> [LDS_SectionType] [LDS_SectionFlags] "{" LDS_SectionBody "}"
	if (token->type != TOK_LDS_SECTION) return NULL;
	Token *srctok = token;
	token = token->next;
	
	if (token->type != TOK_LDS_ID)
	{
		lexDiag(token, CON_ERROR, "expected section name identifier, not `%s'",
			token->value);
		return NULL;
	};
	
	Token *nametok = token;
	const char *name = token->value;
	token = token->next;
	int sectype, flags;
	
	if (strcmp(name, ".bss") == 0)
	{
		sectype = SECTYPE_NOBITS;
	}
	else
	{
		sectype = SECTYPE_PROGBITS;
	};
	
	if (strcmp(name, ".text") == 0)
	{
		flags = SEC_READ | SEC_EXEC;
	}
	else if (strcmp(name, ".init") == 0)
	{
		flags = SEC_READ | SEC_EXEC;
	}
	else if (strcmp(name, ".fini") == 0)
	{
		flags = SEC_READ | SEC_EXEC;
	}
	else if (strcmp(name, ".rodata") == 0)
	{
		flags = SEC_READ;
	}
	else
	{
		flags = SEC_READ | SEC_WRITE;
	};

	if (token->type == TOK_LDS_SECTION_TYPE)
	{
		if (strcmp(token->value, "progbits") == 0)
		{
			sectype = SECTYPE_PROGBITS;
		}
		else
		{
			sectype = SECTYPE_NOBITS;
		};
		
		token = token->next;
	};
	
	if (token->type == TOK_LDS_SECTION_FLAGS)
	{
		flags = 0;
		
		const char *scan;
		for (scan=&token->value[1]; *scan!=0; scan++)
		{
			switch (*scan)
			{
			case 'r':
				flags |= SEC_READ;
				break;
			case 'w':
				flags |= SEC_WRITE;
				break;
			case 'x':
				flags |= SEC_EXEC;
				break;
			};
		};
		
		token = token->next;
	};
	
	if (token->type != TOK_LDS_OPEN_BRACE)
	{
		lexDiag(token, CON_ERROR, "expected `{', not `%s'", token->value);
		return NULL;
	};
	
	token = token->next;
	
	// section body time.
	// NOTE: ldsSectionBody() sets *nextTok to be the token AFTER `}'
	LDS_SectionBody *body = ldsSectionBody(token, nextTok);
	
	LDS_SectionDef *secdef = (LDS_SectionDef*) malloc(sizeof(LDS_SectionDef));
	memset(secdef, 0, sizeof(LDS_SectionDef));
	secdef->name = name;
	secdef->type = sectype;
	secdef->flags = flags;
	secdef->body = body;
	secdef->srctok = srctok;
	secdef->nametok = nametok;
	return secdef;
};

static LDS_Entry* ldsEntry(Token *token, Token **nextTok)
{
	if (token->type != TOK_LDS_ENTRY) return NULL;
	token = token->next;
	
	if (token->type != TOK_LDS_ID)
	{
		lexDiag(token, CON_ERROR, "expected a symbol name, not `%s'", token->value);
		return NULL;
	};
	
	const char *symname = token->value;
	Token *nametok = token;
	token = token->next;
	
	if (token->type != TOK_LDS_SEMICOLON)
	{
		lexDiag(token, CON_ERROR, "expected `;', not `%s'", token->value);
	}
	else
	{
		token = token->next;
	};
	
	*nextTok = token;
	
	LDS_Entry *entry = (LDS_Entry*) malloc(sizeof(LDS_Entry));
	memset(entry, 0, sizeof(LDS_Entry));
	entry->symname = symname;
	entry->nametok = nametok;
	return entry;
};

static LDS_Align* ldsAlign(Token *token, Token **nextTok)
{
	if (token->type != TOK_LDS_ALIGN) return NULL;
	token = token->next;
	
	if (token->type != TOK_LDS_CONST)
	{
		lexDiag(token, CON_ERROR, "expected alignment value, not `%s'", token->value);
		return NULL;
	};
	
	LDS_Align *align = (LDS_Align*) malloc(sizeof(LDS_Align));
	memset(align, 0, sizeof(LDS_Align));
	align->value = token->value;
	align->srctok = token;
	
	token = token->next;
	if (token->type != TOK_LDS_SEMICOLON)
	{
		lexDiag(token, CON_ERROR, "expected `;', not `%s'", token->value);
	}
	else
	{
		token = token->next;
	};
	
	*nextTok = token;
	return align;
};

static LDS_TopLevelStatement* ldsTopLevelStatement(Token *token, Token **nextTok)
{
	// LDS_TopLevelStatement ::= LDS_Assignment | LDS_SectionDef | LDS_Entry
	LDS_Assignment *assign = ldsAssignment(token, nextTok);
	LDS_SectionDef *secdef = NULL;
	LDS_Entry *entry = NULL;
	LDS_Align *align = NULL;
	if (assign == NULL)
	{
		secdef = ldsSectionDef(token, nextTok);
		if (secdef == NULL)
		{
			entry = ldsEntry(token, nextTok);
			if (entry == NULL)
			{
				align = ldsAlign(token, nextTok);
				if (align == NULL) return NULL;
			};
		};
	};
	
	LDS_TopLevelStatement *st = (LDS_TopLevelStatement*) malloc(sizeof(LDS_TopLevelStatement));
	memset(st, 0, sizeof(LDS_TopLevelStatement));
	st->assign = assign;
	st->secdef = secdef;
	st->srctok = token;
	st->entry = entry;
	st->align = align;
	return st;
};

static LDS_Script* ldsScript(Token *token)
{
	// LDS_Script ::= LDS_TopLevelStatement LDS_Script | LDS_TopLevelStatement
	LDS_Script *first = NULL;
	LDS_Script *last = NULL;
	
	while (token->type != TOK_END)
	{
		if (token->type == TOK_LDS_SEMICOLON)
		{
			token = token->next;
			continue;
		};
		
		LDS_TopLevelStatement *st = ldsTopLevelStatement(token, &token);
		if (st == NULL)
		{
			lexDiag(token, CON_ERROR, "expected an expression or section, not `%s'", token->value);
			return NULL;
		};
		
		LDS_Script *script = (LDS_Script*) malloc(sizeof(LDS_Script));
		memset(script, 0, sizeof(LDS_Script));
		script->st = st;
		
		if (last == NULL)
		{
			first = last = script;
		}
		else
		{
			last->next = script;
			last = script;
		};
	};
	
	return first;
};

LDS_Script* ldsParse(const char *script, const char *filename)
{
	if (ldsTokens[0].regex == NULL)
	{
		lexCompileTokenSpecs(ldsTokens);
	};
	
	Token *token = lexTokenize(ldsTokens, script, filename);
	if (token == NULL) return NULL;
	
	return ldsScript(token);
};

static void printTabs(int level)
{
	level *= 4;
	while (level--) fputc(' ', stdout);
};

static void ldsDumpExpr(int level, LDS_PrimaryExpr *expr)
{
	if (expr->value != NULL)
	{
		printTabs(level); printf("<LDS_Const>%s</LDS_Const>\n", expr->value);
	}
	else if (expr->symname != NULL)
	{
		printTabs(level); printf("<LDS_Symbol>%s</LDS_Symbol>\n", expr->symname);
	}
	else
	{
		printTabs(level); printf("<LDS_InvalidPrimaryExpr/>\n");
	};
};

static void ldsDumpSum(int level, LDS_Sum *sum)
{
	printTabs(level); printf("<LDS_Sum>\n");
	ldsDumpExpr(level+1, sum->expr);
	
	char c = 0;
	switch (sum->op)
	{
	case TOK_LDS_ADD:
		c = '+';
		break;
	case TOK_LDS_SUB:
		c = '-';
		break;
	default:
		break;
	};
	
	if (c != 0)
	{
		printTabs(level+1); printf("<LDS_Operation>%c</LDS_Operation>\n", c);
		ldsDumpSum(level+1, sum->next);
	};
	
	printTabs(level); printf("</LDS_Sum>\n");
};

static void ldsDumpAssignment(int level, LDS_Assignment *assign)
{
	printTabs(level); printf("<LDS_SymbolName>%s</LDS_SymbolName>\n", assign->name);
	ldsDumpSum(level, assign->sum);
};

static void ldsDumpSectionDef(int level, LDS_SectionDef *secdef)
{
	const char *typename;
	switch (secdef->type)
	{
	case SECTYPE_PROGBITS:
		typename = "progbits";
		break;
	case SECTYPE_NOBITS:
		typename = "nobits";
		break;
	default:
		typename = "??";
		break;
	};
	
	printTabs(level); printf("<LDS_SectionName>%s</LDS_SectionName>\n", secdef->name);
	printTabs(level); printf("<LDS_SectionType>%s</LDS_SectionType>\n", typename);
	printTabs(level); printf("<LDS_SectionRead>%d</LDS_SectionRead>\n",
					!!(secdef->flags & SEC_READ));
	printTabs(level); printf("<LDS_SectionWrite>%d</LDS_SectionWrite>\n",
					!!(secdef->flags & SEC_WRITE));
	printTabs(level); printf("<LDS_SectionExec>%d</LDS_SectionExec>\n",
					!!(secdef->flags & SEC_EXEC));
	
	LDS_SectionBody *body;
	for (body=secdef->body; body!=NULL; body=body->next)
	{
		LDS_SectionStatement *st = body->st;
		if (st->assign != NULL)
		{
			printTabs(level); printf("<LDS_Assignment>\n");
			ldsDumpAssignment(level+1, st->assign);
			printTabs(level); printf("</LDS_Assignment>\n");
		}
		else if (st->load != NULL)
		{
			printTabs(level); printf("<LDS_Load>%s</LDS_Load>\n", st->load->name);
		}
		else
		{
			printTabs(level); printf("<LDS_InvalidStatement/>\n");
		};
	};
};

static void ldsDumpTopLevelStatement(int level, LDS_TopLevelStatement *st)
{
	if (st->assign != NULL)
	{
		printTabs(level); printf("<LDS_Assignment>\n");
		ldsDumpAssignment(level+1, st->assign);
		printTabs(level); printf("</LDS_Assignment>\n");
	}
	else if (st->secdef != NULL)
	{
		printTabs(level); printf("<LDS_Section>\n");
		ldsDumpSectionDef(level+1, st->secdef);
		printTabs(level); printf("</LDS_Section>\n");
	}
	else if (st->entry != NULL)
	{
		printTabs(level); printf("<LDS_Entry>%s</LDS_Entry>\n", st->entry->symname);
	}
	else if (st->align != NULL)
	{
		printTabs(level); printf("<LDS_Align>%s</LDS_Align>\n", st->align->value);
	}
	else
	{
		printTabs(level); printf("<LDS_InvalidStatement/>\n");
	};
};

void ldsDump(LDS_Script *script)
{
	printf("<LDS_Script>\n");
	for (; script!=NULL; script=script->next)
	{
		ldsDumpTopLevelStatement(1, script->st);
	};
	
	printf("</LDS_Script>\n");
};
