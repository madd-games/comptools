/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/ctir/analysis.h>
#include <comptools/console.h>

#define	REACHED ((void*)1)

static void reachBlock(CTIR_Func *func, int blockIndex)
{
	CTIR_Block *block = func->blocks[blockIndex];
	if (block->cgdata == REACHED) return;
	block->cgdata = REACHED;
	if (block->numCode == 0)
	{
		func->cgdata = REACHED;
		return;
	};
	
	CTIR_Insn *last = &block->code[block->numCode - 1];
	if (last->opcode == CTIR_ret)
	{
		func->cgdata = REACHED;
		return;
	};
	
	int found = 0;
	int i;
	for (i=0; i<last->numOps; i++)
	{
		CTIR_Operand *op = &last->ops[i];
		if (op->kind == CTIR_KIND_BLOCKREF)
		{
			reachBlock(func, op->blockIndex);
			found = 1;
		};
	};
	
	if (!found)
	{
		func->cgdata = REACHED;
	};
};

static void excludeBlock(CTIR_Func *func, int blockIndex)
{
	// remove the block from the list
	int i;
	for (i=blockIndex+1; i<func->numBlocks; i++)
	{
		func->blocks[i-1] = func->blocks[i];
	}
	
	func->numBlocks--;
	
	// fix all references to further blocks
	for (i=0; i<func->numBlocks; i++)
	{
		CTIR_Block *block = func->blocks[i];
		if (block->numCode == 0) continue;
		
		CTIR_Insn *last = &block->code[block->numCode - 1];
		
		int i;
		for (i=0; i<last->numOps; i++)
		{
			CTIR_Operand *op = &last->ops[i];
			if (op->kind == CTIR_KIND_BLOCKREF)
			{
				if (op->blockIndex >= blockIndex) op->blockIndex--;
			};
		};
	};
};

static int eliminateInFunc(CTIR_Func *func)
{
	func->cgdata = NULL;
	
	// first mark all blocks as being unreachable
	int i;
	for (i=0; i<func->numBlocks; i++)
	{
		func->blocks[i]->cgdata = NULL;
	};
	
	// "reach" block 0 and then recursively go to other blocks
	reachBlock(func, 0);
	
	// see which blocks could not be reached
	int status = -1;
	for (i=0; i<func->numBlocks; i++)
	{
		if (func->blocks[i]->cgdata == NULL)
		{
			if (func->blocks[i]->numCode != 0 && (func->blocks[i]->code[func->blocks[i]->numCode-1].opcode != CTIR_jmp || func->blocks[i]->numCode != 1))
			{
				lexDiag(func->blocks[i]->code[0].tok, CON_WARNING, "unreachable statement");
			};
			
			excludeBlock(func, i);
			i--;
			status = 0;
		};
	};
	
	// see if the function actually returns
	if (func->cgdata == NULL)
	{
		// no code path in this function leads to a return
		func->attr |= CTIR_FA_NORETURN;
	};
	
	return status;
};

int ctirOptDeadCode(CTIR_Unit *unit)
{
	int status = -1;
	
	CTIR_Func *func;
	for (func=unit->funcs; func!=NULL; func=func->next)
	{
		if (eliminateInFunc(func) == 0) status = 0;
	};
	
	return status;
};
