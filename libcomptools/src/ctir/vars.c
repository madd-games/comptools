/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/ctir/analysis.h>
#include <comptools/console.h>

// TODO: when blocks do not use a variable, but forward it to another block, there must be some indication of
// it; since CTIR_phi only passes regsiter values form the previous block

/**
 * Special values in the variable table.
 */
#define	VT_INDETERMINATE			-1			/* value cannot be known, must be in memory */
#define	VT_UNDEFINED				0			/* variable is not currently initialized */

/**
 * Function-global information about a variable.
 */
typedef struct
{
	/**
	 * Set to '1' if this is actually a variable (stack-type value).
	 */
	int isStack;
	
	/**
	 * Set to '1' if the variable is used in any operation other than
	 * loads and stores.
	 */
	int needsMemory;
	
	/**
	 * Variable name if known, else NULL.
	 */
	char *name;
} FuncVarInfo;

/**
 * The value of block->cgdata is a description of which CTIR register contains the current value of a variable.
 * See macros above for special values.
 * 
 * The indices are CTIR register numbers for stack-type variables. The values for indices which
 * do not indicate STACK-type variables are ignored, and the array may be smaller, such that non-STACK
 * indices are out of bounds.
 * 
 * The structure below is assigned func->cgdata instead.
 */
typedef struct
{
	/**
	 * Number of entries in the variable table.
	 */
	int tableSize;
	
	/**
	 * Next register to allocate when new ones needed.
	 */
	int nextReg;
	
	/**
	 * Global information about each variable.
	 */
	FuncVarInfo varInfo[];
} FuncInfo;

static void processBlock(CTIR_Func *func, int blockIndex, int *entryTable)
{
	CTIR_Block *block = func->blocks[blockIndex];
	FuncInfo *finfo = (FuncInfo*) func->cgdata;
	
	if (block->numCode == 0) return;
	
	int updated = 0;
	
	if (block->cgdata == NULL)
	{
		block->cgdata = (int*) malloc(sizeof(int) * finfo->tableSize);
		memcpy(block->cgdata, entryTable, sizeof(int) * finfo->tableSize);
		updated = 1;
	}
	else
	{
		int *oldTable = (int*) block->cgdata;
		
		int i;
		for (i=0; i<finfo->tableSize; i++)
		{
			if (oldTable[i] == entryTable[i])
			{
				// nothing at all is changing
				continue;
			};
			
			if (entryTable[i] == VT_UNDEFINED)
			{
				// if the new value is undefined, but the previous one wasn't, this indicates
				// that if the variable is used in this block, it might be uninitialized
				// eventually we should make some kind of warning for that (TODO)
				continue;
			};
	
			// we have multiple possibilities; check if we already have a phi-spec for this
			int j;
			for (j=0; j<block->numCode; j++)
			{
				CTIR_Insn *insn = &block->code[j];
				if (insn->opcode == CTIR_phi && insn->resultReg == oldTable[i])
				{
					// first see if already included
					int k;
					for (k=0; k<insn->numOps; k++)
					{
						if (insn->ops[k].regNo == entryTable[i])
						{
							// already a known possible input
							break;
						};
					};
					
					if (k == insn->numOps)
					{
						int index = insn->numOps++;
						insn->ops = (CTIR_Operand*) realloc(insn->ops, sizeof(CTIR_Operand) * insn->numOps);
						insn->ops[index] = ctirReg(entryTable[i]);
						updated = 1;
					};
					
					break;
				};
			};
			
			// if there is no phi-spec yet, we must create one
			if (j == block->numCode)
			{
				CTIR_Insn *newCode = (CTIR_Insn*) calloc(block->numCode+1, sizeof(CTIR_Insn));
				memcpy(&newCode[1], block->code, sizeof(CTIR_Insn) * block->numCode);
				newCode[0].opcode = CTIR_phi;
				newCode[0].tok = block->code[0].tok;
				newCode[0].resultReg = finfo->nextReg++;
				newCode[0].numOps = 2;
				newCode[0].ops = ctirOperandList(2, ctirReg(oldTable[i]), ctirReg(entryTable[i]));
				if (oldTable[i] == VT_UNDEFINED) newCode[0].ops = ctirOperandList(1, ctirReg(entryTable[i]));
				oldTable[i] = newCode[0].resultReg;
				updated = 1;
				
				block->code = newCode;
				block->numCode++;
			};
		};
	};
	
	// if no update was needed, we don't need to re-scan
	if (!updated) return;
	
	// otherwise, begin scanning the instructions
	int currentTable[finfo->tableSize];
	memcpy(currentTable, block->cgdata, sizeof(int) * finfo->tableSize);
	
	int i;
	for (i=0; i<block->numCode; i++)
	{
		CTIR_Insn *insn = &block->code[i];
		if (insn->opcode == CTIR_alloc)
		{
			finfo->varInfo[insn->resultReg].isStack = 1;
		}
		else if (insn->opcode == CTIR_load && insn->ops[0].kind == CTIR_KIND_REG)
		{
			int subject = insn->ops[0].regNo;
			if (subject < finfo->tableSize && finfo->varInfo[subject].isStack)
			{
				if (currentTable[subject] == VT_UNDEFINED)
				{
					lexDiag(insn->tok, CON_WARNING, "use of an uninitialized variable");
				};
			};
		}
		else if (insn->opcode == CTIR_store && insn->ops[0].kind == CTIR_KIND_REG)
		{
			int subject = insn->ops[0].regNo;
			if (subject < finfo->tableSize && finfo->varInfo[subject].isStack)
			{
				currentTable[subject] = insn->ops[1].regNo;
			};
		}
		else if (insn->opcode == CTIR_debug_var)
		{
			int subject = insn->ops[1].regNo;
			if (finfo->varInfo[subject].isStack)
			{
				finfo->varInfo[subject].name = strdup(insn->ops[0].symbolName);
			};
		}
		else
		{
			int j;
			for (j=0; j<insn->numOps; j++)
			{
				if (insn->ops[j].kind == CTIR_KIND_REG)
				{
					finfo->varInfo[insn->ops[j].regNo].needsMemory = 1;
				};
			};
		};
	};
	
	// now, if we're branching anywhere, pass our exit table
	CTIR_Insn *last = &block->code[block->numCode-1];
	for (i=0; i<last->numOps; i++)
	{
		if (last->ops[i].kind == CTIR_KIND_BLOCKREF)
		{
			int target = last->ops[i].blockIndex;
			
			processBlock(func, target, currentTable);
		};
	};
};

static void makeAlias(CTIR_Func *func, int replace, int with)
{
	int i;
	for (i=0; i<func->numBlocks; i++)
	{
		CTIR_Block *block = func->blocks[i];
		int j;
		for (j=0; j<block->numCode; j++)
		{
			CTIR_Insn *insn = &block->code[j];
			if (insn->opcode == CTIR_store || insn->opcode == CTIR_debug_var) continue;
			
			int k;
			for (k=0; k<insn->numOps; k++)
			{
				if (insn->ops[k].kind == CTIR_KIND_REG)
				{
					if (insn->ops[k].regNo == replace)
					{
						insn->ops[k].regNo = with;
					};
				};
			};
		};
	};
};

static int processFunc(CTIR_Func *func)
{
	// count the number of registers used
	int regsUsed = 0;
	
	int i;
	for (i=0; i<func->numBlocks; i++)
	{
		CTIR_Block *block = func->blocks[i];
		block->cgdata = NULL;
		
		int j;
		for (j=0; j<block->numCode; j++)
		{
			if (block->code[j].resultReg >= regsUsed)
			{
				regsUsed = block->code[j].resultReg + 1;
			};
		};
	};
	
	if (regsUsed == 0) return -1;
	
	// create information on the function
	FuncInfo *finfo = (FuncInfo*) calloc(1, sizeof(FuncInfo) + sizeof(FuncVarInfo) * regsUsed);
	finfo->tableSize = regsUsed;
	finfo->nextReg = regsUsed;
	func->cgdata = finfo;
	
	// make the initial variable table, which indicates all variables as uninitialized
	int initTable[regsUsed];
	memset(initTable, 0, sizeof(initTable));
	
	// figure out the starting states of each block
	processBlock(func, 0, initTable);
	
	// optimise out loads and stores which are already deterministic, and properly specify variable locations
	for (i=0; i<func->numBlocks; i++)
	{
		CTIR_Block *block = func->blocks[i];
		if (block->cgdata == NULL) continue;
		
		int *entryTable = (int*) block->cgdata;
		int varTable[regsUsed];
		memcpy(varTable, entryTable, sizeof(int) * regsUsed);
		
		int j;
		for (j=0; j<block->numCode; j++)
		{
			CTIR_Insn *insn = &block->code[j];
			if (insn->opcode == CTIR_debug_var)
			{
				// a "debug_var" which i did not put in; we must remove it unless it refers
				// to a variable which we are not optimising out
				int subject = insn->ops[1].regNo;
				if (!finfo->varInfo[subject].needsMemory)
				{
					insn->opcode = CTIR_nop;
					insn->numOps = 0;
					free(insn->ops);
					insn->ops = NULL;
				};
			}
			else if (insn->opcode == CTIR_alloc)
			{
				if (!finfo->varInfo[insn->resultReg].needsMemory)
				{
					insn->opcode = CTIR_nop;
					insn->numOps = 0;
					free(insn->ops);
					insn->ops = NULL;
					insn->resultReg = 0;
				};
			}
			else if (insn->opcode == CTIR_load)
			{
				if (insn->ops[0].kind == CTIR_KIND_REG)
				{
					int subject = insn->ops[0].regNo;
					if (finfo->varInfo[subject].isStack)
					{
						if (varTable[subject] == VT_UNDEFINED)
						{
							// well it's uninitialized, sooo undefined behaviour
							insn->opcode = CTIR_crash;
							insn->numOps = 1;
							insn->ops[0] = insn->ops[1];
						}
						else
						{
							// we have an alias
							int resultReg = insn->resultReg;

							// optimise out
							insn->opcode = CTIR_nop;
							insn->numOps = 0;
							free(insn->ops);
							insn->ops = NULL;
							insn->resultReg = 0;
							
							// set up the alias
							makeAlias(func, resultReg, varTable[subject]);
						};
					};
				};
			}
			else if (insn->opcode == CTIR_store)
			{
				if (insn->ops[0].kind == CTIR_KIND_REG)
				{
					int subject = insn->ops[0].regNo;
					if (finfo->varInfo[subject].isStack)
					{
						varTable[subject] = insn->ops[1].regNo;
						
						// if we don't know the variable name, just optimise out;
						// otherwise, turn into debug_var
						if (finfo->varInfo[subject].name == NULL)
						{
							insn->opcode = CTIR_nop;
							insn->numOps = 0;
							free(insn->ops);
							insn->ops = NULL;
							insn->resultReg = 0;
						}
						else
						{
							insn->opcode = CTIR_debug_var;
							insn->resultReg = 0;
							insn->ops[0] = ctirSymbolRef(finfo->varInfo[subject].name, 0);
						};
					};
				};
			};
		};
	};
	
	return 0;
};

int ctirOptVars(CTIR_Unit *unit)
{
	int status = -1;
	
	CTIR_Func *func;
	for (func=unit->funcs; func!=NULL; func=func->next)
	{
		if (processFunc(func) == 0) status = 0;
	};
	
	return status;
};
