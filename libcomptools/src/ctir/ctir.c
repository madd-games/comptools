/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <comptools/ctir/ctir.h>

#define	CTIR_OPCODE_DEF(name)	#name
static const char* ctirOpcodeNames[] = {
#include <comptools/ctir/ctir-opcodes.h>
};

const char* ctirGetOpcodeName(CTIR_Opcode opc)
{
	if (opc < 0 || opc >= CTIR_OPCODE_COUNT)
	{
		return "<invalid>";
	};
	
	return ctirOpcodeNames[opc];
};

CTIR_Unit* ctirNewUnit()
{
	CTIR_Unit *unit = (CTIR_Unit*) calloc(1, sizeof(CTIR_Unit));
	return unit;
};

CTIR_Comm* ctirNewComm(CTIR_Unit *unit, const char *name, size_t size, size_t align)
{
	CTIR_Comm *comm = (CTIR_Comm*) calloc(1, sizeof(CTIR_Comm));
	comm->next = unit->comms;
	unit->comms = comm;
	
	comm->name = name;
	comm->size = size;
	comm->align = align;
	
	return comm;
};

CTIR_Glob* ctirNewGlob(CTIR_Unit *unit, int vis, const char *section, const char *name, size_t align, int numData, CTIR_Data *data)
{
	CTIR_Glob *glob = (CTIR_Glob*) calloc(1, sizeof(CTIR_Glob));
	glob->next = unit->globs;
	unit->globs = glob;
	
	glob->vis = vis;
	glob->section = section;
	glob->name = name;
	glob->align = align;
	glob->numData = numData;
	glob->data = data;
	
	return glob;
};

CTIR_Func* ctirNewFunc(CTIR_Unit *unit, int vis, int attr, const char *name)
{
	CTIR_Func *func = (CTIR_Func*) calloc(1, sizeof(CTIR_Func));
	func->next = unit->funcs;
	unit->funcs = func;
	
	func->vis = vis;
	func->attr = attr;
	func->name = name;
	
	// return type 'void' by default
	func->ret.kind = CTIR_KIND_TYPESPEC;
	
	// empty argument list by default
	func->args.kind = CTIR_KIND_TUPLE;
	
	return func;
};

CTIR_Block* ctirNewBlock(CTIR_Func *func)
{
	CTIR_Block *block = (CTIR_Block*) calloc(1, sizeof(CTIR_Block));
	
	int index = func->numBlocks++;
	func->blocks = (CTIR_Block**) realloc(func->blocks, sizeof(void*) * func->numBlocks);
	func->blocks[index] = block;
	
	block->index = index;
	return block;
};

CTIR_Operand ctirConst(uint64_t val)
{
	CTIR_Operand op;
	op.kind = CTIR_KIND_CONST;
	op.constVal = val;
	return op;
};

CTIR_Operand ctirConstDouble(double val)
{
	CTIR_Operand op;
	op.kind = CTIR_KIND_CONST;
	op.doubleVal = val;
	return op;
};

CTIR_Operand ctirTypeSpec(int type)
{
	CTIR_Operand op;
	op.kind = CTIR_KIND_TYPESPEC;
	op.typeSpec = type;
	return op;
};

CTIR_Operand ctirReg(int num)
{
	assert(num != 0);
	CTIR_Operand op;
	op.kind = CTIR_KIND_REG;
	op.regNo = num;
	return op;
};

CTIR_Operand ctirBlockRef(int num)
{
	CTIR_Operand op;
	op.kind = CTIR_KIND_BLOCKREF;
	op.blockIndex = num;
	return op;
};

CTIR_Operand ctirSymbolRef(const char *name, int isGlobal)
{
	CTIR_Operand op;
	op.kind = CTIR_KIND_SYMREF;
	op.symbolName = name;
	op.isGlobal = isGlobal;
	return op;
};

CTIR_Operand ctirTuple(int count, CTIR_Operand *ops)
{
	CTIR_Operand op;
	op.kind = CTIR_KIND_TUPLE;
	op.numSubs = count;
	op.subs = ops;
	return op;
};

CTIR_Operand ctirTmpLabel(const char *lbl)
{
	CTIR_Operand op;
	op.kind = CTIR_KIND_TMPLABEL;
	op.symbolName = lbl;
	return op;
};

CTIR_Operand* ctirOperandList(int count, ...)
{
	CTIR_Operand *list = (CTIR_Operand*) malloc(sizeof(CTIR_Operand) * count);
	
	va_list ap;
	va_start(ap, count);
	
	int i;
	for (i=0; i<count; i++)
	{
		list[i] = va_arg(ap, CTIR_Operand);
	};
	
	va_end(ap);
	return list;
};

void ctirInsnBuffer(int *counter, CTIR_Insn **buffer, Token *tok, int resultReg, int opcode, int numOps, CTIR_Operand *ops)
{
	int index = (*counter)++;
	(*buffer) = (CTIR_Insn*) realloc(*buffer, sizeof(CTIR_Insn) * (*counter));
	memset(&(*buffer)[index], 0, sizeof(CTIR_Insn));
	
	(*buffer)[index].tok = tok;
	(*buffer)[index].resultReg = resultReg;
	(*buffer)[index].opcode = opcode;
	(*buffer)[index].numOps = numOps;
	(*buffer)[index].ops = ops;
};

void ctirAppendBuffer(int *counter, CTIR_Insn **buffer, int numCode, CTIR_Insn *code)
{
	int index = (*counter);
	(*counter) += numCode;
	(*buffer) = (CTIR_Insn*) realloc(*buffer, sizeof(CTIR_Insn) * (*counter));
	memcpy(&(*buffer)[index], code, sizeof(CTIR_Insn) * numCode);
};

void ctirInsn(CTIR_Block *block, Token *tok, int resultReg, int opcode, int numOps, CTIR_Operand *ops)
{
	ctirInsnBuffer(&block->numCode, &block->code, tok, resultReg, opcode, numOps, ops);
};

void ctirAppend(CTIR_Block *block, int numCode, CTIR_Insn *code)
{
	ctirAppendBuffer(&block->numCode, &block->code, numCode, code);
};

static const char *ctirGetVisName(int vis)
{
	switch (vis)
	{
	case CTIR_VIS_LOCAL:
		return "local";
	case CTIR_VIS_WEAK:
		return "weak";
	case CTIR_VIS_GLOBAL:
		return "global";
	default:
		return "??";
	};
};

static const char* ctirGetTypeName(int typespec)
{
	switch (typespec)
	{
	case CTIR_VOID:		return "void";
	case CTIR_U8:		return "u8";
	case CTIR_U16:		return "u16";
	case CTIR_U32:		return "u32";
	case CTIR_U64:		return "u64";
	case CTIR_I8:		return "i8";
	case CTIR_I16:		return "i16";
	case CTIR_I32:		return "i32";
	case CTIR_I64:		return "i64";
	case CTIR_F32:		return "f32";
	case CTIR_F64:		return "f64";
	case CTIR_STACK:	return "stack";
	default:		return "?";
	};
};

static void ctirDumpOperand(CTIR_Operand *op)
{
	int i;
	switch (op->kind)
	{
	case CTIR_KIND_CONST:
		printf("%lu", (unsigned long) op->constVal);
		break;
	case CTIR_KIND_REG:
		printf("$%d", op->regNo);
		break;
	case CTIR_KIND_TYPESPEC:
		printf("%s", ctirGetTypeName(op->typeSpec));
		break;
	case CTIR_KIND_BLOCKREF:
		printf("@%d", op->blockIndex);
		break;
	case CTIR_KIND_SYMREF:
		if (op->isGlobal) printf("&%s", op->symbolName);
		else printf("!%s", op->symbolName);
		break;
	case CTIR_KIND_TUPLE:
		printf("(");
		for (i=0; i<op->numSubs; i++)
		{
			ctirDumpOperand(&op->subs[i]);
			if (i != op->numSubs-1) printf(", ");
		};
		printf(")");
		break;
	case CTIR_KIND_TMPLABEL:
		printf("#%s", op->symbolName);
		break;
	default:
		printf("?");
		break;
	};
};

void ctirDump(CTIR_Unit *unit)
{
	printf("unit\n{\n");
	
	CTIR_Comm *comm;
	for (comm=unit->comms; comm!=NULL; comm=comm->next)
	{
		printf("\tcomm %s, size=%lu, align=%lu;\n", comm->name, (unsigned long) comm->size, (unsigned long) comm->align);
	};
	
	CTIR_Glob *glob;
	for (glob=unit->globs; glob!=NULL; glob=glob->next)
	{
		printf("\tdata %s \"%s\" %s align=%lu (\n", ctirGetVisName(glob->vis), glob->section, glob->name, (unsigned long) glob->align);
		
		int i;
		for (i=0; i<glob->numData; i++)
		{
			CTIR_Data *data = &glob->data[i];
			printf("\t\t%s(%" PRIu64 "),\n", ctirGetTypeName(data->type), data->val);
		};
		
		printf("\t);\n");
	};
	
	CTIR_Func *func;
	for (func=unit->funcs; func!=NULL; func=func->next)
	{
		printf("\t%s function %s", ctirGetVisName(func->vis), func->name);
		if (func->attr & CTIR_FA_NORETURN) printf(" noreturn");
		if (func->attr & CTIR_FA_RETVAL) printf(" retval ");
		
		ctirDumpOperand(&func->args);
		printf(" -> ");
		ctirDumpOperand(&func->ret);
		
		printf("\n\t{\n");
		
		int i;
		for (i=0; i<func->numBlocks; i++)
		{
			CTIR_Block *block = func->blocks[i];
			printf("\t\tblock %d\n", i);
			printf("\t\t{\n");
			
			int j;
			for (j=0; j<block->numCode; j++)
			{
				printf("\t\t\t");
				CTIR_Insn *insn = &block->code[j];
				if (insn->resultReg != 0) printf("$%d := ", insn->resultReg);
				
				printf("%s ", ctirGetOpcodeName(insn->opcode));
				
				int k;
				for (k=0; k<insn->numOps; k++)
				{
					CTIR_Operand *op = &insn->ops[k];
					ctirDumpOperand(op);
					
					if (k != insn->numOps-1) printf(", ");
				};
				
				printf(";	// %s:%d\n", insn->tok->filename, insn->tok->lineno);
			};
			
			printf("\t\t}\n\n");
		};
		
		printf("\t}\n\n");
	};
	
	printf("}\n");
};
