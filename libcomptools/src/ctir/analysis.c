/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/ctir/analysis.h>

static int optLevel0, optLevel1, optLevel2, optLevel3;

/**
 * List of all analysis/optimisation passes.
 */
static CTIR_Analysis passList[] = {
	{"dead-code", "If enabled, remove dead code (unreachable statements). Enabled on optimisation level 1.", 1},
	//{"opt-vars", "If enabled, try to allocate variables to registers. Enabled on optimisation level 1.", 1},
	
	// LIST TERMINATOR
	{NULL}
};

void ctirAddOptions(OptionList *optList)
{
	passList[0].apply = ctirOptDeadCode;
	//passList[1].apply = ctirOptVars;
	
	// add switches specific to each pass
	CTIR_Analysis *pass;
	for (pass=passList; pass->name!=NULL; pass++)
	{
		char yes[256];
		char no[256];
		
		sprintf(yes, "-f%s", pass->name);
		sprintf(no, "-fno-%s", pass->name);
		
		optAdd(optList, strdup(no), "", pass->help, OPT_FLAG, &pass->disable);
		optAdd(optList, strdup(yes), "", "", OPT_FLAG, &pass->enable);
	};
	
	optAdd(optList, "-O0", "",
		"Disable all optimisations.",
		OPT_FLAG, &optLevel0);
	optAdd(optList, "-O1", "",
		"Perform basic optimisations only, such as dead code elimination.",
		OPT_FLAG, &optLevel1);
	optAdd(optList, "-O2", "",
		"TODO: I don't know what this will do yet",
		OPT_FLAG, &optLevel2);
	optAdd(optList, "-O3", "",
		"TODO: I don't know what this will do yet",
		OPT_FLAG, &optLevel3);
};

void ctirOptimize(CTIR_Unit *unit)
{
	int optLevel;
	if (!optLevel0 && !optLevel1 && !optLevel2 && !optLevel3)
	{
		optLevel = 1;
	}
	else if (optLevel3)
	{
		optLevel = 3;
	}
	else if (optLevel2)
	{
		optLevel = 2;
	}
	else if (optLevel1)
	{
		optLevel = 1;
	}
	else
	{
		optLevel = 0;
	};
	
	CTIR_Analysis *pass;
	for (pass=passList; pass->name!=NULL; pass++)
	{
		if ((pass->level <= optLevel && !pass->disable) || pass->enable)
		{
			pass->apply(unit);
		};
	};
	
	// at the very end, "normalize" the code by removing nops
	CTIR_Func *func;
	for (func=unit->funcs; func!=NULL; func=func->next)
	{
		int i;
		for (i=0; i<func->numBlocks; i++)
		{
			CTIR_Block *block = func->blocks[i];
			
			int j;
			for (j=0; j<block->numCode; j++)
			{
				if (block->code[j].opcode == CTIR_nop)
				{
					memcpy(&block->code[j], &block->code[j+1], (block->numCode-j-1) * sizeof(CTIR_Insn));
					j--;
					block->numCode--;
				};
			};
		};
	};
};
