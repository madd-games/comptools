/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/ld.h>
#include <comptools/console.h>

Linker* ldCreate(const char *source, const char *filename)
{
	LDS_Script *script = ldsParse(source, filename);
	if (conGetError())
	{
		return NULL;
	};
	
	Linker *ld = (Linker*) malloc(sizeof(Linker));
	memset(ld, 0, sizeof(Linker));
	
	ld->script = script;
	
	return ld;
};

void ldSetSoname(Linker *ld, const char *soname)
{
	ld->soname = strdup(soname);
};

void ldAddInput(Linker *ld, Object *in)
{
	if (in->type == OBJTYPE_SHARED)
	{
		int index = ld->numDepends++;
		ld->depends = (Object**) realloc(ld->depends, sizeof(void*) * ld->numDepends);
		ld->depends[index] = in;
		return;
	};
	
	int index = ld->numInputs++;
	ld->inputs = (Object**) realloc(ld->inputs, sizeof(void*) * ld->numInputs);
	ld->inputs[index] = in;
};

void ldOutputType(Linker *ld, int type)
{
	ld->outType = type;
};

static uint64_t ldEvalPrimary(Linker *ld, Section *sect, LDS_PrimaryExpr *expr)
{
	if (expr->value != NULL)
	{
		return strtoul(expr->value, NULL, 0);
	}
	else if (expr->symname != NULL)
	{
		if (strcmp(expr->symname, ".") == 0)
		{
			if (sect != NULL)
			{
				return sect->addr + sect->size;
			}
			else
			{
				return ld->pos;
			};
		};
		
		Symbol *sym = objGetSymbol(ld->out, expr->symname, 0);
		if (sym == NULL)
		{
			lexDiag(expr->srctok, CON_ERROR, "undefined symbol `%s'", expr->symname);
			return 0;
		};
		
		uint64_t value = sym->value;
		if (sym->sect != NULL) value += sym->sect->addr;
		return value;
	}
	else
	{
		lexDiag(expr->srctok, CON_ERROR, "unrecognised primary expression; INTERNAL BUG.");
		return 0;
	};
};

static uint64_t ldEvalSum(Linker *ld, Section *sect, LDS_Sum *sum)
{
	uint64_t a = ldEvalPrimary(ld, sect, sum->expr);
	
	if (sum->next != NULL)
	{
		uint64_t b = ldEvalSum(ld, sect, sum->next);
		switch (sum->op)
		{
		case TOK_LDS_ADD:
			a += b;
			break;
		default:
			a -= b;
			break;
		};
	};
	
	return a;
};

static void ldLoadSection(Linker *ld, Section *out, const char *name, Token *srctok)
{
	int i;
	for (i=0; i<ld->numInputs; i++)
	{
		Object *src = ld->inputs[i];
		Section *sect = objGetSection(src, name);
		if (sect == NULL) continue;
		
		if (sect->type != out->type)
		{
			lexDiag(srctok, CON_ERROR, "type mismatch for section `%s'", name);
			break;
		};
		
		if (sect->loaded)
		{
			lexDiag(srctok, CON_ERROR, "input section `%s' already loaded", name);
			break;
		};
		
		// align the output
		if (out->align < sect->align)
		{
			out->align = sect->align;
		};
		
		char zeroes[sect->align];
		memset(zeroes, 0, sect->align);
		unsigned long bytesLeft = (sect->align - (out->size % sect->align)) % sect->align;

		if (out->type == SECTYPE_PROGBITS)
		{
			objSectionAppend(out, zeroes, bytesLeft);
		}
		else
		{
			objSectionResv(out, bytesLeft);
		};

		sect->loaded = 1;
		uint64_t offset = out->size;
		sect->outsect = out;
		sect->outoff = offset;

		// append the section data to the output
		if (out->type == SECTYPE_PROGBITS)
		{
			objSectionAppend(out, sect->data, sect->size);
		}
		else
		{
			objSectionResv(out, sect->size);
		};
		
		// copy global symbols of the section into the new object
		Symbol *insym;
		for (insym=src->syms; insym!=NULL; insym=insym->next)
		{
			if (insym->binding == SYMB_GLOBAL && insym->sect == sect)
			{
				switch (insym->type)
				{
				case SYMT_NONE:
				case SYMT_OBJECT:
				case SYMT_FUNC:
					{
						Symbol *newsym = objGetSymbol(ld->out,
									insym->name, 1);
						if (newsym->type != SYMT_UNDEF
							&& newsym->type != SYMT_COMMON)
						{
							lexDiag(srctok, CON_ERROR,
								"multiple definition of symbol `%s'",
								insym->name);
							break;
						}
						else
						{
							newsym->type = insym->type;
							newsym->binding = SYMB_GLOBAL;
							newsym->sect = out;
							newsym->value = offset + insym->value;
							if (insym->size > newsym->size) newsym->size = insym->size;
							if (insym->align > newsym->align) newsym->align = insym->align;
						};
					}
				default:
					break;
				};
			};
		};
		
		// copy relocations from the input section to the output section,
		// but make sure they reference the correct symbol
		Reloc *inrel;
		for (inrel=sect->relocs; inrel!=NULL; inrel=inrel->next)
		{
			Reloc *newrel = (Reloc*) malloc(sizeof(Reloc));
			memset(newrel, 0, sizeof(Reloc));
			newrel->type = inrel->type;
			newrel->sect = out;
			newrel->offset = inrel->offset + offset;
			
			if (inrel->sym->type == SYMT_SECTION)
			{
				// gets transformed later
				newrel->sym = inrel->sym;
				newrel->addend = inrel->addend;
			}
			else
			{
				newrel->sym = objGetSymbol(ld->out, inrel->sym->name, 1);
				newrel->addend = inrel->addend;
			};
			
			if (inrel->prop != NULL) newrel->prop = strdup(inrel->prop);
			
			newrel->next = out->relocs;
			out->relocs = newrel;
		};
		
		// copy annotations from the input section to the output section,
		// but make sure they have the correct address
		Annot *anin;
		for (anin=sect->annot; anin!=NULL; anin=anin->next)
		{
			Annot *anout = objAddAnnot(out, anin->name);
			anout->sectOffset = anin->sectOffset + 1;
			anout->name = strdup(anin->name);
			anout->size = anin->size;
			anout->children = anin->children;
		};
	};
};

static void ldRelocDiag(Reloc *reloc, int level, const char *format, ...)
{
	// TODO: metadata about relocation sources
	va_list ap;
	va_start(ap, format);
	conDiagAP("??", 0, 0, 0, level, format, ap);
	va_end(ap);
};

Object* ldRun(Linker *ld, ObjDriver* drv)
{
	const char *entryName = NULL;
	Token *entryToken = NULL;
	
	Object *out = objNew(drv);
	if (out == NULL)
	{
		fprintf(stderr, "ld: failed to create output object\n");
		return NULL;
	};
	
	out->depends = ld->depends;
	out->numDepends = ld->numDepends;
	
	if (ld->soname != NULL) objSetSoname(out, ld->soname);
	ld->out = out;
	
	// import common symbols
	int i;
	for (i=0; i<ld->numInputs; i++)
	{
		Object *in = ld->inputs[i];
		Symbol *sym;
		for (sym=in->syms; sym!=NULL; sym=sym->next)
		{
			if (sym->type == SYMT_COMMON)
			{
				Symbol *newsym = objGetSymbol(out, sym->name, 1);
				if (newsym->type != SYMT_COMMON)
				{
					newsym->type = SYMT_COMMON;
					newsym->binding = SYMB_GLOBAL;
					newsym->size = sym->size;
					newsym->align = sym->align;
				};
			};
		};
	};
	
	LDS_Script *script;
	for (script=ld->script; script!=NULL; script=script->next)
	{
		LDS_TopLevelStatement *st = script->st;
		if (st->assign != NULL)
		{
			LDS_Assignment *assign = st->assign;
			uint64_t sum = ldEvalSum(ld, NULL, assign->sum);
			
			if (strcmp(assign->name, ".") == 0)
			{
				ld->pos = sum;
			}
			else if (objGetSymbol(ld->out, assign->name, 0) != NULL)
			{
				lexDiag(st->srctok, CON_ERROR, "multiple definition of symbol `%s'",
					assign->name);
			}
			else
			{
				Symbol *sym = (Symbol*) malloc(sizeof(Symbol));
				memset(sym, 0, sizeof(Symbol));
				
				sym->name = strdup(assign->name);
				sym->type = SYMT_NONE;
				sym->binding = SYMB_GLOBAL;
				sym->value = sum;
				
				sym->next = ld->out->syms;
				ld->out->syms = sym;
			};
		}
		else if (st->secdef != NULL)
		{
			LDS_SectionDef *secdef = st->secdef;
			Section *sect = objGetSection(out, secdef->name);
			if (sect != NULL)
			{
				lexDiag(secdef->nametok, CON_ERROR, "output section `%s' already exists",
					secdef->name);
				continue;
			};
			
			sect = objCreateSection(out, secdef->name, secdef->type, secdef->flags);
			if (sect == NULL)
			{
				lexDiag(secdef->srctok, CON_ERROR, "failed to create section");
				continue;
			};
			
			sect->addr = ld->pos;
			
			LDS_SectionBody *body;
			for (body=secdef->body; body!=NULL; body=body->next)
			{
				LDS_SectionStatement *st = body->st;
				if (st->assign != NULL)
				{
					LDS_Assignment *assign = st->assign;
					uint64_t sum = ldEvalSum(ld, sect, assign->sum);
					
					if (strcmp(assign->name, ".") == 0)
					{
						lexDiag(st->srctok, CON_ERROR,
							"cannot move `.' inside a section body");
					}
					else if (objGetSymbol(ld->out, assign->name, 0) != NULL)
					{
						lexDiag(st->srctok, CON_ERROR,
							"multiple definition of symbol `%s'",
							assign->name);
					}
					else
					{
						Symbol *sym = (Symbol*) malloc(sizeof(Symbol));
						memset(sym, 0, sizeof(Symbol));
						
						sym->name = strdup(assign->name);
						sym->type = SYMT_NONE;
						sym->binding = SYMB_GLOBAL;
						sym->sect = sect;
						sym->value = sum - sect->addr;
						
						sym->next = ld->out->syms;
						ld->out->syms = sym;
					};
				}
				else if (st->load != NULL)
				{
					const char *name = st->load->name;
					ldLoadSection(ld, sect, name, st->srctok->next);
				}
				else
				{
					lexDiag(st->srctok, CON_ERROR,
						"unrecognised statement; INTERNAL BUG");
				};
			};
			
			ld->pos = sect->addr + sect->size;
		}
		else if (st->entry != NULL)
		{
			entryName = st->entry->symname;
			entryToken = st->entry->nametok;
		}
		else if (st->align != NULL)
		{
			uint64_t align = strtoul(st->align->value, NULL, 0);
			if ((!align) || (align & (align-1)))
			{
				lexDiag(st->align->srctok, CON_ERROR,
					"alignment value must be a power of 2");
			}
			else
			{
				ld->pos = (ld->pos + align - 1) & ~(align-1);
			};
		}
		else
		{
			lexDiag(st->srctok, CON_ERROR, "unrecognised statement: INTERNAL BUG");
			return NULL;
		};
	};
	
	if (conGetError()) return NULL;
	
	// transform relocations against input sections
	Section *sect;
	for (sect=out->sects; sect!=NULL; sect=sect->next)
	{
		Reloc *reloc;
		for (reloc=sect->relocs; reloc!=NULL; reloc=reloc->next)
		{
			if (reloc->sym->type == SYMT_SECTION)
			{
				Section *insect = reloc->sym->sect;
				if (!insect->loaded)
				{
					ldRelocDiag(reloc, CON_ERROR,
						"relocation against unloaded input section `%s'",
						insect->name);
				}
				else
				{
					reloc->sym = insect->outsect->sym;
					reloc->addend += insect->outoff;
				};
			};
		};
	};
	
	if (conGetError()) return NULL;
	
#if 0
	Symbol *sym;
	for (sym=ld->out->syms; sym!=NULL; sym=sym->next)
	{
		uint64_t base = 0;
		if (sym->sect != NULL) base = sym->sect->addr;
		printf("Symbol %s: 0x%016lX + 0x%016lX\n", sym->name, base, sym->value);
	};
	
	Section *sect;
	for (sect=out->sects; sect!=NULL; sect=sect->next)
	{
		printf("Section %s:\n", sect->name);
		
		Reloc *reloc;
		for (reloc=sect->relocs; reloc!=NULL; reloc=reloc->next)
		{
			printf("\t%s against %s+%d\n", reloc->type->name, reloc->sym->name,
					(int) reloc->addend);
		};
	};
#endif

	if (ld->outType == OBJTYPE_RELOC)
	{
		// that's it; we don't need anything more for a relocatable object
		return out;
	};
	
	// set the correct object type
	out->type = ld->outType;
	
	// merge common symbols into BSS if not yet defined
	Section *bss = objGetSection(out, ".bss");
	if (bss == NULL)
	{
		bss = objCreateSection(out, ".bss", SECTYPE_NOBITS, SEC_READ | SEC_WRITE);
	};
	
	if (bss == NULL || bss->type != SECTYPE_NOBITS)
	{
		fprintf(stderr, "ld: failed to find or create `.bss' section as NOBITS\n");
		return NULL;
	};
	
	Symbol *sym;
	for (sym=out->syms; sym!=NULL; sym=sym->next)
	{
		if (sym->type == SYMT_COMMON)
		{
			// leave the type as SYMT_COMMON as this may be needed by the preReloc()
			// callback, for example to generate R_X86_64_COPY relocations for ELF64;
			// but set the section and offset as appropriate
			size_t padding = bss->size % sym->align;
			objSectionResv(bss, padding);
			
			sym->sect = bss;
			sym->value = bss->size;
			objSectionResv(bss, sym->size);
		};
	};

	// set the entry symbol
	if (entryName != NULL)
	{
		Symbol *sym = objGetSymbol(out, entryName, 0);
		if (sym == NULL)
		{
			lexDiag(entryToken, CON_ERROR, "cannot find entry symbol `%s'",
				entryName);
			return NULL;
		};
		
		out->entry = sym;
	};

	// let the object driver do whatever it needs before we apply relocations
	if (out->driver->preReloc != NULL)
	{
		Error err = out->driver->preReloc(out);
		if (err != ERR_OK)
		{
			fprintf(stderr, "ld: preReloc failed: 0x%04X\n", err);
			return NULL;
		};
	};
	
	// perform relocations
	for (sect=out->sects; sect!=NULL; sect=sect->next)
	{
		Reloc *reloc;
		for (reloc=sect->relocs; reloc!=NULL; reloc=reloc->next)
		{
			if (reloc->sym->type == SYMT_UNDEF)
			{
				ldRelocDiag(reloc, CON_ERROR, "undefined reference to `%s'",
					reloc->sym->name);
			}
			else
			{
				uint64_t symaddr = reloc->sym->value;
				if (reloc->sym->sect != NULL) symaddr += reloc->sym->sect->addr;
				
				Error err = reloc->type->resolve(
					(char*) sect->data + reloc->offset,
					symaddr, reloc->addend, reloc);
				if (err == ERR_OK)
				{
					continue;
				}
				else if (err == ERR_RELOC_TRUNC)
				{
					ldRelocDiag(reloc, CON_ERROR,
						"truncated relocation `%s' against %s+%d",
						reloc->type->name, reloc->sym->name, (int) reloc->addend);
				}
				else
				{
					ldRelocDiag(reloc, CON_ERROR,
						"unknown error with relocation `%s' against %s+%d",
						reloc->type->name, reloc->sym->name, (int) reloc->addend);
				};
			};
		};
	};
	
	if (conGetError()) return NULL;
	
	// let the object driver do whatever it needs after we've applied relocations
	if (out->driver->postReloc != NULL)
	{
		Error err = out->driver->postReloc(out);
		if (err != ERR_OK)
		{
			fprintf(stderr, "ld: postReloc failed: 0x%04X\n", err);
			return NULL;
		};
	};
	
	// thanks
	return out;
};
