/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <comptools/types.h>

#define	MAKER_BE(outType, funcName, inType) \
	outType funcName(inType val) \
	{\
		outType out;\
		ctMakeBE(val, &out, sizeof(outType));\
		return out;\
	};

#define	MAKER_LE(outType, funcName, inType) \
	outType funcName(inType val) \
	{\
		outType out;\
		ctMakeLE(val, &out, sizeof(outType));\
		return out;\
	};

#define	GETTER_BE(outType, funcName, inType) \
	outType funcName(inType val) \
	{\
		return ctGetBE(&val, sizeof(outType));\
	};

#define	GETTER_LE(outType, funcName, inType) \
	outType funcName(inType val) \
	{\
		return ctGetLE(&val, sizeof(outType));\
	};

void ctMakeLE(uint64_t value, void *buffer, int count)
{
	uint8_t *put = (uint8_t*) buffer;
	
	while (count--)
	{
		*put++ = (value & 0xFF);
		value >>= 8;
	};
};

void ctMakeBE(uint64_t value, void *buffer, int count)
{
	uint8_t *put = (uint8_t*) buffer;
	int bitPos = (8 * count)-8;
	
	while (count--)
	{
		*put++ = (value >> bitPos);
		value <<= 8;
	};
};

uint64_t ctGetLE(const void *buffer, int count)
{
	uint64_t result = 0;
	const uint8_t *scan = (const uint8_t*) buffer;
	
	int shift = 0;
	while (count--)
	{
		result = result | ((uint64_t)(*scan++) << shift);
		shift += 8;
	};
	
	return result;
};

uint64_t ctGetBE(const void *buffer, int count)
{
	uint64_t result = 0;
	const uint8_t *scan = (const uint8_t*) buffer;
	
	while (count--)
	{
		result = (result << 8) | (*scan++);
	};
	
	return result;
};

MAKER_LE(ule8_t, ule8, uint8_t);
MAKER_LE(ule16_t, ule16, uint16_t);
MAKER_LE(ule32_t, ule32, uint32_t);
MAKER_LE(ule64_t, ule64, uint64_t);

MAKER_LE(sle8_t, sle8, int8_t);
MAKER_LE(sle16_t, sle16, int16_t);
MAKER_LE(sle32_t, sle32, int32_t);
MAKER_LE(sle64_t, sle64, int64_t);

MAKER_BE(ube8_t, ube8, uint8_t);
MAKER_BE(ube16_t, ube16, uint16_t);
MAKER_BE(ube32_t, ube32, uint32_t);
MAKER_BE(ube64_t, ube64, uint64_t);

MAKER_BE(sbe8_t, sbe8, int8_t);
MAKER_BE(sbe16_t, sbe16, int16_t);
MAKER_BE(sbe32_t, sbe32, int32_t);
MAKER_BE(sbe64_t, sbe64, int64_t);

GETTER_LE(uint8_t, ule8_get, ule8_t);
GETTER_LE(uint16_t, ule16_get, ule16_t);
GETTER_LE(uint32_t, ule32_get, ule32_t);
GETTER_LE(uint64_t, ule64_get, ule64_t);

GETTER_LE(int8_t, sle8_get, sle8_t);
GETTER_LE(int16_t, sle16_get, sle16_t);
GETTER_LE(int32_t, sle32_get, sle32_t);
GETTER_LE(int64_t, sle64_get, sle64_t);

GETTER_BE(uint8_t, ube8_get, ube8_t);
GETTER_BE(uint16_t, ube16_get, ube16_t);
GETTER_BE(uint32_t, ube32_get, ube32_t);
GETTER_BE(uint64_t, ube64_get, ube64_t);

GETTER_BE(int8_t, sbe8_get, sbe8_t);
GETTER_BE(int16_t, sbe16_get, sbe16_t);
GETTER_BE(int32_t, sbe32_get, sbe32_t);
GETTER_BE(int64_t, sbe64_get, sbe64_t);
