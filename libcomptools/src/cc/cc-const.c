/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/cc/cc-const.h>
#include <comptools/cc/cc-ast.h>

static CC_Const ccEvalCastExpression(CC_Compiler *cc, CC_CastExpression *expr);

int ccIsTrue(CC_Const value)
{
	switch (value.type)
	{
	case CC_CONST_INT:		return value.i;
	case CC_CONST_FLOAT:		return (int) value.f;
	case CC_CONST_STRING:		return 1;
	default:			return 0;
	};
};

static CC_Const ccEvalPrimaryExpression(CC_Compiler *cc, CC_PrimaryExpression *primary)
{
	char c;
	
	if (primary->expr != NULL)
	{
		return ccEvalExpression(cc, primary->expr);
	}
	else if (primary->generic != NULL)
	{
		// TODO
		CC_Const err;
		err.type = CC_CONST_ERROR;
		err.tok = primary->generic->tok;
		err.err = "feature not yet implemented";
		return err;
	}
	else if (primary->tok != NULL)
	{
		CC_Const result;
		char *buf;
		
		switch (primary->tok->type)
		{
		case TOK_CC_ID:
			if (hashtabHasKey(cc->unit->enums, primary->tok->value))
			{
				CC_EnumVal *eval = (CC_EnumVal*) hashtabGet(cc->unit->enums, primary->tok->value);
				result.type = CC_CONST_INT;
				result.tok = primary->tok;
				result.i = eval->value;
			}
			else
			{
				result.type = CC_CONST_ERROR;
				result.tok = primary->tok;
				result.err = "cannot evaluate identifier at compile-time";
			};
			break;
		case TOK_CC_ICONST:
			result.type = CC_CONST_INT;
			result.tok = primary->tok;
			result.i = strtoul(primary->tok->value, NULL, 0);
			break;
		case TOK_CC_FCONST:
			result.type = CC_CONST_FLOAT;
			result.tok = primary->tok;
			sscanf(primary->tok->value, "%lf", &result.f);
			break;
		case TOK_CC_STRING:
			result.type = CC_CONST_STRING;
			result.tok = primary->tok;
			buf = (char*) malloc(strlen(primary->tok->value)+1);
			result.str = buf;
			c = lexParseString(primary->tok->value, buf);
			if (c != 0)
			{
				const char *msg = "unrecognised escape sequence: `\\%c'";
				free(buf);
				buf = (char*) malloc(strlen(msg)+1);
				result.type = CC_CONST_ERROR;
				result.tok = primary->tok;
				result.err = buf;
				sprintf(buf, msg, c);
			};
			break;
		default:
			// ?
			return (CC_Const) {};
		};
		return result;
	}
	else
	{
		// ?
		return (CC_Const) {};
	};
};

static CC_Const ccEvalPostfixExpression(CC_Compiler *cc, CC_PostfixExpression *postfix)
{
	if (postfix->primary == NULL)
	{
		// TODO
		CC_Const err;
		err.type = CC_CONST_ERROR;
		err.tok = postfix->typeName->tok;
		err.err = "feature not yet implemented";
		return err;
	}
	else
	{
		if (postfix->tail != NULL)
		{
			CC_Const err;
			err.type = CC_CONST_ERROR;
			err.tok = postfix->tail->tok;
			err.err = "operator with side effects cannot be used in constant expression";
			return err;
		};
		
		return ccEvalPrimaryExpression(cc, postfix->primary);
	};
};

static CC_Const ccEvalUnaryExpression(CC_Compiler *cc, CC_UnaryExpression *unary)
{
	if (unary->op == NULL)
	{
		return ccEvalPostfixExpression(cc, unary->postfix);
	};

	
	CC_Type *type = NULL;
	if (unary->typeName != NULL)
	{
		type = ccCompileTypeName(cc, unary->typeName);
		if (type == NULL)
		{
			CC_Const err;
			err.type = CC_CONST_ERROR;
			err.tok = unary->typeName->tok;
			err.err = "invalid type specification";
			return err;
		};
	};

	CC_Const right;
	CC_Const result;
	switch (unary->op->type)
	{
	case TOK_CC_INC:
	case TOK_CC_DEC:
		result.type = CC_CONST_ERROR;
		result.tok = unary->op;
		result.err = "operation with side effects not permitted in constant expression";
		break;
	case TOK_CC_BAND:
	case TOK_CC_AST:
		result.type = CC_CONST_ERROR;
		result.tok = unary->op;
		result.err = "operator cannot be evaluted in constant expression";
		break;
	case TOK_CC_PLUS:
		result = ccEvalCastExpression(cc, unary->cast);
		if (result.type != CC_CONST_INT && result.type != CC_CONST_FLOAT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = unary->op;
			result.err = "operand to unary `+' must have arithmetic type";
		};
		break;
	case TOK_CC_MINUS:
		result = ccEvalCastExpression(cc, unary->cast);
		if (result.type == CC_CONST_INT)
		{
			result.i = -result.i;
		}
		else if (result.type == CC_CONST_FLOAT)
		{
			result.f = -result.f;
		}
		else
		{
			result.type = CC_CONST_ERROR;
			result.tok = unary->op;
			result.err = "operand to unary `-' must have arithmetic type";
		};
		break;
	case TOK_CC_TILDE:
		result = ccEvalCastExpression(cc, unary->cast);
		if (result.type == CC_CONST_INT)
		{
			result.i = ~result.i;
		}
		else
		{
			result.type = CC_CONST_ERROR;
			result.tok = unary->op;
			result.err = "operand to unary `~' must have integer type";
		};
		break;
	case TOK_CC_EXCL:
		right = ccEvalCastExpression(cc, unary->cast);
		result.type = CC_CONST_INT;
		result.tok = unary->op;
		result.i = !ccIsTrue(right);
		break;
	case TOK_CC_SIZEOF:
		if (unary->unary != NULL)
		{
			right = ccEvalUnaryExpression(cc, unary->unary);
			result.type = CC_CONST_INT;
			result.tok = unary->op;
			switch (right.type)
			{
			case CC_CONST_ERROR:
				return right;
			case CC_CONST_INT:
				result.i = cc->dataModel->sizeofInt;
			case CC_CONST_FLOAT:
				result.i = cc->dataModel->sizeofFloat;
			case CC_CONST_STRING:
				result.i = cc->dataModel->sizeofPtr;
			default:
				result.i = 0;
			};
		}
		else
		{
			size_t sz = ccGetTypeSize(cc, type);
			if (sz == 0)
			{
				result.type = CC_CONST_ERROR;
				result.tok = unary->op;
				result.err = "the specified type does not currently have a known size";
			}
			else
			{
				result.type = CC_CONST_INT;
				result.tok = unary->op;
				result.i = sz;
			};
		};
		break;
	case TOK_CC_ALIGNOF:
		{
			size_t sz = ccGetTypeAlign(cc, type);
			if (sz == 0)
			{
				result.type = CC_CONST_ERROR;
				result.tok = unary->op;
				result.err = "the specified type does not current have a known alignment";
			}
			else
			{
				result.type = CC_CONST_INT;
				result.tok = unary->op;
				result.i = sz;
			};
		};
		break;
	};
	
	return result;
};

static CC_Const ccEvalCastExpression(CC_Compiler *cc, CC_CastExpression *expr)
{
	if (expr->unary != NULL)
	{
		return ccEvalUnaryExpression(cc, expr->unary);
	}
	else
	{
		CC_Type *type = ccCompileTypeName(cc, expr->typeName);
		if (type == NULL)
		{
			CC_Const err;
			err.type = CC_CONST_ERROR;
			err.tok = expr->typeName->tok;
			err.err = "invalid type specification";
			return err;
		};
		
		CC_Const right = ccEvalCastExpression(cc, expr->cast);
		if (right.type == CC_CONST_ERROR) return right;
		
		if (right.type == CC_CONST_STRING)
		{
			CC_Const err;
			err.type = CC_CONST_ERROR;
			err.tok = expr->typeName->tok;
			err.err = "cannot cast strings in constant expressions";
			return err;
		};
		
		CC_Const result;
		
		switch (type->kind)
		{
		case CC_TYPE_VOID:
		case CC_TYPE_FLOAT_COMPLEX:
		case CC_TYPE_DOUBLE_COMPLEX:
		case CC_TYPE_LONG_DOUBLE_COMPLEX:
		case CC_TYPE_STRUCT_OR_UNION:
		case CC_TYPE_ARRAY:
		case CC_TYPE_FUNC:
			result.type = CC_CONST_ERROR;
			result.tok = expr->typeName->tok;
			result.err = "cast not possible in constant expression";
			break;
		case CC_TYPE_CHAR:
		case CC_TYPE_SIGNED_CHAR:
		case CC_TYPE_UNSIGNED_CHAR:
		case CC_TYPE_SIGNED_SHORT:
		case CC_TYPE_UNSIGNED_SHORT:
		case CC_TYPE_SIGNED_INT:
		case CC_TYPE_UNSIGNED_INT:
		case CC_TYPE_SIGNED_LONG:
		case CC_TYPE_UNSIGNED_LONG:
		case CC_TYPE_SIGNED_LONG_LONG:
		case CC_TYPE_UNSIGNED_LONG_LONG:
		case CC_TYPE_BOOL:
		case CC_TYPE_PTR:
			result.type = CC_CONST_INT;
			result.tok = expr->typeName->tok;
			if (right.type == CC_CONST_INT) result.i = right.i;
			else result.i = (uint64_t) right.f;
			break;
		case CC_TYPE_FLOAT:
		case CC_TYPE_DOUBLE:
		case CC_TYPE_LONG_DOUBLE:
			result.type = CC_CONST_FLOAT;
			result.tok = expr->typeName->tok;
			if (right.type == CC_CONST_FLOAT) result.f = right.f;
			else result.f = (float) right.i;
			break;
		};
		
		return result;
	};
};

static CC_Const ccEvalMultiplicativeExpression(CC_Compiler *cc, CC_MultiplicativeExpression *expr)
{
	CC_Const result = ccEvalCastExpression(cc, expr->cast);
	if (result.type == CC_CONST_ERROR) return result;
	
	Token *op = expr->op;
	for (expr=expr->next; expr!=NULL; expr=expr->next)
	{
		if (result.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "left operand to additive operator is not an integer";
			return result;
		};
		
		CC_Const second = ccEvalCastExpression(cc, expr->cast);
		if (second.type == CC_CONST_ERROR) return second;
		
		if (second.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "right operand to additive operator is not an integer";
			return result;
		};
		
		result.tok = op;
		switch (op->type)
		{
		case TOK_CC_AST:
			result.i *= second.i;
			break;
		case TOK_CC_SLASH:
			if (result.i == 0)
			{
				result.type = CC_CONST_ERROR;
				result.tok = op;
				result.err = "division by zero";
				return result;
			};
			result.i /= second.i;
			break;
		case TOK_CC_MODULO:
			if (result.i == 0)
			{
				result.type = CC_CONST_ERROR;
				result.tok = op;
				result.err = "division by zero";
				return result;
			};
			result.i %= second.i;
			break;
		};
	};
	
	return result;
};

static CC_Const ccEvalAdditiveExpression(CC_Compiler *cc, CC_AdditiveExpression *expr)
{
	CC_Const result = ccEvalMultiplicativeExpression(cc, expr->mul);
	if (result.type == CC_CONST_ERROR) return result;
	
	Token *op = expr->op;
	for (expr=expr->next; expr!=NULL; expr=expr->next)
	{
		if (result.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "left operand to additive operator is not an integer";
			return result;
		};
		
		CC_Const second = ccEvalMultiplicativeExpression(cc, expr->mul);
		if (second.type == CC_CONST_ERROR) return second;
		
		if (second.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "right operand to additive operator is not an integer";
			return result;
		};
		
		result.tok = op;
		switch (op->type)
		{
		case TOK_CC_PLUS:
			result.i += second.i;
			break;
		case TOK_CC_MINUS:
			result.i -= second.i;
			break;
		};
	};
	
	return result;
};

static CC_Const ccEvalShiftExpression(CC_Compiler *cc, CC_ShiftExpression *expr)
{
	CC_Const result = ccEvalAdditiveExpression(cc, expr->add);
	if (result.type == CC_CONST_ERROR) return result;
	
	Token *op = expr->op;
	for (expr=expr->next; expr!=NULL; expr=expr->next)
	{
		if (result.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "left operand to shift is not an integer";
			return result;
		};
		
		CC_Const second = ccEvalAdditiveExpression(cc, expr->add);
		if (second.type == CC_CONST_ERROR) return second;
		
		if (second.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "right operand to shift is not an integer";
			return result;
		};
		
		if (second.i < 0 || second.i >= 64)
		{
			result.type = CC_CONST_ERROR;
			result.tok = second.tok;
			result.err = "right operand to shift out of range";
			return result;
		};
		
		result.tok = op;
		switch (op->type)
		{
		case TOK_CC_LARROW:
			result.i <<= second.i;
			break;
		case TOK_CC_RARROW:
			result.i >>= second.i;
			break;
		};
	};
	
	return result;
};

static CC_Const ccEvalRelationalExpression(CC_Compiler *cc, CC_RelationalExpression *expr)
{
	CC_Const result = ccEvalShiftExpression(cc, expr->shift);
	if (result.type == CC_CONST_ERROR) return result;
	
	Token *op = expr->op;
	for (expr=expr->next; expr!=NULL; expr=expr->next)
	{
		if (result.type != CC_CONST_INT && result.type != CC_CONST_FLOAT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "relational operators require integer or floating-point types";
			return result;
		};
		
		CC_Const second = ccEvalShiftExpression(cc, expr->shift);
		if (second.type == CC_CONST_ERROR) return second;
		
		if (second.type != result.type)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "relational operator operand type mismatch";
			return result;
		};
		
		result.tok = op;
		
		if (result.type == CC_CONST_INT)
		{
			switch (op->type)
			{
			case TOK_CC_LARROW:
				result.i = (result.i < second.i);
				break;
			case TOK_CC_LARROW_EQ:
				result.i = (result.i <= second.i);
				break;
			case TOK_CC_RARROW:
				result.i = (result.i > second.i);
				break;
			case TOK_CC_RARROW_EQ:
				result.i = (result.i >= second.i);
				break;
			};
		}
		else
		{
			switch (op->type)
			{
			case TOK_CC_LARROW:
				result.i = (result.f < second.f);
				break;
			case TOK_CC_LARROW_EQ:
				result.i = (result.f <= second.f);
				break;
			case TOK_CC_RARROW:
				result.i = (result.f > second.f);
				break;
			case TOK_CC_RARROW_EQ:
				result.i = (result.f >= second.f);
				break;
			};
		};
		
		result.type = CC_CONST_INT;
	};
	
	return result;
};

static CC_Const ccEvalEqualityExpression(CC_Compiler *cc, CC_EqualityExpression *expr)
{
	CC_Const result = ccEvalRelationalExpression(cc, expr->rel);
	if (result.type == CC_CONST_ERROR) return result;
	
	Token *op = expr->op;
	for (expr=expr->next; expr!=NULL; expr=expr->next)
	{
		if (result.type != CC_CONST_INT && result.type != CC_CONST_FLOAT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "comparison operators require integer or floating-point types";
			return result;
		};
		
		CC_Const second = ccEvalRelationalExpression(cc, expr->rel);
		if (second.type == CC_CONST_ERROR) return second;
		
		if (second.type != result.type)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "comparison operator operand type mismatch";
			return result;
		};
		
		result.tok = op;
		
		if (result.type == CC_CONST_INT)
		{
			switch (op->type)
			{
			case TOK_CC_EQ:
				result.i = (result.i == second.i);
				break;
			case TOK_CC_NEQ:
				result.i = (result.i != second.i);
				break;
			};
		}
		else
		{
			switch (op->type)
			{
			case TOK_CC_EQ:
				result.i = (result.f == second.f);
				break;
			case TOK_CC_NEQ:
				result.i = (result.f != second.f);
				break;
			};
		};
		
		result.type = CC_CONST_INT;
	};
	
	return result;
};

static CC_Const ccEvalBitwiseANDExpression(CC_Compiler *cc, CC_BitwiseANDExpression *expr)
{
	CC_Const result = ccEvalEqualityExpression(cc, expr->equality);
	if (result.type == CC_CONST_ERROR) return result;
	
	Token *op = expr->op;
	for (expr=expr->next; expr!=NULL; expr=expr->next)
	{
		if (result.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "left operand to `&' is not an integer";
			return result;
		};
		
		CC_Const second = ccEvalEqualityExpression(cc, expr->equality);
		if (second.type == CC_CONST_ERROR) return second;
		
		if (second.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "right operand to `&' is not an integer";
			return result;
		};
		
		result.tok = op;
		result.i &= second.i;
	};
	
	return result;
};

static CC_Const ccEvalBitwiseXORExpression(CC_Compiler *cc, CC_BitwiseXORExpression *expr)
{
	CC_Const result = ccEvalBitwiseANDExpression(cc, expr->bitwiseAND);
	if (result.type == CC_CONST_ERROR) return result;
	
	Token *op = expr->op;
	for (expr=expr->next; expr!=NULL; expr=expr->next)
	{
		if (result.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "left operand to `^' is not an integer";
			return result;
		};
		
		CC_Const second = ccEvalBitwiseANDExpression(cc, expr->bitwiseAND);
		if (second.type == CC_CONST_ERROR) return second;
		
		if (second.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "right operand to `^' is not an integer";
			return result;
		};
		
		result.tok = op;
		result.i ^= second.i;
	};
	
	return result;
};

static CC_Const ccEvalBitwiseORExpression(CC_Compiler *cc, CC_BitwiseORExpression *expr)
{
	CC_Const result = ccEvalBitwiseXORExpression(cc, expr->bitwiseXOR);
	if (result.type == CC_CONST_ERROR) return result;
	
	Token *op = expr->op;
	for (expr=expr->next; expr!=NULL; expr=expr->next)
	{
		if (result.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "left operand to `|' is not an integer";
			return result;
		};
		
		CC_Const second = ccEvalBitwiseXORExpression(cc, expr->bitwiseXOR);
		if (second.type == CC_CONST_ERROR) return second;
		
		if (second.type != CC_CONST_INT)
		{
			result.type = CC_CONST_ERROR;
			result.tok = op;
			result.err = "right operand to `|' is not an integer";
			return result;
		};
		
		result.tok = op;
		result.i |= second.i;
	};
	
	return result;
};

static CC_Const ccEvalLogicalANDExpression(CC_Compiler *cc, CC_LogicalANDExpression *expr)
{
	CC_Const result = ccEvalBitwiseORExpression(cc, expr->bitwiseOR);
	if (result.type == CC_CONST_ERROR) return result;
	
	Token *op = expr->op;
	for (expr=expr->next; expr!=NULL; expr=expr->next)
	{
		CC_Const second = ccEvalBitwiseORExpression(cc, expr->bitwiseOR);
		if (second.type == CC_CONST_ERROR) return result;
		
		int truth = ccIsTrue(result) && ccIsTrue(second);
		result.type = CC_CONST_INT;
		result.tok = op;
		result.i = truth;
		
		// if this was false, it'll never stop being false
		if (!truth) break;
	};
	
	return result;
};

static CC_Const ccEvalLogicalORExpression(CC_Compiler *cc, CC_LogicalORExpression *expr)
{
	CC_Const result = ccEvalLogicalANDExpression(cc, expr->logicalAND);
	if (result.type == CC_CONST_ERROR) return result;
	
	Token *op = expr->op;
	for (expr=expr->next; expr!=NULL; expr=expr->next)
	{
		CC_Const second = ccEvalLogicalANDExpression(cc, expr->logicalAND);
		if (second.type == CC_CONST_ERROR) return result;
		
		int truth = ccIsTrue(result) || ccIsTrue(second);
		result.type = CC_CONST_INT;
		result.tok = op;
		result.i = truth;
		
		// if this was true, it'll never stop being true
		if (truth) break;
	};
	
	return result;
};

static CC_Const ccEvalConditionalExpression(CC_Compiler *cc, CC_ConditionalExpression *expr)
{
	CC_Const result = ccEvalLogicalORExpression(cc, expr->left);
	if (result.type == CC_CONST_ERROR) return result;
	if (expr->trueExpr == NULL) return result;		// no conditional
	
	if (ccIsTrue(result))
	{
		return ccEvalExpression(cc, expr->trueExpr);
	}
	else
	{
		return ccEvalConditionalExpression(cc, expr->falseExpr);
	};
};

CC_Const ccEvalConstantExpression(CC_Compiler *cc, CC_ConstantExpression *expr)
{
	return ccEvalConditionalExpression(cc, expr->cond);
};

CC_Const ccEvalAssignmentExpression(CC_Compiler *cc, CC_AssignmentExpression *expr)
{
	// assignment expressions can only be evaluated if there is no assignment
	if (expr->op != NULL)
	{
		CC_Const result;
		result.type = CC_CONST_ERROR;
		result.tok = expr->op;
		result.err = "assignment is not possible at compile-time";
		return result;
	}
	else
	{
		return ccEvalConditionalExpression(cc, expr->cond);
	};
};

CC_Const ccEvalExpression(CC_Compiler *cc, CC_Expression *expr)
{
	// for Expression, the last assignment expression is the one actually evaluated,
	// so we just ignore the rest, except that we return errors
	CC_Const result;
	for (; expr!=NULL; expr=expr->next)
	{
		result = ccEvalAssignmentExpression(cc, expr->assign);
		if (result.type == CC_CONST_ERROR) return result;
	};
	
	return result;
};
