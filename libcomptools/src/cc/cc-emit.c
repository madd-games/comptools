/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <comptools/cc/cc-emit.h>
#include <comptools/lex.h>
#include <comptools/console.h>

#define	TODO assert(1 == 0)
#define	INVALID_CODE_PATH assert(2 == 3)

#define	ASSIGN_CONST_VALUE(val, op, x) \
	val.vi8 op (int8_t) x;\
	val.vi16 op (int16_t) x;\
	val.vi32 op (int32_t) x;\
	val.vi64 op (int64_t) x;\
	val.vu8 op (uint8_t) x;\
	val.vu16 op (uint16_t) x;\
	val.vu32 op (uint32_t) x;\
	val.vu64 op (uint64_t) x;\
	val.vf op (float) x;\
	val.vd op (double) x;

static int ccGetCtirType(CC_Compiler *cc, CC_Type *type)
{
	switch (type->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_SIGNED_CHAR:
	case CC_TYPE_SIGNED_SHORT:
	case CC_TYPE_SIGNED_INT:
	case CC_TYPE_SIGNED_LONG:
	case CC_TYPE_SIGNED_LONG_LONG:
		switch (ccGetTypeSize(cc, type))
		{
		case 1:		return CTIR_I8;
		case 2:		return CTIR_I16;
		case 4:		return CTIR_I32;
		case 8:		return CTIR_I64;
		};
		INVALID_CODE_PATH;
	case CC_TYPE_UNSIGNED_CHAR:
	case CC_TYPE_UNSIGNED_SHORT:
	case CC_TYPE_UNSIGNED_INT:
	case CC_TYPE_UNSIGNED_LONG:
	case CC_TYPE_UNSIGNED_LONG_LONG:
	case CC_TYPE_BOOL:
	case CC_TYPE_PTR:
		switch (ccGetTypeSize(cc, type))
		{
		case 1:		return CTIR_U8;
		case 2:		return CTIR_U16;
		case 4:		return CTIR_U32;
		case 8:		return CTIR_U64;
		};
		INVALID_CODE_PATH;
	case CC_TYPE_FLOAT:
	case CC_TYPE_DOUBLE:
	case CC_TYPE_LONG_DOUBLE:
		switch (ccGetTypeSize(cc, type))
		{
		case 4:		return CTIR_F32;
		case 8:		return CTIR_F64;
		};
	default:
		return 0;
	};
};

static CC_Type* ccMakeBasicType(int kind)
{
	CC_Type *type = (CC_Type*) calloc(1, sizeof(CC_Type));
	type->kind = kind;
	return type;
};

static CC_Type* ccMakePtrType(CC_Type *sub)
{
	CC_Type *type = (CC_Type*) calloc(1, sizeof(CC_Type));
	type->kind = CC_TYPE_PTR;
	type->base = sub;
	return type;
};

static int ccGetIntConstOpcode(CC_FlowContext *ctx)
{
	switch (ctx->cc->dataModel->sizeofInt)
	{
	case 1:			return CTIR_i8;
	case 2:			return CTIR_i16;
	case 4:			return CTIR_i32;
	case 8:			return CTIR_i64;
	default:		INVALID_CODE_PATH;
	};

	return 0;
};

static int ccGetDoubleConstOpcode(CC_FlowContext *ctx)
{
	switch (ctx->cc->dataModel->sizeofDouble)
	{
	case 4:			return CTIR_f32;
	case 8:			return CTIR_f64;
	default:		INVALID_CODE_PATH;
	};

	return 0;
};

static int ccIsArithmeticType(CC_Type *type)
{
	switch (type->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_SIGNED_CHAR:
	case CC_TYPE_UNSIGNED_CHAR:
	case CC_TYPE_SIGNED_SHORT:
	case CC_TYPE_UNSIGNED_SHORT:
	case CC_TYPE_SIGNED_INT:
	case CC_TYPE_UNSIGNED_INT:
	case CC_TYPE_SIGNED_LONG:
	case CC_TYPE_UNSIGNED_LONG:
	case CC_TYPE_SIGNED_LONG_LONG:
	case CC_TYPE_UNSIGNED_LONG_LONG:
	case CC_TYPE_FLOAT:
	case CC_TYPE_FLOAT_COMPLEX:
	case CC_TYPE_DOUBLE:
	case CC_TYPE_DOUBLE_COMPLEX:
	case CC_TYPE_LONG_DOUBLE:
	case CC_TYPE_LONG_DOUBLE_COMPLEX:
	case CC_TYPE_BOOL:
		return 1;
	default:
		return 0;
	};
};

static int ccIsRealType(CC_Type *type)
{
	switch (type->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_SIGNED_CHAR:
	case CC_TYPE_UNSIGNED_CHAR:
	case CC_TYPE_SIGNED_SHORT:
	case CC_TYPE_UNSIGNED_SHORT:
	case CC_TYPE_SIGNED_INT:
	case CC_TYPE_UNSIGNED_INT:
	case CC_TYPE_SIGNED_LONG:
	case CC_TYPE_UNSIGNED_LONG:
	case CC_TYPE_SIGNED_LONG_LONG:
	case CC_TYPE_UNSIGNED_LONG_LONG:
	case CC_TYPE_FLOAT:
	case CC_TYPE_DOUBLE:
	case CC_TYPE_LONG_DOUBLE:
	case CC_TYPE_BOOL:
		return 1;
	default:
		return 0;
	};
};

static int ccIsScalarType(CC_Type *type)
{
	return type->kind == CC_TYPE_PTR || ccIsArithmeticType(type);
};

static char* ccGenTmpLabel(CC_FlowContext *ctx)
{
	char buffer[64];
	sprintf(buffer, "TMP%d", ctx->glob->nextTmpNum++);
	return strdup(buffer);
};

static int ccIsIntegerType(CC_Type *type)
{
	switch (type->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_SIGNED_CHAR:
	case CC_TYPE_UNSIGNED_CHAR:
	case CC_TYPE_SIGNED_SHORT:
	case CC_TYPE_UNSIGNED_SHORT:
	case CC_TYPE_SIGNED_INT:
	case CC_TYPE_UNSIGNED_INT:
	case CC_TYPE_SIGNED_LONG:
	case CC_TYPE_UNSIGNED_LONG:
	case CC_TYPE_SIGNED_LONG_LONG:
	case CC_TYPE_UNSIGNED_LONG_LONG:
	case CC_TYPE_BOOL:
		return 1;
	default:
		return 0;
	};
};

static CC_ExprValue ccImplicitConvert(CC_ExprValue value, CC_FlowContext *ctx, CC_Type *destType)
{
	int targetReg;
	if (ccIsTypeCompatible(value.type, destType)) return value;
	
	switch (destType->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_SIGNED_CHAR:
	case CC_TYPE_SIGNED_SHORT:
	case CC_TYPE_SIGNED_INT:
	case CC_TYPE_SIGNED_LONG:
	case CC_TYPE_SIGNED_LONG_LONG:
		if (!ccIsArithmeticType(value.type))
		{
			lexDiag(value.tok, CON_ERROR, "cannot implicitly convert `%s' to `%s'", ccGetTypeName(value.type), ccGetTypeName(destType));
			value.type = NULL;
			return value;
		};
		switch (ccGetTypeSize(ctx->cc, destType))
		{
		case 1:
			targetReg = ctx->glob->nextReg++;
			ctirInsnBuffer(&value.numCode, &value.code, value.tok, targetReg, CTIR_i8, 1, ctirOperandList(1, ctirReg(value.reg)));
			value.reg = targetReg;
			value.type = destType;
			return value;
		case 2:
			targetReg = ctx->glob->nextReg++;
			ctirInsnBuffer(&value.numCode, &value.code, value.tok, targetReg, CTIR_i16, 1, ctirOperandList(1, ctirReg(value.reg)));
			value.reg = targetReg;
			value.type = destType;
			return value;
		case 4:
			targetReg = ctx->glob->nextReg++;
			ctirInsnBuffer(&value.numCode, &value.code, value.tok, targetReg, CTIR_i32, 1, ctirOperandList(1, ctirReg(value.reg)));
			value.reg = targetReg;
			value.type = destType;
			return value;
		case 8:
			targetReg = ctx->glob->nextReg++;
			ctirInsnBuffer(&value.numCode, &value.code, value.tok, targetReg, CTIR_i64, 1, ctirOperandList(1, ctirReg(value.reg)));
			value.reg = targetReg;
			value.type = destType;
			return value;
		};
		INVALID_CODE_PATH;
	case CC_TYPE_UNSIGNED_CHAR:
	case CC_TYPE_UNSIGNED_SHORT:
	case CC_TYPE_UNSIGNED_INT:
	case CC_TYPE_UNSIGNED_LONG:
	case CC_TYPE_UNSIGNED_LONG_LONG:
		if (!ccIsArithmeticType(value.type))
		{
			lexDiag(value.tok, CON_ERROR, "cannot implicitly convert `%s' to `%s'", ccGetTypeName(value.type), ccGetTypeName(destType));
			value.type = NULL;
			return value;
		};
		switch (ccGetTypeSize(ctx->cc, destType))
		{
		case 1:
			targetReg = ctx->glob->nextReg++;
			ctirInsnBuffer(&value.numCode, &value.code, value.tok, targetReg, CTIR_u8, 1, ctirOperandList(1, ctirReg(value.reg)));
			value.reg = targetReg;
			value.type = destType;
			return value;
		case 2:
			targetReg = ctx->glob->nextReg++;
			ctirInsnBuffer(&value.numCode, &value.code, value.tok, targetReg, CTIR_u16, 1, ctirOperandList(1, ctirReg(value.reg)));
			value.reg = targetReg;
			value.type = destType;
			return value;
		case 4:
			targetReg = ctx->glob->nextReg++;
			ctirInsnBuffer(&value.numCode, &value.code, value.tok, targetReg, CTIR_u32, 1, ctirOperandList(1, ctirReg(value.reg)));
			value.reg = targetReg;
			value.type = destType;
			return value;
		case 8:
			targetReg = ctx->glob->nextReg++;
			ctirInsnBuffer(&value.numCode, &value.code, value.tok, targetReg, CTIR_u64, 1, ctirOperandList(1, ctirReg(value.reg)));
			value.reg = targetReg;
			value.type = destType;
			return value;
		};
		INVALID_CODE_PATH;
	case CC_TYPE_FLOAT:
	case CC_TYPE_DOUBLE:
	case CC_TYPE_LONG_DOUBLE:
		if (!ccIsArithmeticType(value.type))
		{
			lexDiag(value.tok, CON_ERROR, "cannot implicitly convert `%s' to `%s'", ccGetTypeName(value.type), ccGetTypeName(destType));
			value.type = NULL;
			return value;
		};
		switch (ccGetTypeSize(ctx->cc, destType))
		{
		case 4:
			targetReg = ctx->glob->nextReg++;
			ctirInsnBuffer(&value.numCode, &value.code, value.tok, targetReg, CTIR_f32, 1, ctirOperandList(1, ctirReg(value.reg)));
			value.reg = targetReg;
			value.type = destType;
			return value;
		case 8:
			targetReg = ctx->glob->nextReg++;
			ctirInsnBuffer(&value.numCode, &value.code, value.tok, targetReg, CTIR_f64, 1, ctirOperandList(1, ctirReg(value.reg)));
			value.reg = targetReg;
			value.type = destType;
			return value;
		};
		INVALID_CODE_PATH;
	case CC_TYPE_PTR:
		if (value.type->kind == CC_TYPE_FUNC && ccIsTypeCompatible(value.type, destType->base))
		{
			CC_ExprValue newVal;
			newVal.type = destType;
			newVal.reg = value.addrReg;
			newVal.code = value.addrCode;
			newVal.numCode = value.numAddrCode;
			return newVal;
		}
		else
		{
			lexDiag(value.tok, CON_ERROR, "conversion from `%s' to incompatible pointer type `%s'", ccGetTypeName(value.type), ccGetTypeName(destType));
			return (CC_ExprValue) {.type = NULL};
		};
	default:
		TODO;
	};

	CC_ExprValue fail = {};
	return fail;
};

static CC_ExprValue ccExplicitConvert(CC_ExprValue value, CC_FlowContext *ctx, CC_Type *destType)
{
	if (destType->kind == CC_TYPE_VOID)
	{
		CC_ExprValue result = value;
		result.type = destType;
		result.sideEffects = 1;
		return result;
	};
	
	if (destType->kind == value.type->kind)
	{
		// no conversion needed
		value.type = destType;
		return value;
	};
	
	if (destType->kind == CC_TYPE_PTR)
	{
		// conversion to pointer
		if (ccIsIntegerType(value.type))
		{
			int compatType;
			if (ctx->cc->dataModel->sizeofPtr == ctx->cc->dataModel->sizeofInt)
			{
				compatType = CC_TYPE_UNSIGNED_INT;
			}
			else if (ctx->cc->dataModel->sizeofPtr == ctx->cc->dataModel->sizeofLong)
			{
				compatType = CC_TYPE_UNSIGNED_LONG;
			}
			else
			{
				INVALID_CODE_PATH;
			};
			
			CC_ExprValue result = ccImplicitConvert(value, ctx, ccMakeBasicType(compatType));
			result.type = destType;
			return result;
		}
		else
		{
			lexDiag(value.tok, CON_ERROR, "cannot convert from type `%s' to `%s'", ccGetTypeName(value.type), ccGetTypeName(destType));
			value.type = NULL;
			return value;
		};
	}
	else if (value.type->kind == CC_TYPE_PTR)
	{
		if (ccIsIntegerType(destType))
		{
			int compatType;
			if (ctx->cc->dataModel->sizeofPtr == ctx->cc->dataModel->sizeofInt)
			{
				compatType = CC_TYPE_UNSIGNED_INT;
			}
			else if (ctx->cc->dataModel->sizeofPtr == ctx->cc->dataModel->sizeofLong)
			{
				compatType = CC_TYPE_UNSIGNED_LONG;
			}
			else
			{
				INVALID_CODE_PATH;
			};
			
			if (ccGetTypeSize(ctx->cc, destType) < ctx->cc->dataModel->sizeofPtr)
			{
				lexDiag(value.tok, CON_WARNING, "conversion from pointer to a smaller integer type may lead to truncation");
			};
			
			value.type = ccMakeBasicType(compatType);
			return ccImplicitConvert(value, ctx, destType);
		}
		else
		{
			lexDiag(value.tok, CON_ERROR, "cannot convert from type `%s' to `%s'", ccGetTypeName(value.type), ccGetTypeName(destType));
			value.type = NULL;
			return value;
		};
	};
	
	return ccImplicitConvert(value, ctx, destType);
};

static int ccGetTypeRank(CC_Type *type)
{
	switch (type->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_UNSIGNED_CHAR:
	case CC_TYPE_SIGNED_CHAR:
		return 1;
	case CC_TYPE_SIGNED_SHORT:
	case CC_TYPE_UNSIGNED_SHORT:
		return 2;
	case CC_TYPE_SIGNED_INT:
	case CC_TYPE_UNSIGNED_INT:
		return 3;
	case CC_TYPE_SIGNED_LONG:
	case CC_TYPE_UNSIGNED_LONG:
		return 4;
	case CC_TYPE_SIGNED_LONG_LONG:
	case CC_TYPE_UNSIGNED_LONG_LONG:
		return 5;
	default:
		INVALID_CODE_PATH;
	};

	return 0;
};

static CC_ExprValue ccIntegerPromotions(CC_FlowContext *ctx, CC_ExprValue old)
{
	CC_ExprValue new;
	memset(&new, 0, sizeof(CC_ExprValue));
	
	switch (old.type->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_SIGNED_CHAR:
	case CC_TYPE_UNSIGNED_CHAR:
	case CC_TYPE_SIGNED_SHORT:
	case CC_TYPE_UNSIGNED_SHORT:
		return ccImplicitConvert(old, ctx, ccMakeBasicType(CC_TYPE_SIGNED_INT));
	default:
		return old;
	};
};

static CC_ExprValue ccArgumentPromotions(CC_FlowContext *ctx, CC_ExprValue old)
{
	CC_ExprValue new = ccIntegerPromotions(ctx, old);
	if (new.type->kind == CC_TYPE_FLOAT)
	{
		return ccImplicitConvert(new, ctx, ccMakeBasicType(CC_TYPE_DOUBLE));
	}
	else if (new.type->kind == CC_TYPE_FUNC)
	{
		return ccImplicitConvert(new, ctx, ccMakePtrType(new.type));
	}
	else
	{
		return new;
	};
};

static int ccIsComplexType(CC_Type *type)
{
	switch (type->kind)
	{
	case CC_TYPE_FLOAT_COMPLEX:
	case CC_TYPE_DOUBLE_COMPLEX:
	case CC_TYPE_LONG_DOUBLE_COMPLEX:
		return 1;
	default:
		return 0;
	};
};

static int ccIsSignedIntegerType(CC_Type *type)
{
	switch (type->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_SIGNED_CHAR:
	case CC_TYPE_SIGNED_SHORT:
	case CC_TYPE_SIGNED_INT:
	case CC_TYPE_SIGNED_LONG:
	case CC_TYPE_SIGNED_LONG_LONG:
		return 1;
	default:
		return 0;
	};
};

static int ccGetCorrespondingUnsignedType(CC_Type *type)
{
	switch (type->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_SIGNED_CHAR:
		return CC_TYPE_UNSIGNED_CHAR;
	case CC_TYPE_SIGNED_SHORT:
		return CC_TYPE_UNSIGNED_SHORT;
	case CC_TYPE_SIGNED_INT:
		return CC_TYPE_UNSIGNED_INT;
	case CC_TYPE_SIGNED_LONG:
		return CC_TYPE_UNSIGNED_LONG;
	case CC_TYPE_SIGNED_LONG_LONG:
		return CC_TYPE_UNSIGNED_LONG_LONG;
	default:
		INVALID_CODE_PATH;
	};

	return 0;
};

static void ccUsualArithmeticConversions(CC_FlowContext *ctx, CC_ExprValue *a, CC_ExprValue *b)
{
	if (a->type->kind == CC_TYPE_LONG_DOUBLE || a->type->kind == CC_TYPE_LONG_DOUBLE_COMPLEX)
	{
		*b = ccImplicitConvert(*b, ctx, ccMakeBasicType(ccIsComplexType(b->type) ? CC_TYPE_LONG_DOUBLE_COMPLEX : CC_TYPE_LONG_DOUBLE));
	}
	else if (b->type->kind == CC_TYPE_LONG_DOUBLE || b->type->kind == CC_TYPE_LONG_DOUBLE_COMPLEX)
	{
		*a = ccImplicitConvert(*a, ctx, ccMakeBasicType(ccIsComplexType(a->type) ? CC_TYPE_LONG_DOUBLE_COMPLEX : CC_TYPE_LONG_DOUBLE));
	}
	else if (a->type->kind == CC_TYPE_DOUBLE || a->type->kind == CC_TYPE_DOUBLE_COMPLEX)
	{
		*b = ccImplicitConvert(*b, ctx, ccMakeBasicType(ccIsComplexType(b->type) ? CC_TYPE_DOUBLE_COMPLEX : CC_TYPE_DOUBLE));
	}
	else if (b->type->kind == CC_TYPE_DOUBLE || b->type->kind == CC_TYPE_DOUBLE_COMPLEX)
	{
		*a = ccImplicitConvert(*a, ctx, ccMakeBasicType(ccIsComplexType(a->type) ? CC_TYPE_DOUBLE_COMPLEX : CC_TYPE_DOUBLE));
	}
	else if (a->type->kind == CC_TYPE_FLOAT || a->type->kind == CC_TYPE_FLOAT_COMPLEX)
	{
		*b = ccImplicitConvert(*b, ctx, ccMakeBasicType(ccIsComplexType(b->type) ? CC_TYPE_FLOAT_COMPLEX : CC_TYPE_FLOAT));
	}
	else if (b->type->kind == CC_TYPE_FLOAT || b->type->kind == CC_TYPE_FLOAT_COMPLEX)
	{
		*a = ccImplicitConvert(*a, ctx, ccMakeBasicType(ccIsComplexType(a->type) ? CC_TYPE_FLOAT_COMPLEX : CC_TYPE_FLOAT));
	}
	else
	{
		*a = ccIntegerPromotions(ctx, *a);
		*b = ccIntegerPromotions(ctx, *b);
		
		if (ccIsSignedIntegerType(a->type) && ccIsSignedIntegerType(b->type))
		{
			if (ccGetTypeRank(a->type) > ccGetTypeRank(b->type))
			{
				*b = ccImplicitConvert(*b, ctx, a->type);
			}
			else
			{
				*a = ccImplicitConvert(*a, ctx, b->type);
			};
		}
		else if (!ccIsSignedIntegerType(a->type) && !ccIsSignedIntegerType(b->type))
		{
			if (ccGetTypeRank(a->type) > ccGetTypeRank(b->type))
			{
				*b = ccImplicitConvert(*b, ctx, a->type);
			}
			else
			{
				*a = ccImplicitConvert(*a, ctx, b->type);
			};
		}
		else if (!ccIsSignedIntegerType(a->type) && ccGetTypeRank(a->type) >= ccGetTypeRank(b->type))
		{
			*b = ccImplicitConvert(*b, ctx, a->type);
		}
		else if (!ccIsSignedIntegerType(b->type) && ccGetTypeRank(b->type) >= ccGetTypeRank(a->type))
		{
			*a = ccImplicitConvert(*a, ctx, b->type);
		}
		else if (ccIsSignedIntegerType(a->type) && ccGetTypeSize(ctx->cc, a->type) > ccGetTypeSize(ctx->cc, b->type))
		{
			*b = ccImplicitConvert(*b, ctx, a->type);
		}
		else if (ccIsSignedIntegerType(b->type) && ccGetTypeSize(ctx->cc, b->type) > ccGetTypeSize(ctx->cc, a->type))
		{
			*a = ccImplicitConvert(*a, ctx, b->type);
		}
		else if (ccIsSignedIntegerType(a->type))
		{
			CC_Type *newType = ccMakeBasicType(ccGetCorrespondingUnsignedType(a->type));
			*a = ccImplicitConvert(*a, ctx, newType);
			*b = ccImplicitConvert(*b, ctx, newType);
		}
		else
		{
			CC_Type *newType = ccMakeBasicType(ccGetCorrespondingUnsignedType(b->type));
			*a = ccImplicitConvert(*a, ctx, newType);
			*b = ccImplicitConvert(*b, ctx, newType);
		};
	};
	
	// after all these conversions have been performed, if one of the types is complex,
	// it means the other type now has the same corresponding real type with it, and must
	// be converted to the complex type
	if (ccIsComplexType(a->type))
	{
		*b = ccImplicitConvert(*b, ctx, a->type);
	}
	else if (ccIsComplexType(b->type))
	{
		*a = ccImplicitConvert(*a, ctx, b->type);
	};
};

static void ccAppendNorm(CC_FlowContext *ctx, int numCode, CTIR_Insn *code)
{
	// generate a hashtable of blocks
	HashTable *tab = hashtabNew();
	
	int i;
	for (i=0; i<numCode; i++)
	{
		CTIR_Insn *insn = &code[i];
		if (insn->opcode == CTIR_label)
		{
			hashtabSet(tab, insn->ops[0].symbolName, ctirNewBlock(ctx->func));
		};
	};
	
	// now we can emit the code
	for (i=0; i<numCode; i++)
	{
		CTIR_Insn *insn = &code[i];
		if (insn->opcode == CTIR_label)
		{
			ctx->block = (CTIR_Block*) hashtabGet(tab, insn->ops[0].symbolName);
		}
		else
		{
			int j;
			for (j=0; j<insn->numOps; j++)
			{
				CTIR_Operand *op = &insn->ops[j];
				if (op->kind == CTIR_KIND_TMPLABEL)
				{
					CTIR_Block *dest = (CTIR_Block*) hashtabGet(tab, op->symbolName);
					op->blockIndex = dest->index;
					op->kind = CTIR_KIND_BLOCKREF;
				};
			};
			
			ctirInsn(ctx->block, insn->tok, insn->resultReg, insn->opcode, insn->numOps, insn->ops);
		};
	};
};

static CC_ExprValue ccTranslateExpr(CC_FlowContext *ctx, CC_Node *expr)
{
	CC_ExprValue val;
	memset(&val, 0, sizeof(CC_ExprValue));
	
	static int nextStringNum = 0;
	
	switch (expr->type)
	{
	case CC_int:
		val.tok = expr->tok;
		val.type = ccMakeBasicType(CC_TYPE_SIGNED_INT);
		val.reg = ctx->glob->nextReg++;
		ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, ccGetIntConstOpcode(ctx), 1, ctirOperandList(1, ctirConst(expr->cval.valI)));
		val.hasConst = 1;
		ASSIGN_CONST_VALUE(val, =, expr->cval.valI);
		break;
	case CC_double:
		val.tok = expr->tok;
		val.type = ccMakeBasicType(CC_TYPE_DOUBLE);
		val.reg = ctx->glob->nextReg++;
		ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, ccGetDoubleConstOpcode(ctx), 1, ctirOperandList(1, ctirConstDouble(expr->cval.valDouble)));
		val.hasConst = 1;
		ASSIGN_CONST_VALUE(val, =, expr->cval.valDouble);
		break;
	case CC_string:
		{
			char name[256];
			sprintf(name, "_.STR%d", nextStringNum++);
			
			CTIR_Data *data = (CTIR_Data*) calloc(strlen(expr->str)+1, sizeof(CTIR_Data));
			int numData = strlen(expr->str) + 1;
			
			int i;
			for (i=0; i<numData; i++)
			{
				data[i].type = CTIR_I8;
				data[i].val = (uint64_t) expr->str[i];
			};
			
			ctirNewGlob(ctx->glob->out, CTIR_VIS_LOCAL, ".rodata", strdup(name), 1, numData, data);
			
			val.tok = expr->tok;
			val.type = ccMakePtrType(ccMakeBasicType(CC_TYPE_CHAR));
			val.reg = ctx->glob->nextReg++;
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_addrof, 1, ctirOperandList(1, ctirSymbolRef(strdup(name), 0)));
			break;
		};
		break;
	case CC_id:
		{
			const char *varName = expr->str;
			if (hashtabHasKey(ctx->vars, varName))
			{
				CC_VarInfo *varInfo = (CC_VarInfo*) hashtabGet(ctx->vars, varName);
				int ctirType = ccGetCtirType(ctx->cc, varInfo->type);
				
				val.tok = expr->tok;
				val.type = varInfo->type;
				
				if (ctirType != 0)
				{
					val.reg = ctx->glob->nextReg++;
					ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_load, 2, ctirOperandList(2, ctirReg(varInfo->stackReg), ctirTypeSpec(ctirType)));
				};
				
				val.addrReg = ctx->glob->nextReg++;
				ctirInsnBuffer(&val.numAddrCode, &val.addrCode, expr->tok, val.addrReg, CTIR_addrof, 1, ctirOperandList(1, ctirReg(varInfo->stackReg)));
				
				val.storeOp = ctirReg(varInfo->stackReg);
			}
			else if (hashtabHasKey(ctx->glob->unit->globals, varName))
			{
				CC_Decl *decl = (CC_Decl*) hashtabGet(ctx->glob->unit->globals, varName);
				if (decl->storageClass == CC_STOR_TYPEDEF)
				{
					lexDiag(expr->tok, CON_ERROR, "cannot evaluate a type name");
					val.type = NULL;
					return val;
				};
				if (ccGetTypeSize(ctx->cc, decl->type) == 0)
				{
					val.tok = expr->tok;
					val.type = decl->type;
					val.reg = 0;
					
					val.addrReg = ctx->glob->nextReg++;
					ctirInsnBuffer(&val.numAddrCode, &val.addrCode, expr->tok, val.addrReg, CTIR_addrof, 1, ctirOperandList(1, ctirSymbolRef(varName, decl->storageClass != CC_STOR_STATIC)));
					
					val.storeOp = ctirSymbolRef(varName, decl->storageClass != CC_STOR_STATIC);
				}
				else
				{
					int ctirType = ccGetCtirType(ctx->cc, decl->type);
					assert(ctirType != 0);
					
					val.tok = expr->tok;
					val.type = decl->type;
					val.reg = ctx->glob->nextReg++;
					ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_load, 2, ctirOperandList(2, ctirSymbolRef(varName, decl->storageClass != CC_STOR_STATIC), ctirTypeSpec(ctirType)));
					
					val.addrReg = ctx->glob->nextReg++;
					ctirInsnBuffer(&val.numAddrCode, &val.addrCode, expr->tok, val.addrReg, CTIR_addrof, 1, ctirOperandList(1, ctirSymbolRef(varName, decl->storageClass != CC_STOR_STATIC)));
					
					val.storeOp = ctirSymbolRef(varName, decl->storageClass != CC_STOR_STATIC);
				};
			}
			else
			{
				lexDiag(expr->tok, CON_ERROR, "undeclared variable `%s'", varName);
				break;
			};
			
			if (val.type->qualifs & CC_TQ_VOLATILE)
			{
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_soft_fence, 0, NULL);
			};
		};
		break;
	case CC_ptr:
		{
			CC_ExprValue subval = ccTranslateExpr(ctx, expr->children[0]);
			if (subval.type == NULL) break;
			
			if (subval.addrReg == 0)
			{
				lexDiag(expr->tok, CON_ERROR, "cannot compute a pointer to the specified object (not an lvalue)");
				break;
			};
			
			val.tok = expr->tok;
			val.type = ccMakePtrType(subval.type);
			val.reg = subval.addrReg;
			val.numCode = subval.numAddrCode;
			val.code = subval.addrCode;
		};
		break;
	case CC_deref:
		{
			CC_ExprValue subval = ccTranslateExpr(ctx, expr->children[0]);
			if (subval.type == NULL) break;
			
			if (subval.type->kind != CC_TYPE_PTR)
			{
				lexDiag(expr->tok, CON_ERROR, "value being dereferenced is not a pointer");
				break;
			};
			
			val.tok = expr->tok;
			val.type = subval.type->base;
			val.addrReg = subval.reg;
			val.numAddrCode = subval.numCode;
			val.addrCode = subval.code;
			
			int ctirType = ccGetCtirType(ctx->cc, val.type);
			if (ctirType != 0)
			{
				int resultReg = ctx->glob->nextReg++;
				val.reg = resultReg;
				ctirAppendBuffer(&val.numCode, &val.code, val.numAddrCode, val.addrCode);
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, resultReg, CTIR_load, 2, ctirOperandList(2, ctirReg(val.addrReg), ctirTypeSpec(ctirType)));
			};
		};
		break;
	case CC_assign:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			if (left.type == NULL) return left;
			
			if (ccGetTypeSize(ctx->cc, left.type) == 0)
			{
				lexDiag(left.tok, CON_ERROR, "assignment to a variable of incomplete type");
				val.type = NULL;
				return val;
			};
			
			if (left.addrReg == 0)
			{
				lexDiag(left.tok, CON_ERROR, "left side of assignment operator is not an lvalue");
				val.type = NULL;
				return val;
			};
			
			if (left.type->qualifs & CC_TQ_CONST)
			{
				lexDiag(left.tok, CON_ERROR, "assignment to object qualified as `const'");
				val.type = NULL;
				return val;
			};
			
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (right.type == NULL) return right;
			
			right = ccImplicitConvert(right, ctx, left.type);
			if (right.type == NULL) return right;
			
			val.type = left.type;
			
			int ctirType = ccGetCtirType(ctx->cc, val.type);
			if (ctirType != 0)
			{
				val.reg = right.reg;
				ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
				
				if (left.storeOp.kind != CTIR_KIND_CONST)
				{
					ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_store, 2, ctirOperandList(2, left.storeOp, ctirReg(right.reg)));
				}
				else
				{
					ctirAppendBuffer(&val.numCode, &val.code, left.numAddrCode, left.addrCode);
					ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_store, 2, ctirOperandList(2, ctirReg(left.addrReg), ctirReg(right.reg)));
				};
				
				if (left.type->qualifs & CC_TQ_VOLATILE)
				{
					ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_soft_fence, 0, NULL);
				};
				
				val.sideEffects = 1;
			}
			else
			{
				int sizeReg = ctx->glob->nextReg++;
				
				int constOp;
				if (ctx->cc->dataModel->sizeofPtr == 8) constOp = CTIR_u64;
				else constOp = CTIR_u32;
				
				val.reg = 0;
				val.addrReg = left.addrReg;
				
				ctirAppendBuffer(&val.numAddrCode, &val.addrCode, right.numAddrCode, right.addrCode);
				ctirAppendBuffer(&val.numAddrCode, &val.addrCode, left.numAddrCode, left.addrCode);
				ctirInsnBuffer(&val.numAddrCode, &val.addrCode, expr->tok, sizeReg, constOp, 1, ctirOperandList(1, ctirConst(ccGetTypeSize(ctx->cc, val.type))));
				ctirInsnBuffer(&val.numAddrCode, &val.addrCode, expr->tok, 0, CTIR_call, 5, ctirOperandList(
					5, ctirTypeSpec(CTIR_VOID), ctirSymbolRef("memcpy", 1), ctirReg(left.addrReg), ctirReg(right.addrReg), ctirReg(sizeReg)));
				ctirAppendBuffer(&val.numCode, &val.code, val.numAddrCode, val.addrCode);
				
				val.sideEffects = 1;
			};
		};
		break;
	case CC_add:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (left.type == NULL) return left;
			if (right.type == NULL) return right;

			if (ccIsArithmeticType(left.type) && ccIsArithmeticType(right.type))
			{
				ccUsualArithmeticConversions(ctx, &left, &right);
				if (left.type == NULL || right.type == NULL)
				{
					val.type = NULL;
					return val;
				};
				
				val.tok = expr->tok;
				val.type = left.type;
				val.reg = ctx->glob->nextReg++;
				
				if (left.hasConst && right.hasConst)
				{
					val.hasConst = 1;
					val.vi8 = left.vi8 + right.vi8;
					val.vi16 = left.vi16 + right.vi16;
					val.vi32 = left.vi32 + right.vi32;
					val.vi64 = left.vi64 + right.vi64;
					val.vu8 = left.vu8 + right.vu8;
					val.vu16 = left.vu16 + right.vu16;
					val.vu32 = left.vu32 + right.vu32;
					val.vu64 = left.vu64 + right.vu64;
					val.vf = left.vf + right.vf;
					val.vd = left.vd + right.vd;
				};

				ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
				ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_add, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(right.reg)));
			}
			else if (left.type->kind == CC_TYPE_PTR && ccIsIntegerType(right.type))
			{
				size_t size = ccGetTypeSize(ctx->cc, left.type->base);
				if (size == 0)
				{
					lexDiag(expr->tok, CON_ERROR, "cannot perform pointer arithmetic on a pointer to an incomplete type `%s'", ccGetTypeName(left.type->base));
					val.type = NULL;
					return val;
				};
				
				int constOp;
				if (ctx->cc->dataModel->sizeofPtr == 4)
				{
					constOp = CTIR_u32;
				}
				else if (ctx->cc->dataModel->sizeofPtr == 8)
				{
					constOp = CTIR_u64;
				}
				else
				{
					INVALID_CODE_PATH;
				};
				
				int sizeReg = ctx->glob->nextReg++;
				int indexReg = ctx->glob->nextReg++;
				int dispReg = ctx->glob->nextReg++;

				val.reg = ctx->glob->nextReg++;
				val.type = left.type;
				val.tok = expr->tok;
				
				ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
				ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, sizeReg, constOp, 1, ctirOperandList(1, ctirConst(size)));
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, indexReg, constOp, 1, ctirOperandList(1, ctirReg(right.reg)));
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, dispReg, CTIR_mul, 2, ctirOperandList(2, ctirReg(sizeReg), ctirReg(indexReg)));
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_add, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(dispReg)));
			}
			else
			{
				lexDiag(expr->tok, CON_ERROR, "addition on unsupported type pair `%s' with `%s'", ccGetTypeName(left.type), ccGetTypeName(right.type));
				val.type = NULL;
			};
		};
		break;
	case CC_sub:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (left.type == NULL) return left;
			if (right.type == NULL) return right;

			if (ccIsArithmeticType(left.type) && ccIsArithmeticType(right.type))
			{
				ccUsualArithmeticConversions(ctx, &left, &right);
				if (left.type == NULL || right.type == NULL)
				{
					val.type = NULL;
					return val;
				};
				
				val.tok = expr->tok;
				val.type = left.type;
				val.reg = ctx->glob->nextReg++;
				
				if (left.hasConst && right.hasConst)
				{
					val.hasConst = 1;
					val.vi8 = left.vi8 - right.vi8;
					val.vi16 = left.vi16 - right.vi16;
					val.vi32 = left.vi32 - right.vi32;
					val.vi64 = left.vi64 - right.vi64;
					val.vu8 = left.vu8 - right.vu8;
					val.vu16 = left.vu16 - right.vu16;
					val.vu32 = left.vu32 - right.vu32;
					val.vu64 = left.vu64 - right.vu64;
					val.vf = left.vf - right.vf;
					val.vd = left.vd - right.vd;
				};

				ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
				ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_sub, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(right.reg)));
			}
			else if (left.type->kind == CC_TYPE_PTR && right.type->kind == CC_TYPE_PTR)
			{
				size_t size = ccGetTypeSize(ctx->cc, left.type->base);
				if (size == 0)
				{
					lexDiag(expr->tok, CON_ERROR, "cannot perform pointer arithmetic on a pointer to an incomplete type `%s'", ccGetTypeName(left.type->base));
					val.type = NULL;
					return val;
				};
				
				if (!ccIsTypeCompatible(left.type->base, right.type->base))
				{
					lexDiag(expr->tok, CON_ERROR, "subtraction of pointers to incompatible types `%s' and `%s'", ccGetTypeName(left.type->base), ccGetTypeName(right.type->base));
					val.type = NULL;
					return val;
				};
				
				int constOp;
				CC_Type *resultType;
				if (ctx->cc->dataModel->sizeofPtr == 4)
				{
					constOp = CTIR_i32;
				}
				else if (ctx->cc->dataModel->sizeofPtr == 8)
				{
					constOp = CTIR_i64;
				}
				else
				{
					INVALID_CODE_PATH;
				};
				
				if (ctx->cc->dataModel->sizeofPtr == ctx->cc->dataModel->sizeofInt)
				{
					resultType = ccMakeBasicType(CC_TYPE_SIGNED_INT);
				}
				else if (ctx->cc->dataModel->sizeofPtr == ctx->cc->dataModel->sizeofLong)
				{
					resultType = ccMakeBasicType(CC_TYPE_SIGNED_LONG);
				};
				
				int sizeReg = ctx->glob->nextReg++;
				int dispReg = ctx->glob->nextReg++;
				int tempReg = ctx->glob->nextReg++;
				
				val.reg = ctx->glob->nextReg++;
				val.type = resultType;
				val.tok = expr->tok;
				
				ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
				ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, dispReg, CTIR_sub, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(right.reg)));
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, tempReg, constOp, 1, ctirOperandList(1, ctirReg(dispReg)));
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, sizeReg, constOp, 1, ctirOperandList(1, ctirConst(size)));
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_div, 2, ctirOperandList(2, ctirReg(tempReg), ctirReg(sizeReg)));
			}
			else if (left.type->kind == CC_TYPE_PTR && ccIsIntegerType(right.type))
			{
				size_t size = ccGetTypeSize(ctx->cc, left.type->base);
				if (size == 0)
				{
					lexDiag(expr->tok, CON_ERROR, "cannot perform pointer arithmetic on a pointer to an incomplete type `%s'", ccGetTypeName(left.type->base));
					val.type = NULL;
					return val;
				};
				
				int constOp;
				if (ctx->cc->dataModel->sizeofPtr == 4)
				{
					constOp = CTIR_u32;
				}
				else if (ctx->cc->dataModel->sizeofPtr == 8)
				{
					constOp = CTIR_u64;
				}
				else
				{
					INVALID_CODE_PATH;
				};
				
				int sizeReg = ctx->glob->nextReg++;
				int indexReg = ctx->glob->nextReg++;
				int dispReg = ctx->glob->nextReg++;

				val.reg = ctx->glob->nextReg++;
				val.type = left.type;
				val.tok = expr->tok;
				
				ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
				ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, sizeReg, constOp, 1, ctirOperandList(1, ctirConst(size)));
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, indexReg, constOp, 1, ctirOperandList(1, ctirReg(right.reg)));
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, dispReg, CTIR_mul, 2, ctirOperandList(2, ctirReg(sizeReg), ctirReg(indexReg)));
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_sub, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(dispReg)));
			}
			else
			{
				lexDiag(expr->tok, CON_ERROR, "subtraction on unsupported type pair `%s' with `%s'", ccGetTypeName(left.type), ccGetTypeName(right.type));
				val.type = NULL;
			};
		};
		break;
	case CC_mul:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (left.type == NULL) return left;
			if (right.type == NULL) return right;

			if (ccIsArithmeticType(left.type) && ccIsArithmeticType(right.type))
			{
				ccUsualArithmeticConversions(ctx, &left, &right);
				if (left.type == NULL || right.type == NULL)
				{
					val.type = NULL;
					return val;
				};
				
				val.tok = expr->tok;
				val.type = left.type;
				val.reg = ctx->glob->nextReg++;
				
				if (left.hasConst && right.hasConst)
				{
					val.hasConst = 1;
					val.vi8 = left.vi8 * right.vi8;
					val.vi16 = left.vi16 * right.vi16;
					val.vi32 = left.vi32 * right.vi32;
					val.vi64 = left.vi64 * right.vi64;
					val.vu8 = left.vu8 * right.vu8;
					val.vu16 = left.vu16 * right.vu16;
					val.vu32 = left.vu32 * right.vu32;
					val.vu64 = left.vu64 * right.vu64;
					val.vf = left.vf * right.vf;
					val.vd = left.vd * right.vd;
				};

				ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
				ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_mul, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(right.reg)));
			}
			else
			{
				lexDiag(expr->tok, CON_ERROR, "multiplication on unsupported type pair `%s' with `%s'", ccGetTypeName(left.type), ccGetTypeName(right.type));
				val.type = NULL;
			};
		};
		break;
	case CC_div:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (left.type == NULL) return left;
			if (right.type == NULL) return right;

			if (ccIsArithmeticType(left.type) && ccIsArithmeticType(right.type))
			{
				ccUsualArithmeticConversions(ctx, &left, &right);
				if (left.type == NULL || right.type == NULL)
				{
					val.type = NULL;
					return val;
				};
				
				val.tok = expr->tok;
				val.type = left.type;
				val.reg = ctx->glob->nextReg++;
				
				if (left.hasConst && right.hasConst)
				{
					if (right.vi8 == 0 || right.vi16 == 0 || right.vi32 == 0 || right.vi64 == 0 || right.vu8 == 0 || right.vu16 == 0 || right.vu32 == 0 || right.vu64 == 0)
					{
						lexDiag(expr->tok, CON_ERROR, "division by constant zero");
						val.type = NULL;
						return val;
					};
					
					val.hasConst = 1;
					val.vi8 = left.vi8 / right.vi8;
					val.vi16 = left.vi16 / right.vi16;
					val.vi32 = left.vi32 / right.vi32;
					val.vi64 = left.vi64 / right.vi64;
					val.vu8 = left.vu8 / right.vu8;
					val.vu16 = left.vu16 / right.vu16;
					val.vu32 = left.vu32 / right.vu32;
					val.vu64 = left.vu64 / right.vu64;
					val.vf = left.vf / right.vf;
					val.vd = left.vd / right.vd;
				};

				ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
				ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_div, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(right.reg)));
			}
			else
			{
				lexDiag(expr->tok, CON_ERROR, "division on unsupported type pair `%s' with `%s'", ccGetTypeName(left.type), ccGetTypeName(right.type));
				val.type = NULL;
			};
		};
		break;
	case CC_mod:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (left.type == NULL) return left;
			if (right.type == NULL) return right;

			if (ccIsIntegerType(left.type) && ccIsIntegerType(right.type))
			{
				ccUsualArithmeticConversions(ctx, &left, &right);
				if (left.type == NULL || right.type == NULL)
				{
					val.type = NULL;
					return val;
				};
				
				val.tok = expr->tok;
				val.type = left.type;
				val.reg = ctx->glob->nextReg++;
				
				if (left.hasConst && right.hasConst)
				{
					if (right.vi8 == 0 || right.vi16 == 0 || right.vi32 == 0 || right.vi64 == 0 || right.vu8 == 0 || right.vu16 == 0 || right.vu32 == 0 || right.vu64 == 0)
					{
						lexDiag(expr->tok, CON_ERROR, "modulo division by constant zero");
						val.type = NULL;
						return val;
					};
					
					val.hasConst = 1;
					val.vi8 = left.vi8 % right.vi8;
					val.vi16 = left.vi16 % right.vi16;
					val.vi32 = left.vi32 % right.vi32;
					val.vi64 = left.vi64 % right.vi64;
					val.vu8 = left.vu8 % right.vu8;
					val.vu16 = left.vu16 % right.vu16;
					val.vu32 = left.vu32 % right.vu32;
					val.vu64 = left.vu64 % right.vu64;
					val.vf = (float) left.vi64;
					val.vd = (double) left.vi64;
				};

				ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
				ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_mod, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(right.reg)));
			}
			else
			{
				lexDiag(expr->tok, CON_ERROR, "modulo division on unsupported type pair `%s' with `%s'", ccGetTypeName(left.type), ccGetTypeName(right.type));
				val.type = NULL;
			};
		};
		break;
	case CC_call:
		{
			CTIR_Operand *ops = (CTIR_Operand*) malloc(sizeof(CTIR_Operand) * (expr->numChildren+1));
			CC_ExprValue subject = ccTranslateExpr(ctx, expr->children[0]);
			if (subject.type == NULL) return subject;
			CC_Type *funcType;
			
			const char *funcName = "<unknown>";
			if (subject.type->kind == CC_TYPE_FUNC && subject.storeOp.kind != 0)
			{
				// direct call
				funcName = subject.storeOp.symbolName;
				ops[1] = subject.storeOp;
				funcType = subject.type;
			}
			else
			{
				// dereferencing a pointer
				if (subject.type->kind == CC_TYPE_FUNC)
				{
					CC_Type *conv = (CC_Type*) calloc(1, sizeof(CC_Type));
					conv->kind = CC_TYPE_PTR;
					conv->base = subject.type;
					subject = ccImplicitConvert(subject, ctx, conv);
					if (subject.type == NULL) return subject;
				};
				
				if (subject.type->kind != CC_TYPE_PTR || subject.type->base->kind != CC_TYPE_FUNC)
				{
					lexDiag(subject.tok, CON_ERROR, "attempting to call a value which does not denote a function or function pointer");
					val.type = NULL;
					return val;
				};
				
				funcType = subject.type->base;
				ctirAppendBuffer(&val.numCode, &val.code, subject.numCode, subject.code);
				ops[1] = ctirReg(subject.reg);
			};
			
			int numArgsPassed = expr->numChildren - 1;
			if (funcType->valist)
			{
				if (numArgsPassed < funcType->numArgs)
				{
					lexDiag(expr->tok, CON_ERROR, "too few arguments to function `%s'", funcName);
					val.type = NULL;
					return val;
				};
			}
			else
			{
				if (numArgsPassed != funcType->numArgs)
				{
					lexDiag(expr->tok, CON_ERROR, "incorrect number of arguments to function `%s'", funcName);
					val.type = NULL;
					return val;
				};
			};
			
			int i;
			for (i=0; i<numArgsPassed; i++)
			{
				CC_ExprValue argValue = ccTranslateExpr(ctx, expr->children[i+1]);
				if (argValue.type == NULL) return argValue;
				
				if (i < funcType->numArgs)
				{
					CC_Type *targetType = funcType->argTypes[i];
					argValue = ccImplicitConvert(argValue, ctx, targetType);
					if (argValue.type == NULL) return argValue;
				}
				else
				{
					argValue = ccArgumentPromotions(ctx, argValue);
					if (argValue.type == NULL) return argValue;
				};
				
				assert(argValue.reg != 0);	// TODO: passing of weird things like structs
				ctirAppendBuffer(&val.numCode, &val.code, argValue.numCode, argValue.code);
				ops[i+2] = ctirReg(argValue.reg);
			};
			
			val.reg = ctx->glob->nextReg++;
			val.type = funcType->base;
			val.tok = expr->tok;
			
			ops[0] = ctirTypeSpec(ccGetCtirType(ctx->cc, val.type));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_call, expr->numChildren+1, ops);
			
			val.sideEffects = 1;
			return val;
		};
		break;
	case CC_member:
		{
			CC_ExprValue subject = ccTranslateExpr(ctx, expr->children[0]);
			if (subject.type == NULL) return subject;
			
			if (subject.type->kind != CC_TYPE_STRUCT_OR_UNION)
			{
				lexDiag(expr->tok, CON_ERROR, "attempting to access a member of something that is not a struct or union");
				val.type = NULL;
				return val;
			};
			
			assert(subject.addrReg != 0);
			
			CC_StructDef *def = (CC_StructDef*) hashtabGet(ctx->cc->unit->structs, subject.type->tag);
			if (!hashtabHasKey(def->memberTable, expr->str))
			{
				lexDiag(expr->tok, CON_ERROR, "struct or union `%s' has no member named `%s'", subject.type->tag, expr->str);
				val.type = NULL;
				return val;
			};
			
			CC_StructMember *member = (CC_StructMember*) hashtabGet(def->memberTable, expr->str);
			if (member->bitOff != 0)
			{
				TODO;
			};
			
			val.type = member->type;
			val.tok = expr->tok;
			
			int constOp;
			if (ctx->cc->dataModel->sizeofPtr == 8)
			{
				constOp = CTIR_u64;
			}
			else
			{
				constOp = CTIR_u32;
			};
			
			int addrReg = ctx->glob->nextReg++;
			int offsetReg = ctx->glob->nextReg++;
			
			ctirAppendBuffer(&val.numAddrCode, &val.addrCode, subject.numAddrCode, subject.addrCode);
			ctirInsnBuffer(&val.numAddrCode, &val.addrCode, expr->tok, offsetReg, constOp, 1, ctirOperandList(1, ctirConst(member->offset)));
			ctirInsnBuffer(&val.numAddrCode, &val.addrCode, expr->tok, addrReg, CTIR_add, 2, ctirOperandList(2, ctirReg(subject.addrReg), ctirReg(offsetReg)));
			val.addrReg = addrReg;
			
			int ctirType = ccGetCtirType(ctx->cc, val.type);
			if (ctirType != 0)
			{
				val.reg = ctx->glob->nextReg++;
				ctirAppendBuffer(&val.numCode, &val.code, val.numAddrCode, val.addrCode);
				ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, CTIR_load, 2, ctirOperandList(2, ctirReg(addrReg), ctirTypeSpec(ctirType)));
			};
			
			return val;
		};
		break;
	case CC_logical_or:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			if (left.type == NULL) return left;
			
			if (!ccIsScalarType(left.type))
			{
				lexDiag(left.tok, CON_ERROR, "left side of `||' operator must have scalar type");
				val.type = NULL;
				return val;
			};
			
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (right.type == NULL) return right;

			if (!ccIsScalarType(right.type))
			{
				lexDiag(left.tok, CON_ERROR, "right side of `||' operator must have scalar type");
				val.type = NULL;
				return val;
			};
			
			int constOp;
			if (ctx->cc->dataModel->sizeofInt == 4)
			{
				constOp = CTIR_i32;
			}
			else if (ctx->cc->dataModel->sizeofInt == 8)
			{
				constOp = CTIR_i64;
			}
			else
			{
				INVALID_CODE_PATH;
			};
			
			int zeroReg = ctx->glob->nextReg++;
			int oneReg = ctx->glob->nextReg++;
			int oneReg2 = ctx->glob->nextReg++;
			int resultReg = ctx->glob->nextReg++;
			
			char *labelB = ccGenTmpLabel(ctx);
			char *labelZero = ccGenTmpLabel(ctx);
			char *endLabel = ccGenTmpLabel(ctx);
			
			// first, evaluate A, and if it evaluates to nonzero, evaluate to 1;
			// otherwise, jump to the B code
			ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, oneReg, constOp, 1, ctirOperandList(1, ctirConst(1)));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_select, 3, ctirOperandList(
				3, ctirReg(left.reg), ctirTmpLabel(endLabel), ctirTmpLabel(labelB)));
			
			// now evaluate B, and jump to the end if it evaluates to 1, otherwise jump to
			// the "zero" code
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_label, 1, ctirOperandList(1, ctirTmpLabel(labelB)));
			ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, oneReg2, constOp, 1, ctirOperandList(1, ctirConst(1)));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_select, 3, ctirOperandList(
				3, ctirReg(right.reg), ctirTmpLabel(endLabel), ctirTmpLabel(labelZero)));
			
			// generate the "zero" code
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_label, 1, ctirOperandList(1, ctirTmpLabel(labelZero)));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, zeroReg, constOp, 1, ctirOperandList(1, ctirConst(0)));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_jmp, 1, ctirOperandList(1, ctirTmpLabel(endLabel)));
			
			// at the end block, we need a phi spec to determine the final result
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_label, 1, ctirOperandList(1, ctirTmpLabel(endLabel)));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, resultReg, CTIR_phi, 3, ctirOperandList(
				3, ctirReg(oneReg), ctirReg(oneReg2), ctirReg(zeroReg)));
			
			// finally done
			val.tok = expr->tok;
			val.type = ccMakeBasicType(CC_TYPE_SIGNED_INT);
			val.reg = resultReg;
			
			return val;
		};
		break;
	case CC_logical_and:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			if (left.type == NULL) return left;
			
			if (!ccIsScalarType(left.type))
			{
				lexDiag(left.tok, CON_ERROR, "left side of `&&' operator must have scalar type");
				val.type = NULL;
				return val;
			};
			
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (right.type == NULL) return right;

			if (!ccIsScalarType(right.type))
			{
				lexDiag(left.tok, CON_ERROR, "right side of `&&' operator must have scalar type");
				val.type = NULL;
				return val;
			};
			
			int constOp;
			if (ctx->cc->dataModel->sizeofInt == 4)
			{
				constOp = CTIR_i32;
			}
			else if (ctx->cc->dataModel->sizeofInt == 8)
			{
				constOp = CTIR_i64;
			}
			else
			{
				INVALID_CODE_PATH;
			};
			
			int oneReg = ctx->glob->nextReg++;
			int zeroReg = ctx->glob->nextReg++;
			int zeroReg2 = ctx->glob->nextReg++;
			int resultReg = ctx->glob->nextReg++;
			
			char *labelB = ccGenTmpLabel(ctx);
			char *labelOne = ccGenTmpLabel(ctx);
			char *endLabel = ccGenTmpLabel(ctx);
			
			// first, evaluate A, and if it evaluates to zero, evaluate to 0;
			// otherwise, jump to the B code
			ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, zeroReg, constOp, 1, ctirOperandList(1, ctirConst(0)));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_select, 3, ctirOperandList(
				3, ctirReg(left.reg), ctirTmpLabel(labelB), ctirTmpLabel(endLabel)));
			
			// now evaluate B, and jump to the end if it evaluates to 0, otherwise jump to
			// the "one" code
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_label, 1, ctirOperandList(1, ctirTmpLabel(labelB)));
			ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, zeroReg2, constOp, 1, ctirOperandList(1, ctirConst(0)));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_select, 3, ctirOperandList(
				3, ctirReg(right.reg), ctirTmpLabel(labelOne), ctirTmpLabel(endLabel)));
			
			// generate the "one" code
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_label, 1, ctirOperandList(1, ctirTmpLabel(labelOne)));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, oneReg, constOp, 1, ctirOperandList(1, ctirConst(1)));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_jmp, 1, ctirOperandList(1, ctirTmpLabel(endLabel)));
			
			// at the end block, we need a phi spec to determine the final result
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, 0, CTIR_label, 1, ctirOperandList(1, ctirTmpLabel(endLabel)));
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, resultReg, CTIR_phi, 3, ctirOperandList(
				3, ctirReg(oneReg), ctirReg(zeroReg), ctirReg(zeroReg2)));
			
			// finally done
			val.tok = expr->tok;
			val.type = ccMakeBasicType(CC_TYPE_SIGNED_INT);
			val.reg = resultReg;
			
			return val;
		};
		break;
	case CC_cast:
		{
			CC_ExprValue subject = ccTranslateExpr(ctx, expr->children[0]);
			if (subject.type == NULL) return subject;
			
			subject = ccExplicitConvert(subject, ctx, expr->cval.valType);
			if (subject.type == NULL) return subject;
			
			return subject;
		};
		break;
	case CC_eq:
	case CC_neq:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			if (left.type == NULL) return left;
			
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (right.type == NULL) return right;
			
			if (ccIsArithmeticType(left.type) && ccIsArithmeticType(right.type))
			{
				ccUsualArithmeticConversions(ctx, &left, &right);
				if (left.type == NULL || right.type == NULL)
				{
					val.type = NULL;
					return val;
				};
			}
			else if (left.type->kind == CC_TYPE_PTR && right.type->kind == CC_TYPE_PTR && ccIsTypeCompatible(left.type->base, right.type->base))
			{
				// OK
			}
			else if (left.type->kind == CC_TYPE_PTR && right.type->kind == CC_TYPE_PTR && (right.type->base->kind == CC_TYPE_VOID || left.type->base->kind == CC_TYPE_VOID))
			{
				// OK
			}
			else
			{
				// not OK
				lexDiag(expr->tok, CON_ERROR, "cannot compare objects of types `%s' and `%s'", ccGetTypeName(left.type), ccGetTypeName(right.type));
				val.type = NULL;
				return val;
			};
			
			int opcode;
			switch (expr->type)
			{
			case CC_eq:
				opcode = CTIR_eq;
				break;
			case CC_neq:
				opcode = CTIR_neq;
				break;
			default:
				INVALID_CODE_PATH;
			};
			
			val.tok = expr->tok;
			val.type = ccMakeBasicType(CC_TYPE_SIGNED_INT);
			val.reg = ctx->glob->nextReg++;
			ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
			ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, opcode, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(right.reg)));
			return val;
		};
		break;
	case CC_less:
	case CC_less_eq:
	case CC_greater:
	case CC_greater_eq:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			if (left.type == NULL) return left;
			
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (right.type == NULL) return right;
			
			if (ccIsRealType(left.type) && ccIsRealType(right.type))
			{
				ccUsualArithmeticConversions(ctx, &left, &right);
				if (left.type == NULL || right.type == NULL)
				{
					val.type = NULL;
					return val;
				};
			}
			else if (left.type->kind == CC_TYPE_PTR && right.type->kind == CC_TYPE_PTR && ccIsTypeCompatible(left.type->base, right.type->base))
			{
				// OK
			}
			else
			{
				// not OK
				lexDiag(expr->tok, CON_ERROR, "cannot compare objects of types `%s' and `%s'", ccGetTypeName(left.type), ccGetTypeName(right.type));
				val.type = NULL;
				return val;
			};
			
			int opcode;
			switch (expr->type)
			{
			case CC_less:
				opcode = CTIR_less;
				break;
			case CC_less_eq:
				opcode = CTIR_less_eq;
				break;
			case CC_greater:
				opcode = CTIR_greater;
				break;
			case CC_greater_eq:
				opcode = CTIR_greater_eq;
				break;
			default:
				INVALID_CODE_PATH;
			};
			
			val.tok = expr->tok;
			val.type = ccMakeBasicType(CC_TYPE_SIGNED_INT);
			val.reg = ctx->glob->nextReg++;
			ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
			ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, opcode, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(right.reg)));
			return val;
		};
		break;
	case CC_shl:
	case CC_shr:
		{
			CC_ExprValue left = ccTranslateExpr(ctx, expr->children[0]);
			if (left.type == NULL) return left;
			
			CC_ExprValue right = ccTranslateExpr(ctx, expr->children[1]);
			if (right.type == NULL) return right;
			
			left = ccIntegerPromotions(ctx, left);
			right = ccIntegerPromotions(ctx, right);
			
			if (left.type == NULL) return left;
			if (right.type == NULL) return right;
			
			if (!ccIsIntegerType(left.type))
			{
				lexDiag(expr->tok, CON_ERROR, "left side of shift operator must be an integer type, not `%s'", ccGetTypeName(left.type));
				val.type = NULL;
				return val;
			};
			
			if (!ccIsIntegerType(right.type))
			{
				lexDiag(expr->tok, CON_ERROR, "left side of shift operator must be an integer type, not `%s'", ccGetTypeName(left.type));
				val.type = NULL;
				return val;
			};
			
			int opcode;
			switch (expr->type)
			{
			case CC_shl:
				opcode = CTIR_shl;
				break;
			case CC_shr:
				opcode = CTIR_shr;
				if (ccIsSignedIntegerType(left.type)) opcode = CTIR_sar;
				break;
			default:
				INVALID_CODE_PATH;
			};
			
			val.tok = expr->tok;
			val.type = left.type;
			val.reg = ctx->glob->nextReg++;
			ctirAppendBuffer(&val.numCode, &val.code, left.numCode, left.code);
			ctirAppendBuffer(&val.numCode, &val.code, right.numCode, right.code);
			ctirInsnBuffer(&val.numCode, &val.code, expr->tok, val.reg, opcode, 2, ctirOperandList(2, ctirReg(left.reg), ctirReg(right.reg)));
			return val;
		};
		break;
	default:
		lexDiag(expr->tok, CON_ERROR, "unrecognised node type `%s'", ccGetNodeTypeName(expr->type));
		TODO;
	};
	
	return val;
};

static CC_FlowContext* ccNewFlowContext(CC_FlowContext *ctx)
{
	CC_FlowContext *new = (CC_FlowContext*) calloc(1, sizeof(CC_FlowContext));
	new->func = ctx->func;
	new->block = ctirNewBlock(ctx->func);
	new->cc = ctx->cc;
	new->retType = ctx->retType;
	new->vars = hashtabClone(ctx->vars);
	new->labels = ctx->labels;
	new->glob = ctx->glob;
	new->currentSwitch = NULL;
	new->continueTarget = ctx->continueTarget;
	new->breakTarget = ctx->breakTarget;
	
	return new;
};

static CC_Label* ccGetLabel(CC_FlowContext *ctx, const char *name, Token *tok)
{
	CC_Label *label;
	if (hashtabHasKey(ctx->labels, name))
	{
		label = (CC_Label*) hashtabGet(ctx->labels, name);
	}
	else
	{
		label = (CC_Label*) calloc(1, sizeof(CC_Label));
		label->block = ctirNewBlock(ctx->func);
		
		hashtabSet(ctx->labels, name, label);
	};
	
	label->tok = tok;
	return label;
};

static void ccDeleteContext(CC_FlowContext *ctx)
{
	hashtabDelete(ctx->vars);
	free(ctx);
};

static void ccTranslateStatement(CC_FlowContext *ctx, CC_Node *st)
{
	switch (st->type)
	{
	case CC_break:
		{
			if (ctx->breakTarget == 0)
			{
				lexDiag(st->tok, CON_ERROR, "`break' without an enclosing loop or `switch' statement");
				break;
			}
			else
			{
				ctirInsn(ctx->block, st->tok, 0, CTIR_jmp, 1, ctirOperandList(1, ctirBlockRef(ctx->breakTarget)));
				ctx->block = ctirNewBlock(ctx->func);
				break;
			};
		};
		break;
	case CC_continue:
		{
			if (ctx->continueTarget == 0)
			{
				lexDiag(st->tok, CON_ERROR, "`continue' without an enclosing loop or `switch' statement");
				break;
			}
			else
			{
				ctirInsn(ctx->block, st->tok, 0, CTIR_jmp, 1, ctirOperandList(1, ctirBlockRef(ctx->continueTarget)));
				ctx->block = ctirNewBlock(ctx->func);
				break;
			};
		};
		break;
	case CC_case:
		{
			if (ctx->currentSwitch == NULL)
			{
				lexDiag(st->tok, CON_ERROR, "`case' label without a corresponding `switch' statement");
				break;
			};
			
			CTIR_Block *newBlock = ctirNewBlock(ctx->func);
			ctirInsn(ctx->block, st->tok, 0, CTIR_jmp, 1, ctirOperandList(1, ctirBlockRef(newBlock->index)));
			ctx->block = newBlock;
			
			int i;
			for (i=2; i<ctx->currentSwitch->numOps; i+=2)
			{
				if (ctx->currentSwitch->ops[i].constVal == st->cval.valI)
				{
					lexDiag(st->tok, CON_ERROR, "repetition of a `case' within the same `switch' statement");
					break;
				};
			};
			
			if (i < ctx->currentSwitch->numOps) break;
			
			int index = ctx->currentSwitch->numOps;
			ctx->currentSwitch->numOps += 2;
			ctx->currentSwitch->ops = (CTIR_Operand*) realloc(ctx->currentSwitch->ops, sizeof(CTIR_Operand) * ctx->currentSwitch->numOps);
			ctx->currentSwitch->ops[index] = ctirConst(st->cval.valI);
			ctx->currentSwitch->ops[index+1] = ctirBlockRef(newBlock->index);
		};
		break;
	case CC_default:
		{
			if (ctx->currentSwitch == NULL)
			{
				lexDiag(st->tok, CON_ERROR, "`default' label without a corresponding `switch' statement");
				break;
			};
			
			CTIR_Block *newBlock = ctirNewBlock(ctx->func);
			ctirInsn(ctx->block, st->tok, 0, CTIR_jmp, 1, ctirOperandList(1, ctirBlockRef(newBlock->index)));
			ctx->block = newBlock;
			
			if (ctx->currentSwitch->ops[1].blockIndex != 0)
			{
				lexDiag(st->tok, CON_ERROR, "repetition of existing `default' label");
				break;
			};
			
			ctx->currentSwitch->ops[1].blockIndex = newBlock->index;
		};
		break;
	case CC_switch:
		{
			// start by evaluating the controlling expression
			CC_ExprValue value = ccTranslateExpr(ctx, st->children[0]);
			if (value.type == NULL) break;
			
			// perform integer promotions
			value = ccIntegerPromotions(ctx, value);
			if (value.type == NULL) break;
			
			if (!ccIsIntegerType(value.type))
			{
				lexDiag(value.tok, CON_ERROR, "the controlling expression of a `switch' statement must have integer type, but we have `%s'", ccGetTypeName(value.type));
				break;
			};
			
			// evaluate the controlling expression
			ccAppendNorm(ctx, value.numCode, value.code);
			
			// keep the current block so that we can add a 'CTIR_switch' later
			CTIR_Block *ctrlBlock = ctx->block;
			ctirInsn(ctrlBlock, st->tok, 0, CTIR_switch, 2, ctirOperandList(2, ctirReg(value.reg), ctirBlockRef(0)));
			CTIR_Insn *ctrl = &ctrlBlock->code[ctrlBlock->numCode-1];
			
			// set up a new context for the switch body
			CC_FlowContext *subctx = ccNewFlowContext(ctx);
			subctx->currentSwitch = ctrl;
			
			CTIR_Block *after = ctirNewBlock(ctx->func);
			subctx->breakTarget = after->index;
			
			CC_Node *body = st->children[1];
			
			// compile the block body
			int i;
			for (i=0; i<body->numChildren; i++)
			{
				ccTranslateStatement(subctx, body->children[i]);
			};
			
			// append a jump to the "after" block to the subctx block
			ctx->block = after;
			ctirInsn(subctx->block, st->tok, 0, CTIR_jmp, 1, ctirOperandList(1, ctirBlockRef(after->index)));
			
			// finished with this context
			ccDeleteContext(subctx);
			
			// if we didn't decide on a "default", then make it the "after" block
			if (ctrl->ops[1].blockIndex == 0)
			{
				ctrl->ops[1].blockIndex = after->index;
			};
		};
		break;
	case CC_label:
		{
			CC_Label *label = ccGetLabel(ctx, st->str, st->tok);
			label->haveDef = 1;
			ctirInsn(ctx->block, st->tok, 0, CTIR_jmp, 1, ctirOperandList(1, ctirBlockRef(label->block->index)));
			ctx->block = label->block;
		};
		break;
	case CC_goto:
		{
			CC_Label *label = ccGetLabel(ctx, st->str, st->tok);
			ctirInsn(ctx->block, st->tok, 0, CTIR_jmp, 1, ctirOperandList(1, ctirBlockRef(label->block->index)));
			ctx->block = ctirNewBlock(ctx->func);
		};
		break;
	case CC_ret:
		if (st->numChildren == 0)
		{
			if (ctx->retType->kind != CC_TYPE_VOID)
			{
				lexDiag(st->tok, CON_ERROR, "`return' with no value in function returning non-void");
				break;
			};
		}
		else
		{
			if (ctx->retType->kind == CC_TYPE_VOID)
			{
				lexDiag(st->tok, CON_ERROR, "`return' with a value in function returning void");
				break;
			};
			
			CC_ExprValue value = ccTranslateExpr(ctx, st->children[0]);
			if (value.type == NULL) break;
			
			value = ccImplicitConvert(value, ctx, ctx->retType);
			if (value.type == NULL) break;
			
			// TODO: structs do not have a 'reg'; handle that!
			ccAppendNorm(ctx, value.numCode, value.code);
			ctirInsn(ctx->block, st->tok, 0, CTIR_ret, 1, ctirOperandList(1, ctirReg(value.reg)));
		};

		// end the current block, and make a new (unreachable) block, because
		// a return statement terminates flow
		ctx->block = ctirNewBlock(ctx->func);
		break;
	case CC_alloc_local:
		{
			const char *varName = st->str;
			if (hashtabHasKey(ctx->vars, varName))
			{
				lexDiag(st->tok, CON_ERROR, "variable `%s' already declared in this scope", varName);
				break;
			};
			
			CC_Type *varType = st->cval.valType;
			size_t size = ccGetTypeSize(ctx->cc, varType);
			size_t align = ccGetTypeAlign(ctx->cc, varType);
			
			if (size == 0 || align == 0)
			{
				lexDiag(st->tok, CON_ERROR, "variable `%s' declared with incomplete type", varName);
				break;
			};
			
			CC_VarInfo *info = (CC_VarInfo*) calloc(1, sizeof(CC_VarInfo));
			info->stackReg = ctx->glob->nextReg++;
			info->type = varType;
			info->tok = st->tok;
			
			hashtabSet(ctx->vars, varName, info);
			
			ctirInsn(ctx->block, st->tok, info->stackReg, CTIR_alloc, 2, ctirOperandList(2, ctirConst(align), ctirConst(size)));
			ctirInsn(ctx->block, st->tok, 0, CTIR_debug_var, 2, ctirOperandList(2, ctirSymbolRef(varName, 0), ctirReg(info->stackReg)));
			
			// see if we have an initializer
			if (st->numChildren != 0)
			{
				CC_ExprValue value = ccTranslateExpr(ctx, st->children[0]);
				if (value.type == NULL) break;
				
				value = ccImplicitConvert(value, ctx, varType);
				if (value.type == NULL) break;
				
				if (value.reg == 0)
				{
					ccAppendNorm(ctx, value.numAddrCode, value.addrCode);
					
					int ptrReg = ctx->glob->nextReg++;
					int sizeReg = ctx->glob->nextReg++;
					
					int constOp;
					if (ctx->cc->dataModel->sizeofPtr == 8) constOp = CTIR_u64;
					else constOp = CTIR_u32;
					
					ctirInsn(ctx->block, st->tok, ptrReg, CTIR_addrof, 1, ctirOperandList(1, ctirReg(info->stackReg)));
					ctirInsn(ctx->block, st->tok, sizeReg, constOp, 1, ctirOperandList(1, ctirConst(ccGetTypeSize(ctx->cc, value.type))));
					ctirInsn(ctx->block, st->tok, 0, CTIR_call, 5, ctirOperandList(
						5, ctirTypeSpec(CTIR_VOID), ctirSymbolRef("memcpy", 1), ctirReg(ptrReg), ctirReg(value.addrReg), ctirReg(sizeReg)));
				}
				else
				{
					ccAppendNorm(ctx, value.numCode, value.code);
					ctirInsn(ctx->block, st->tok, 0, CTIR_store, 2, ctirOperandList(2, ctirReg(info->stackReg), ctirReg(value.reg)));
				};
			};
		};
		break;
	case CC_block:
		{
			CC_FlowContext *new = ccNewFlowContext(ctx);
			
			// append a jump to the new block in the current block
			ctirInsn(ctx->block, st->tok, 0, CTIR_jmp, 1, ctirOperandList(1, ctirBlockRef(new->block->index)));
			
			// compile the block body
			int i;
			for (i=0; i<st->numChildren; i++)
			{
				ccTranslateStatement(new, st->children[i]);
			};
			
			ctx->block = new->block;
			
			// finished with this context
			ccDeleteContext(new);
		};
		break;
	case CC_nop:
		// NOP
		break;
	default:
		{
			CC_ExprValue value = ccTranslateExpr(ctx, st);
			if (value.type == NULL) break;
			
			if (!value.sideEffects)
			{
				lexDiag(value.tok, CON_WARNING, "calculation without side effects");
			};
			
			ccAppendNorm(ctx, value.numCode, value.code);
		};
		break;
	};
};

static CTIR_Operand ccGetTypeSpec(CC_FlowContext *ctx, CC_Type *type)
{
	if (ccIsArithmeticType(type) || type->kind == CC_TYPE_PTR)
	{
		return ctirTypeSpec(ccGetCtirType(ctx->cc, type));
	}
	else
	{
		TODO;
		CTIR_Operand ok = {};
		return ok;
	};
};

static int ccTranslateInit(CC_Compiler *cc, CC_GlobalContext *gctx, CC_Type *type, CC_Node *init, int *numDataOut, CTIR_Data **dataOut)
{
	if (ccIsArithmeticType(type))
	{
		if (init->type == CC_init_list)
		{
			lexDiag(init->tok, CON_ERROR, "expected an expression of type `%s', not an initializer list", ccGetTypeName(type));
			return -1;
		};
		
		CC_FlowContext dummy;
		dummy.cc = cc;
		dummy.vars = hashtabNew();
		dummy.glob = gctx;
		
		CC_ExprValue value = ccTranslateExpr(&dummy, init);
		if (value.type == NULL) return -1;
		
		value = ccImplicitConvert(value, &dummy, type);
		if (value.type == NULL) return -1;
		
		int ctirType = ccGetCtirType(cc, type);
		assert(ctirType != 0);
		
		union
		{
			uint64_t ival;
			double dval;
		} trick;
		
		union
		{
			uint32_t ival;
			float fval;
		} trick32;
		
		if (ctirType == CTIR_F32)
		{
			trick32.fval = value.vf;
			trick.ival = (uint64_t) trick32.ival;
			ctirType = CTIR_U32;
		}
		else if (ctirType == CTIR_F64)
		{
			trick.dval = value.vd;
			ctirType = CTIR_U64;
		}
		else
		{
			trick.ival = value.vu64;
		};
		
		CTIR_Data *data = (CTIR_Data*) calloc(1, sizeof(CTIR_Data));
		data->type = ctirType;
		data->val = trick.ival;
		
		*numDataOut = 1;
		*dataOut = data;
		return 0;
	}
	else
	{
		TODO;
		return -1;
	};
};

CTIR_Unit* ccEmit(CC_Compiler *cc, CC_Unit *input)
{
	CC_GlobalContext glob;
	glob.nextReg = 1;
	glob.unit = input;
	glob.nextTmpNum = 0;
	
	CTIR_Unit *out = ctirNewUnit();
	glob.out = out;
	
	// first emit global variables
	const char *name;
	HASHTAB_FOREACH(input->globals, CC_Decl *, name, decl)
	{
		if (decl->storageClass == CC_STOR_STATIC)
		{
			// if the size of the object is zero, it means it is incomplete. static variables cannot
			// be incomplete.
			if (decl->type->kind == CC_TYPE_VOID)
			{
				lexDiag(decl->tok, CON_ERROR, "variable `%s' declare with `void' type", name);
				continue;
			};
			
			if (decl->type->kind == CC_TYPE_FUNC)
			{
				continue;
			};
			
			size_t size = ccGetTypeSize(cc, decl->type);
			if (size == 0)
			{
				lexDiag(decl->tok, CON_ERROR, "variable `%s' has an initializer but has an incomplete type", name);
				continue;
			};
			
			size_t align = ccGetTypeAlign(cc, decl->type);
			
			if (decl->init == NULL)
			{
				CTIR_Glob *glob = ctirNewGlob(out, CTIR_VIS_LOCAL, ".bss", name, align, 0, NULL);
				glob->size = size;
			}
			else
			{
				CTIR_Data *data;
				int numData;
				
				if (ccTranslateInit(cc, &glob, decl->type, decl->init, &numData, &data) == -1)
				{
					continue;
				};
				
				// emit a global data descriptor
				ctirNewGlob(out, CTIR_VIS_LOCAL, ".data", name, ccGetTypeAlign(cc, decl->type), numData, data);
			};
		}
		else if (decl->storageClass == CC_STOR_EXTERN)
		{
			// if the size of the object is zero, it means it is either a function,
			// an incomplete (external) array, or an incomplete object.
			if (decl->type->kind == CC_TYPE_VOID)
			{
				lexDiag(decl->tok, CON_ERROR, "variable `%s' declare with `void' type", name);
				continue;
			};
			
			size_t size = ccGetTypeSize(cc, decl->type);
			if (size == 0)
			{
				if (decl->init == NULL)
				{
					// do not emit variables
					continue;
				}
				else
				{
					lexDiag(decl->tok, CON_ERROR, "variable `%s' has an initializer but has an incomplete type", name);
					continue;
				};
			};
			
			size_t align = ccGetTypeAlign(cc, decl->type);
			assert(align != 0);
			
			if (decl->init != NULL)
			{
				CTIR_Data *data;
				int numData;
				
				if (ccTranslateInit(cc, &glob, decl->type, decl->init, &numData, &data) == -1)
				{
					continue;
				};
				
				// emit a global data descriptor
				ctirNewGlob(out, CTIR_VIS_GLOBAL, ".data", name, ccGetTypeAlign(cc, decl->type), numData, data);
			}
			else
			{
				// emit a common variable
				ctirNewComm(out, name, size, align);
			};
		}
		else if (decl->storageClass == CC_STOR_TYPEDEF)
		{
			// NOP
		}
		else
		{
			fprintf(stderr, "TODO: storage class `%d'", decl->storageClass);
			TODO;
		};
	};
	
	if (conGetError() != 0) return NULL;
	
	// now translate functions
	CC_Func *func;
	for (func=input->funcs; func!=NULL; func=func->next)
	{
		int vis = CTIR_VIS_GLOBAL;
		if (func->storageClass == CC_STOR_STATIC) vis = CTIR_VIS_LOCAL;
		
		int attr = 0;
		if (func->type->base->kind != CC_TYPE_VOID) attr |= CTIR_FA_RETVAL;
		
		CC_FlowContext *ctx = (CC_FlowContext*) calloc(1, sizeof(CC_FlowContext));
		ctx->func = ctirNewFunc(out, vis, attr, func->name);
		ctx->block = ctirNewBlock(ctx->func);
		ctx->cc = cc;
		ctx->retType = func->type->base;
		ctx->vars = hashtabNew();
		ctx->labels = hashtabNew();
		ctx->glob = &glob;
		
		if (ctx->retType->kind != CC_TYPE_VOID)
		{
			ctx->func->ret = ccGetTypeSpec(ctx, ctx->retType);
		};
		
		// add the argument types to the argument tuple, and remember the registers
		// for them
		int i;
		for (i=0; i<func->type->numArgs; i++)
		{
			CC_Type *argType = func->type->argTypes[i];
			const char *name = func->type->argNames[i];
			Token *argToken = func->type->argTokens[i];
			if (name[0] == 0)
			{
				lexDiag(func->tok, CON_ERROR, "arguments without names in definition of function `%s'", func->name);
				return NULL;
			};
			
			if (hashtabHasKey(ctx->vars, name))
			{
				lexDiag(argToken, CON_ERROR, "multiple arguments named `%s'", name);
				return NULL;
			};
			
			if (ccGetTypeSize(ctx->cc, argType) == 0)
			{
				lexDiag(argToken, CON_ERROR, "argument `%s' to function `%s' has incomplete type", name, func->name);
				return NULL;
			};
			
			int index = ctx->func->args.numSubs++;
			ctx->func->args.subs = (CTIR_Operand*) realloc(ctx->func->args.subs, sizeof(CTIR_Operand) * ctx->func->args.numSubs);
			ctx->func->args.subs[index] = ccGetTypeSpec(ctx, argType);
			
			CC_VarInfo *info = (CC_VarInfo*) calloc(1, sizeof(CC_VarInfo));
			info->valueReg = glob.nextReg++;
			info->type = argType;
			info->tok = argToken;
			hashtabSet(ctx->vars, name, info);
			
			// tuples are automatically spilled onto the stack, so stackReg will
			// be the same as valueReg
			if (ctx->func->args.subs[index].kind == CTIR_KIND_TUPLE)
			{
				info->stackReg = info->valueReg;
			};
		};

		const char *argname;
		HASHTAB_FOREACH(ctx->vars, CC_VarInfo *, argname, info)
		{
			if (info->stackReg == 0)
			{
				// spill onto the stack
				int reg = glob.nextReg++;
				info->stackReg = reg;
				
				size_t size = ccGetTypeSize(cc, info->type);
				size_t align = ccGetTypeAlign(cc, info->type);
				
				ctirInsn(ctx->block, info->tok, reg, CTIR_alloc, 2, ctirOperandList(2, ctirConst(align), ctirConst(size)));
				ctirInsn(ctx->block, info->tok, 0, CTIR_store, 2, ctirOperandList(2, ctirReg(reg), ctirReg(info->valueReg)));
			};

			ctirInsn(ctx->block, info->tok, 0, CTIR_debug_var, 2, ctirOperandList(2, ctirSymbolRef(argname, 0), ctirReg(info->stackReg)));
		};
		
		// compile the function body
		for (i=0; i<func->body->numChildren; i++)
		{
			ccTranslateStatement(ctx, func->body->children[i]);
		};
		
		HASHTAB_FOREACH(ctx->labels, CC_Label *, argname, label)
		{
			if (!label->haveDef)
			{
				lexDiag(label->tok, CON_ERROR, "label `%s' is used here, but is not defined", argname);
			};
			
			free(label);
		};
		
		hashtabDelete(ctx->labels);
	};
	
	if (conGetError() != 0) return NULL;
	return out;
};
