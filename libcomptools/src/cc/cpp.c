/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/cc/cpp.h>
#include <comptools/lex.h>
#include <comptools/console.h>

typedef struct CPPContext_
{
	struct CPPContext_ *prev;
	Token *token;
} CPPContext;

/**
 * Token specifications for C.
 */
static TokenSpec ccTokens[] = {
	// PUNCTUATORS
	{TOK_CC_LSQPAREN, 		"\\["},
	{TOK_CC_RSQPAREN,		"\\]"},
	{TOK_CC_LPAREN,			"\\("},
	{TOK_CC_RPAREN,			"\\)"},
	{TOK_CC_LBRACE,			"\\{"},
	{TOK_CC_RBRACE,			"\\}"},
	{TOK_CC_DOT,			"\\."},
	{TOK_CC_MARROW,			"\\-\\>"},
	{TOK_CC_INC,			"\\+\\+"},
	{TOK_CC_DEC,			"\\-\\-"},
	{TOK_CC_BAND,			"\\&"},
	{TOK_CC_AST,			"\\*"},
	{TOK_CC_PLUS,			"\\+"},
	{TOK_CC_MINUS,			"\\-"},
	{TOK_CC_TILDE,			"\\~"},
	{TOK_CC_EXCL,			"\\!"},
	{TOK_CC_SLASH,			"\\/"},
	{TOK_CC_MODULO,			"\\%"},
	{TOK_CC_LSHIFT,			"\\<\\<"},
	{TOK_CC_RSHIFT,			"\\>\\>"},
	{TOK_CC_LARROW,			"\\<"},
	{TOK_CC_RARROW,			"\\>"},
	{TOK_CC_LARROW_EQ,		"\\<\\="},
	{TOK_CC_RARROW_EQ,		"\\>\\="},
	{TOK_CC_EQ,			"\\=\\="},
	{TOK_CC_NEQ,			"\\!\\="},
	{TOK_CC_XOR,			"\\^"},
	{TOK_CC_BOR,			"\\|"},
	{TOK_CC_LAND,			"\\&\\&"},
	{TOK_CC_LOR,			"\\|\\|"},
	{TOK_CC_QUEST,			"\\?"},
	{TOK_CC_COLON,			"\\:"},
	{TOK_CC_SEMICOLON,		"\\;"},
	{TOK_CC_ELLIPSIS,		"\\.\\.\\."},
	{TOK_CC_ASSIGN,			"\\="},
	{TOK_CC_ASSIGN_AST,		"\\*\\="},
	{TOK_CC_ASSIGN_SLASH,		"\\/\\="},
	{TOK_CC_ASSIGN_MODULO,		"\\%\\="},
	{TOK_CC_ASSIGN_PLUS,		"\\+\\="},
	{TOK_CC_ASSIGN_MINUS,		"\\-\\="},
	{TOK_CC_ASSIGN_LSHIFT,		"\\<\\<\\="},
	{TOK_CC_ASSIGN_RSHIFT,		"\\>\\>\\="},
	{TOK_CC_ASSIGN_AND,		"\\&\\="},
	{TOK_CC_ASSIGN_XOR,		"\\^\\="},
	{TOK_CC_ASSIGN_OR,		"\\|\\="},
	{TOK_CC_COMMA,			"\\,"},
	{TOK_CC_HASH,			"\\#"},
	{TOK_CC_DOUBLE_HASH,		"\\#\\#"},
	{TOK_CC_BACKSLASH,		"\\\\"},

	// OTHER STUFF
	// NOTE: TOK_CC_ID must be here, after keywords, so that keywords
	//       can actually be matched.
	{TOK_CC_DIR,			"\\#($*)[_a-zA-Z0-9]+"},
	{TOK_CC_INCLUDE,		"\\#($*)include$+\\\".*?\\\""},
	{TOK_CC_INCLUDE,		"\\#($*)include$+\\<.*?\\>"},
	{TOK_CC_NEWLINE,		"\n$*"},
	{TOK_CC_ID,			"[_a-zA-Z][_0-9a-zA-Z]*"},
	{TOK_CC_ICONST,			"[1-9][0-9]*([uU][lL][lL]|[uU][lL]|[uU]|[lLlL]|[lL])?"},
	{TOK_CC_ICONST,			"0[0-7]*([uU][lL][lL]|[uU][lL]|[uU]|[lLlL]|[lL])?"},
	{TOK_CC_ICONST,			"0x[0-9a-fA-F]+([uU][lL][lL]|[uU][lL]|[uU]|[lLlL]|[lL])?"},
	{TOK_CC_FCONST,			"[0-9]*\\.[0-9]+([eE][-+][0-9]*)?[fF]?"},
	{TOK_CC_CHARCONST,		"L?('\\\\.'|'.')"},
	{TOK_CC_STRING,			"\\\"(\\\\.|.)*?\\\""},

	// WHITESPACE
	{TOK_WHITESPACE,		"\\\\\n"},
	{TOK_WHITESPACE,		"[ \t]+"},
	{TOK_WHITESPACE,		"\\/\\*.*?(\\*\\/)"},
	{TOK_CC_NEWLINE,		"\\/\\/.*?\n"},
	{TOK_WHITESPACE,		"\x0C"},
	
	// LIST TERMINATOR
	{TOK_ENDLIST},
};

typedef struct
{
	int type;
	const char *kw;
} KeywordSpec;

static KeywordSpec keywordSpecs[] = {
	{TOK_CC_AUTO,			"auto"},
	{TOK_CC_BREAK,			"break"},
	{TOK_CC_CASE,			"case"},
	{TOK_CC_CHAR,			"char"},
	{TOK_CC_CONST,			"const"},
	{TOK_CC_CONTINUE,		"continue"},
	{TOK_CC_DEFAULT,		"default"},
	{TOK_CC_DO,			"do"},
	{TOK_CC_DOUBLE,			"double"},
	{TOK_CC_ELSE,			"else"},
	{TOK_CC_ENUM,			"enum"},
	{TOK_CC_EXTERN,			"extern"},
	{TOK_CC_FLOAT,			"float"},
	{TOK_CC_FOR,			"for"},
	{TOK_CC_GOTO,			"goto"},
	{TOK_CC_IF,			"if"},
	{TOK_CC_INLINE,			"inline"},
	{TOK_CC_INT,			"int"},
	{TOK_CC_LONG,			"long"},
	{TOK_CC_REGISTER,		"register"},
	{TOK_CC_RESTRICT,		"restrict"},
	{TOK_CC_RETURN,			"return"},
	{TOK_CC_SHORT,			"short"},
	{TOK_CC_SIGNED,			"signed"},
	{TOK_CC_SIZEOF,			"sizeof"},
	{TOK_CC_STATIC,			"static"},
	{TOK_CC_STRUCT,			"struct"},
	{TOK_CC_SWITCH,			"switch"},
	{TOK_CC_TYPEDEF,		"typedef"},
	{TOK_CC_UNION,			"union"},
	{TOK_CC_UNSIGNED,		"unsigned"},
	{TOK_CC_VOID,			"void"},
	{TOK_CC_VOLATILE,		"volatile"},
	{TOK_CC_WHILE,			"while"},
	{TOK_CC_ALIGNAS,		"_Alignas"},
	{TOK_CC_ALIGNOF,		"_Alignof"},
	{TOK_CC_ATOMIC,			"_Atomic"},
	{TOK_CC_BOOL,			"_Bool"},
	{TOK_CC_COMPLEX,		"_Complex"},
	{TOK_CC_GENERIC,		"_Generic"},
	{TOK_CC_IMAGINARY,		"_Imaginary"},
	{TOK_CC_NORETURN,		"_Noreturn"},
	{TOK_CC_STATIC_ASSERT,		"_Static_assert"},
	{TOK_CC_THREAD_LOCAL,		"_Thread_local"},
	
	// COMPTOOLS-SPECIFIC
	{TOK_CC_ATTRIBUTE,		"__attribute__"},
	
	// LIST TERMINATOR
	{TOK_ENDLIST}
};

Token* cppTokenize(const char *data, const char *filename)
{
	if (ccTokens[0].regex == NULL)
	{
		lexCompileTokenSpecs(ccTokens);
	};
	
	return lexTokenize(ccTokens, data, filename);
};

CPP* cppNew()
{
	CPP *cpp = (CPP*) malloc(sizeof(CPP));
	memset(cpp, 0, sizeof(CPP));
	
	cpp->macros = hashtabNew();
	
	return cpp;
};

void cppDefine(CPP *cpp, const char *name, const char *str)
{
	if (hashtabHasKey(cpp->macros, name))
	{
		const char *format = "macro `%s' already defined";
		char *errmsg = (char*) malloc(strlen(format) + strlen(name));
		sprintf(errmsg, format, name);
		
		conDiag(name, 1, 0, 0, CON_ERROR, errmsg);
		free(errmsg);
		return;
	};
	
	Token *tokens = cppTokenize(str, name);
	MacroDef *def = (MacroDef*) malloc(sizeof(MacroDef));
	memset(def, 0, sizeof(MacroDef));
	def->name = strdup(name);
	def->argNames = NULL;
	def->numArgs = 0;
	def->tokens = tokens;
	
	hashtabSet(cpp->macros, name, def);
};

static void cppAppend(CPP *cpp, Token *token, Token *parent)
{
	Token *newtok = (Token*) malloc(sizeof(Token));
	memset(newtok, 0, sizeof(Token));
	
	newtok->filename = strdup(token->filename);
	newtok->lineno = token->lineno;
	newtok->col = token->col;
	newtok->type = token->type;
	newtok->value = strdup(token->value);
	newtok->prevType = token->prevType;
	newtok->parent = parent;
	
	if (cpp->last == NULL)
	{
		cpp->first = cpp->last = newtok;
	}
	else
	{
		cpp->last->next = newtok;
		cpp->last = newtok;
	};
	
	if (token->type == TOK_CC_ID)
	{
		KeywordSpec *spec;
		for (spec=keywordSpecs; spec->type!=TOK_ENDLIST; spec++)
		{
			if (strcmp(newtok->value, spec->kw) == 0)
			{
				newtok->type = spec->type;
				break;
			};
		};
	};
};

static void cppDiag(CPPContext *ctx, int level, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	
	conDiagAP(ctx->token->filename, ctx->token->lineno, ctx->token->col, strlen(ctx->token->value), level, fmt, ap);
	va_end(ap);
	
	for (ctx=ctx->prev; ctx!=NULL; ctx=ctx->prev)
	{
		lexDiag(ctx->token, CON_NOTE, "...expanded from here");
	};
};

static int cppIsDir(Token *token, const char *dirname)
{
	if (token->type != TOK_CC_DIR) return 0;
	
	// the TOK_CC_DIR token has the form '#' <whitespace> <name>
	// so skip over the '#' and then any whitespace
	const char *scan = token->value;
	scan++;
	while (*scan == ' ' || *scan == '\t') scan++;
	
	// now compare
	return strcmp(scan, dirname) == 0;
};

static void cppExpandMacro(CPPContext *ctx, CPP *cpp, MacroDef *def, int depth)
{
	Token *basetok = ctx->token;
	
	if (depth == 32)
	{
		cppDiag(ctx, CON_ERROR, "maximum macro replacement depth has been exceeded");
		return;
	};

	HashTable *args = hashtabNew();
	if (def->func)
	{
		ctx->token = ctx->token->next;
		if (ctx->token->type != TOK_CC_LPAREN)
		{
			cppAppend(cpp, basetok, basetok->parent);
			return;
		};
		
		int i;
		for (i=0; i<def->numArgs; i++)
		{
			if (ctx->token->type == TOK_CC_RPAREN)
			{
				cppDiag(ctx, CON_ERROR, "not enough arguments to function-like macro `%s'", def->name);
				return;
			};
			
			ctx->token = ctx->token->next;
			int parenDepth = 0;
			
			Token *first = NULL;
			Token *last = NULL;
			
			while ((ctx->token->type != TOK_CC_COMMA && ctx->token->type != TOK_CC_RPAREN) || parenDepth != 0)
			{
				Token *copy = (Token*) malloc(sizeof(Token));
				memset(copy, 0, sizeof(Token));
				copy->filename = strdup(ctx->token->filename);
				copy->lineno = ctx->token->lineno;
				copy->col = ctx->token->col;
				copy->value = strdup(ctx->token->value);
				copy->type = ctx->token->type;
				copy->prevType = ctx->token->prevType;
				
				if (last == NULL)
				{
					first = last = copy;
				}
				else
				{
					last->next = copy;
					last = copy;
				};
				
				if (copy->type == TOK_CC_LPAREN)
				{
					parenDepth++;
				}
				else if (copy->type == TOK_CC_RPAREN)
				{
					parenDepth--;
				};
				
				ctx->token = ctx->token->next;
			};
			
			hashtabSet(args, def->argNames[i], first);
		};
		
		if (def->vargs)
		{
			if (ctx->token->type != TOK_CC_RPAREN)
			{
				ctx->token = ctx->token->next;
				int parenDepth = 0;
				
				Token *first = NULL;
				Token *last = NULL;
				
				while (ctx->token->type != TOK_CC_RPAREN || parenDepth != 0)
				{
					Token *copy = (Token*) malloc(sizeof(Token));
					memset(copy, 0, sizeof(Token));
					copy->filename = strdup(ctx->token->filename);
					copy->lineno = ctx->token->lineno;
					copy->col = ctx->token->col;
					copy->value = strdup(ctx->token->value);
					copy->type = ctx->token->type;
					copy->prevType = ctx->token->prevType;
					
					if (last == NULL)
					{
						first = last = copy;
					}
					else
					{
						last->next = copy;
						last = copy;
					};
					
					if (copy->type == TOK_CC_LPAREN)
					{
						parenDepth++;
					}
					else if (copy->type == TOK_CC_RPAREN)
					{
						parenDepth--;
					};
					
					ctx->token = ctx->token->next;
				};
				
				hashtabSet(args, "__VA_ARGS__", first);
			}
			else
			{
				hashtabSet(args, "__VA_ARGS__", NULL);
			};
		}
		else
		{
			if (ctx->token->type != TOK_CC_RPAREN)
			{
				cppDiag(ctx, CON_ERROR, "expected `)' not `%s' (perhaps you are passing too many arguments to the function-like macro `%s'?)", ctx->token->value, def->name);
				return;
			};
		};
	};
	
	Token *tok;
	for (tok=def->tokens; tok!=NULL; tok=tok->next)
	{
		if (tok->type == TOK_END) break;
		
		if (tok->type == TOK_CC_DIR)
		{	
			const char *argname = tok->value;
			argname++;
			while (*argname == ' ' || *argname == '\t') argname++;
			
			if (*argname == 0)
			{
				CPPContext subctx;
				subctx.prev = ctx;
				subctx.token = tok;
				cppDiag(&subctx, CON_ERROR, "`#' without parameter");
				break;
			};
			
			if (!hashtabHasKey(args, argname))
			{
				CPPContext subctx;
				subctx.prev = ctx;
				subctx.token = tok;
				cppDiag(&subctx, CON_ERROR, "no parameter named `%s'", argname);
				break;
			};
			
			Token *toklist = (Token*) hashtabGet(args, argname);
			int count = 0;
			
			Token *scan;
			for (scan=toklist; scan!=NULL; scan=scan->next)
			{
				if (scan->prevType == TOK_WHITESPACE) count++;
				count += strlen(scan->value);
			};
			
			char *buffer = (char*) malloc(2*count+3);
			char *put = buffer;
			*put++ = '"';
			
			for (scan=toklist; scan!=NULL; scan=scan->next)
			{
				if (scan->prevType == TOK_WHITESPACE) *put++ = ' ';
				
				const char *s = scan->value;
				while (*s != 0)
				{
					if (*s == '\\')
					{
						*put++ = '\\';
					};
					
					*put++ = *s++;
				};
			};
			
			strcpy(put, "\"");
			
			Token pseudotok;
			memset(&pseudotok, 0, sizeof(Token));
			pseudotok.filename = tok->filename;
			pseudotok.lineno = tok->lineno;
			pseudotok.col = tok->col;
			pseudotok.value = buffer;
			pseudotok.type = TOK_CC_STRING;
			cppAppend(cpp, &pseudotok, basetok);
			
			free(buffer);
		}
		else if (tok->type == TOK_CC_ID && hashtabHasKey(args, tok->value))
		{
			Token *scan;
			for (scan=(Token*) hashtabGet(args, tok->value); scan!=NULL; scan=scan->next)
			{
				cppAppend(cpp, scan, basetok);
			};
		}
		else if (tok->type == TOK_CC_ID && hashtabHasKey(cpp->macros, tok->value) && strcmp(tok->value, def->name) != 0)
		{
			MacroDef *subdef = (MacroDef*) hashtabGet(cpp->macros, tok->value);
			CPPContext subctx;
			subctx.prev = ctx;
			subctx.token = tok;
			cppExpandMacro(&subctx, cpp, subdef, depth+1);
			if (conGetError() != 0) return;
			tok = subctx.token;
		}
		else
		{
			cppAppend(cpp, tok, basetok);
		};
	};
	
	hashtabDelete(args);
};

static int cppEvalExpr(CPPContext *ctx, CPP *cpp);

static int cppEvalPrimary(CPPContext *ctx, CPP *cpp)
{
	// primary-expression ::= constant
	//		| identifier
	//		| '(' expression ')'
	if (ctx->token->type == TOK_CC_ICONST)
	{
		int result = (int) strtoul(ctx->token->value, NULL, 0);
		ctx->token = ctx->token->next;
		return result;
	}
	else if (ctx->token->type == TOK_CC_CHARCONST)
	{
		const char *scan = ctx->token->value;
		if (*scan == 'L') scan++;
		
		int value = 0;
		
		scan++;		// skip over '
		if (*scan == '\\')
		{
			scan++;
			int value = lexGetCharByEscape(*scan);
			if (value == -1)
			{
				if (conGetError() == 0) cppDiag(ctx, CON_ERROR, "invalid escape sequence `\\%c'", *scan);
				return 0;
			};
		}
		else
		{
			value = (int) *scan;
		};
		
		ctx->token = ctx->token->next;
		return value;
	}
	else if (ctx->token->type == TOK_CC_ID)
	{
		if (!hashtabHasKey(cpp->macros, ctx->token->value))
		{
			// undefined macros evaluate to 0
			ctx->token = ctx->token->next;
			return 0;
		};
		
		const char *macroName = ctx->token->value;
		MacroDef *def = (MacroDef*) hashtabGet(cpp->macros, macroName);
		
		// evaluate it
		Token *orgFirst = cpp->first;
		Token *orgLast = cpp->last;
		
		cpp->first = cpp->last = NULL;
		cppExpandMacro(ctx, cpp, def, 0);
		if (conGetError() != 0) return 0;
		Token *endTok = (Token*) malloc(sizeof(Token));
		memset(endTok, 0, sizeof(Token));
		endTok->next = endTok;
		endTok->filename = strdup(ctx->token->filename);
		endTok->lineno = ctx->token->lineno;
		endTok->col = ctx->token->col;
		endTok->value = strdup("");
		endTok->type = TOK_END;
		if (cpp->last == NULL)
		{
			cpp->first = cpp->last = endTok;
		}
		else
		{
			cpp->last->next = endTok;
		};
		
		CPPContext subctx;
		subctx.prev = ctx;
		subctx.token = cpp->first;
		
		cpp->first = orgFirst;
		cpp->last = orgLast;
		
		int result = cppEvalExpr(&subctx, cpp);
		
		// return the result and skip over the identifier / parenthesis closing
		ctx->token = ctx->token->next;
		return result;
		
	}
	else if (ctx->token->type == TOK_CC_LPAREN)
	{
		ctx->token = ctx->token->next;
		int result = cppEvalExpr(ctx, cpp);
		if (ctx->token->type != TOK_CC_RPAREN)
		{
			if (conGetError() == 0) cppDiag(ctx, CON_ERROR, "expected `)', not `%s'", ctx->token->value);
			return 0;
		};
		
		ctx->token = ctx->token->next;
		return result;
	}
	else
	{
		if (conGetError() == 0) cppDiag(ctx, CON_ERROR, "expected an integer constant or identifier, not `%s' (in preprocessor-time evaluation)", ctx->token->value);
		return 0;
	};
};

static int cppEvalUnary(CPPContext *ctx, CPP *cpp)
{
	// unary-expression ::= primary-expression
	//		| unary-operator unary-expression
	//		| 'defined' ['('] identifier [')']
	// unary-operator ::= '+' | '-' | '!' | '~'
	if (ctx->token->type == TOK_CC_PLUS)
	{
		ctx->token = ctx->token->next;
		return cppEvalUnary(ctx, cpp);
	}
	else if (ctx->token->type == TOK_CC_MINUS)
	{
		ctx->token = ctx->token->next;
		return -cppEvalUnary(ctx, cpp);
	}
	else if (ctx->token->type == TOK_CC_EXCL)
	{
		ctx->token = ctx->token->next;
		return !cppEvalUnary(ctx, cpp);
	}
	else if (ctx->token->type == TOK_CC_TILDE)
	{
		ctx->token = ctx->token->next;
		return ~cppEvalUnary(ctx, cpp);
	}
	else if (strcmp(ctx->token->value, "defined") == 0)
	{
		ctx->token = ctx->token->next;
		
		int paren = 0;
		if (ctx->token->type == TOK_CC_LPAREN)
		{
			paren = 1;
			ctx->token = ctx->token->next;
		};
		
		int result = hashtabHasKey(cpp->macros, ctx->token->value);
		ctx->token = ctx->token->next;
		
		if (paren)
		{
			if (ctx->token->type != TOK_CC_RPAREN)
			{
				if (conGetError() == 0) cppDiag(ctx, CON_ERROR, "expected `)' for `defined' with parenthesis, not `%s'", ctx->token->value);
				return 0;
			};
			
			ctx->token = ctx->token->next;
		};
		
		return result;
	}
	else
	{
		return cppEvalPrimary(ctx, cpp);
	};
};

static int cppEvalMult(CPPContext *ctx, CPP *cpp)
{
	// multiplicative-expression ::= unary-expression
	//		| multiplicative-expression multiplicative-operator unary-expression
	// multiplicative-operator ::= '*' | '/' | '%'
	// Transformed as follows to avoid left recursion:
	// multiplicative-expression ::= unary-expression [multiplicative-tail]
	// multiplicative-tail ::= shift-operator multiplicative-expression
	int x = cppEvalUnary(ctx, cpp);
	
	while (ctx->token->type == TOK_CC_AST || ctx->token->type == TOK_CC_SLASH || ctx->token->type == TOK_CC_MODULO)
	{
		Token *optok = ctx->token;
		int type = ctx->token->type;
		ctx->token = ctx->token->next;
		int y = cppEvalMult(ctx, cpp);
		
		switch (type)
		{
		case TOK_CC_AST:
			x *= y;
			break;
		case TOK_CC_SLASH:
			if (y == 0)
			{
				ctx->token = optok;
				if (conGetError() == 0) cppDiag(ctx, CON_ERROR, "division by 0 in #if evaluation");
			}
			else x /= y;
			break;
		case TOK_CC_MODULO:
			if (y == 0)
			{
				ctx->token = optok;
				if (conGetError() == 0) cppDiag(ctx, CON_ERROR, "division by 0 in #if evaluation");
			}
			else x %= y;
			break;
		};
	};
	
	return x;
};

static int cppEvalAdd(CPPContext *ctx, CPP *cpp)
{
	// additive-expression ::= multiplicative-expression
	//		| additive-expression additive-operator multiplicative-expression
	// additive-operator ::= '+' | '-'
	// Transformed as follows to avoid left recursion:
	// additive-expression ::= multiplicative-expression [additive-tail]
	// additive-tail ::= shift-operator additive-expression
	int x = cppEvalMult(ctx, cpp);
	
	while (ctx->token->type == TOK_CC_PLUS || ctx->token->type == TOK_CC_MINUS)
	{
		int type = ctx->token->type;
		ctx->token = ctx->token->next;
		int y = cppEvalAdd(ctx, cpp);
		
		switch (type)
		{
		case TOK_CC_PLUS:
			x += y;
			break;
		case TOK_CC_MINUS:
			x -= y;
			break;
		};
	};
	
	return x;
};

static int cppEvalShift(CPPContext *ctx, CPP *cpp)
{
	// shift-expression ::= additive-expression
	//		| shift-expression shift-operator additive-expression
	// shift-operator ::= '<<' | '>>'
	// Transformed as follows to avoid left recursion:
	// shift-expression ::= additive-expression [shift-tail]
	// shift-tail ::= shift-operator shift-expression
	int x = cppEvalAdd(ctx, cpp);
	
	while (ctx->token->type == TOK_CC_LSHIFT || ctx->token->type == TOK_CC_RSHIFT)
	{
		int type = ctx->token->type;
		ctx->token = ctx->token->next;
		int y = cppEvalShift(ctx, cpp);
		
		switch (type)
		{
		case TOK_CC_LSHIFT:
			x = (x << y);
			break;
		case TOK_CC_RSHIFT:
			x = (x >> y);
			break;
		};
	};
	
	return x;
};

static int cppEvalRelational(CPPContext *ctx, CPP *cpp)
{
	// relational-expression ::= shift-expression
	//			| relational-expression relational-operator shift-expression
	// relational-operator ::= '<' | '>' | '<=' | '>='
	// Transformed as follows to avoid left recursion:
	// relational-expression ::= shift-expression [relational-tail]
	// relational-tail ::= relational-operator relational-expression
	int x = cppEvalShift(ctx, cpp);
	
	while (ctx->token->type == TOK_CC_LARROW || ctx->token->type == TOK_CC_RARROW
		|| ctx->token->type == TOK_CC_LARROW_EQ || ctx->token->type == TOK_CC_RARROW_EQ)
	{
		int type = ctx->token->type;
		ctx->token = ctx->token->next;
		int y = cppEvalRelational(ctx, cpp);
		
		switch (type)
		{
		case TOK_CC_LARROW:
			x = (x < y);
			break;
		case TOK_CC_RARROW:
			x = (x > y);
			break;
		case TOK_CC_LARROW_EQ:
			x = (x <= y);
			break;
		case TOK_CC_RARROW_EQ:
			x = (x >= y);
			break;
		};
	};
	
	return x;
};

static int cppEvalEquality(CPPContext *ctx, CPP *cpp)
{
	// equality-expression ::= relational-expression
	//		| equality-expression '==' relational-expression
	//		| equality-expression '!=' relational-expression
	// Transformed as follows to avoid left recursion:
	// equality-expression ::= relational-expression [equality-tail]
	// equality-tail ::= ('==' | '!=') equality-expression
	int x = cppEvalRelational(ctx, cpp);
	
	while (ctx->token->type == TOK_CC_EQ || ctx->token->type == TOK_CC_NEQ)
	{
		int type = ctx->token->type;
		ctx->token = ctx->token->next;
		int y = cppEvalEquality(ctx, cpp);
		
		if (type == TOK_CC_EQ)
		{
			x = (x == y);
		}
		else
		{
			x = (x != y);
		};
	};
	
	return x;
};

static int cppEvalBitwiseAND(CPPContext *ctx, CPP *cpp)
{
	// bitwise-AND-expression ::= equality-expression
	//			| bitwise-AND-expression '&&' equality-expression
	// Transformed as follows to avoid left recursion:
	// bitwise-AND-expression ::= equality-expression [AND-tail]
	// AND-tail ::= '&' equality-expression
	int x = cppEvalEquality(ctx, cpp);
	
	while (ctx->token->type == TOK_CC_BAND)
	{
		ctx->token = ctx->token->next;
		int y = cppEvalBitwiseAND(ctx, cpp);
		x &= y;
	};
	
	return x;
};

static int cppEvalBitwiseXOR(CPPContext *ctx, CPP *cpp)
{
	// bitwise-XOR-expression ::= bitwise-AND-expression
	//			| bitwise-XOR-expression '^' bitwise-AND-expression
	// Transformed as follows to avoid left recursion:
	// bitwise-XOR-expression ::= bitwise-AND-expression [XOR-tail]
	// XOR-tail ::= '^' bitwise-AND-expression
	int x = cppEvalBitwiseAND(ctx, cpp);
	
	while (ctx->token->type == TOK_CC_XOR)
	{
		ctx->token = ctx->token->next;
		int y = cppEvalBitwiseXOR(ctx, cpp);
		x ^= y;
	};
	
	return x;
};

static int cppEvalBitwiseOR(CPPContext *ctx, CPP *cpp)
{
	// bitwise-OR-expression ::= bitwise-XOR-expression
	//			| bitwise-OR-expression '|' bitwise-XOR-expression
	// Transformed as follows to avoid left recursion:
	// bitwise-OR-expression ::= bitwise-XOR-expression [OR-tail]
	// OR-tail ::= '|' bitwise-OR-expression
	int x = cppEvalBitwiseXOR(ctx, cpp);
	
	while (ctx->token->type == TOK_CC_BOR)
	{
		ctx->token = ctx->token->next;
		int y = cppEvalBitwiseOR(ctx, cpp);
		x |= y;
	};
	
	return x;
};

static int cppEvalLogicalAND(CPPContext *ctx, CPP *cpp)
{
	// logical-AND-expression ::= bitwise-OR-expression
	//			| logical-AND-expression '&&' bitwise-OR-expression
	// Transformed as follows to avoid left recursion:
	// logical-AND-expression ::= bitwise-OR-expression [AND-tail]
	// AND-tail ::= '&&' logical-AND-expression
	int x = cppEvalBitwiseOR(ctx, cpp);
	
	while (ctx->token->type == TOK_CC_LAND)
	{
		ctx->token = ctx->token->next;
		int y = cppEvalLogicalAND(ctx, cpp);
		x = x && y;
	};
	
	return x;
};

static int cppEvalLogicalOR(CPPContext *ctx, CPP *cpp)
{
	// logical-OR-expression ::= logical-AND-expression
	//			| logical-OR-expression '||' logical-AND-expression
	// Transformed as follows to avoid left recursion:
	// logical-OR-expression ::= logical-AND-expression [OR-tail]
	// OR-tail ::= '||' logical-OR-expression
	int x = cppEvalLogicalAND(ctx, cpp);
	
	while (ctx->token->type == TOK_CC_LOR)
	{
		ctx->token = ctx->token->next;
		int y = cppEvalLogicalOR(ctx, cpp);
		x = x || y;
	};
	
	return x;
};

static int cppEvalConditional(CPPContext *ctx, CPP *cpp)
{
	// conditional-expression ::= logical-OR-expression
	//			| logical-OR-expression '?' expression ':' logical-OR-expression
	int a = cppEvalLogicalOR(ctx, cpp);
	
	if (ctx->token->type == TOK_CC_QUEST)
	{
		ctx->token = ctx->token->next;
		int trueval = cppEvalExpr(ctx, cpp);
		
		if (ctx->token->type != TOK_CC_COLON)
		{
			if (conGetError() == 0) cppDiag(ctx, CON_ERROR, "expected `:', not `%s'", ctx->token->value);
			return 0;
		};
		
		ctx->token = ctx->token->next;
		
		int falseval = cppEvalLogicalOR(ctx, cpp);
		
		if (a) return trueval;
		else return falseval;
	};
	
	return a;
};

static int cppEvalExpr(CPPContext *ctx, CPP *cpp)
{
	// constant-expression ::= conditional-expression
	return cppEvalConditional(ctx, cpp);
};

static void cppRunRecur(CPPContext *prevCTX, CPP *cpp, const char *data, const char *name, int incdepth)
{
	if (incdepth == 16)
	{
		cppDiag(prevCTX, CON_ERROR, "maximum inclusion depth exceeded");
		return;
	};
	
	CPPContext ctx;
	ctx.prev = prevCTX;
	ctx.token = cppTokenize(data, name);
	if (conGetError() != 0) return;
	
	uint64_t activeFlags = 1;
	uint64_t hadTruth = 1;
	int depth = 0;
	
	while (1)
	{
		int currentActive = !!(activeFlags == (1UL << (depth+1))-1);
		
		uint64_t mask = (1UL << (depth))-1;
		int prevActive = !!((activeFlags & mask) == mask);

		if (ctx.token->type == TOK_END)
		{
			if (depth != 0)
			{
				cppDiag(&ctx, CON_ERROR, "unterminated #if");
			};
			
			if (ctx.prev == NULL)
			{
				cppAppend(cpp, ctx.token, NULL);
			};
			
			break;
		}
		else if (ctx.token->type == TOK_CC_DIR)
		{
			if (cppIsDir(ctx.token, "define"))
			{
				if (currentActive)
				{
					ctx.token = ctx.token->next;
					if (ctx.token->type != TOK_CC_ID)
					{
						cppDiag(&ctx, CON_ERROR, "expected identifier after `#define', not `%s'", ctx.token->value);
						break;
					};
					
					Token *endTok = (Token*) malloc(sizeof(Token));
					memset(endTok, 0, sizeof(Token));
					endTok->next = endTok;
					endTok->value = strdup("");
					endTok->type = TOK_END;
					
					Token *nameTok = ctx.token;
					const char *macroName = ctx.token->value;
					ctx.token = ctx.token->next;
										
					MacroDef *def = (MacroDef*) malloc(sizeof(MacroDef));
					memset(def, 0, sizeof(MacroDef));
					def->name = strdup(macroName);
					def->argNames = NULL;
					def->numArgs = 0;
					def->tokens = endTok;
					Token *last = NULL;
					
					if (ctx.token->type == TOK_CC_LPAREN && ctx.token->prevType != TOK_WHITESPACE)
					{
						def->func = 1;
						ctx.token = ctx.token->next;
						
						int keepGoing = (ctx.token->type != TOK_CC_RPAREN);
						while (keepGoing)
						{
							if (ctx.token->type == TOK_CC_ELLIPSIS)
							{
								ctx.token = ctx.token->next;
								if (ctx.token->type != TOK_CC_RPAREN)
								{
									cppDiag(&ctx, CON_ERROR, "expected `)' after `...', not `%s'", ctx.token->value);
									return;
								};
								
								def->vargs = 1;
								ctx.token = ctx.token->next;
								break;
							}
							else if (ctx.token->type != TOK_CC_ID)
							{
								cppDiag(&ctx, CON_ERROR, "expected an identifier for macro argument name, not `%s'", ctx.token->value);
								return;
							};
							
							const char *argname = ctx.token->value;
							int i;
							for (i=0; i<def->numArgs; i++)
							{
								if (strcmp(def->argNames[i], argname) == 0)
								{
									cppDiag(&ctx, CON_ERROR, "argument name `%s' already in use", argname);
									return;
								};
							};
							
							int index = def->numArgs++;
							def->argNames = (char**) realloc(def->argNames, sizeof(void*) * def->numArgs);
							def->argNames[index] = strdup(argname);
							
							ctx.token = ctx.token->next;
							switch (ctx.token->type)
							{
							case TOK_CC_RPAREN:
								keepGoing = 0;
								/* no break */
							case TOK_CC_COMMA:
								ctx.token = ctx.token->next;
								break;
							default:
								cppDiag(&ctx, CON_ERROR, "expected `,' or `)', not `%s'", ctx.token->value);
								return;
							};
						};
					};
					
					while (ctx.token->type != TOK_END && ctx.token->type != TOK_CC_NEWLINE)
					{
						Token *newtok = (Token*) malloc(sizeof(Token));
						memset(newtok, 0, sizeof(Token));
						
						newtok->next = endTok;
						newtok->filename = strdup(ctx.token->filename);
						newtok->lineno = ctx.token->lineno;
						newtok->col = ctx.token->col;
						newtok->type = ctx.token->type;
						newtok->value = strdup(ctx.token->value);
						newtok->prevType = ctx.token->prevType;
						
						if (last == NULL)
						{
							def->tokens = last = newtok;
						}
						else
						{
							last->next = newtok;
							last = newtok;
						};
						
						ctx.token = ctx.token->next;
					};
					
					endTok->filename = strdup(ctx.token->filename);
					endTok->lineno = ctx.token->lineno;
					endTok->col = ctx.token->col;
					
					if (hashtabHasKey(cpp->macros, macroName))
					{
						MacroDef *old = (MacroDef*) hashtabGet(cpp->macros, macroName);
						int match = 1;
						if (old->numArgs != def->numArgs || old->func != def->func)
						{
							match = 0;
						};
						
						Token *oldTok = old->tokens;
						Token *newTok = def->tokens;
						
						while (oldTok->type != TOK_END || newTok->type != TOK_END)
						{
							if (strcmp(oldTok->value, newTok->value) != 0)
							{
								match = 0;
								break;
							};
							
							oldTok = oldTok->next;
							newTok = newTok->next;
						};
						
						if (!match)
						{
							Token *thisTok = ctx.token;
							ctx.token = nameTok;
							cppDiag(&ctx, CON_WARNING, "incompatible redefinition of macro `%s'", macroName);
							ctx.token = thisTok;
							hashtabSet(cpp->macros, macroName, def);
						};
					}
					else
					{
						hashtabSet(cpp->macros, macroName, def);
					};
					
					// skip over newline
					ctx.token = ctx.token->next;
				}
				else
				{
					// skip the whole line
					while (ctx.token->type != TOK_END && ctx.token->type != TOK_CC_NEWLINE)
					{
						ctx.token = ctx.token->next;
					};
					
					// skip over newline
					ctx.token = ctx.token->next;
				};
			}
			else if (cppIsDir(ctx.token, "ifdef") || cppIsDir(ctx.token, "ifndef"))
			{
				const char *kind = ctx.token->value;
				int negate = 0;
				if (cppIsDir(ctx.token, "ifndef"))
				{
					negate = 1;
				};
				
				Token *baseToken = ctx.token;
				
				ctx.token = ctx.token->next;
				
				if (ctx.token->type != TOK_CC_ID)
				{
					cppDiag(&ctx, CON_ERROR, "expected an identifier after `%s', not `%s'", kind, ctx.token->value);
					break;
				};
				
				const char *macroName = ctx.token->value;
				ctx.token = ctx.token->next;
				
				if (ctx.token->type != TOK_CC_NEWLINE)
				{
					cppDiag(&ctx, CON_ERROR, "expected end-of-line, not `%s'", ctx.token->value);
					break;
				};
				
				ctx.token = ctx.token->next;
				
				if (depth == 63)
				{
					ctx.token = baseToken;
					cppDiag(&ctx, CON_ERROR, "the maximum `#if' nesting depth has been exceeded");
					break;
				};
				
				depth++;
				
				int cond = !!(hashtabHasKey(cpp->macros, macroName));
				if (cond == (!negate))
				{
					activeFlags |= (1UL << depth);
					hadTruth |= (1UL << depth);
				};
			}
			else if (cppIsDir(ctx.token, "endif"))
			{
				if (depth == 0)
				{
					cppDiag(&ctx, CON_ERROR, "`#endif' without a matching `#if'");
					break;
				};
				
				activeFlags &= ~(1UL << depth);
				hadTruth &= ~(1UL << depth);
				depth--;
				
				ctx.token = ctx.token->next;
			}
			else if (cppIsDir(ctx.token, "else"))
			{
				ctx.token = ctx.token->next;
				if (ctx.token->type != TOK_CC_NEWLINE)
				{
					cppDiag(&ctx, CON_ERROR, "expected end-of-line, not `%s'", ctx.token->value);
					break;
				};
				
				ctx.token = ctx.token->next;
				if (!currentActive && ((hadTruth & (1UL << depth)) == 0))
				{
					activeFlags |= (1UL << depth);
					hadTruth |= (1UL << depth);
				}
				else
				{
					activeFlags &= ~(1UL << depth);
				};
			}
			else if (cppIsDir(ctx.token, "error"))
			{
				if (currentActive)
				{
					cppDiag(&ctx, CON_ERROR, "#error directive");
					break;
				}
				else
				{
					while (ctx.token->type != TOK_END && ctx.token->type != TOK_CC_NEWLINE)
					{
						ctx.token = ctx.token->next;
					};
					
					// skip over newline
					ctx.token = ctx.token->next;
				};
			}
			else if (cppIsDir(ctx.token, "warning"))
			{
				if (currentActive)
				{
					cppDiag(&ctx, CON_WARNING, "#warning directive");
					break;
				}
				else
				{
					while (ctx.token->type != TOK_END && ctx.token->type != TOK_CC_NEWLINE)
					{
						ctx.token = ctx.token->next;
					};
					
					// skip over newline
					ctx.token = ctx.token->next;
				};
			}
			else if (cppIsDir(ctx.token, "undef"))
			{
				ctx.token = ctx.token->next;
				
				if (ctx.token->type != TOK_CC_ID)
				{
					cppDiag(&ctx, CON_ERROR, "expected an identifier after `#undef', not `%s'", ctx.token->value);
					break;
				};
				
				const char *macroName = ctx.token->value;
				ctx.token = ctx.token->next;
				
				if (ctx.token->type != TOK_CC_NEWLINE)
				{
					cppDiag(&ctx, CON_ERROR, "expected end-of-line, not `%s'", ctx.token->value);
					break;
				};
				
				ctx.token = ctx.token->next;
				
				if (currentActive) hashtabUnset(cpp->macros, macroName);
			}
			else if (cppIsDir(ctx.token, "if") || cppIsDir(ctx.token, "elif"))
			{
				Token *baseToken = ctx.token;
				ctx.token = ctx.token->next;
				int value;
				
				if (prevActive)
				{
					value = cppEvalExpr(&ctx, cpp);
					if (conGetError() != 0) break;
					if (ctx.token->type != TOK_CC_NEWLINE)
					{
						cppDiag(&ctx, CON_ERROR, "expected end-of-line, not `%s'", ctx.token->value);
						break;
					};
				}
				else
				{
					while (ctx.token->type != TOK_END && ctx.token->type != TOK_CC_NEWLINE)
					{
						ctx.token = ctx.token->next;
					};
					
					value = 0;
				};
				
				// skip over the new-line
				ctx.token = ctx.token->next;
				
				if (cppIsDir(baseToken, "if"))
				{
					if (depth == 63)
					{
						ctx.token = baseToken;
						cppDiag(&ctx, CON_ERROR, "the maximum `#if' nesting depth has been exceeded");
						break;
					};
					
					depth++;
					if (value)
					{
						activeFlags |= (1UL << depth);
						hadTruth |= (1UL << depth);
					};
				}
				else
				{
					int shouldSwitch = !(hadTruth & (1UL << depth));
					if (value && !currentActive && shouldSwitch)
					{
						activeFlags |= (1UL << depth);
						hadTruth |= (1UL << depth);
					}
					else
					{
						activeFlags &= ~(1UL << depth);
					};
				};
			}
			else if (cppIsDir(ctx.token, "include"))
			{
				ctx.token = ctx.token->next;
				if (ctx.token->type != TOK_CC_ID)
				{
					cppDiag(&ctx, CON_ERROR, "expected an identifier, not `%s'", ctx.token->value);
					break;
				};
				
				const char *macroName = ctx.token->value;
				
				if (currentActive)
				{
					if (!hashtabHasKey(cpp->macros, macroName))
					{
						cppDiag(&ctx, CON_ERROR, "undefined macro `%s' used in `#include' directive", macroName);
						break;
					};
					
					MacroDef *def = (MacroDef*) hashtabGet(cpp->macros, macroName);
					if (def->func)
					{
						cppDiag(&ctx, CON_ERROR, "function-like macro used in `#include' directive");
						break;
					};
					
					if (def->tokens == NULL || def->tokens->type != TOK_CC_STRING || def->tokens->next->type != TOK_END)
					{
						cppDiag(&ctx, CON_ERROR, "expected a string macro as parameter to `#include' directive");
						break;
					};
					
					const char *headerName = def->tokens->value;
					const char *subdata;
					const char *subfile;
					
					Error err = ERR_IO;
					if (cpp->includer != NULL)
					{
						err = cpp->includer(headerName, &subdata, &subfile);
					};
					
					if (err != ERR_OK)
					{
						cppDiag(&ctx, CON_ERROR, "cannot find header file `%s'", headerName);
						break;
					};
					
					if (subfile[0] != '<')
					{
						int depidx = cpp->numDeps++;
						cpp->deps = (char**) realloc(cpp->deps, sizeof(void*) * cpp->numDeps);
						cpp->deps[depidx] = strdup(subfile);
					};
					
					cppRunRecur(&ctx, cpp, subdata, subfile, incdepth+1);
					if (conGetError() != 0) break;
				};
				
				ctx.token = ctx.token->next;
				if (ctx.token->type != TOK_CC_NEWLINE)
				{
					cppDiag(&ctx, CON_ERROR, "expected end-of-line, not `%s'", ctx.token->value);
					break;
				};
				
				ctx.token = ctx.token->next;
			}
			else
			{
				cppDiag(&ctx, CON_ERROR, "invalid pre-processor directive `%s'", ctx.token->value);
				break;
			};
		}
		else if (ctx.token->type == TOK_CC_INCLUDE)
		{
			// find the header name; the token value has the form
			// '#' <whitespace> 'include' <headername>
			if (currentActive)
			{
				const char *headerName = ctx.token->value;
				headerName++;
				while (memcmp(headerName, "include", 7) != 0) headerName++;
				headerName += 7;
				while (*headerName == ' ' || *headerName == '\t') headerName++;
				
				const char *subdata;
				const char *subfile;
				
				Error err = ERR_IO;
				if (cpp->includer != NULL)
				{
					err = cpp->includer(headerName, &subdata, &subfile);
				};
				
				if (err != ERR_OK)
				{
					cppDiag(&ctx, CON_ERROR, "cannot find header file `%s'", headerName);
					break;
				};
				
				if (subfile[0] != '<')
				{
					int depidx = cpp->numDeps++;
					cpp->deps = (char**) realloc(cpp->deps, sizeof(void*) * cpp->numDeps);
					cpp->deps[depidx] = strdup(subfile);
				};
				
				cppRunRecur(&ctx, cpp, subdata, subfile, incdepth+1);
				if (conGetError() != 0) break;
			};
			
			ctx.token = ctx.token->next;
			
			if (ctx.token->type != TOK_CC_NEWLINE)
			{
				cppDiag(&ctx, CON_ERROR, "expected end-of-line, not `%s'", ctx.token->value);
				break;
			};
			
			ctx.token = ctx.token->next;
		}
		else
		{
			while (ctx.token->type != TOK_END && ctx.token->type != TOK_CC_NEWLINE)
			{
				if (currentActive)
				{
					if (ctx.token->type == TOK_CC_ID && hashtabHasKey(cpp->macros, ctx.token->value))
					{
						MacroDef *def = (MacroDef*) hashtabGet(cpp->macros, ctx.token->value);
						cppExpandMacro(&ctx, cpp, def, 0);
						if (conGetError() != 0) return;
					}
					else
					{
						cppAppend(cpp, ctx.token, NULL);
					};
				};
				
				ctx.token = ctx.token->next;
			};
			
			// skip over newline
			ctx.token = ctx.token->next;
		};
	};
};

Token* cppRun(CPP *cpp, const char *data, const char *name)
{
	cppRunRecur(NULL, cpp, data, name, 0);
	return cpp->first;
};
