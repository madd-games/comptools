/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <comptools/cc/cc-parse.h>
#include <comptools/console.h>

static CC_Pointer* Pointer(CC_Context *ctx);
static CC_DeclaratorTail* DeclaratorTail(CC_Context *ctx);
static CC_Declarator* Declarator(CC_Context *ctx);
static CC_AssignmentExpression* AssignmentExpression(CC_Context *ctx);
static CC_Expression* Expression(CC_Context *ctx);
static CC_CastExpression* CastExpression(CC_Context *ctx);
static CC_TypeName* TypeName(CC_Context *ctx);
static CC_AlignmentSpecifier* AlignmentSpecifier(CC_Context *ctx);
static CC_ArgumentExpressionList* ArgumentExpressionList(CC_Context *ctx);
static Token* Identifier(CC_Context *ctx);
static CC_InitializerList* InitializerList(CC_Context *ctx);
static CC_Initializer* Initializer(CC_Context *ctx);
static CC_AtomicTypeSpecifier* AtomicTypeSpecifier(CC_Context *ctx);
static CC_ConstantExpression* ConstantExpression(CC_Context *ctx);
static CC_SpecifierQualifierList* SpecifierQualifierList(CC_Context *ctx);
static CC_CompoundStatement* CompoundStatement(CC_Context *ctx);
static CC_Statement* Statement(CC_Context *ctx);

static void ccAbort(CC_Context *ctx)
{
	longjmp(ctx->retbuf, 1);
};

static void ccPushScope(CC_Context *ctx)
{
	CC_NameScope *scope = (CC_NameScope*) calloc(1, sizeof(CC_NameScope));
	scope->prev = ctx->scope;
	scope->typedefNames = hashtabClone(ctx->scope->typedefNames);
	ctx->scope = scope;
};

static void ccPopScope(CC_Context *ctx)
{
	CC_NameScope *current = ctx->scope;
	ctx->scope = current->prev;
	
	hashtabDelete(current->typedefNames);
	free(current);
};

static int ccFormatMsg(char **strp, const char *fmt, va_list ap)
{
	char buffer[256];
	int count = vsnprintf(buffer, 256, fmt, ap);
	
	if (count < 256)
	{
		*strp = strdup(buffer);
		return count;
	}
	else
	{
		*strp = (char*) malloc((size_t)count + 1);
		return vsnprintf(*strp, (size_t)count + 1, fmt, ap);
	};
};

static void ccError(CC_Context *ctx, const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	
	free(ctx->lastError);
	ctx->errToken = ctx->token;
	ccFormatMsg(&ctx->lastError, format, ap);
	
	va_end(ap);
};

static Token* ccConsume(CC_Context *ctx)
{
	Token *ret = ctx->token;
	ctx->token = ctx->token->next;
	return ret;
};

static CC_StaticAssert* StaticAssert(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_STATIC_ASSERT) return NULL;
	ccConsume(ctx);		// consume the '_Static_assert'
	
	if (ctx->token->type != TOK_CC_LPAREN)
	{
		ccError(ctx, "expected `(', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '('
	
	CC_ConstantExpression *expr = ConstantExpression(ctx);
	if (expr == NULL)
	{
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_COMMA)
	{
		ccError(ctx, "expected `,', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ','
	
	if (ctx->token->type != TOK_CC_STRING)
	{
		ccError(ctx, "expected a string literal, not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	Token *str = ccConsume(ctx);
	
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ccError(ctx, "expected `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ')'
	
	if (ctx->token->type != TOK_CC_SEMICOLON)
	{
		ccError(ctx, "expected `;', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ';'
	
	CC_StaticAssert *staticAssert = (CC_StaticAssert*) calloc(1, sizeof(CC_StaticAssert));
	staticAssert->op = org;
	staticAssert->expr = expr;
	staticAssert->str = str;
	return staticAssert;
};

static Token* StorageClassSpecifier(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_TYPEDEF:
	case TOK_CC_EXTERN:
	case TOK_CC_STATIC:
	case TOK_CC_THREAD_LOCAL:
	case TOK_CC_AUTO:
	case TOK_CC_REGISTER:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static int isTypedefName(CC_Context *ctx, const char *value)
{
	return hashtabHasKey(ctx->scope->typedefNames, value);
};

static CC_EnumeratorList* EnumeratorList(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	Token *id = Identifier(ctx);
	if (id == NULL)
	{
		return NULL;
	};
	
	CC_ConstantExpression *expr = NULL;
	if (ctx->token->type == TOK_CC_ASSIGN)
	{
		ccConsume(ctx);	// consume the '='
		
		expr = ConstantExpression(ctx);
		if (expr == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_EnumeratorList *next = NULL;
	if (ctx->token->type == TOK_CC_COMMA)
	{
		ccConsume(ctx);	// consume the ','
		
		next = EnumeratorList(ctx);	// optional
	};
	
	CC_EnumeratorList *list = (CC_EnumeratorList*) calloc(1, sizeof(CC_EnumeratorList));
	list->id = id;
	list->expr = expr;
	list->next = next;
	return list;
};

static CC_EnumSpecifier* EnumSpecifier(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_ENUM) return NULL;
	Token *keyword = ccConsume(ctx);		// consume the 'enum'
	
	Token *id = Identifier(ctx);
	if (id == NULL)
	{
		if (ctx->token->type != TOK_CC_LBRACE)
		{
			ccError(ctx, "expected an identifier or `{', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_EnumeratorList *list = NULL;
	if (ctx->token->type == TOK_CC_LBRACE)
	{
		ccConsume(ctx);	// consume the '{'
		list = EnumeratorList(ctx);
		
		if (ctx->token->type != TOK_CC_RBRACE)
		{
			ccError(ctx, "expected `}', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);	// consume the '}'
	};
	
	CC_EnumSpecifier *enumSpecifier = (CC_EnumSpecifier*) calloc(1, sizeof(CC_EnumSpecifier));
	enumSpecifier->keyword = keyword;
	enumSpecifier->id = id;
	enumSpecifier->list = list;
	return enumSpecifier;
};

static CC_StructDeclaratorList* StructDeclaratorList(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_Declarator *decl = Declarator(ctx);
	if (decl == NULL)
	{
		if (ctx->token->type != TOK_CC_COLON) return NULL;
	};
	
	CC_ConstantExpression *bits = NULL;
	if (ctx->token->type == TOK_CC_COLON)
	{
		ccConsume(ctx);		// consume the ':'
		
		bits = ConstantExpression(ctx);
		if (bits == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_StructDeclaratorList *next = NULL;
	if (ctx->token->type == TOK_CC_COMMA)
	{
		ccConsume(ctx);		// consume the ','
		
		next = StructDeclaratorList(ctx);
		if (next == NULL)
		{
			ccError(ctx, "expected a member declarator, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_StructDeclaratorList *list = (CC_StructDeclaratorList*) calloc(1, sizeof(CC_StructDeclaratorList));
	list->decl = decl;
	list->bits = bits;
	list->next = next;
	return list;
};

static CC_StructDeclarationList* StructDeclarationList(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_StaticAssert *staticAssert = StaticAssert(ctx);
	if (staticAssert != NULL)
	{
		CC_StructDeclarationList *list = (CC_StructDeclarationList*) calloc(1, sizeof(CC_StructDeclarationList));
		list->staticAssert = staticAssert;
		list->next = StructDeclarationList(ctx);
		return list;
	};
	
	CC_SpecifierQualifierList *qualifiers = SpecifierQualifierList(ctx);
	if (qualifiers == NULL) return NULL;
	
	CC_StructDeclaratorList *decls = StructDeclaratorList(ctx);
	if (ctx->token->type != TOK_CC_SEMICOLON)
	{
		ccError(ctx, "expected `;' not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ';'
	
	CC_StructDeclarationList *list = (CC_StructDeclarationList*) calloc(1, sizeof(CC_StructDeclarationList));
	list->tok = org;
	list->qualifiers = qualifiers;
	list->decls = decls;
	list->next = StructDeclarationList(ctx);
	return list;
};

static Token* StructOrUnion(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_STRUCT:
	case TOK_CC_UNION:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_StructOrUnionSpecifier* StructOrUnionSpecifier(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	Token *keyword = StructOrUnion(ctx);
	if (keyword == NULL) return NULL;
	
	Token *id = Identifier(ctx);
	if (id == NULL)
	{
		if (ctx->token->type != TOK_CC_LBRACE)
		{
			ccError(ctx, "expected an identifier or `{', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_StructDeclarationList *decls = NULL;
	if (ctx->token->type == TOK_CC_LBRACE)
	{
		ccConsume(ctx);	// consume the '{'
		
		decls = StructDeclarationList(ctx);
		if (decls == NULL)
		{
			ccError(ctx, "expected a member declaration, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		if (ctx->token->type != TOK_CC_RBRACE)
		{
			ccError(ctx, "expected `}', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);	// consume the '}'
	};
	
	CC_StructOrUnionSpecifier *compSpecifier = (CC_StructOrUnionSpecifier*) calloc(1, sizeof(CC_StructOrUnionSpecifier));
	compSpecifier->keyword = keyword;
	compSpecifier->id = id;
	compSpecifier->decls = decls;
	return compSpecifier;
};

static CC_TypeSpecifier* TypeSpecifier(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_ID:
		if (isTypedefName(ctx, ctx->token->value))
		{
			CC_TypeSpecifier *spec = (CC_TypeSpecifier*) calloc(1, sizeof(CC_TypeSpecifier));
			spec->token = ccConsume(ctx);
			return spec;
		};
		return NULL;
	case TOK_CC_VOID:
	case TOK_CC_CHAR:
	case TOK_CC_SHORT:
	case TOK_CC_INT:
	case TOK_CC_LONG:
	case TOK_CC_FLOAT:
	case TOK_CC_DOUBLE:
	case TOK_CC_SIGNED:
	case TOK_CC_UNSIGNED:
	case TOK_CC_BOOL:
	case TOK_CC_COMPLEX:
		{
			CC_TypeSpecifier *spec = (CC_TypeSpecifier*) calloc(1, sizeof(CC_TypeSpecifier));
			spec->token = ccConsume(ctx);
			return spec;
		};
	default:
		{
			CC_AtomicTypeSpecifier *atomicSpecifier = AtomicTypeSpecifier(ctx);
			if (atomicSpecifier != NULL)
			{
				CC_TypeSpecifier *spec = (CC_TypeSpecifier*) calloc(1, sizeof(CC_TypeSpecifier));
				spec->atomicSpecifier = atomicSpecifier;
				return spec;
			};
			
			CC_EnumSpecifier *enumSpecifier = EnumSpecifier(ctx);
			if (enumSpecifier != NULL)
			{
				CC_TypeSpecifier *spec = (CC_TypeSpecifier*) calloc(1, sizeof(CC_TypeSpecifier));
				spec->enumSpecifier = enumSpecifier;
				return spec;
			};
			
			CC_StructOrUnionSpecifier *compSpecifier = StructOrUnionSpecifier(ctx);
			if (compSpecifier != NULL)
			{
				CC_TypeSpecifier *spec = (CC_TypeSpecifier*) calloc(1, sizeof(CC_TypeSpecifier));
				spec->compSpecifier = compSpecifier;
				return spec;
			};
		};
		return NULL;
	};
};

static Token* TypeQualifier(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_ATOMIC:
		// _Atomic is only a type qualifier if not followed by `('
		// return NULL in that case, otherwise fall through
		if (ctx->token->next->type == TOK_CC_LPAREN) return NULL;
	case TOK_CC_CONST:
	case TOK_CC_RESTRICT:
	case TOK_CC_VOLATILE:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static Token* FunctionSpecifier(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_INLINE:
	case TOK_CC_NORETURN:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_GenericAssocList* GenericAssocList(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_TypeName *typeName;
	if (ctx->token->type == TOK_CC_DEFAULT)
	{
		ccConsume(ctx);		// consume 'default'
		typeName = NULL;
	}
	else
	{
		typeName = TypeName(ctx);
		if (typeName == NULL)
		{
			ctx->token = org;
			return NULL;
		};
	};
	
	if (ctx->token->type != TOK_CC_COLON)
	{
		ctx->token = org;
		return NULL;
	};
	
	ccConsume(ctx);			// consume ':'
	
	CC_AssignmentExpression *expr = AssignmentExpression(ctx);
	if (expr == NULL)
	{
		ctx->token = org;
		return NULL;
	};
	
	CC_GenericAssocList *next = NULL;
	if (ctx->token->type == TOK_CC_COMMA)
	{
		ccConsume(ctx);		// consume the ','
		
		next = GenericAssocList(ctx);
		if (next == NULL)
		{
			ccError(ctx, "expected a generic association, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_GenericAssocList *assoc = (CC_GenericAssocList*) calloc(1, sizeof(CC_GenericAssocList));
	assoc->typeName = typeName;
	assoc->expr = expr;
	assoc->next = next;
	return assoc;	
};

static CC_GenericSelection* GenericSelection(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_GENERIC) return NULL;
	ccConsume(ctx);		// consume '_Generic'
	
	if (ctx->token->type != TOK_CC_LPAREN)
	{
		ccError(ctx, "expected `(', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '('
	
	CC_AssignmentExpression *expr = AssignmentExpression(ctx);
	if (expr == NULL)
	{
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_COMMA)
	{
		ccError(ctx, "expected `,', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ','
	
	CC_GenericAssocList *assocs = GenericAssocList(ctx);
	if (assocs == NULL)
	{
		ccError(ctx, "expected at least one generic association, not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ccError(ctx, "expected `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ')'
	
	CC_GenericSelection *generic = (CC_GenericSelection*) calloc(1, sizeof(CC_GenericSelection));
	generic->tok = org;
	generic->expr = expr;
	generic->assocs = assocs;
	return generic;
};

static CC_PrimaryExpression* PrimaryExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	switch (ctx->token->type)
	{
	case TOK_CC_LPAREN:
		{
			ccConsume(ctx);		// consume the '('
			CC_Expression *expr = Expression(ctx);
			if (expr == NULL)
			{
				ctx->token = org;
				return NULL;
			};
			if (ctx->token->type != TOK_CC_RPAREN)
			{
				ccError(ctx, "expected `)' not `%s'", ctx->token->value);
				ctx->token = org;
				ccAbort(ctx);
				return NULL;
			};
			ccConsume(ctx);		// consume the ')'
			CC_PrimaryExpression *primary = (CC_PrimaryExpression*) calloc(1, sizeof(CC_PrimaryExpression));
			primary->expr = expr;
			return primary;
		};
	case TOK_CC_ID:
		if (isTypedefName(ctx, ctx->token->value))
		{
			ccError(ctx, "expected an identifier, constant, string literal or paranthesis, not `%s'", ctx->token->value);
			return NULL;
		};
		// no break
	case TOK_CC_ICONST:
	case TOK_CC_FCONST:
	case TOK_CC_CHARCONST:
	case TOK_CC_STRING:
		{
			CC_PrimaryExpression *primary = (CC_PrimaryExpression*) calloc(1, sizeof(CC_PrimaryExpression));
			primary->tok = ccConsume(ctx);
			return primary;
		};
	case TOK_CC_GENERIC:
		{
			CC_PrimaryExpression *primary = (CC_PrimaryExpression*) calloc(1, sizeof(CC_PrimaryExpression));
			primary->generic = GenericSelection(ctx);	// does not return NULL in this case
			return primary;
		};
	default:
		ccError(ctx, "expected an identifier, constant, string literal or paranthesis, not `%s'", ctx->token->value);
		return NULL;
	};
};

static CC_SubscriptTail* SubscriptTail(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_LSQPAREN) return NULL;
	ccConsume(ctx);		// consume the '['
	
	CC_Expression *expr = Expression(ctx);
	if (expr == NULL)
	{
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_RSQPAREN)
	{
		ccError(ctx, "expected `]', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ']'
	
	CC_SubscriptTail *subscript = (CC_SubscriptTail*) calloc(1, sizeof(CC_SubscriptTail));
	subscript->expr = expr;
	return subscript;
};

static CC_CallTail* CallTail(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_LPAREN) return NULL;
	ccConsume(ctx);		// consume the '('
	
	CC_ArgumentExpressionList *list = ArgumentExpressionList(ctx);
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ccError(ctx, "expected arguments or `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ')'
	
	CC_CallTail *call = (CC_CallTail*) calloc(1, sizeof(CC_CallTail));
	call->list = list;
	return call;
};

static Token* MemberOperator(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_MARROW:
	case TOK_CC_DOT:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_MemberTail* MemberTail(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	Token *op = MemberOperator(ctx);
	if (op == NULL) return NULL;
	
	Token *id = Identifier(ctx);
	if (id == NULL)
	{
		ccError(ctx, "expected an identifier, not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	CC_MemberTail *member = (CC_MemberTail*) calloc(1, sizeof(CC_MemberTail));
	member->op = op;
	member->id = id;
	return member;
};

static Token* PostfixOperator(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_INC:
	case TOK_CC_DEC:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_PostfixTail* PostfixTail(CC_Context *ctx)
{
	Token *tok = ctx->token;
	CC_SubscriptTail *subscript = NULL;
	CC_CallTail *call = NULL;
	CC_MemberTail *member = NULL;
	Token *op = NULL;
	
	if (
		   (subscript = SubscriptTail(ctx))
		|| (call = CallTail(ctx))
		|| (member = MemberTail(ctx))
		|| (op = PostfixOperator(ctx))
	)
	{
		CC_PostfixTail *tail = (CC_PostfixTail*) calloc(1, sizeof(CC_PostfixTail));
		tail->subscript = subscript;
		tail->call = call;
		tail->member = member;
		tail->op = op;
		tail->next = PostfixTail(ctx);
		tail->tok = tok;
		return tail;
	};
	
	return NULL;
};

static CC_PostfixExpression* PostfixExpressionAlt(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_LPAREN) return NULL;
	ccConsume(ctx);		// consume the ')'
	
	CC_TypeName *typeName = TypeName(ctx);
	if (typeName == NULL)
	{
		ctx->token = org;
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ctx->token = org;
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ')'
	
	if (ctx->token->type != TOK_CC_LBRACE)
	{
		ctx->token = org;
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '{'
	
	CC_InitializerList *initList = InitializerList(ctx);
	if (ctx->token->type != TOK_CC_RBRACE)
	{
		ctx->token = org;
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '}'
	
	CC_PostfixExpression *postfix = (CC_PostfixExpression*) calloc(1, sizeof(CC_PostfixExpression));
	postfix->typeName = typeName;
	postfix->initList = initList;
	return postfix;
};

static CC_PostfixExpression* PostfixExpression(CC_Context *ctx)
{
	CC_PrimaryExpression *primary = PrimaryExpression(ctx);
	if (primary == NULL) return PostfixExpressionAlt(ctx);
	
	CC_PostfixExpression *postfix = (CC_PostfixExpression*) calloc(1, sizeof(CC_PostfixExpression));
	postfix->primary = primary;
	postfix->tail = PostfixTail(ctx);
	return postfix;
};

static Token* UnaryOperator(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_BAND:
	case TOK_CC_AST:
	case TOK_CC_PLUS:
	case TOK_CC_MINUS:
	case TOK_CC_TILDE:
	case TOK_CC_EXCL:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_UnaryExpression* UnaryExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	Token *op = NULL;	
	CC_PostfixExpression *postfix = NULL;
	CC_UnaryExpression *sub = NULL;
	CC_TypeName *typeName = NULL;
	CC_CastExpression *cast = NULL;
	
	if (ctx->token->type == TOK_CC_INC || ctx->token->type == TOK_CC_DEC)
	{
		op = ccConsume(ctx);
		sub = UnaryExpression(ctx);
		if (sub == NULL)
		{
			ccError(ctx, "expected a unary expression, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	}
	else if ((op = UnaryOperator(ctx)) != NULL)
	{
		cast = CastExpression(ctx);
		if (cast == NULL)
		{
			ccError(ctx, "expected a cast expression, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	}
	else if (ctx->token->type == TOK_CC_SIZEOF)
	{
		op = ccConsume(ctx);
		
		Token *revert = ctx->token;
		if (ctx->token->type == TOK_CC_LPAREN)
		{
			ccConsume(ctx);		// consume the '('
			
			typeName = TypeName(ctx);
		};
		
		if (typeName == NULL)
		{
			ctx->token = revert;
			sub = UnaryExpression(ctx);
		}
		else
		{
			if (ctx->token->type != TOK_CC_RPAREN)
			{
				ccError(ctx, "expected `)' not `%s'", ctx->token->value);
				ctx->token = org;
				ccAbort(ctx);
				return NULL;
			};
			
			ccConsume(ctx);		// consume the ')'
		};
	}
	else if (ctx->token->type == TOK_CC_ALIGNOF)
	{
		op = ccConsume(ctx);
		
		if (ctx->token->type != TOK_CC_LPAREN)
		{
			ccError(ctx, "expected `(' not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);			// consume the '('
		
		typeName = TypeName(ctx);
		if (typeName == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		if (ctx->token->type != TOK_CC_RPAREN)
		{
			ccError(ctx, "expected `)', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);			// consume the ')'
	}
	else if ((postfix = PostfixExpression(ctx)) == NULL)
	{
		return NULL;
	};
	
	CC_UnaryExpression *unary = (CC_UnaryExpression*) calloc(1, sizeof(CC_UnaryExpression));
	unary->op = op;
	unary->unary = sub;
	unary->typeName = typeName;
	unary->postfix = postfix;
	unary->cast = cast;
	return unary;
};

static CC_CastExpression* CastExpression_1(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_LPAREN) return NULL;
	ccConsume(ctx);		// consume the '('
	
	CC_TypeName *typeName = TypeName(ctx);
	if (typeName == NULL)
	{
		ctx->token = org;
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ctx->token = org;
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ')'
	
	CC_CastExpression *subcast = CastExpression(ctx);
	if (subcast == NULL)
	{
		ctx->token = org;
		return NULL;
	};
	
	CC_CastExpression *cast = (CC_CastExpression*) calloc(1, sizeof(CC_CastExpression));
	cast->typeName = typeName;
	cast->cast = subcast;
	return cast;
};

static CC_CastExpression* CastExpression_2(CC_Context *ctx)
{
	CC_UnaryExpression *unary = UnaryExpression(ctx);
	if (unary == NULL) return NULL;
	
	CC_CastExpression *cast = (CC_CastExpression*) calloc(1, sizeof(CC_CastExpression));
	cast->unary = unary;
	return cast;
};

static CC_CastExpression* CastExpression(CC_Context *ctx)
{
	CC_CastExpression *cast = CastExpression_1(ctx);
	if (cast == NULL) return CastExpression_2(ctx);
	else return cast;
};

static Token* MultiplicativeOperator(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_AST:
	case TOK_CC_SLASH:
	case TOK_CC_MODULO:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_MultiplicativeExpression* MultiplicativeExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_CastExpression *cast = CastExpression(ctx);
	if (cast == NULL) return NULL;
	
	Token *op = MultiplicativeOperator(ctx);
	CC_MultiplicativeExpression *next = NULL;
	if (op != NULL)
	{
		next = MultiplicativeExpression(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_MultiplicativeExpression *mul = (CC_MultiplicativeExpression*) calloc(1, sizeof(CC_MultiplicativeExpression));
	mul->cast = cast;
	mul->op = op;
	mul->next = next;
	return mul;
};

static Token* AdditiveOperator(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_PLUS:
	case TOK_CC_MINUS:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_AdditiveExpression* AdditiveExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_MultiplicativeExpression *mul = MultiplicativeExpression(ctx);
	if (mul == NULL) return NULL;
	
	Token *op = AdditiveOperator(ctx);
	CC_AdditiveExpression *next = NULL;
	if (op != NULL)
	{
		next = AdditiveExpression(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_AdditiveExpression *add = (CC_AdditiveExpression*) calloc(1, sizeof(CC_AdditiveExpression));
	add->mul = mul;
	add->op = op;
	add->next = next;
	return add;
};

static Token* ShiftOperator(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_LSHIFT:
	case TOK_CC_RSHIFT:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_ShiftExpression* ShiftExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_AdditiveExpression *add = AdditiveExpression(ctx);
	if (add == NULL) return NULL;
	
	Token *op = ShiftOperator(ctx);
	CC_ShiftExpression *next = NULL;
	if (op != NULL)
	{
		next = ShiftExpression(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_ShiftExpression *shift = (CC_ShiftExpression*) calloc(1, sizeof(CC_ShiftExpression));
	shift->add = add;
	shift->op = op;
	shift->next = next;
	return shift;
};

static Token* RelationalOperator(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_LARROW:
	case TOK_CC_RARROW:
	case TOK_CC_LARROW_EQ:
	case TOK_CC_RARROW_EQ:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_RelationalExpression* RelationalExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_ShiftExpression *shift = ShiftExpression(ctx);
	if (shift == NULL) return NULL;
	
	Token *op = RelationalOperator(ctx);
	CC_RelationalExpression *next = NULL;
	if (op != NULL)
	{
		next = RelationalExpression(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_RelationalExpression *rel = (CC_RelationalExpression*) calloc(1, sizeof(CC_RelationalExpression));
	rel->shift = shift;
	rel->op = op;
	rel->next = next;
	return rel;
};

static Token* EqualityOperator(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_EQ:
	case TOK_CC_NEQ:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_EqualityExpression* EqualityExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_RelationalExpression *rel = RelationalExpression(ctx);
	if (rel == NULL) return NULL;
	
	Token *op = EqualityOperator(ctx);
	CC_EqualityExpression *next = NULL;
	if (op != NULL)
	{
		next = EqualityExpression(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_EqualityExpression *equality = (CC_EqualityExpression*) calloc(1, sizeof(CC_EqualityExpression));
	equality->rel = rel;
	equality->op = op;
	equality->next = next;
	return equality;
};

static CC_BitwiseANDExpression* BitwiseANDExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	Token *op = NULL;
	
	CC_EqualityExpression *equality = EqualityExpression(ctx);
	if (equality == NULL) return NULL;
	
	CC_BitwiseANDExpression *next = NULL;
	if (ctx->token->type == TOK_CC_BAND)
	{
		op = ccConsume(ctx);		// consume '&'
		
		next = BitwiseANDExpression(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_BitwiseANDExpression *bitwiseAND = (CC_BitwiseANDExpression*) calloc(1, sizeof(CC_BitwiseANDExpression));
	bitwiseAND->equality = equality;
	bitwiseAND->op = op;
	bitwiseAND->next = next;
	return bitwiseAND;
};

static CC_BitwiseXORExpression* BitwiseXORExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	Token *op = NULL;
	
	CC_BitwiseANDExpression *bitwiseAND = BitwiseANDExpression(ctx);
	if (bitwiseAND == NULL) return NULL;
	
	CC_BitwiseXORExpression *next = NULL;
	if (ctx->token->type == TOK_CC_XOR)
	{
		op = ccConsume(ctx);		// consume '^'
		
		next = BitwiseXORExpression(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_BitwiseXORExpression *bitwiseXOR = (CC_BitwiseXORExpression*) calloc(1, sizeof(CC_BitwiseXORExpression));
	bitwiseXOR->bitwiseAND = bitwiseAND;
	bitwiseXOR->op = op;
	bitwiseXOR->next = next;
	return bitwiseXOR;
};

static CC_BitwiseORExpression* BitwiseORExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	Token *op = NULL;
	
	CC_BitwiseXORExpression *bitwiseXOR = BitwiseXORExpression(ctx);
	if (bitwiseXOR == NULL) return NULL;
	
	CC_BitwiseORExpression *next = NULL;
	if (ctx->token->type == TOK_CC_BOR)
	{
		op = ccConsume(ctx);		// consume the '|'
		
		next = BitwiseORExpression(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_BitwiseORExpression *bitwiseOR = (CC_BitwiseORExpression*) calloc(1, sizeof(CC_BitwiseORExpression));
	bitwiseOR->bitwiseXOR = bitwiseXOR;
	bitwiseOR->op = op;
	bitwiseOR->next = next;
	return bitwiseOR;
};

static CC_LogicalANDExpression* LogicalANDExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	Token *op = NULL;
	
	CC_BitwiseORExpression *bitwiseOR = BitwiseORExpression(ctx);
	if (bitwiseOR == NULL) return NULL;
	
	CC_LogicalANDExpression *next = NULL;
	if (ctx->token->type == TOK_CC_LAND)
	{
		op = ccConsume(ctx);		// consume the '&&'
		
		next = LogicalANDExpression(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_LogicalANDExpression *logicalAND = (CC_LogicalANDExpression*) calloc(1, sizeof(CC_LogicalANDExpression));
	logicalAND->bitwiseOR = bitwiseOR;
	logicalAND->op = op;
	logicalAND->next = next;
	return logicalAND;
};

static CC_LogicalORExpression* LogicalORExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	Token *op = NULL;
	
	CC_LogicalANDExpression *logicalAND = LogicalANDExpression(ctx);
	if (logicalAND == NULL) return NULL;
	
	CC_LogicalORExpression *next = NULL;
	if (ctx->token->type == TOK_CC_LOR)
	{
		op = ccConsume(ctx);		// consume the '||'
		
		next = LogicalORExpression(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_LogicalORExpression *logicalOR = (CC_LogicalORExpression*) calloc(1, sizeof(CC_LogicalORExpression));
	logicalOR->logicalAND = logicalAND;
	logicalOR->op = op;
	logicalOR->next = next;
	return logicalOR;
};

static CC_ConditionalExpression* ConditionalExpression(CC_Context *ctx)
{
	Token *org = ctx->token;
	Token *op = NULL;
	
	CC_LogicalORExpression *left = LogicalORExpression(ctx);
	if (left == NULL) return NULL;
	
	CC_Expression *trueExpr = NULL;
	CC_ConditionalExpression *falseExpr = NULL;
	
	if (ctx->token->type == TOK_CC_QUEST)
	{
		op = ccConsume(ctx);		// consume the '?'
		
		trueExpr = Expression(ctx);
		if (trueExpr == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		if (ctx->token->type != TOK_CC_COLON)
		{
			ccError(ctx, "expected `:', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);		// consume the ':'
		
		falseExpr = ConditionalExpression(ctx);
		if (falseExpr == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_ConditionalExpression *cond = (CC_ConditionalExpression*) calloc(1, sizeof(CC_ConditionalExpression));
	cond->left = left;
	cond->op = op;
	cond->trueExpr = trueExpr;
	cond->falseExpr = falseExpr;
	return cond;
};

static Token* AssignmentOperator(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_ASSIGN:
	case TOK_CC_ASSIGN_AST:
	case TOK_CC_ASSIGN_SLASH:
	case TOK_CC_ASSIGN_MODULO:
	case TOK_CC_ASSIGN_PLUS:
	case TOK_CC_ASSIGN_MINUS:
	case TOK_CC_ASSIGN_LSHIFT:
	case TOK_CC_ASSIGN_RSHIFT:
	case TOK_CC_ASSIGN_AND:
	case TOK_CC_ASSIGN_XOR:
	case TOK_CC_ASSIGN_OR:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_AssignmentExpression* AssignmentExpression_1(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_UnaryExpression *left = UnaryExpression(ctx);
	if (left == NULL) return NULL;
	
	Token *op = AssignmentOperator(ctx);
	if (op == NULL)
	{
		ctx->token = org;
		return NULL;
	};
	
	CC_AssignmentExpression *right = AssignmentExpression(ctx);
	if (right == NULL)
	{
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	CC_AssignmentExpression *assign = (CC_AssignmentExpression*) calloc(1, sizeof(CC_AssignmentExpression));
	assign->left = left;
	assign->op = op;
	assign->right = right;
	return assign;
};

static CC_AssignmentExpression* AssignmentExpression_2(CC_Context *ctx)
{
	CC_ConditionalExpression *cond = ConditionalExpression(ctx);
	if (cond == NULL) return NULL;
	
	CC_AssignmentExpression *assign = (CC_AssignmentExpression*) calloc(1, sizeof(CC_AssignmentExpression));
	assign->cond = cond;
	return assign;
};

static CC_AssignmentExpression* AssignmentExpression(CC_Context *ctx)
{
	CC_AssignmentExpression *assign = AssignmentExpression_1(ctx);
	if (assign != NULL) return assign;
	else return AssignmentExpression_2(ctx);
};

static CC_Expression* Expression(CC_Context *ctx)
{
	Token *org = ctx->token;
	Token *op = NULL;
	
	CC_AssignmentExpression *assign = AssignmentExpression(ctx);
	if (assign == NULL) return NULL;
	
	CC_Expression *next = NULL;
	if (ctx->token->type == TOK_CC_COMMA)
	{
		op = ccConsume(ctx);	// consume the ','
		
		next = Expression(ctx);
		if (next == NULL)
		{
			// carry over the error from the top of Expression() parsing
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_Expression *expr = (CC_Expression*) calloc(1, sizeof(CC_Expression));
	expr->assign = assign;
	expr->op = op;
	expr->next = next;
	return expr;
};

static CC_ArgumentExpressionList* ArgumentExpressionList(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_AssignmentExpression *assign = AssignmentExpression(ctx);
	if (assign == NULL) return NULL;
	
	CC_ArgumentExpressionList *next = NULL;
	if (ctx->token->type == TOK_CC_COMMA)
	{
		ccConsume(ctx);	// consume the ','
		
		next = ArgumentExpressionList(ctx);
		if (next == NULL)
		{
			// carry over the error from the top of Expression() parsing
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_ArgumentExpressionList *expr = (CC_ArgumentExpressionList*) calloc(1, sizeof(CC_ArgumentExpressionList));
	expr->assign = assign;
	expr->next = next;
	return expr;
};

static CC_SpecifierQualifierList* SpecifierQualifierList(CC_Context *ctx)
{
	CC_TypeSpecifier *typeSpecifier = NULL;
	Token *typeQualifier = NULL;
	CC_AlignmentSpecifier *alignSpecifier = NULL;
	
	if (
		   (typeSpecifier = TypeSpecifier(ctx))
		|| (typeQualifier = TypeQualifier(ctx))
		|| (alignSpecifier = AlignmentSpecifier(ctx))
	)
	{
		CC_SpecifierQualifierList *list = (CC_SpecifierQualifierList*) calloc(1, sizeof(CC_SpecifierQualifierList));
		list->typeSpecifier = typeSpecifier;
		list->typeQualifier = typeQualifier;
		list->alignSpecifier = alignSpecifier;
		list->next = SpecifierQualifierList(ctx);
		return list;
	}
	else
	{
		return NULL;
	};
};

static CC_AbstractDeclarator* AbstractDeclarator(CC_Context *ctx);
static CC_DirectAbstractDeclarator* DirectAbstractDeclarator(CC_Context *ctx)
{
	Token *org = ctx->token;
	CC_DirectAbstractDeclarator *direct = (CC_DirectAbstractDeclarator*) calloc(1, sizeof(CC_DirectAbstractDeclarator));
	
	if (ctx->token->type == TOK_CC_LPAREN)
	{
		ccConsume(ctx);			// consume the '('
		CC_AbstractDeclarator *subdecl = AbstractDeclarator(ctx);
		if (subdecl == NULL)
		{
			ccError(ctx, "expected an abstract declarator, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		if (ctx->token->type != TOK_CC_RPAREN)
		{
			ccError(ctx, "expected `)', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);			// consume the ')'
	};
	
	direct->tail = DeclaratorTail(ctx);
	return direct;
};

static CC_AbstractDeclarator* AbstractDeclarator(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_Pointer *ptr = Pointer(ctx);
	CC_DirectAbstractDeclarator *direct = DirectAbstractDeclarator(ctx);
	
	if (ptr == NULL && direct == NULL)
	{
		// there is no "abstract declarator" if we matched neither
		ctx->token = org;
		return NULL;
	};
	
	CC_AbstractDeclarator *abs = (CC_AbstractDeclarator*) calloc(1, sizeof(CC_AbstractDeclarator));
	abs->ptr = ptr;
	abs->direct = direct;
	return abs;
};

static CC_TypeName* TypeName(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_SpecifierQualifierList *list = SpecifierQualifierList(ctx);
	if (list == NULL)
	{
		ccError(ctx, "expected a specifier-qualifier-list, not `%s'", ctx->token->value);
		return NULL;
	};
	
	CC_AbstractDeclarator *decl = AbstractDeclarator(ctx);
	
	CC_TypeName *typeName = (CC_TypeName*) calloc(1, sizeof(CC_TypeName));
	typeName->tok = org;
	typeName->specs = list;
	typeName->decl = decl;
	return typeName;
};

static CC_AtomicTypeSpecifier* AtomicTypeSpecifier(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_ATOMIC || ctx->token->next->type != TOK_CC_LPAREN)
	{
		return NULL;
	};
	
	// consume '_Atomic' '('
	Token *keyword = ccConsume(ctx);
	ccConsume(ctx);
	
	CC_TypeName *typeName = TypeName(ctx);
	if (typeName == NULL)
	{
		// carry over error from TypeName()
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ccError(ctx, "expected `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	// consume the `)'
	ccConsume(ctx);
	
	CC_AtomicTypeSpecifier *spec = (CC_AtomicTypeSpecifier*) calloc(1, sizeof(CC_AtomicTypeSpecifier));
	spec->keyword = keyword;
	spec->typeName = typeName;
	return spec;
};

static CC_ConstantExpression* ConstantExpression(CC_Context *ctx)
{
	CC_ConditionalExpression *cond = ConditionalExpression(ctx);
	if (cond == NULL) return NULL;
	
	CC_ConstantExpression *expr = (CC_ConstantExpression*) calloc(1, sizeof(CC_ConstantExpression));
	expr->cond = cond;
	return expr;
};

static CC_AlignmentSpecifier* AlignmentSpecifier(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_ALIGNAS)
	{
		return NULL;
	};
	
	ccConsume(ctx);		// consume '_Alignas'
	
	if (ctx->token->type != TOK_CC_LPAREN)
	{
		ccError(ctx, "expected `(' not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume '('
	
	CC_TypeName *typeName = TypeName(ctx);
	CC_ConstantExpression *expr = NULL;
	
	if (typeName == NULL)
	{
		expr = ConstantExpression(ctx);
		if (expr == NULL)
		{
			ccError(ctx, "expected a type name or constant expression, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ccError(ctx, "expected `)' not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume ')'
	
	CC_AlignmentSpecifier *spec = (CC_AlignmentSpecifier*) calloc(1, sizeof(CC_AlignmentSpecifier));
	spec->typeName = typeName;
	spec->expr = expr;
	return spec;
};

static CC_AttributeSpecifier* AttributeSpecifier(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (strcmp(ctx->token->value, "__attribute__") != 0) return NULL;
	ccConsume(ctx);		// consume the '__attribute__'
	
	// we expect '(' x2
	int i;
	for (i=0; i<2; i++)
	{
		if (ctx->token->type != TOK_CC_LPAREN)
		{
			ccError(ctx, "expected `(', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);
	};
	
	Token *id = Identifier(ctx);
	if (id == NULL)
	{
		ccError(ctx, "expected an identifier for attribute type, not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	// we expect ')' x2
	for (i=0; i<2; i++)
	{
		if (ctx->token->type != TOK_CC_RPAREN)
		{
			ccError(ctx, "expected `)', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);
	};
	
	CC_AttributeSpecifier *attrSpecifier = (CC_AttributeSpecifier*) calloc(1, sizeof(CC_AttributeSpecifier));
	attrSpecifier->id = id;
	return attrSpecifier;
};

static CC_DeclarationSpecifiers* DeclarationSpecifiers(CC_Context *ctx)
{
	Token *storageClass = NULL;
	CC_TypeSpecifier *typeSpecifier = NULL;
	Token *typeQualifier = NULL;
	Token *funcSpecifier = NULL;
	CC_AlignmentSpecifier *alignSpecifier = NULL;
	CC_AttributeSpecifier *attrSpecifier = NULL;
	
	if (
		   (storageClass = StorageClassSpecifier(ctx))
		|| (typeSpecifier = TypeSpecifier(ctx))
		|| (typeQualifier = TypeQualifier(ctx))
		|| (funcSpecifier = FunctionSpecifier(ctx))
		|| (alignSpecifier = AlignmentSpecifier(ctx))
		|| (attrSpecifier = AttributeSpecifier(ctx))
	)
	{
		CC_DeclarationSpecifiers *declSpecs = (CC_DeclarationSpecifiers*) calloc(1, sizeof(CC_DeclarationSpecifiers));
		declSpecs->storageClass = storageClass;
		declSpecs->typeSpecifier = typeSpecifier;
		declSpecs->typeQualifier = typeQualifier;
		declSpecs->funcSpecifier = funcSpecifier;
		declSpecs->alignSpecifier = alignSpecifier;
		declSpecs->attrSpecifier = attrSpecifier;
		declSpecs->next = DeclarationSpecifiers(ctx);
		return declSpecs;
	}
	else
	{
		ccError(ctx, "expected a storage class specifier, type specifier, type qualifier, function specifier or alignment qualifier, not `%s'", ctx->token->value);
		return NULL;
	};
};

static CC_TypeQualifierList* TypeQualifierList(CC_Context *ctx)
{
	CC_TypeQualifierList *first = NULL;
	CC_TypeQualifierList *last = NULL;
	
	Token *tok;
	while ((tok = TypeQualifier(ctx)) != NULL)
	{
		CC_TypeQualifierList *entry = (CC_TypeQualifierList*) calloc(1, sizeof(CC_TypeQualifierList));
		entry->qualifier = tok;
		
		if (first == NULL)
		{
			first = last = entry;
		}
		else
		{
			last->next = entry;
			last = entry;
		};
	};
	
	return first;
};

static CC_Pointer* Pointer(CC_Context *ctx)
{
	if (ctx->token->type != TOK_CC_AST)
	{
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '*'
	
	CC_Pointer *ptr = (CC_Pointer*) calloc(1, sizeof(CC_Pointer));
	ptr->qualifs = TypeQualifierList(ctx);
	ptr->next = Pointer(ctx);
	return ptr;
};

static Token* Identifier(CC_Context *ctx)
{
	if (ctx->token->type != TOK_CC_ID) return NULL;
	return ccConsume(ctx);
};

static CC_ParameterTypeList* ParameterTypeList(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_DeclarationSpecifiers *declSpecs = DeclarationSpecifiers(ctx);
	if (declSpecs == NULL)
	{
		ccError(ctx, "expected a declaration specifier, not `%s'", ctx->token->value);
		return NULL;
	};
	
	CC_Declarator *decl = Declarator(ctx);
	CC_AbstractDeclarator *absDecl = NULL;
	
	if (decl == NULL)
	{
		absDecl = AbstractDeclarator(ctx);
		if (absDecl == NULL)
		{
			ccError(ctx, "expected a declarator or abstract declarator, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_ParameterTypeList *next = NULL;
	if (ctx->token->type == TOK_CC_COMMA)
	{
		ccConsume(ctx); // consume the ','
		
		if (ctx->token->type == TOK_CC_ELLIPSIS)
		{
			ccConsume(ctx);	// consume the '...'
			next = (CC_ParameterTypeList*) calloc(1, sizeof(CC_ParameterTypeList));
			// leave all fields as NULL, as explained in the comment in cc-parse.h
		}
		else
		{
			next = ParameterTypeList(ctx);
			if (next == NULL)
			{
				// carry over the error from recursive ParameterTypeList()
				ctx->token = org;
				ccAbort(ctx);
				return NULL;
			};
		};
	};
	
	CC_ParameterTypeList *param = (CC_ParameterTypeList*) calloc(1, sizeof(CC_ParameterTypeList));
	param->decl = decl;
	param->declSpecs = declSpecs;
	param->absDecl = absDecl;
	param->next = next;
	return param;
};

static CC_DeclaratorTail* DeclaratorTail(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type == TOK_CC_LSQPAREN)
	{
		ccConsume(ctx);		// consume the '['
		int vla = 0;
		CC_AssignmentExpression *expr = NULL;
		
		Token *isStatic = NULL;
		if (ctx->token->type == TOK_CC_STATIC)
		{
			isStatic = ccConsume(ctx);
		};
		
		CC_TypeQualifierList *qualifs = TypeQualifierList(ctx);
		if (isStatic == NULL)
		{
			if (ctx->token->type == TOK_CC_STATIC)
			{
				isStatic = ccConsume(ctx);
			};
		};
		
		if (ctx->token->type == TOK_CC_AST && isStatic == NULL)
		{
			vla = 1;
			ccConsume(ctx);		// consume the '*'
		}
		else
		{
			expr = AssignmentExpression(ctx);
			if (expr == NULL && isStatic != NULL)
			{
				ccError(ctx, "expected an expression after `static', not `%s'", ctx->token->value);
				ctx->token = org;
				ccAbort(ctx);
				return NULL;
			};
		};
		
		if (ctx->token->type != TOK_CC_RSQPAREN)
		{
			ccError(ctx, "expected `]', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);	// consume the ']'
		
		CC_DeclaratorTail *tail = (CC_DeclaratorTail*) calloc(1, sizeof(CC_DeclaratorTail));
		tail->tok = org;
		tail->array = 1;
		tail->isStatic = isStatic;
		tail->qualifs = qualifs;
		tail->expr = expr;
		tail->vla = vla;
		tail->next = DeclaratorTail(ctx);
		return tail;
	}
	else if (ctx->token->type == TOK_CC_LPAREN)
	{
		ccConsume(ctx);	// consume the '('
		
		CC_ParameterTypeList *paramList = ParameterTypeList(ctx);
		
		if (ctx->token->type != TOK_CC_RPAREN)
		{
			ccError(ctx, "expected `)', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx); // consume the ')'
		
		CC_DeclaratorTail *tail = (CC_DeclaratorTail*) calloc(1, sizeof(CC_DeclaratorTail));
		tail->tok = org;
		tail->paramList = paramList;
		return tail;
	}
	else
	{
		return NULL;
	};
};

static CC_DirectDeclarator* DirectDeclarator(CC_Context *ctx)
{
	Token *org = ctx->token;
	Token *id;
	CC_DirectDeclarator *direct;
	
	if (
		   (id = Identifier(ctx))
	)
	{
		direct = (CC_DirectDeclarator*) calloc(1, sizeof(CC_DirectDeclarator));
		direct->id = id;
	}
	else if (ctx->token->type == TOK_CC_LPAREN)
	{
		ccConsume(ctx);		// consume the '('
		CC_Declarator *subdecl = Declarator(ctx);
		if (subdecl == NULL)
		{
			ccError(ctx, "expected a declarator, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		if (ctx->token->type != TOK_CC_RPAREN)
		{
			ccError(ctx, "expected `)', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);		// consume the ')'
		
		direct = (CC_DirectDeclarator*) calloc(1, sizeof(CC_DirectDeclarator));
		direct->decl = subdecl;
	}
	else
	{
		ctx->token = org;
		return NULL;
	};
	
	direct->tail = DeclaratorTail(ctx);
	return direct;
};

static CC_Declarator* Declarator(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_Pointer *ptr = Pointer(ctx);
	CC_DirectDeclarator *direct = DirectDeclarator(ctx);
	if (direct == NULL)
	{
		ccError(ctx, "expected a declarator, not `%s'", ctx->token->value);
		ctx->token = org;
		return NULL;
	};
	
	CC_Declarator *decl = (CC_Declarator*) calloc(1, sizeof(CC_Declarator));
	decl->ptr = ptr;
	decl->direct = direct;
	return decl;
};

static CC_DesignatorList* DesignatorList(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type == TOK_CC_DOT)
	{
		ccConsume(ctx);		// consume the '.'
		Token *id = Identifier(ctx);
		
		if (id == NULL)
		{
			ccError(ctx, "expected an identifier, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		CC_DesignatorList *list = (CC_DesignatorList*) calloc(1, sizeof(CC_DesignatorList));
		list->id = id;
		list->next = DesignatorList(ctx);
		return list;
	}
	else if (ctx->token->type == TOK_CC_LSQPAREN)
	{
		ccConsume(ctx);		// consume the '['
		CC_ConstantExpression *expr = ConstantExpression(ctx);
		if (expr == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		if (ctx->token->type != TOK_CC_RSQPAREN)
		{
			ccError(ctx, "expected `]', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);		// consume the ']'
		
		CC_DesignatorList *list = (CC_DesignatorList*) calloc(1, sizeof(CC_DesignatorList));
		list->expr = expr;
		list->next = DesignatorList(ctx);
		return list;
	}
	else
	{
		return NULL;
	};
};

static CC_Designation* Designation(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_DesignatorList *list = DesignatorList(ctx);
	if (list == NULL) return NULL;
	
	if (ctx->token->type != TOK_CC_ASSIGN)
	{
		ccError(ctx, "expected `=', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '='
	
	CC_Designation *designation = (CC_Designation*) calloc(1, sizeof(CC_Designation));
	designation->list = list;
	return designation;
};

static CC_InitializerList* InitializerList(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_Designation *designation = Designation(ctx);
	CC_Initializer *init = Initializer(ctx);
	
	if (init == NULL)
	{
		if (designation != NULL)
		{
			ccError(ctx, "expected an initializer, not `%s'", ctx->token->value);
			ccAbort(ctx);
		};
		
		ctx->token = org;
		return NULL;
	};
	
	CC_InitializerList *next = NULL;
	if (ctx->token->type == TOK_CC_COMMA)
	{
		ccConsume(ctx);		// consume the ','
		
		next = InitializerList(ctx);	// optional
	};
	
	CC_InitializerList *initList = (CC_InitializerList*) calloc(1, sizeof(CC_InitializerList));
	initList->tok = org;
	initList->designation = designation;
	initList->init = init;
	initList->next = next;
	return initList;
};

static CC_Initializer* Initializer(CC_Context *ctx)
{
	Token *org = ctx->token;
		
	if (ctx->token->type == TOK_CC_LBRACE)
	{
		ccConsume(ctx);		// consume the '{'
		
		CC_InitializerList *initList = InitializerList(ctx);
		
		if (ctx->token->type != TOK_CC_RBRACE)
		{
			ccError(ctx, "expected `}', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);		// consume the '}'
		
		CC_Initializer *init = (CC_Initializer*) calloc(1, sizeof(CC_Initializer));
		init->initList = initList;
		return init;
	};
	
	CC_AssignmentExpression *assign = AssignmentExpression(ctx);
	if (assign == NULL) return NULL;
	
	CC_Initializer *init = (CC_Initializer*) calloc(1, sizeof(CC_Initializer));
	init->assign = assign;
	return init;
};

static CC_InitDeclaratorList* InitDeclaratorList(CC_Context *ctx)
{
	CC_Declarator *decl = NULL;
	CC_Initializer *init = NULL;
	CC_InitDeclaratorList *next = NULL;
	
	Token *org = ctx->token;
	
	decl = Declarator(ctx);
	if (decl == NULL)
	{
		ccError(ctx, "expected a declarator, not `%s'", ctx->token->value);
		return NULL;
	};
	
	if (ctx->token->type == TOK_CC_ASSIGN)
	{
		ccConsume(ctx);
		
		init = Initializer(ctx);
		if (init == NULL)
		{
			ccError(ctx, "expected an initializer, not `%s'", ctx->token->value);
			ctx->token = org;
			return NULL;
		};
	};
	
	if (ctx->token->type == TOK_CC_COMMA)
	{
		ccConsume(ctx);
		
		next = InitDeclaratorList(ctx);
		if (next == NULL)
		{
			ctx->token = org;
			return NULL;
		};
	};
	
	CC_InitDeclaratorList *result = (CC_InitDeclaratorList*) calloc(1, sizeof(CC_InitDeclaratorList));
	result->decl = decl;
	result->init = init;
	result->next = next;
	return result;
};

static int ccIsTypedef(CC_Declaration *decl)
{
	CC_DeclarationSpecifiers *spec;
	for (spec=decl->declSpecs; spec!=NULL; spec=spec->next)
	{
		if (spec->storageClass != NULL && spec->storageClass->type == TOK_CC_TYPEDEF)
		{
			return 1;
		};
	};
	
	return 0;
};

static const char* ccGetDeclaratorName(CC_Declarator *decl)
{
	CC_DirectDeclarator *direct = decl->direct;
	if (direct->decl != NULL) return ccGetDeclaratorName(direct->decl);
	else return direct->id->value;
};

static void ccProcessDeclaration(CC_Context *ctx, CC_Declaration *decl)
{
	if (ccIsTypedef(decl))
	{
		// find all names and add them to the typedef table
		CC_InitDeclaratorList *initDecl;
		for (initDecl=decl->initDeclList; initDecl!=NULL; initDecl=initDecl->next)
		{
			const char *name = ccGetDeclaratorName(initDecl->decl);
			hashtabSet(ctx->scope->typedefNames, name, NULL);
		};
	};
};

static CC_Declaration* Declaration(CC_Context *ctx)
{
	CC_StaticAssert *staticAssert = StaticAssert(ctx);
	if (staticAssert != NULL)
	{
		CC_Declaration *decl = (CC_Declaration*) calloc(1, sizeof(CC_Declaration));
		decl->staticAssert = staticAssert;
		return decl;
	};
	
	CC_DeclarationSpecifiers *declSpecs;
	CC_InitDeclaratorList *initDeclList;
	
	declSpecs = DeclarationSpecifiers(ctx);
	if (declSpecs == NULL)
	{
		ccError(ctx, "expected a declaration specifier, not `%s'", ctx->token->value);
		return NULL;
	};
	
	initDeclList = InitDeclaratorList(ctx); // optional
	
	if (ctx->token->type != TOK_CC_SEMICOLON)
	{
		ccError(ctx, "expected `;', `,' or `=', not `%s'", ctx->token->value);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the semicolon
	
	CC_Declaration *decl = (CC_Declaration*) calloc(1, sizeof(CC_Declaration));
	decl->declSpecs = declSpecs;
	decl->initDeclList = initDeclList;
	
	ccProcessDeclaration(ctx, decl);
	
	return decl;
};

static CC_LabeledStatement* LabeledStatement_1(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type == TOK_CC_ID && ctx->token->next->type == TOK_CC_COLON)
	{
		Token *id = ccConsume(ctx);
		ccConsume(ctx);			// consume the ':'
		
		CC_Statement *st = Statement(ctx);
		if (st == NULL)
		{
			ccError(ctx, "expected a statement, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		CC_LabeledStatement *label = (CC_LabeledStatement*) calloc(1, sizeof(CC_LabeledStatement));
		label->id = id;
		label->st = st;
		return label;
	};
	
	return NULL;
};

static Token* LabelKeyword(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_CASE:
	case TOK_CC_DEFAULT:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_LabeledStatement* LabeledStatement_2(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	Token *keyword = LabelKeyword(ctx);
	if (keyword == NULL) return NULL;
	
	CC_ConstantExpression *expr = NULL;
	if (keyword->type == TOK_CC_CASE)
	{
		expr = ConstantExpression(ctx);
		if (expr == NULL)
		{
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	if (ctx->token->type != TOK_CC_COLON)
	{
		ccError(ctx, "expected `:' not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ':'
	
	CC_Statement *st = Statement(ctx);
	if (st == NULL)
	{
		ccError(ctx, "expected a statement, not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	CC_LabeledStatement *label = (CC_LabeledStatement*) calloc(1, sizeof(CC_LabeledStatement));
	label->keyword = keyword;
	label->expr = expr;
	label->st = st;
	return label;
};

static CC_LabeledStatement* LabeledStatement(CC_Context *ctx)
{
	CC_LabeledStatement *st = LabeledStatement_1(ctx);
	if (st != NULL) return st;
	
	st = LabeledStatement_2(ctx);
	if (st != NULL) return st;
	
	return NULL;
};

static CC_Expression* ExpressionStatement(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_Expression *expr = Expression(ctx);
	if (expr == NULL) return NULL;
	
	if (ctx->token->type != TOK_CC_SEMICOLON)
	{
		ccError(ctx, "expected `;' not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);	// consume the ';'
	return expr;
};

static CC_IfStatement* IfStatement(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_IF) return NULL;
	ccConsume(ctx);		// consume the 'if'
	
	if (ctx->token->type != TOK_CC_LPAREN)
	{
		ccError(ctx, "expected `(', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '('
	
	CC_Expression *expr = Expression(ctx);
	if (expr == NULL)
	{
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ccError(ctx, "expected `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ')'
	
	CC_Statement *trueAction = Statement(ctx);
	if (trueAction == NULL)
	{
		ccError(ctx, "expected a statement, not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	CC_Statement *falseAction = NULL;
	if (ctx->token->type == TOK_CC_ELSE)
	{
		ccConsume(ctx);
		
		falseAction = Statement(ctx);
		if (falseAction == NULL)
		{
			ccError(ctx, "expected a statement, not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_IfStatement *ifs = (CC_IfStatement*) calloc(1, sizeof(CC_IfStatement));
	ifs->op = org;
	ifs->expr = expr;
	ifs->trueAction = trueAction;
	ifs->falseAction = falseAction;
	return ifs;
};

static CC_SwitchStatement* SwitchStatement(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_SWITCH) return NULL;
	ccConsume(ctx);		// consume the 'switch'
	
	if (ctx->token->type != TOK_CC_LPAREN)
	{
		ccError(ctx, "expected `(', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '('
	
	CC_Expression *expr = Expression(ctx);
	if (expr == NULL)
	{
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ccError(ctx, "expected `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ')'
	
	CC_Statement *st = Statement(ctx);
	if (st == NULL)
	{
		ccError(ctx, "expected `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	CC_SwitchStatement *sw = (CC_SwitchStatement*) calloc(1, sizeof(CC_SwitchStatement));
	sw->op = org;
	sw->expr = expr;
	sw->st = st;
	return sw;
};

static CC_WhileStatement* WhileStatement(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_WHILE) return NULL;
	ccConsume(ctx);		// consume the 'while'
	
	if (ctx->token->type != TOK_CC_LPAREN)
	{
		ccError(ctx, "expected `(', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '('
	
	CC_Expression *expr = Expression(ctx);
	if (expr == NULL)
	{
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ccError(ctx, "expected `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ')'
	
	CC_Statement *st = Statement(ctx);
	if (st == NULL)
	{
		ccError(ctx, "expected `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	CC_WhileStatement *whiles = (CC_WhileStatement*) calloc(1, sizeof(CC_WhileStatement));
	whiles->op = org;
	whiles->expr = expr;
	whiles->st = st;
	return whiles;
};

static CC_DoWhileStatement* DoWhileStatement(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_DO) return NULL;
	ccConsume(ctx);		// consume the 'do'
	
	CC_Statement *st = Statement(ctx);
	if (st == NULL)
	{
		ccError(ctx, "expected a statement, not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_WHILE)
	{
		ccError(ctx, "expected `while', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the 'while'
	
	if (ctx->token->type != TOK_CC_LPAREN)
	{
		ccError(ctx, "expected `(', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '('
	
	CC_Expression *expr = Expression(ctx);
	if (expr == NULL)
	{
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ccError(ctx, "expected `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ')'
	
	if (ctx->token->type != TOK_CC_SEMICOLON)
	{
		ccError(ctx, "expected `;', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ';'
	
	CC_DoWhileStatement *doWhile = (CC_DoWhileStatement*) calloc(1, sizeof(CC_DoWhileStatement));
	doWhile->op = org;
	doWhile->expr = expr;
	doWhile->st = st;
	return doWhile;
};

static CC_ForStatement* ForStatement(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_FOR) return NULL;
	ccConsume(ctx);		// consume the 'for'
	
	if (ctx->token->type != TOK_CC_LPAREN)
	{
		ccError(ctx, "expected `(', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '('
	
	CC_Declaration *decl = Declaration(ctx);
	CC_Expression *init = NULL;
	if (decl == NULL)
	{
		init = Expression(ctx);
		
		if (ctx->token->type != TOK_CC_SEMICOLON)
		{
			ccError(ctx, "expected `;', not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
		
		ccConsume(ctx);		// consume the ';'
	};
	
	CC_Expression *cond = Expression(ctx);
	if (ctx->token->type != TOK_CC_SEMICOLON)
	{
		ccError(ctx, "expected `;', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ';'
	
	CC_Expression *iter = Expression(ctx);
	if (ctx->token->type != TOK_CC_RPAREN)
	{
		ccError(ctx, "expected `)', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the ')'
	
	CC_Statement *st = Statement(ctx);
	if (st == NULL)
	{
		ccError(ctx, "expected a statement, not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	CC_ForStatement *fors = (CC_ForStatement*) calloc(1, sizeof(CC_ForStatement));
	fors->op = org;
	fors->decl = decl;
	fors->init = init;
	fors->cond = cond;
	fors->iter = iter;
	fors->st = st;
	return fors;
};

static Token* JumpKeyword(CC_Context *ctx)
{
	switch (ctx->token->type)
	{
	case TOK_CC_GOTO:
	case TOK_CC_CONTINUE:
	case TOK_CC_BREAK:
	case TOK_CC_RETURN:
		return ccConsume(ctx);
	default:
		return NULL;
	};
};

static CC_JumpStatement* JumpStatement(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	Token *keyword = JumpKeyword(ctx);
	if (keyword == NULL) return NULL;
	
	Token *id = NULL;
	if (keyword->type == TOK_CC_GOTO)
	{
		id = Identifier(ctx);
		if (id == NULL)
		{
			ccError(ctx, "expected an identifier (label name), not `%s'", ctx->token->value);
			ctx->token = org;
			ccAbort(ctx);
			return NULL;
		};
	};
	
	CC_Expression *expr = NULL;
	if (keyword->type == TOK_CC_RETURN)
	{
		expr = Expression(ctx);	// optional
	};
	
	if (ctx->token->type != TOK_CC_SEMICOLON)
	{
		ccError(ctx, "expected `;', not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);	// consume the ';'
	
	CC_JumpStatement *jmp = (CC_JumpStatement*) calloc(1, sizeof(CC_JumpStatement));
	jmp->keyword = keyword;
	jmp->id = id;
	jmp->expr = expr;
	return jmp;
};

static CC_Statement* Statement(CC_Context *ctx)
{
	Token *org = ctx->token;
	CC_LabeledStatement *label = NULL;
	CC_CompoundStatement *comp = NULL;
	CC_Expression *expr = NULL;
	CC_IfStatement *ifs = NULL;
	CC_SwitchStatement *sw = NULL;
	CC_WhileStatement *whiles = NULL;
	CC_DoWhileStatement *doWhile = NULL;
	CC_ForStatement *fors = NULL;
	CC_JumpStatement *jmp = NULL;
	
	if (ctx->token->type == TOK_CC_SEMICOLON)
	{
		ccConsume(ctx);
		CC_Statement *st = (CC_Statement*) calloc(1, sizeof(CC_Statement));
		st->tok = org;
		return st;
	};
	
	if (
		   (label = LabeledStatement(ctx))
		|| (comp = CompoundStatement(ctx))
		|| (expr = ExpressionStatement(ctx))
		|| (ifs = IfStatement(ctx))
		|| (sw = SwitchStatement(ctx))
		|| (whiles = WhileStatement(ctx))
		|| (doWhile = DoWhileStatement(ctx))
		|| (fors = ForStatement(ctx))
		|| (jmp = JumpStatement(ctx))
	)
	{
		CC_Statement *st = (CC_Statement*) calloc(1, sizeof(CC_Statement));
		st->tok = org;
		st->label = label;
		st->comp = comp;
		st->expr = expr;
		st->ifs = ifs;
		st->sw = sw;
		st->whiles = whiles;
		st->doWhile = doWhile;
		st->fors = fors;
		st->jmp = jmp;
		return st;
	}
	else
	{
		return NULL;
	};
};

static CC_BlockItemList* BlockItemList(CC_Context *ctx)
{
	CC_Declaration *decl = NULL;
	CC_Statement *st = NULL;
	
	if (
		   (decl = Declaration(ctx))
		|| (st = Statement(ctx))
	)
	{
		CC_BlockItemList *item = (CC_BlockItemList*) calloc(1, sizeof(CC_BlockItemList));
		item->decl = decl;
		item->st = st;
		item->next = BlockItemList(ctx);
		return item;
	}
	else
	{
		return NULL;
	};
};

static CC_CompoundStatement* CompoundStatement(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	if (ctx->token->type != TOK_CC_LBRACE) return NULL;
	ccConsume(ctx);		// consume the '{'
	
	ccPushScope(ctx);
	CC_BlockItemList *items = BlockItemList(ctx);
	ccPopScope(ctx);
	
	if (ctx->token->type != TOK_CC_RBRACE)
	{
		ccError(ctx, "expected `}' or a statement, not `%s'", ctx->token->value);
		ctx->token = org;
		ccAbort(ctx);
		return NULL;
	};
	
	ccConsume(ctx);		// consume the '}'
	
	CC_CompoundStatement *comp = (CC_CompoundStatement*) calloc(1, sizeof(CC_CompoundStatement));
	comp->tok = org;
	comp->items = items;
	return comp;
};

static CC_FunctionDefinition* FunctionDefinition(CC_Context *ctx)
{
	Token *org = ctx->token;
	
	CC_DeclarationSpecifiers *specs = DeclarationSpecifiers(ctx);
	if (specs == NULL) return NULL;
	
	CC_Declarator *decl = Declarator(ctx);
	if (decl == NULL)
	{
		ctx->token = org;
		return NULL;
	};
	
	// validate the declarator
	if (decl->direct->tail == NULL || decl->direct->tail->array || decl->direct->tail->next != NULL)
	{
		ctx->token = org;
		return NULL;
	};
	
	CC_CompoundStatement *body = CompoundStatement(ctx);
	if (body == NULL)
	{
		ctx->token = org;
		return NULL;
	};
	
	CC_FunctionDefinition *funcDef = (CC_FunctionDefinition*) calloc(1, sizeof(CC_FunctionDefinition));
	funcDef->specs = specs;
	funcDef->decl = decl;
	funcDef->body = body;
	return funcDef;
};

static CC_ExternalDeclaration* ExternalDeclaration(CC_Context *ctx)
{
	CC_FunctionDefinition *funcDef = FunctionDefinition(ctx);
	CC_Declaration *decl = NULL;
	
	if (funcDef == NULL)
	{
		decl = Declaration(ctx);
		if (decl == NULL)
		{
			return NULL;
		};
	};
	
	CC_ExternalDeclaration *xdecl = (CC_ExternalDeclaration*) calloc(1, sizeof(CC_ExternalDeclaration));
	xdecl->decl = decl;
	xdecl->funcDef = funcDef;
	
	return xdecl;
};

CC_Context* ccParse(Token *toklist)
{
	CC_Context *ctx = (CC_Context*) malloc(sizeof(CC_Context));
	memset(ctx, 0, sizeof(CC_Context));
	
	ctx->token = toklist;
	
	// create the global scope
	CC_NameScope *globalScope = (CC_NameScope*) malloc(sizeof(CC_NameScope));
	memset(globalScope, 0, sizeof(CC_NameScope));
	globalScope->typedefNames = hashtabNew();
	
	// make the global scope the current scope
	ctx->scope = globalScope;
	
	CC_ExternalDeclaration *first = NULL;
	CC_ExternalDeclaration *last = NULL;
	
	if (setjmp(ctx->retbuf))
	{
		assert(ctx->lastError != NULL);
		lexDiag(ctx->errToken, CON_ERROR, "%s", ctx->lastError);
		return NULL;
	};
	
	while (ctx->token->type != TOK_END)
	{
		// allow empty external declarations, i.e. ";" on their own
		// technically not allowed by the standard, but most compilers
		// do it, and besides, look at the end of every function definition
		// in this file ;)
		if (ctx->token->type == TOK_CC_SEMICOLON)
		{
			ccConsume(ctx);
			continue;
		};
		
		CC_ExternalDeclaration *decl = ExternalDeclaration(ctx);
		if (decl == NULL)
		{
			assert(ctx->lastError != NULL);
			lexDiag(ctx->errToken, CON_ERROR, "%s", ctx->lastError);
			return NULL;
		};
		
		if (first == NULL)
		{
			first = last = decl;
		}
		else
		{
			last->next = decl;
			last = decl;
		};
	};
	
	ctx->declHead = first;
	return ctx;
};
