/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/cc/cc-ast.h>
#include <comptools/cc/cc-const.h>
#include <comptools/console.h>

#define	TODO assert(0 == 1)
#define	INVALID_CODE_PATH assert(0 == 1)

/**
 * A structure for counting how many times each type specifier appeared in the
 * declaration specifiers in a declaration. Used to later figure out what type
 * was actually specified.
 */
typedef struct
{
	int numVoid;
	int numChar;
	int numShort;
	int numInt;
	int numLong;
	int numFloat;
	int numDouble;
	int numSigned;
	int numUnsigned;
	int numBool;
	int numComplex;
} TypeSpecCounter;

/**
 * Definition of a primitive type based on the tokens it is made up of.
 */
typedef struct
{
	int kind;		/* CC_TYPE_* */
	TypeSpecCounter specs;
} PrimitiveType;

/**
 * List of primitive types and how they are specified.
 */
static PrimitiveType primTypes[] = {
	{CC_TYPE_VOID, {.numVoid = 1}},
	{CC_TYPE_CHAR, {.numChar = 1}},
	{CC_TYPE_SIGNED_CHAR, {.numSigned = 1, .numChar = 1}},
	{CC_TYPE_UNSIGNED_CHAR, {.numUnsigned = 1, .numChar = 1}},
	{CC_TYPE_SIGNED_SHORT, {.numShort = 1}},
	{CC_TYPE_SIGNED_SHORT, {.numSigned = 1, .numShort = 1}},
	{CC_TYPE_SIGNED_SHORT, {.numShort = 1, .numInt = 1}},
	{CC_TYPE_SIGNED_SHORT, {.numSigned = 1, .numShort = 1, .numInt = 1}},
	{CC_TYPE_UNSIGNED_SHORT, {.numUnsigned = 1, .numShort = 1}},
	{CC_TYPE_UNSIGNED_SHORT, {.numUnsigned = 1, .numShort = 1, .numInt = 1}},
	{CC_TYPE_SIGNED_INT, {.numInt = 1}},
	{CC_TYPE_SIGNED_INT, {.numSigned = 1}},
	{CC_TYPE_SIGNED_INT, {.numSigned = 1, .numInt = 1}},
	{CC_TYPE_UNSIGNED_INT, {.numUnsigned = 1}},
	{CC_TYPE_UNSIGNED_INT, {.numUnsigned = 1, .numInt = 1}},
	{CC_TYPE_SIGNED_LONG, {.numLong = 1}},
	{CC_TYPE_SIGNED_LONG, {.numSigned = 1, .numLong = 1}},
	{CC_TYPE_SIGNED_LONG, {.numLong = 1, .numInt = 1}},
	{CC_TYPE_SIGNED_LONG, {.numSigned = 1, .numLong = 1, .numInt = 1}},
	{CC_TYPE_UNSIGNED_LONG, {.numUnsigned = 1, .numLong = 1}},
	{CC_TYPE_UNSIGNED_LONG, {.numUnsigned = 1, .numLong = 1, .numInt = 1}},
	{CC_TYPE_SIGNED_LONG_LONG, {.numLong = 2}},
	{CC_TYPE_SIGNED_LONG_LONG, {.numSigned = 1, .numLong = 2}},
	{CC_TYPE_SIGNED_LONG_LONG, {.numLong = 2, .numInt = 1}},
	{CC_TYPE_SIGNED_LONG_LONG, {.numSigned = 1, .numLong = 2, .numInt = 1}},
	{CC_TYPE_UNSIGNED_LONG_LONG, {.numUnsigned = 1, .numLong = 2}},
	{CC_TYPE_UNSIGNED_LONG_LONG, {.numUnsigned = 1, .numLong = 2, .numInt = 1}},
	{CC_TYPE_FLOAT, {.numFloat = 1}},
	{CC_TYPE_DOUBLE, {.numDouble = 1}},
	{CC_TYPE_LONG_DOUBLE, {.numLong = 1, .numDouble = 1}},
	{CC_TYPE_BOOL, {.numBool = 1}},
	{CC_TYPE_FLOAT_COMPLEX, {.numFloat = 1, .numComplex = 1}},
	{CC_TYPE_DOUBLE_COMPLEX, {.numDouble = 1, .numComplex = 1}},
	{CC_TYPE_LONG_DOUBLE_COMPLEX, {.numLong = 1, .numDouble = 1, .numComplex = 1}},
	
	// LIST TERMINATOR
	{-1}
};

/**
 * Names of node types.
 */
#define	CC_DEF_NODE_TYPE(name)	#name
static const char *nodeTypeNames[] = {
#include <comptools/cc/cc-nodetypes.h>
};

static CC_Node* ccCompileExpression(CC_Compiler *cc, CC_Expression *expr);
static CC_Node* ccCompileCastExpression(CC_Compiler *cc, CC_CastExpression *cast);
static CC_Node* ccCompileAssignmentExpression(CC_Compiler *cc, CC_AssignmentExpression *assign);
static CC_Node* ccCompileInitializerList(CC_Compiler *cc, CC_InitializerList *initList, Token *tok);
static CC_Node* ccCompileCompoundStatement(CC_Compiler *cc, CC_CompoundStatement *comp);
static CC_Node* ccCompileStatement(CC_Compiler *cc, CC_Statement *st);

char* ccGetTypeName(CC_Type *type)
{
	char myPart[256];
	myPart[0] = 0;
	
	if (type->qualifs & CC_TQ_ATOMIC) strcat(myPart, "atomic ");
	if (type->qualifs & CC_TQ_CONST) strcat(myPart, "const ");
	if (type->qualifs & CC_TQ_RESTRICT) strcat(myPart, "restrict ");
	if (type->qualifs & CC_TQ_VOLATILE) strcat(myPart, "volatile ");
	
	char *sub;
	char *result;
	
	switch (type->kind)
	{
	case CC_TYPE_ARRAY:
		strcat(myPart, "array of ");
		sub = ccGetTypeName(type->base);
		result = (char*) malloc(strlen(myPart) + strlen(sub) + 1);
		sprintf(result, "%s%s", myPart, sub);
		free(sub);
		return result;
	case CC_TYPE_BOOL:
		strcat(myPart, "bool");
		return strdup(myPart);
	case CC_TYPE_CHAR:
		strcat(myPart, "char");
		return strdup(myPart);
	case CC_TYPE_DOUBLE:
		strcat(myPart, "double");
		return strdup(myPart);
	case CC_TYPE_DOUBLE_COMPLEX:
		strcat(myPart, "double complex");
		return strdup(myPart);
	case CC_TYPE_FLOAT:
		strcat(myPart, "float");
		return strdup(myPart);
	case CC_TYPE_FLOAT_COMPLEX:
		strcat(myPart, "float complex");
		return strdup(myPart);
	case CC_TYPE_FUNC:
		strcpy(myPart, "function");	// strcpy not strcat
		return strdup(myPart);
	case CC_TYPE_LONG_DOUBLE:
		strcat(myPart, "long double");
		return strdup(myPart);
	case CC_TYPE_LONG_DOUBLE_COMPLEX:
		strcat(myPart, "long double complex");
		return strdup(myPart);
	case CC_TYPE_PTR:
		strcat(myPart, "pointer to ");
		sub = ccGetTypeName(type->base);
		result = (char*) malloc(strlen(myPart) + strlen(sub) + 1);
		sprintf(result, "%s%s", myPart, sub);
		free(sub);
		return result;
	case CC_TYPE_SIGNED_CHAR:
		strcat(myPart, "signed char");
		return strdup(myPart);
	case CC_TYPE_SIGNED_INT:
		strcat(myPart, "signed int");
		return strdup(myPart);
	case CC_TYPE_SIGNED_LONG:
		strcat(myPart, "signed long");
		return strdup(myPart);
	case CC_TYPE_SIGNED_LONG_LONG:
		strcat(myPart, "signed long long");
		return strdup(myPart);
	case CC_TYPE_SIGNED_SHORT:
		strcat(myPart, "signed short");
		return strdup(myPart);
	case CC_TYPE_STRUCT_OR_UNION:
		strcat(myPart, "struct-or-union ");
		result = (char*) malloc(strlen(myPart) + strlen(type->tag) + 1);
		sprintf(result, "%s%s", myPart, type->tag);
		return result;
	case CC_TYPE_UNSIGNED_CHAR:
		strcat(myPart, "unsigned char");
		return strdup(myPart);
	case CC_TYPE_UNSIGNED_INT:
		strcat(myPart, "unsigned int");
		return strdup(myPart);
	case CC_TYPE_UNSIGNED_LONG:
		strcat(myPart, "unsigned long");
		return strdup(myPart);
	case CC_TYPE_UNSIGNED_LONG_LONG:
		strcat(myPart, "unsigned long long");
		return strdup(myPart);
	case CC_TYPE_UNSIGNED_SHORT:
		strcat(myPart, "unsigned short");
		return strdup(myPart);
	case CC_TYPE_VOID:
		strcat(myPart, "void");
		return strdup(myPart);
	default:
		INVALID_CODE_PATH;
		return NULL;
	};
};

void ccDumpType(CC_Type *type)
{
	if (type->qualifs & CC_TQ_ATOMIC) printf("atomic ");
	if (type->qualifs & CC_TQ_CONST) printf("const ");
	if (type->qualifs & CC_TQ_RESTRICT) printf("restrict ");
	if (type->qualifs & CC_TQ_VOLATILE) printf("volatile ");
	
	int i;
	switch (type->kind)
	{
	case CC_TYPE_ARRAY:
		printf("array [%d] of ", (int) type->numElements);
		ccDumpType(type->base);
		break;
	case CC_TYPE_BOOL:
		printf("bool");
		break;
	case CC_TYPE_CHAR:
		printf("char");
		break;
	case CC_TYPE_DOUBLE:
		printf("double");
		break;
	case CC_TYPE_DOUBLE_COMPLEX:
		printf("double complex");
		break;
	case CC_TYPE_FLOAT:
		printf("float");
		break;
	case CC_TYPE_FLOAT_COMPLEX:
		printf("float complex");
		break;
	case CC_TYPE_FUNC:
		printf("function taking (");
		for (i=0; i<type->numArgs; i++)
		{
			ccDumpType(type->argTypes[i]);
			printf(" `%s'", type->argNames[i]);
			if (i != (type->numArgs-1) || type->valist) printf(", ");
		};
		if (type->valist) printf("...");
		printf(") returning ");
		ccDumpType(type->base);
		break;
	case CC_TYPE_LONG_DOUBLE:
		printf("long double");
		break;
	case CC_TYPE_LONG_DOUBLE_COMPLEX:
		printf("long double complex");
		break;
	case CC_TYPE_PTR:
		printf("pointer to ");
		ccDumpType(type->base);
		break;
	case CC_TYPE_SIGNED_CHAR:
		printf("signed char");
		break;
	case CC_TYPE_SIGNED_INT:
		printf("signed int");
		break;
	case CC_TYPE_SIGNED_LONG:
		printf("signed long");
		break;
	case CC_TYPE_SIGNED_LONG_LONG:
		printf("signed long long");
		break;
	case CC_TYPE_SIGNED_SHORT:
		printf("signed short");
		break;
	case CC_TYPE_STRUCT_OR_UNION:
		printf("struct-or-union %s", type->tag);
		break;
	case CC_TYPE_UNSIGNED_CHAR:
		printf("unsigned char");
		break;
	case CC_TYPE_UNSIGNED_INT:
		printf("unsigned int");
		break;
	case CC_TYPE_UNSIGNED_LONG:
		printf("unsigned long");
		break;
	case CC_TYPE_UNSIGNED_LONG_LONG:
		printf("unsigned long long");
		break;
	case CC_TYPE_UNSIGNED_SHORT:
		printf("unsigned short");
		break;
	case CC_TYPE_VOID:
		printf("void");
		break;
	default:
		INVALID_CODE_PATH;
		break;
	};
};

const char* ccGetNodeTypeName(int nodeType)
{
	if (nodeType < 0 || nodeType >= CC_NUM_NODE_TYPES) return "<invalid>";
	else return nodeTypeNames[nodeType];
};

CC_Compiler* ccNew(CC_DataModel *dataModel)
{
	CC_Compiler *cc = (CC_Compiler*) calloc(1, sizeof(CC_Compiler));
	cc->dataModel = dataModel;
	cc->nextTagNum = 1;
	return cc;
};

static int ccCompileQualifier(Token *q)
{
	switch (q->type)
	{
	case TOK_CC_CONST:		return CC_TQ_CONST;
	case TOK_CC_RESTRICT:		return CC_TQ_RESTRICT;
	case TOK_CC_VOLATILE:		return CC_TQ_VOLATILE;
	case TOK_CC_ATOMIC:		return CC_TQ_ATOMIC;
	default:			INVALID_CODE_PATH;
	};

	return 0;
};

CC_Type* ccCompileTypeName(CC_Compiler *cc, CC_TypeName *typeName)
{
	CC_Type *baseType = ccCompileSpecifierQualifierList(cc, typeName->specs);
	if (baseType == NULL) return NULL;
	
	const char *ignore;
	CC_Type *outType;
	Token *ignore2;
	if (ccCompileDeclarator(cc, baseType, typeName->decl, &outType, &ignore, &ignore2) == -1) return NULL;
	
	return outType;
};

int ccCompileDeclarator(CC_Compiler *cc, CC_Type *baseType, CC_Declarator *decl, CC_Type **outType, const char **outName, Token **outTok)
{	
	// do pointer derivation
	CC_Pointer *ptr;
	for (ptr=decl->ptr; ptr!=NULL; ptr=ptr->next)
	{
		CC_Type *subtype = (CC_Type*) calloc(1, sizeof(CC_Type));
		subtype->kind = CC_TYPE_PTR;
		subtype->base = baseType;
		
		CC_TypeQualifierList *q;
		for (q=ptr->qualifs; q!=NULL; q=q->next)
		{
			subtype->qualifs |= ccCompileQualifier(q->qualifier);
		};
		
		baseType = subtype;
	};
	
	// do tail derivation
	CC_DirectDeclarator *direct = decl->direct;
	CC_DeclaratorTail *tail;
	CC_DeclaratorTail *stop = NULL;
	while (stop != direct->tail)
	{
		for (tail=direct->tail; tail->next!=stop; tail=tail->next);
		stop = tail;
		
		if (tail->array)
		{
			// TODO: take "static" into account in function declarations
			// it's technically not required to accept valid programs
			
			if (ccGetTypeSize(cc, baseType) == 0)
			{
				lexDiag(tail->tok, CON_ERROR, "cannot derive an array from an incomplete type");
				return -1;
			};
			
			CC_Type *subtype = (CC_Type*) calloc(1, sizeof(CC_Type));
			subtype->base = baseType;
			subtype->kind = CC_TYPE_ARRAY;
			
			CC_TypeQualifierList *q;
			for (q=tail->qualifs; q!=NULL; q=q->next)
			{
				subtype->qualifs |= ccCompileQualifier(q->qualifier);
			};
			
			if (tail->vla)
			{
				subtype->numElements = -1;
			}
			else if (tail->expr != NULL)
			{
				CC_Const value = ccEvalAssignmentExpression(cc, tail->expr);
				if (value.type == CC_CONST_ERROR)
				{
					lexDiag(value.tok, CON_ERROR, value.err);
					subtype->numElements = 0;
				}
				else if (value.type == CC_CONST_INT)
				{
					subtype->numElements = value.i;
				}
				else
				{
					lexDiag(value.tok, CON_ERROR, "expected an integer value for array size");
					subtype->numElements = 0;
				};
			}
			else
			{
				subtype->numElements = 0;
			};
			
			baseType = subtype;
		}
		else
		{
			CC_Type *subtype = (CC_Type*) calloc(1, sizeof(CC_Type));
			subtype->base = baseType;
			subtype->kind = CC_TYPE_FUNC;
			
			CC_ParameterTypeList *param;
			for (param=tail->paramList; param!=NULL; param=param->next)
			{
				if (param->declSpecs == NULL)
				{
					subtype->valist = 1;
				}
				else
				{
					CC_Type *type = ccCompileSpecifierQualifierList(cc, (CC_SpecifierQualifierList*) param->declSpecs);
					if (type == NULL)
					{
						return -1;
					};
					
					CC_Type *argtype;
					const char *argName = "";
					Token *argToken = NULL;
					if (param->decl == NULL && param->absDecl == NULL)
					{
						argtype = type;
					}
					else
					{
						if (ccCompileDeclarator(cc, type, (param->decl ? param->decl : param->absDecl), &argtype, &argName, &argToken) == -1) return -1;
					};
					
					// argument conversions
					if (argtype->kind == CC_TYPE_ARRAY)
					{
						argtype->kind = CC_TYPE_PTR;
					};
					
					if (argtype->kind == CC_TYPE_FUNC)
					{
						CC_Type *derive = (CC_Type*) calloc(1, sizeof(CC_Type));
						derive->base = argtype;
						derive->kind = CC_TYPE_PTR;
						argtype = derive;
					};
					
					int index = subtype->numArgs++;
					subtype->argTypes = (CC_Type**) realloc(subtype->argTypes, sizeof(void*)*subtype->numArgs);
					subtype->argNames = (const char**) realloc(subtype->argNames, sizeof(void*)*subtype->numArgs);
					subtype->argTokens = (Token**) realloc(subtype->argTokens, sizeof(void*)*subtype->numArgs);
					subtype->argTypes[index] = argtype;
					subtype->argNames[index] = argName;
					subtype->argTokens[index] = argToken;
				};
			};
			
			baseType = subtype;
		};
	};
	
	// derive the parenthesized declarator, or get the name
	if (direct->id != NULL)
	{
		*outName = direct->id->value;
		*outTok = direct->id;
	}
	else if (direct->decl != NULL)
	{
		if (ccCompileDeclarator(cc, baseType, direct->decl, &baseType, outName, outTok) == -1) return -1;
	};
	
	*outType = baseType;
	return 0;
};

static int ccCompileStaticAssert(CC_Compiler *cc, CC_StaticAssert *staticAssert)
{
	CC_Const val = ccEvalConstantExpression(cc, staticAssert->expr);
	if (val.type == CC_CONST_ERROR)
	{
		lexDiag(val.tok, CON_ERROR, "%s", val.err);
		return -1;
	};
	
	if (!ccIsTrue(val))
	{
		char *buffer = (char*) malloc(strlen(staticAssert->str->value));
		char c = lexParseString(staticAssert->str->value, buffer);
		if (c != 0)
		{
			lexDiag(staticAssert->str, CON_ERROR, "unrecognized escape sequence `\\%c'", c);
		}
		else
		{
			lexDiag(staticAssert->op, CON_ERROR, "static assertion failed: %s", buffer);
		};
		
		free(buffer);
		return -1;
	};
	
	return 0;
};

CC_Type* ccCompileSpecifierQualifierList(CC_Compiler *cc, CC_SpecifierQualifierList *list)
{
	// count up how many times each type specifier appeared in the declaration, so that
	// we can figure out what the type is supposed to be in a minute.
	// ALSO, if an atomic type, structure, enum or typedef name specifier appears,
	// keep track of that too
	TypeSpecCounter typeCounts;
	int seenTypeKeywords = 0;
	memset(&typeCounts, 0, sizeof(TypeSpecCounter));
	CC_AtomicTypeSpecifier *atomicSpec = NULL;
	CC_StructOrUnionSpecifier *structSpec = NULL;
	CC_EnumSpecifier *enumSpec = NULL;
	Token *typedefName = NULL;
	Token *lastTypeKeywordToken = NULL;
	
	CC_SpecifierQualifierList *spec;
	for (spec=list; spec!=NULL; spec=spec->next)
	{
		if (spec->alignSpecifier != NULL)
		{
			TODO;
		};
		
		CC_TypeSpecifier *typeSpec = spec->typeSpecifier;
		if (typeSpec != NULL)
		{
			if (typeSpec->atomicSpecifier != NULL)
			{
				if (atomicSpec != NULL || structSpec != NULL || enumSpec != NULL || typedefName != NULL || seenTypeKeywords)
				{
					lexDiag(typeSpec->atomicSpecifier->keyword, CON_ERROR, "atomic type specifier must not be mixed with other type specifiers");
					return NULL;
				};
				
				atomicSpec = typeSpec->atomicSpecifier;
			}
			else if (typeSpec->enumSpecifier != NULL)
			{
				if (atomicSpec != NULL || structSpec != NULL || enumSpec != NULL || typedefName != NULL || seenTypeKeywords)
				{
					lexDiag(typeSpec->enumSpecifier->keyword, CON_ERROR, "enum type specifier must not be mixed with other type specifiers");
					return NULL;
				};
				
				enumSpec = typeSpec->enumSpecifier;
			}
			else if (typeSpec->compSpecifier != NULL)
			{
				if (atomicSpec != NULL || structSpec != NULL || enumSpec != NULL || typedefName != NULL || seenTypeKeywords)
				{
					lexDiag(typeSpec->compSpecifier->keyword, CON_ERROR, "%s type specifier must not be mixed with other type specifiers", typeSpec->compSpecifier->keyword->value);
					return NULL;
				};
				
				structSpec = typeSpec->compSpecifier;
			}
			else if (typeSpec->token != NULL)
			{
				if (atomicSpec != NULL || structSpec != NULL || enumSpec != NULL || typedefName != NULL)
				{
					lexDiag(typeSpec->token, CON_ERROR, "invalid combination of type specifiers");
					return NULL;
				};
				
				switch (typeSpec->token->type)
				{
				case TOK_CC_ID:
					if (atomicSpec != NULL || structSpec != NULL || enumSpec != NULL || typedefName != NULL || seenTypeKeywords)
					{
						lexDiag(typeSpec->token, CON_ERROR, "typedef name `%s' must not be mixed with other type specifiers", typeSpec->token->value);
						return NULL;
					};
					typedefName = typeSpec->token;
					break;
				case TOK_CC_VOID:
					typeCounts.numVoid++;
					break;
				case TOK_CC_CHAR:
					typeCounts.numChar++;
					break;
				case TOK_CC_SHORT:
					typeCounts.numShort++;
					break;
				case TOK_CC_INT:
					typeCounts.numInt++;
					break;
				case TOK_CC_LONG:
					typeCounts.numLong++;
					break;
				case TOK_CC_FLOAT:
					typeCounts.numFloat++;
					break;
				case TOK_CC_DOUBLE:
					typeCounts.numDouble++;
					break;
				case TOK_CC_SIGNED:
					typeCounts.numSigned++;
					break;
				case TOK_CC_UNSIGNED:
					typeCounts.numUnsigned++;
					break;
				case TOK_CC_BOOL:
					typeCounts.numBool++;
					break;
				case TOK_CC_COMPLEX:
					typeCounts.numComplex++;
					break;
				default:
					INVALID_CODE_PATH;
					break;
				};
				
				if (typeSpec->token->type != TOK_CC_ID)
				{
					lastTypeKeywordToken = typeSpec->token;
					seenTypeKeywords = 1;
				};
			}
			else
			{
				INVALID_CODE_PATH;
			};
		};
	};
	
	// interpret qualifiers
	int qualifs = 0;
	for (spec=list; spec!=NULL; spec=spec->next)
	{
		if (spec->typeQualifier != NULL)
		{
			qualifs |= ccCompileQualifier(spec->typeQualifier);
		};
	};
	
	// interpret attributes
	int packed = 0;
	for (spec=list; spec!=NULL; spec=spec->next)
	{
		if (spec->attr != NULL)
		{
			CC_AttributeSpecifier *attr = spec->attr;
			if (strcmp(attr->id->value, "packed") == 0)
			{
				if (structSpec == NULL || structSpec->decls == NULL)
				{
					lexDiag(attr->id, CON_ERROR, "the `packed' attribute can only be specified in a struct or union definition");
				};
				
				packed = 1;
			}
			else
			{
				lexDiag(attr->id, CON_ERROR, "unrecognized attribute `%s'", attr->id->value);
			};
		};
	};
	
	// figure out which type was specified
	CC_Type *baseType;
	if (seenTypeKeywords)
	{
		int kind;
		
		PrimitiveType *prim;
		for (prim=primTypes; prim->kind!=-1; prim++)
		{
			if (memcmp(&prim->specs, &typeCounts, sizeof(TypeSpecCounter)) == 0)
			{
				break;
			};
		};
		
		if (prim->kind == -1)
		{
			lexDiag(lastTypeKeywordToken, CON_ERROR, "invalid combination of type specifiers");
			return NULL;
		};
		
		kind = prim->kind;
		
		baseType = (CC_Type*) calloc(1, sizeof(CC_Type));
		baseType->kind = kind;
		baseType->qualifs = qualifs;
	}
	else if (atomicSpec != NULL)
	{
		baseType = ccCompileTypeName(cc, atomicSpec->typeName);
		if (baseType == NULL) return NULL;
		baseType->qualifs |= CC_TQ_ATOMIC;
	}
	else if (structSpec != NULL)
	{
		const char *tag;
		if (structSpec->id != NULL)
		{
			tag = structSpec->id->value;
		}
		else
		{
			const char *format = "<anonymous struct-or-union #%d>";
			char *buffer = (char*) malloc(strlen(format) + 1);
			sprintf(buffer, format, cc->nextTagNum++);
			tag = buffer;
		};
		
		if (structSpec->decls != NULL)
		{
			if (hashtabHasKey(cc->unit->structs, tag))
			{
				lexDiag(structSpec->keyword, CON_ERROR, "redefinition of struct or union `%s'", tag);
				return NULL;
			};
			
			CC_StructDef *def = (CC_StructDef*) calloc(1, sizeof(CC_StructDef));
			if (structSpec->keyword->type == TOK_CC_UNION)
			{
				def->isUnion = 1;
			};
			
			def->align = 1;
			def->memberTable = hashtabNew();
			
			size_t currentOffset = 0;
			size_t largestMember = 0;
			
			CC_StructMember *lastMember = NULL;
			CC_StructDeclarationList *sdecl;
			for (sdecl=structSpec->decls; sdecl!=NULL; sdecl=sdecl->next)
			{
				if (sdecl->staticAssert != NULL)
				{
					if (ccCompileStaticAssert(cc, sdecl->staticAssert) == -1) return NULL;
					continue;
				};
				
				CC_Type *memberBaseType = ccCompileSpecifierQualifierList(cc, sdecl->qualifiers);
				if (memberBaseType == NULL)
				{
					return NULL;
				};
				
				if (sdecl->decls == NULL)
				{
					// if there are no declarators, then if a 'struct' or 'union'
					// was specified, we embed its members inline.
					if (memberBaseType->kind == CC_TYPE_STRUCT_OR_UNION)
					{
						if (!hashtabHasKey(cc->unit->structs, memberBaseType->tag))
						{
							lexDiag(sdecl->tok, CON_ERROR, "anonymous struct or union member is incomplete");
							return NULL;
						};
						
						CC_StructDef *subdef = (CC_StructDef*) hashtabGet(cc->unit->structs, memberBaseType->tag);
						
						if (!packed)
						{
							// align the offset
							currentOffset = (currentOffset + subdef->align - 1) & ~(subdef->align-1);
						};
						
						CC_StructMember *submember;
						for (submember=subdef->memberList; submember!=NULL; submember=submember->next)
						{
							if (submember->name == NULL) continue;
							if (hashtabHasKey(def->memberTable, submember->name))
							{
								lexDiag(submember->tok, CON_ERROR, "multiple definition of member `%s'", submember->name);
								
								CC_StructMember *prevDef = (CC_StructMember*) hashtabGet(def->memberTable, submember->name);
								
								lexDiag(prevDef->tok, CON_NOTE, "previous definition was here");
								return NULL;
							};
							
							CC_StructMember *member = (CC_StructMember*) calloc(1, sizeof(CC_StructMember));
							memcpy(member, submember, sizeof(CC_StructMember));
							member->offset += currentOffset;
							
							if (lastMember == NULL)
							{
								def->memberList = lastMember = member;
							}
							else
							{
								lastMember->next = member;
								lastMember = member;
							};
							
							hashtabSet(def->memberTable, member->name, member);
						};
						
						if (subdef->align > def->align)
						{
							def->align = subdef->align;
						};
						
						if (!def->isUnion) currentOffset += subdef->size;
						if (subdef->size > largestMember) largestMember = subdef->size;
					}
					else
					{
						lexDiag(sdecl->tok, CON_ERROR, "no name specified for struct or union member");
					};
				}
				else
				{
					CC_StructDeclaratorList *memberDecl;
					for (memberDecl=sdecl->decls; memberDecl!=NULL; memberDecl=memberDecl->next)
					{
						int bits = -1;
						const char *memberName = NULL;
						CC_Type *memberType = memberBaseType;
						Token *memberTok = NULL;
						
						if (memberDecl->bits != NULL)
						{
							CC_Const val = ccEvalConstantExpression(cc, memberDecl->bits);
							if (val.type == CC_CONST_ERROR)
							{
								lexDiag(val.tok, CON_ERROR, val.err);
								return NULL;
							};
							
							if (val.type != CC_CONST_INT || val.i < 0)
							{
								lexDiag(val.tok, CON_ERROR, "expected a positive integer value");
								return NULL;
							};

							bits = val.i;
							memberTok = val.tok;
						};
						
						if (memberDecl->decl == NULL)
						{
							if (bits == 0)
							{
								if (lastMember != NULL)
								{
									lastMember->nopack = 1;
								};
								
								continue;
							}
							else if (bits == -1)
							{
								INVALID_CODE_PATH;
							};
						}
						else
						{
							
							if (ccCompileDeclarator(cc, memberBaseType, memberDecl->decl, &memberType, &memberName, &memberTok) != 0) return NULL;
						};
						
						assert(memberTok != NULL);
						
						if (memberName != NULL)
						{
							if (hashtabHasKey(def->memberTable, memberName))
							{
								CC_StructMember *old = (CC_StructMember*) hashtabGet(def->memberTable, memberName);
								lexDiag(memberTok, CON_ERROR, "multiple definition of struct or union member `%s'", memberName);
								lexDiag(old->tok, CON_NOTE, "previous definition was here");
								return NULL;
							};
						};
						
						CC_StructMember *member = (CC_StructMember*) calloc(1, sizeof(CC_StructMember));
						member->name = memberName;
						member->size = ccGetTypeSize(cc, memberType);
						if (member->size == 0)
						{
							lexDiag(memberTok, CON_ERROR, "struct or union member has incomplete type");
							return NULL;
						};
						
						member->align = ccGetTypeAlign(cc, memberType);
						if (member->align == 0)
						{
							lexDiag(memberTok, CON_ERROR, "struct or union member has incomplete type");
							return NULL;
						};
						if (member->align == 0) member->align = 1;
						if (member->align > def->align) def->align = member->align;
						member->type = memberType;
						member->tok = memberTok;
						
						int advance = 1;
						if (bits != -1)
						{
							if (memberType->kind != CC_TYPE_BOOL && memberType->kind != CC_TYPE_SIGNED_INT && memberType->kind != CC_TYPE_UNSIGNED_INT)
							{
								lexDiag(sdecl->tok, CON_ERROR, "bit-fields must have type `_Bool', `signed int' or `unsigned int'");
								return NULL;
							};
							
							if (lastMember != NULL && lastMember->type->kind == memberType->kind && (lastMember->bitOff + lastMember->bitLen + bits) <= (8*member->size) && (!lastMember->nopack))
							{
								member->bitOff = lastMember->bitOff + lastMember->bitLen;
								member->bitLen = bits;
								member->offset = lastMember->offset;
								advance = 0;
							}
							else
							{
								member->bitOff = 0;
								member->bitLen = bits;
							};
						};
						
						if (advance)
						{
							if (!packed)
							{
								currentOffset = (currentOffset + member->align - 1) & ~(member->align - 1);
							};
							
							member->offset = currentOffset;
							if (!def->isUnion) currentOffset += member->size;
							if (member->size > largestMember) largestMember = member->size;
						};
						
						if (lastMember == NULL)
						{
							def->memberList = lastMember = member;
						}
						else
						{
							lastMember->next = member;
							lastMember = member;
						};
						
						if (memberName != NULL)
						{
							hashtabSet(def->memberTable, memberName, member);
						};
					};
				};
			};
			
			if (!packed)
			{
				currentOffset = (currentOffset + def->align - 1) & ~(def->align - 1);
				largestMember = (largestMember + def->align - 1) & ~(def->align - 1);
			};
			
			if (def->isUnion) def->size = largestMember;
			else def->size = currentOffset;

			hashtabSet(cc->unit->structs, tag, def);
		};
		
		baseType = (CC_Type*) calloc(1, sizeof(CC_Type));
		baseType->kind = CC_TYPE_STRUCT_OR_UNION;
		baseType->tag = tag;
		baseType->qualifs = qualifs;
	}
	else if (enumSpec != NULL)
	{
		CC_EnumeratorList *eval;
		int nextVal = 0;
		for (eval=enumSpec->list; eval!=NULL; eval=eval->next)
		{
			if (eval->expr != NULL)
			{
				CC_Const val = ccEvalConstantExpression(cc, eval->expr);
				if (val.type == CC_CONST_ERROR)
				{
					lexDiag(val.tok, CON_ERROR, val.err);
				}
				else if (val.type != CC_CONST_INT)
				{
					lexDiag(val.tok, CON_ERROR, "enumeration constant must be an integer");
				}
				else
				{
					nextVal = val.i;
				};
			};
			
			if (ccGetVarType(cc, eval->id->value) != NULL)
			{
				lexDiag(eval->id, CON_ERROR, "the identifier `%s' is already defined in the current scope when trying to make it an enumeration constant", eval->id->value);
			}
			else if (hashtabHasKey(cc->unit->enums, eval->id->value))
			{
				CC_EnumVal *old = (CC_EnumVal*) hashtabGet(cc->unit->enums, eval->id->value);
				lexDiag(eval->id, CON_ERROR, "multiple definition of enumeration constant `%s'", eval->id->value);
				lexDiag(old->tok, CON_NOTE, "previous definition was here");
			}
			else
			{
				CC_EnumVal *val = (CC_EnumVal*) calloc(1, sizeof(CC_EnumVal));
				val->tok = eval->id;
				val->value = nextVal++;
				
				hashtabSet(cc->unit->enums, eval->id->value, val);
			};
		};
		
		baseType = (CC_Type*) calloc(1, sizeof(CC_Type));
		baseType->kind = CC_TYPE_SIGNED_INT;
		baseType->qualifs = qualifs;
	}
	else if (typedefName != NULL)
	{
		CC_Type *currentType = ccGetVarType(cc, typedefName->value);
		assert(currentType != NULL);
		
		CC_Type *clone = (CC_Type*) malloc(sizeof(CC_Type));
		memcpy(clone, currentType, sizeof(CC_Type));
		clone->qualifs = qualifs;
		baseType = clone;
	}
	else
	{
		INVALID_CODE_PATH;
	};
	
	return baseType;
};

static int ccAddDecl(CC_Compiler *cc, const char *name, CC_Type *finalType, Token *tok, CC_Node *init, int storageClass)
{
	if (cc->scope != NULL)
	{
		if (hashtabHasKey(cc->scope->vars, name))
		{
			CC_Decl *old = (CC_Decl*) hashtabGet(cc->scope->vars, name);
			lexDiag(tok, CON_ERROR, "multiple definition of local variable `%s' in the same scope", name);
			lexDiag(old->tok, CON_NOTE, "first definition was here");
			return -1;
		}
		else
		{
			CC_Decl *gd = (CC_Decl*) calloc(1, sizeof(CC_Decl));
			gd->storageClass = storageClass;
			gd->type = finalType;
			gd->name = name;
			gd->init = init;
			gd->tok = tok;
			hashtabSet(cc->scope->vars, name, gd);
			return 0;
		};
	}
	else if (hashtabHasKey(cc->unit->globals, name))
	{
		CC_Decl *old = (CC_Decl*) hashtabGet(cc->unit->globals, name);
		if (old->storageClass != storageClass)
		{
			lexDiag(tok, CON_ERROR, "redeclaration of `%s' with incompatible storage class", name);
			lexDiag(old->tok, CON_NOTE, "previous declaration was here");
			return -1;
		};
		
		if (old->init != NULL && init != NULL)
		{
			lexDiag(tok, CON_ERROR, "multiple definition of `%s' with initializer", name);
			lexDiag(old->tok, CON_NOTE, "previous declaration was here");
			return -1;
		};
		
		if (!ccIsTypeCompatible(old->type, finalType))
		{
			lexDiag(tok, CON_ERROR, "redeclaration of `%s' with incompatible type", name);
			lexDiag(old->tok, CON_NOTE, "previous declaration was here");
			return -1;
		};
		
		if (init != NULL)
		{
			old->init = init;
		};
		
		// if the types were compatible, it could mean that the new type
		// completes the previous, so override it; this means we must also
		// mark it with the new token, so that in case an incompatible
		// declaration appears later, we can generate a proper diagnostic.
		if (ccGetTypeSize(cc, old->type) == 0)
		{
			old->type = finalType;
			old->tok = tok;
		};
	}
	else
	{
		CC_Decl *gd = (CC_Decl*) calloc(1, sizeof(CC_Decl));
		gd->storageClass = storageClass;
		gd->type = finalType;
		gd->name = name;
		gd->init = init;
		gd->tok = tok;
		hashtabSet(cc->unit->globals, name, gd);
	};
	
	return 0;
};

static int ccCompileDeclaration(CC_Compiler *cc, CC_Declaration *decl)
{
	if (decl->staticAssert != NULL)
	{
		return ccCompileStaticAssert(cc, decl->staticAssert);
	};
	
	// figure out the storage class
	int storageClass = CC_STOR_NONE;
	Token *lastStorageClassToken = NULL;
	
	CC_DeclarationSpecifiers *spec;
	for (spec=decl->declSpecs; spec!=NULL; spec=spec->next)
	{
		if (spec->funcSpecifier != NULL)
		{
			TODO;
		};
		
		if (spec->storageClass != NULL)
		{
			switch (spec->storageClass->type)
			{
			case TOK_CC_TYPEDEF:
				if (storageClass == CC_STOR_NONE)
				{
					storageClass = CC_STOR_TYPEDEF;
				}
				else
				{
					lexDiag(spec->storageClass, CON_ERROR, "invalid storage class specification");
					return -1;
				};
				break;
			case TOK_CC_EXTERN:
				if (storageClass == CC_STOR_NONE)
				{
					storageClass = CC_STOR_EXTERN;
				}
				else if (storageClass == CC_STOR_THREAD_LOCAL)
				{
					storageClass = CC_STOR_EXTERN_THREAD_LOCAL;
				}
				else
				{
					lexDiag(spec->storageClass, CON_ERROR, "invalid storage class specification");
					return -1;
				};
				break;
			case TOK_CC_STATIC:
				if (storageClass == CC_STOR_NONE)
				{
					storageClass = CC_STOR_STATIC;
				}
				else if (storageClass == CC_STOR_THREAD_LOCAL)
				{
					storageClass = CC_STOR_STATIC_THREAD_LOCAL;
				}
				else
				{
					lexDiag(spec->storageClass, CON_ERROR, "invalid storage class specification");
					return -1;
				};
				break;
			case TOK_CC_THREAD_LOCAL:
				if (storageClass == CC_STOR_NONE)
				{
					storageClass = CC_STOR_THREAD_LOCAL;
				}
				else if (storageClass == CC_STOR_EXTERN)
				{
					storageClass = CC_STOR_EXTERN_THREAD_LOCAL;
				}
				else if (storageClass == CC_STOR_STATIC)
				{
					storageClass = CC_STOR_STATIC_THREAD_LOCAL;
				}
				else
				{
					lexDiag(spec->storageClass, CON_ERROR, "invalid storage class specification");
					return -1;
				};
				break;
			case TOK_CC_AUTO:
				if (storageClass == CC_STOR_NONE)
				{
					storageClass = CC_STOR_AUTO;
				}
				else
				{
					lexDiag(spec->storageClass, CON_ERROR, "invalid storage class specification");
					return -1;
				};
				break;
			case TOK_CC_REGISTER:
				if (storageClass == CC_STOR_REGISTER)
				{
					storageClass = CC_STOR_REGISTER;
				}
				else
				{
					lexDiag(spec->storageClass, CON_ERROR, "invalid storage class specification");
					return -1;
				};
				break;
			default:
				INVALID_CODE_PATH;
				return -1;
			};
			
			lastStorageClassToken = spec->storageClass;
		};
	};
	
	if (cc->scope == NULL)
	{
		// global scope
		if (storageClass == CC_STOR_NONE)
		{
			// identifiers in global scope have external linkage by default
			storageClass = CC_STOR_EXTERN;
		};
		
		if (storageClass == CC_STOR_REGISTER || storageClass == CC_STOR_AUTO)
		{
			assert(lastStorageClassToken != NULL);
			lexDiag(lastStorageClassToken, CON_ERROR, "storage classes `register' and `auto' cannot be used on global variables");
			return -1;
		};
	}
	else
	{
		// block scope
		if (storageClass == CC_STOR_NONE)
		{
			// identifiers in block scope have auto linkage by default
			storageClass = CC_STOR_AUTO;
		};
		
		if (storageClass == CC_STOR_THREAD_LOCAL)
		{
			assert(lastStorageClassToken != NULL);
			lexDiag(lastStorageClassToken, CON_ERROR, "thread-local variable in block scope must be declared with either `extern' or `static' storage class (on top of `_Thread_local')");
			return -1;
		};
	};
	
	CC_Type *baseType = ccCompileSpecifierQualifierList(cc, (CC_SpecifierQualifierList*) decl->declSpecs);
	if (baseType == NULL) return -1;
	
	// go through declarators
	CC_InitDeclaratorList *initDecl;
	for (initDecl=decl->initDeclList; initDecl!=NULL; initDecl=initDecl->next)
	{
		const char *name = NULL;
		CC_Type *finalType = NULL;
		Token *tok;
		
		if (ccCompileDeclarator(cc, baseType, initDecl->decl, &finalType, &name, &tok) != 0) return -1;
		
		assert(name != NULL);
		assert(finalType != NULL);
		
		CC_Node *init = NULL;
		if (initDecl->init != NULL)
		{
			if (initDecl->init->assign != NULL)
			{
				init = ccCompileAssignmentExpression(cc, initDecl->init->assign);
			}
			else
			{
				init = ccCompileInitializerList(cc, initDecl->init->initList, tok);
			};
		};		
		
		if (ccAddDecl(cc, name, finalType, tok, init, storageClass) != 0) return -1;
	};
	
	return 0;
};

static CC_Node* ccNodeAP(Token *tok, int type, const char *str, CC_NodeConst cval, int numChildren, va_list ap)
{
	CC_Node *node = (CC_Node*) malloc(sizeof(CC_Node) + sizeof(void*) * numChildren);
	node->tok = tok;
	node->type = type;
	node->str = str;
	node->cval = cval;
	node->numChildren = numChildren;
	
	int i = 0;
	while (numChildren--)
	{
		node->children[i++] = va_arg(ap, CC_Node*);
	};
	
	return node;
};

static CC_Node* ccNodeV(Token *tok, int type, const char *str, CC_NodeConst cval, int numChildren, CC_Node **children)
{
	CC_Node *node = (CC_Node*) malloc(sizeof(CC_Node) + sizeof(void*) * numChildren);
	node->tok = tok;
	node->type = type;
	node->str = str;
	node->cval = cval;
	node->numChildren = numChildren;
	
	int i = 0;
	while (numChildren--)
	{
		node->children[i] = children[i];
		i++;
	};
	
	return node;
};

static CC_Node* ccNode(Token *tok, int type, const char *str, CC_NodeConst cval, int numChildren, ...)
{
	va_list ap;
	va_start(ap, numChildren);
	
	CC_Node *node = ccNodeAP(tok, type, str, cval, numChildren, ap);
	
	va_end(ap);
	return node;
};

static CC_Node* ccNodeC(Token *tok, int type, int numChildren, ...)
{
	va_list ap;
	va_start(ap, numChildren);
	
	CC_Node *node = ccNodeAP(tok, type, NULL, (CC_NodeConst) {0}, numChildren, ap);
	
	va_end(ap);
	return node;
};

static CC_Node* ccConstNode(Token *tok, int type, CC_NodeConst cval)
{
	return ccNode(tok, type, NULL, cval, 0);
};

static CC_Node* ccAllocLocal(Token *tok, CC_Type *type, const char *name, CC_Node *init)
{
	CC_Node *node;
	CC_NodeConst cval = {.valType = type};
	if (init != NULL)
	{
		node = ccNode(tok, CC_alloc_local, name, cval, 1, init);
	}
	else
	{
		node = ccNode(tok, CC_alloc_local, name, cval, 0);
	};
	
	return node;
};

static CC_Node* ccExprSeq(Token *tok, CC_Node *left, CC_Node *right)
{
	return ccNode(tok, CC_expr_seq, NULL, (CC_NodeConst) {0}, 2, left, right);
};

static CC_Node* ccCompileIntConst(Token *tok)
{
	const char *scan = (const char*) tok->value;
	
	uint64_t num = 0;
	uint64_t base = 10;
	if (*scan == '0')
	{
		base = 8;
		scan++;
		
		if (*scan == 'x')
		{
			base = 16;
			scan++;
		};
	};
	
	while (1)
	{
		char c = *scan;
		
		uint64_t digit;
		if (c >= '0' && c <= '9')
		{
			digit = c - '0';
		}
		else if (c >= 'a' && c <= 'f')
		{
			digit = c - 'a' + 16;
		}
		else if (c >= 'A' && c <= 'F')
		{
			digit = c - 'A' + 16;
		}
		else
		{
			break;
		};
		
		num = num * base + digit;
		scan++;
	};
	
	int type;
	if (*scan == 0)
	{
		type = CC_int;
	}
	else if (strcmp(scan, "L") == 0)
	{
		type = CC_long;
	}
	else if (strcmp(scan, "U") == 0)
	{
		type = CC_uint;
	}
	else if (strcmp(scan, "LL") == 0)
	{
		type = CC_longlong;
	}
	else if (strcmp(scan, "ULL") == 0)
	{
		type = CC_ulonglong;
	}
	else
	{
		INVALID_CODE_PATH;
	};
	
	CC_NodeConst cval = {.valU = num};
	return ccNode(tok, type, NULL, cval, 0);
};

static CC_Node* ccCompileFloatConst(Token *tok)
{
	CC_NodeConst cval = {.valDouble = atof(tok->value)};
	return ccNode(tok, CC_double, NULL, cval, 0);
};

static CC_Node* ccCompileCharConst(Token *tok)
{
	char buf[2];
	char c = lexParseString(tok->value, buf);
	if (c != 0)
	{
		lexDiag(tok, CON_ERROR, "invalid escape sequence `\\%c'", c);
		return NULL;
	};
	
	CC_NodeConst cval = {.valI = (int) buf[0]};
	return ccNode(tok, CC_int, NULL, cval, 0);
};

static CC_Node* ccCompileString(Token *tok)
{
	char *buffer = (char*) malloc(strlen(tok->value)+1);
	char c = lexParseString(tok->value, buffer);
	if (c != 0)
	{
		free(buffer);
		lexDiag(tok, CON_ERROR, "invalid escape sequence `\\%c'", c);
		return NULL;
	};
	
	return ccNode(tok, CC_string, buffer, (CC_NodeConst) {0}, 0);
};

static CC_Node* ccCompilePrimaryExpression(CC_Compiler *cc, CC_PrimaryExpression *primary)
{
	if (primary->tok != NULL)
	{
		switch (primary->tok->type)
		{
		case TOK_CC_ID:
			return ccNode(primary->tok, CC_id, primary->tok->value, (CC_NodeConst) {0}, 0);
		case TOK_CC_ICONST:
			return ccCompileIntConst(primary->tok);
		case TOK_CC_FCONST:
			return ccCompileFloatConst(primary->tok);
		case TOK_CC_CHARCONST:
			return ccCompileCharConst(primary->tok);
		case TOK_CC_STRING:
			return ccCompileString(primary->tok);
		default:
			INVALID_CODE_PATH;
			return NULL;
		};
	}
	else if (primary->expr != NULL)
	{
		return ccCompileExpression(cc, primary->expr);
	}
	else
	{
		TODO;
		return NULL;
	};
};

static CC_Node* ccCompileDesignatorList(CC_Compiler *cc, CC_DesignatorList *desig)
{
	const char *str = NULL;
	CC_NodeConst cval = {.valI = 0};
	Token *tok;
	
	if (desig->expr != NULL)
	{
		CC_Const val = ccEvalConstantExpression(cc, desig->expr);
		if (val.type == CC_CONST_ERROR)
		{
			lexDiag(val.tok, CON_ERROR, val.err);
			return NULL;
		};
		
		if (val.type != CC_CONST_INT)
		{
			lexDiag(val.tok, CON_ERROR, "expected an integer constant");
			return NULL;
		};
		
		cval.valI = val.i;
		tok = val.tok;
	}
	else
	{
		str = desig->id->value;
		tok = desig->id;
	};
	
	if (desig->next != NULL)
	{
		CC_Node *child = ccCompileDesignatorList(cc, desig->next);
		if (child == NULL) return NULL;
		
		return ccNode(tok, CC_desig, str, cval, 1, child);
	}
	else
	{
		return ccNode(tok, CC_desig, str, cval, 0);
	};
};

static CC_Node* ccCompileInitializerList(CC_Compiler *cc, CC_InitializerList *initList, Token *tok)
{
	CC_Node **nodes = NULL;
	int numNodes = 0;
	
	for (; initList!=NULL; initList=initList->next)
	{
		CC_Node *desig = NULL;
		if (initList->designation != NULL)
		{
			desig = ccCompileDesignatorList(cc, initList->designation->list);
			if (desig == NULL) return NULL;
		};
		
		CC_Node *expr;
		if (initList->init->assign != NULL)
		{
			expr = ccCompileAssignmentExpression(cc, initList->init->assign);
		}
		else
		{
			expr = ccCompileInitializerList(cc, initList->init->initList, initList->tok);
		};
		if (expr == NULL) return NULL;
		
		CC_Node *child;
		if (desig == NULL) child = ccNodeC(expr->tok, CC_init, 1, expr);
		else child = ccNodeC(expr->tok, CC_init, 2, expr, desig);
		
		int index = numNodes++;
		nodes = (CC_Node**) realloc(nodes, sizeof(void*) * numNodes);
		nodes[index] = child;
	};
	
	return ccNodeV(tok, CC_init_list, NULL, (CC_NodeConst) {0}, numNodes, nodes);
};

static CC_Node* ccCompilePostfixExpression(CC_Compiler *cc, CC_PostfixExpression *postfix)
{
	if (postfix->primary == NULL)
	{
		CC_Type *type = ccCompileTypeName(cc, postfix->typeName);
		if (type == NULL) return NULL;
		
		CC_Node *initList = ccCompileInitializerList(cc, postfix->initList, postfix->typeName->tok);
		if (initList == NULL) return NULL;
		
		CC_NodeConst cval = {.valType = type};
		return ccNode(postfix->typeName->tok, CC_compound_const, NULL, cval, 1, initList);
	}
	else
	{
		CC_Node *node = ccCompilePrimaryExpression(cc, postfix->primary);
		if (node == NULL) return NULL;
		
		CC_PostfixTail *tail;
		for (tail=postfix->tail; tail!=NULL; tail=tail->next)
		{
			if (tail->subscript != NULL)
			{
				CC_Node *index = ccCompileExpression(cc, tail->subscript->expr);
				if (index == NULL) return NULL;
				
				node = ccNodeC(tail->tok, CC_subscript, 2, node, index);
			}
			else if (tail->call != NULL)
			{
				CC_Node **argNodes = (CC_Node**) malloc(sizeof(void*));
				argNodes[0] = node;
				int numNodes = 1;
				
				CC_ArgumentExpressionList *arg;
				for (arg=tail->call->list; arg!=NULL; arg=arg->next)
				{
					CC_Node *sub = ccCompileAssignmentExpression(cc, arg->assign);
					if (sub == NULL) return NULL;
					
					int index = numNodes++;
					argNodes = (CC_Node**) realloc(argNodes, sizeof(void*) * numNodes);
					argNodes[index] = sub;
				};
				
				node = ccNodeV(tail->tok, CC_call, NULL, (CC_NodeConst) {0}, numNodes, argNodes);
			}
			else if (tail->member != NULL)
			{
				switch (tail->member->op->type)
				{
				case TOK_CC_DOT:
					node = ccNode(tail->member->id, CC_member, tail->member->id->value, (CC_NodeConst) {0}, 1, node);
					break;
				case TOK_CC_MARROW:
					node = ccNode(tail->member->id, CC_member, tail->member->id->value, (CC_NodeConst) {0}, 1, ccNodeC(tail->tok, CC_deref, 1, node));
					break;
				default:
					INVALID_CODE_PATH;
					break;
				};
			}
			else if (tail->op != NULL)
			{
				switch (tail->op->type)
				{
				case TOK_CC_INC:
					node = ccNodeC(tail->op, CC_post_inc, 1, node);
					break;
				case TOK_CC_DEC:
					node = ccNodeC(tail->op, CC_post_dec, 1, node);
					break;
				default:
					INVALID_CODE_PATH;
					break;
				};
			}
			else
			{
				INVALID_CODE_PATH;
			};
		};
		
		return node;
	};
};

static CC_Node* ccCompileUnaryExpression(CC_Compiler *cc, CC_UnaryExpression *unary)
{
	if (unary->postfix != NULL)
	{
		return ccCompilePostfixExpression(cc, unary->postfix);
	}
	else
	{
		CC_Node *sub;
		
		switch (unary->op->type)
		{
		case TOK_CC_INC:
			sub = ccCompileUnaryExpression(cc, unary->unary);
			if (sub == NULL) return NULL;
			return ccNodeC(unary->op, CC_pre_inc, 1, sub);
		case TOK_CC_DEC:
			sub = ccCompileUnaryExpression(cc, unary->unary);
			if (sub == NULL) return NULL;
			return ccNodeC(unary->op, CC_pre_dec, 1, sub);
		case TOK_CC_BAND:
			sub = ccCompileCastExpression(cc, unary->cast);
			if (sub == NULL) return NULL;
			return ccNodeC(unary->op, CC_ptr, 1, sub);
		case TOK_CC_AST:
			sub = ccCompileCastExpression(cc, unary->cast);
			if (sub == NULL) return NULL;
			return ccNodeC(unary->op, CC_deref, 1, sub);
		case TOK_CC_PLUS:
			sub = ccCompileCastExpression(cc, unary->cast);
			if (sub == NULL) return NULL;
			return ccNodeC(unary->op, CC_promote, 1, sub);
		case TOK_CC_MINUS:
			sub = ccCompileCastExpression(cc, unary->cast);
			if (sub == NULL) return NULL;
			return ccNodeC(unary->op, CC_negate, 1, sub);
		case TOK_CC_TILDE:
			sub = ccCompileCastExpression(cc, unary->cast);
			if (sub == NULL) return NULL;
			return ccNodeC(unary->op, CC_bitwise_not, 1, sub);
		case TOK_CC_EXCL:
			sub = ccCompileCastExpression(cc, unary->cast);
			if (sub == NULL) return NULL;
			return ccNodeC(unary->op, CC_logical_not, 1, sub);
		case TOK_CC_SIZEOF:
			if (unary->unary != NULL)
			{
				sub = ccCompileUnaryExpression(cc, unary->unary);
				if (sub == NULL) return NULL;
				return ccNodeC(unary->op, CC_sizeof, 1, sub);
			}
			else
			{
				CC_Type *type = ccCompileTypeName(cc, unary->typeName);
				if (type == NULL) return NULL;
				size_t size = ccGetTypeSize(cc, type);
				if (cc->dataModel->sizeofPtr == cc->dataModel->sizeofInt)
				{
					return ccConstNode(unary->op, CC_uint, (CC_NodeConst) {.valI = (uint64_t) size});
				}
				else if (cc->dataModel->sizeofPtr == cc->dataModel->sizeofLong)
				{
					return ccConstNode(unary->op, CC_ulong, (CC_NodeConst) {.valU = (uint64_t) size});
				}
				else
				{
					INVALID_CODE_PATH;
				};
			};
		case TOK_CC_ALIGNOF:
			{
				CC_Type *type = ccCompileTypeName(cc, unary->typeName);
				if (type == NULL) return NULL;
				size_t align = ccGetTypeAlign(cc, type);
				if (cc->dataModel->sizeofPtr == cc->dataModel->sizeofInt)
				{
					return ccConstNode(unary->op, CC_uint, (CC_NodeConst) {.valI = (uint64_t) align});
				}
				else if (cc->dataModel->sizeofPtr == cc->dataModel->sizeofLong)
				{
					return ccConstNode(unary->op, CC_ulong, (CC_NodeConst) {.valU = (uint64_t) align});
				}
				else
				{
					INVALID_CODE_PATH;
				};
			};
		default:
			INVALID_CODE_PATH;
			return NULL;
		};
	};
};

static CC_Node* ccCompileCastExpression(CC_Compiler *cc, CC_CastExpression *cast)
{
	if (cast->unary != NULL)
	{
		return ccCompileUnaryExpression(cc, cast->unary);
	}
	else
	{
		CC_Type *type = ccCompileTypeName(cc, cast->typeName);
		if (type == NULL) return NULL;
		
		CC_NodeConst cval = {.valType = type};
		CC_Node *node = ccCompileCastExpression(cc, cast->cast);
		return ccNode(cast->typeName->tok, CC_cast, NULL, cval, 1, node);
	};
};

static CC_Node* ccCompileMultiplicativeExpression(CC_Compiler *cc, CC_MultiplicativeExpression *mul)
{
	CC_Node *node = ccCompileCastExpression(cc, mul->cast);
	if (node == NULL) return NULL;
	
	while (mul->next != NULL)
	{
		Token *op = mul->op;
		mul = mul->next;
		
		CC_Node *sub = ccCompileCastExpression(cc, mul->cast);
		if (sub == NULL) return NULL;
		
		int type;
		switch (op->type)
		{
		case TOK_CC_AST:
			type = CC_mul;
			break;
		case TOK_CC_SLASH:
			type = CC_div;
			break;
		case TOK_CC_MODULO:
			type = CC_mod;
			break;
		default:
			INVALID_CODE_PATH;
			break;
		};
		
		node = ccNodeC(op, type, 2, node, sub);
	};
	
	return node;
};

static CC_Node* ccCompileAdditiveExpression(CC_Compiler *cc, CC_AdditiveExpression *add)
{
	CC_Node *node = ccCompileMultiplicativeExpression(cc, add->mul);
	if (node == NULL) return NULL;
	
	while (add->next != NULL)
	{
		Token *op = add->op;
		add = add->next;
		
		CC_Node *sub = ccCompileMultiplicativeExpression(cc, add->mul);
		if (sub == NULL) return NULL;
		
		int type;
		switch (op->type)
		{
		case TOK_CC_PLUS:
			type = CC_add;
			break;
		case TOK_CC_MINUS:
			type = CC_sub;
			break;
		default:
			INVALID_CODE_PATH;
			break;
		};
		
		node = ccNodeC(op, type, 2, node, sub);
	};
	
	return node;
};

static CC_Node* ccCompileShiftExpression(CC_Compiler *cc, CC_ShiftExpression *shift)
{
	CC_Node *node = ccCompileAdditiveExpression(cc, shift->add);
	if (node == NULL) return NULL;
	
	while (shift->next != NULL)
	{
		Token *op = shift->op;
		shift = shift->next;
		
		CC_Node *sub = ccCompileAdditiveExpression(cc, shift->add);
		if (sub == NULL) return NULL;
		
		int type;
		switch (op->type)
		{
		case TOK_CC_LSHIFT:
			type = CC_shl;
			break;
		case TOK_CC_RSHIFT:
			type = CC_shr;
			break;
		default:
			INVALID_CODE_PATH;
			break;
		};
		
		node = ccNodeC(op, type, 2, node, sub);
	};
	
	return node;
};

static CC_Node* ccCompileRelationalExpression(CC_Compiler *cc, CC_RelationalExpression *rel)
{
	CC_Node *node = ccCompileShiftExpression(cc, rel->shift);
	if (node == NULL) return NULL;
	
	while (rel->next != NULL)
	{
		Token *op = rel->op;
		rel = rel->next;
		
		CC_Node *sub = ccCompileShiftExpression(cc, rel->shift);
		if (sub == NULL) return NULL;
		
		int type;
		switch (op->type)
		{
		case TOK_CC_LARROW:
			type = CC_less;
			break;
		case TOK_CC_RARROW:
			type = CC_greater;
			break;
		case TOK_CC_LARROW_EQ:
			type = CC_less_eq;
			break;
		case TOK_CC_RARROW_EQ:
			type = CC_greater_eq;
			break;
		default:
			INVALID_CODE_PATH;
			break;
		};
		
		node = ccNodeC(op, type, 2, node, sub);
	};
	
	return node;
};

static CC_Node* ccCompileEqualityExpression(CC_Compiler *cc, CC_EqualityExpression *equality)
{
	CC_Node *node = ccCompileRelationalExpression(cc, equality->rel);
	if (node == NULL) return NULL;
	
	while (equality->next != NULL)
	{
		Token *op = equality->op;
		equality = equality->next;
		
		CC_Node *sub = ccCompileRelationalExpression(cc, equality->rel);
		if (sub == NULL) return NULL;
		
		int type;
		switch (op->type)
		{
		case TOK_CC_EQ:
			type = CC_eq;
			break;
		case TOK_CC_NEQ:
			type = CC_neq;
			break;
		default:
			INVALID_CODE_PATH;
			break;
		};
		
		node = ccNodeC(op, type, 2, node, sub);
	};
	
	return node;
};

static CC_Node* ccCompileBitwiseANDExpression(CC_Compiler *cc, CC_BitwiseANDExpression *bitwiseAND)
{
	CC_Node *node = ccCompileEqualityExpression(cc, bitwiseAND->equality);
	if (node == NULL) return NULL;
	
	while (bitwiseAND->next != NULL)
	{
		Token *op = bitwiseAND->op;
		bitwiseAND = bitwiseAND->next;
		
		CC_Node *sub = ccCompileEqualityExpression(cc, bitwiseAND->equality);
		if (sub == NULL) return NULL;
		
		node = ccNodeC(op, CC_bitwise_and, 2, node, sub);
	};
	
	return node;
};

static CC_Node* ccCompileBitwiseXORExpression(CC_Compiler *cc, CC_BitwiseXORExpression *bitwiseXOR)
{
	CC_Node *node = ccCompileBitwiseANDExpression(cc, bitwiseXOR->bitwiseAND);
	if (node == NULL) return NULL;
	
	while (bitwiseXOR->next != NULL)
	{
		Token *op = bitwiseXOR->op;
		bitwiseXOR = bitwiseXOR->next;
		
		CC_Node *sub = ccCompileBitwiseANDExpression(cc, bitwiseXOR->bitwiseAND);
		if (sub == NULL) return NULL;
		
		node = ccNodeC(op, CC_bitwise_xor, 2, node, sub);
	};
	
	return node;
};

static CC_Node* ccCompileBitwiseORExpression(CC_Compiler *cc, CC_BitwiseORExpression *bitwiseOR)
{
	CC_Node *node = ccCompileBitwiseXORExpression(cc, bitwiseOR->bitwiseXOR);
	if (node == NULL) return NULL;
	
	while (bitwiseOR->next != NULL)
	{
		Token *op = bitwiseOR->op;
		bitwiseOR = bitwiseOR->next;
		
		CC_Node *sub = ccCompileBitwiseXORExpression(cc, bitwiseOR->bitwiseXOR);
		if (sub == NULL) return NULL;
		
		node = ccNodeC(op, CC_bitwise_or, 2, node, sub);
	};
	
	return node;
};

static CC_Node* ccCompileLogicalANDExpression(CC_Compiler *cc, CC_LogicalANDExpression *logicalAND)
{
	CC_Node *node = ccCompileBitwiseORExpression(cc, logicalAND->bitwiseOR);
	if (node == NULL) return NULL;
	
	while (logicalAND->next != NULL)
	{
		Token *op = logicalAND->op;
		logicalAND = logicalAND->next;
		
		CC_Node *sub = ccCompileBitwiseORExpression(cc, logicalAND->bitwiseOR);
		if (sub == NULL) return NULL;
		
		node = ccNodeC(op, CC_logical_and, 2, node, sub);
	};
	
	return node;
};

static CC_Node* ccCompileLogicalORExpression(CC_Compiler *cc, CC_LogicalORExpression *logicalOR)
{
	CC_Node *node = ccCompileLogicalANDExpression(cc, logicalOR->logicalAND);
	if (node == NULL) return NULL;
	
	while (logicalOR->next != NULL)
	{
		Token *op = logicalOR->op;
		logicalOR = logicalOR->next;
		
		CC_Node *sub = ccCompileLogicalANDExpression(cc, logicalOR->logicalAND);
		if (sub == NULL) return NULL;
		
		node = ccNodeC(op, CC_logical_or, 2, node, sub);
	};
	
	return node;
};

static CC_Node* ccCompileConditionalExpression(CC_Compiler *cc, CC_ConditionalExpression *cond)
{
	CC_Node *left = ccCompileLogicalORExpression(cc, cond->left);
	if (left == NULL) return NULL;
	if (cond->trueExpr == NULL) return left;
	
	CC_Node *trueExpr = ccCompileExpression(cc, cond->trueExpr);
	CC_Node *falseExpr = ccCompileConditionalExpression(cc, cond->falseExpr);
	if (trueExpr == NULL || falseExpr == NULL) return NULL;
	
	return ccNodeC(cond->op, CC_cond, 3, left, trueExpr, falseExpr);
};

static CC_Node* ccCompileAssignmentExpression(CC_Compiler *cc, CC_AssignmentExpression *assign)
{
	if (assign->cond != NULL)
	{
		return ccCompileConditionalExpression(cc, assign->cond);
	}
	else
	{
		CC_Node *left = ccCompileUnaryExpression(cc, assign->left);
		CC_Node *right = ccCompileAssignmentExpression(cc, assign->right);
		if (left == NULL || right == NULL) return NULL;
		
		int op;
		switch (assign->op->type)
		{
		case TOK_CC_ASSIGN:
			op = CC_assign;
			break;
		case TOK_CC_ASSIGN_AST:
			op = CC_assign_mul;
			break;
		case TOK_CC_ASSIGN_SLASH:
			op = CC_assign_div;
			break;
		case TOK_CC_ASSIGN_MODULO:
			op = CC_assign_mod;
			break;
		case TOK_CC_ASSIGN_PLUS:
			op = CC_assign_add;
			break;
		case TOK_CC_ASSIGN_MINUS:
			op = CC_assign_sub;
			break;
		case TOK_CC_ASSIGN_LSHIFT:
			op = CC_assign_shl;
			break;
		case TOK_CC_ASSIGN_RSHIFT:
			op = CC_assign_shr;
			break;
		case TOK_CC_ASSIGN_AND:
			op = CC_assign_and;
			break;
		case TOK_CC_ASSIGN_XOR:
			op = CC_assign_xor;
			break;
		case TOK_CC_ASSIGN_OR:
			op = CC_assign_or;
			break;
		default:
			INVALID_CODE_PATH;
			break;
		};
		
		return ccNodeC(assign->op, op, 2, left, right);
	};
};

static CC_Node* ccCompileExpression(CC_Compiler *cc, CC_Expression *expr)
{
	if (expr->next == NULL)
	{
		return ccCompileAssignmentExpression(cc, expr->assign);
	}
	else
	{
		CC_Node *left = ccCompileAssignmentExpression(cc, expr->assign);
		CC_Node *right = ccCompileExpression(cc, expr->next);
		if (left == NULL || right == NULL) return NULL;
		return ccExprSeq(expr->op, left, right);
	};
};

static CC_Node* ccCompileJumpStatement(CC_Compiler *cc, CC_JumpStatement *jmp)
{
	switch (jmp->keyword->type)
	{
	case TOK_CC_RETURN:
		{
			if (jmp->expr != NULL)
			{
				CC_Node *node = ccCompileExpression(cc, jmp->expr);
				if (node == NULL) return NULL;
				
				return ccNodeC(jmp->keyword, CC_ret, 1, node);
			}
			else
			{
				return ccNodeC(jmp->keyword, CC_ret, 0);
			};
		};
	case TOK_CC_CONTINUE:
		return ccNodeC(jmp->keyword, CC_continue, 0);
	case TOK_CC_BREAK:
		return ccNodeC(jmp->keyword, CC_break, 0);
	case TOK_CC_GOTO:
		return ccNode(jmp->keyword, CC_goto, jmp->id->value, (CC_NodeConst) {0}, 0);
	default:
		INVALID_CODE_PATH;
		return NULL;
	};
};

static CC_Node* ccCompileIfStatement(CC_Compiler *cc, CC_IfStatement *ifs)
{
	CC_Node *expr = ccCompileExpression(cc, ifs->expr);
	if (expr == NULL) return NULL;
	
	CC_Node *trueAction = ccCompileStatement(cc, ifs->trueAction);
	if (trueAction == NULL) return NULL;
	
	if (ifs->falseAction != NULL)
	{
		CC_Node *falseAction = ccCompileStatement(cc, ifs->falseAction);
		if (falseAction == NULL) return NULL;
		
		return ccNodeC(ifs->op, CC_if, 3, expr, trueAction, falseAction);
	}
	else
	{
		return ccNodeC(ifs->op, CC_if, 2, expr, trueAction);
	};
};

static CC_Node* ccCompileLoopStatement(CC_Compiler *cc, CC_SwitchStatement *sw)
{
	CC_Node *expr = ccCompileExpression(cc, sw->expr);
	if (expr == NULL) return NULL;
	
	CC_Node *body = ccCompileStatement(cc, sw->st);
	if (body == NULL) return NULL;
	
	return ccNodeC(sw->op, CC_switch, 2, expr, body);
};

static CC_Node* ccCompileWhileStatement(CC_Compiler *cc, CC_WhileStatement *sw)
{
	CC_Node *expr = ccCompileExpression(cc, sw->expr);
	if (expr == NULL) return NULL;
	
	CC_Node *body = ccCompileStatement(cc, sw->st);
	if (body == NULL) return NULL;
	
	return ccNodeC(sw->op, CC_while, 2, expr, body);
};

static CC_Node* ccCompileDoWhileStatement(CC_Compiler *cc, CC_DoWhileStatement *sw)
{
	CC_Node *expr = ccCompileExpression(cc, sw->expr);
	if (expr == NULL) return NULL;
	
	CC_Node *body = ccCompileStatement(cc, sw->st);
	if (body == NULL) return NULL;
	
	return ccNodeC(sw->op, CC_do_while, 2, expr, body);
};

static CC_Node* ccCompileForStatement(CC_Compiler *cc, CC_ForStatement *fors)
{
	CC_Node *init;
	if (fors->decl != NULL)
	{
		TODO;
		return NULL;
	}
	else if (fors->init != NULL)
	{
		init = ccCompileExpression(cc, fors->init);
	}
	else
	{
		init = ccNodeC(fors->op, CC_nop, 0);
	};
	
	CC_Node *cond;
	if (fors->cond != NULL)
	{
		cond = ccCompileExpression(cc, fors->cond);
	}
	else
	{
		cond = ccNode(fors->op, CC_uint, NULL, (CC_NodeConst) {.valU = 1}, 0);
	};
	
	CC_Node *iter;
	if (fors->iter != NULL)
	{
		iter = ccCompileExpression(cc, fors->iter);
	}
	else
	{
		iter = ccNodeC(fors->op, CC_nop, 0);
	};
	
	CC_Node *body = ccCompileStatement(cc, fors->st);
	if (body == NULL) return NULL;
	
	return ccNodeC(fors->op, CC_for, 4, init, cond, iter, body);
};

static CC_Node* ccCompileStatement(CC_Compiler *cc, CC_Statement *st)
{
	if (st->comp != NULL)
	{
		return ccCompileCompoundStatement(cc, st->comp);
	}
	else if (st->expr != NULL)
	{
		return ccCompileExpression(cc, st->expr);
	}
	else if (st->ifs != NULL)
	{
		return ccCompileIfStatement(cc, st->ifs);
	}
	else if (st->sw != NULL)
	{
		return ccCompileLoopStatement(cc, st->sw);
	}
	else if (st->whiles != NULL)
	{
		return ccCompileWhileStatement(cc, st->whiles);
	}
	else if (st->doWhile != NULL)
	{
		return ccCompileDoWhileStatement(cc, st->doWhile);
	}
	else if (st->fors != NULL)
	{
		return ccCompileForStatement(cc, st->fors);
	}
	else if (st->jmp != NULL)
	{
		return ccCompileJumpStatement(cc, st->jmp);
	}
	else
	{
		// if everything was NULL, it indicates a no-op
		return ccNodeC(st->tok, CC_nop, 0);
	};
};

static CC_Node* ccCompileCompoundStatement(CC_Compiler *cc, CC_CompoundStatement *comp)
{
	CC_Node **nodes = NULL;
	int numNodes = 0;
	
	CC_Scope *scope = (CC_Scope*) calloc(1, sizeof(CC_Scope));
	scope->prev = cc->scope;
	scope->vars = hashtabNew();
	cc->scope = scope;
	
	CC_BlockItemList *item;
	for (item=comp->items; item!=NULL; item=item->next)
	{
		if (item->decl != NULL)
		{
			ccCompileDeclaration(cc, item->decl);
			
			const char *varname;
			HASHTAB_FOREACH(scope->vars, CC_Decl *, varname, decl)
			{
				if (!decl->initDone && decl->storageClass == CC_STOR_AUTO)
				{
					decl->initDone = 1;
					
					if (decl->type->kind == CC_TYPE_FUNC)
					{
						lexDiag(decl->tok, CON_ERROR, "function declaration in block scope");
						continue;
					};
					
					CC_Node *child = ccAllocLocal(decl->tok, decl->type, strdup(varname), decl->init);
					
					int index = numNodes++;
					nodes = (CC_Node**) realloc(nodes, sizeof(void*) * numNodes);
					nodes[index] = child;
				};
			};
		}
		else if (item->st != NULL)
		{
			CC_Statement *st = item->st;
			while (st->label != NULL)
			{
				CC_LabeledStatement *label = st->label;
				st = label->st;
				
				CC_Node *child;
				if (label->id != NULL)
				{
					child = ccNode(label->id, CC_label, label->id->value, (CC_NodeConst) {0}, 0);
				}
				else if (label->keyword->type == TOK_CC_CASE)
				{
					CC_Const val = ccEvalConstantExpression(cc, label->expr);
					if (val.type == CC_CONST_ERROR)
					{
						lexDiag(val.tok, CON_ERROR, val.err);
						continue;
					};
					
					if (val.type != CC_CONST_INT)
					{
						lexDiag(val.tok, CON_ERROR, "expected an integer constant expression for `case' statement");
						continue;
					};
					
					child = ccNode(label->keyword, CC_case, NULL, (CC_NodeConst) {.valI = val.i}, 0);
				}
				else if (label->keyword->type == TOK_CC_DEFAULT)
				{
					child = ccNodeC(label->keyword, CC_default, 0);
				}
				else
				{
					INVALID_CODE_PATH;
				};

				int index = numNodes++;
				nodes = (CC_Node**) realloc(nodes, sizeof(void*) * numNodes);
				nodes[index] = child;
			};
			
			CC_Node *child = ccCompileStatement(cc, st);
			if (child == NULL) continue;
			
			int index = numNodes++;
			nodes = (CC_Node**) realloc(nodes, sizeof(void*) * numNodes);
			nodes[index] = child;
		}
		else
		{
			INVALID_CODE_PATH;
		};
	};
	
	cc->scope = scope->prev;
	hashtabDelete(scope->vars);
	free(scope);
	
	return ccNodeV(comp->tok, CC_block, NULL, (CC_NodeConst) {0}, numNodes, nodes);
};

static int ccCompileFunctionDefinition(CC_Compiler *cc, CC_FunctionDefinition *funcDef)
{
	// figure out the storage class
	int storageClass = CC_STOR_NONE;
	
	CC_DeclarationSpecifiers *spec;
	for (spec=funcDef->specs; spec!=NULL; spec=spec->next)
	{	
		if (spec->storageClass != NULL)
		{
			switch (spec->storageClass->type)
			{
			case TOK_CC_EXTERN:
				if (storageClass == CC_STOR_NONE)
				{
					storageClass = CC_STOR_EXTERN;
				}
				else if (storageClass == CC_STOR_THREAD_LOCAL)
				{
					storageClass = CC_STOR_EXTERN_THREAD_LOCAL;
				}
				else
				{
					lexDiag(spec->storageClass, CON_ERROR, "invalid storage class specification");
					return -1;
				};
				break;
			case TOK_CC_STATIC:
				if (storageClass == CC_STOR_NONE)
				{
					storageClass = CC_STOR_STATIC;
				}
				else if (storageClass == CC_STOR_THREAD_LOCAL)
				{
					storageClass = CC_STOR_STATIC_THREAD_LOCAL;
				}
				else
				{
					lexDiag(spec->storageClass, CON_ERROR, "invalid storage class specification");
					return -1;
				};
				break;
			default:
				lexDiag(spec->storageClass, CON_ERROR, "storage class `%s' not allowed on functions", spec->storageClass->value);
				return -1;
			};
		};
	};
	
	if (storageClass == CC_STOR_NONE)
	{
		storageClass = CC_STOR_EXTERN;
	};
	
	CC_Type *type = ccCompileSpecifierQualifierList(cc, (CC_SpecifierQualifierList*) funcDef->specs);
	if (type == NULL) return -1;
	
	const char *funcName;
	Token *funcTok;
	if (ccCompileDeclarator(cc, type, funcDef->decl, &type, &funcName, &funcTok) != 0) return -1;
	
	if (type->kind != CC_TYPE_FUNC)
	{
		lexDiag(funcDef->body->tok, CON_ERROR, "function body on a declarator which does not declare a function");
		return -1;
	};
	
	if (type->base->kind == CC_TYPE_ARRAY)
	{
		lexDiag(funcTok, CON_ERROR, "the return type of a function cannot be an array type");
		return -1;
	};
	
	// add the declaration to the table
	if (ccAddDecl(cc, funcName, type, funcTok, NULL, storageClass) != 0) return -1;
	
	CC_Node *body = ccCompileCompoundStatement(cc, funcDef->body);
	if (body == NULL) return -1;
	
	CC_Func *func = (CC_Func*) calloc(1, sizeof(CC_Func));
	func->next = cc->unit->funcs;
	cc->unit->funcs = func;
	
	func->storageClass = storageClass;
	func->flags = 0;		// TODO
	func->type = type;
	func->name = funcName;
	func->tok = funcTok;
	func->body = body;
	
	return 0;
};

CC_Unit* ccCompile(CC_Compiler *cc, CC_Context *ctx)
{
	CC_Unit *unit = (CC_Unit*) calloc(1, sizeof(CC_Unit));
	cc->unit = unit;
	
	unit->globals = hashtabNew();
	unit->structs = hashtabNew();
	unit->enums = hashtabNew();
	unit->dataModel = cc->dataModel;
	
	CC_ExternalDeclaration *xdecl;
	for (xdecl=ctx->declHead; xdecl!=NULL; xdecl=xdecl->next)
	{
		if (xdecl->decl != NULL)
		{
			ccCompileDeclaration(cc, xdecl->decl);
		}
		else
		{
			ccCompileFunctionDefinition(cc, xdecl->funcDef);
		};
	};
	
	if (conGetError() != 0) return NULL;
	return cc->unit;
};

CC_Type* ccGetVarType(CC_Compiler *cc, const char *name)
{
	CC_Scope *scope;
	for (scope=cc->scope; scope!=NULL; scope=scope->prev)
	{
		if (hashtabHasKey(scope->vars, name))
		{
			CC_Decl *decl = (CC_Decl*) hashtabGet(scope->vars, name);
			return decl->type;
		};
	};
	
	if (hashtabHasKey(cc->unit->globals, name))
	{
		CC_Decl *decl = (CC_Decl*) hashtabGet(cc->unit->globals, name);
		return decl->type;
	};
	
	return NULL;
};

size_t ccGetTypeSize(CC_Compiler *cc, CC_Type *type)
{
	switch (type->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_SIGNED_CHAR:
	case CC_TYPE_UNSIGNED_CHAR:
		return 1;
	case CC_TYPE_SIGNED_SHORT:
	case CC_TYPE_UNSIGNED_SHORT:
		return cc->dataModel->sizeofShort;
	case CC_TYPE_SIGNED_INT:
	case CC_TYPE_UNSIGNED_INT:
		return cc->dataModel->sizeofInt;
	case CC_TYPE_SIGNED_LONG:
	case CC_TYPE_UNSIGNED_LONG:
		return cc->dataModel->sizeofLong;
	case CC_TYPE_SIGNED_LONG_LONG:
	case CC_TYPE_UNSIGNED_LONG_LONG:
		return cc->dataModel->sizeofLongLong;
	case CC_TYPE_FLOAT:
		return cc->dataModel->sizeofFloat;
	case CC_TYPE_DOUBLE:
		return cc->dataModel->sizeofDouble;
	case CC_TYPE_LONG_DOUBLE:
		return cc->dataModel->sizeofLongDouble;
	case CC_TYPE_BOOL:
		return cc->dataModel->sizeofBool;
	case CC_TYPE_FLOAT_COMPLEX:
		return cc->dataModel->sizeofFloatComplex;
	case CC_TYPE_DOUBLE_COMPLEX:
		return cc->dataModel->sizeofDoubleComplex;
	case CC_TYPE_LONG_DOUBLE_COMPLEX:
		return cc->dataModel->sizeofLongDoubleComplex;
	case CC_TYPE_STRUCT_OR_UNION:
		if (hashtabHasKey(cc->unit->structs, type->tag))
		{
			CC_StructDef *def = (CC_StructDef*) hashtabGet(cc->unit->structs, type->tag);
			return def->size;
		};
		return 0;
	case CC_TYPE_ARRAY:
		if (type->numElements < 1) return 0;
		else return type->numElements * ccGetTypeSize(cc, type->base);
	case CC_TYPE_PTR:
		return cc->dataModel->sizeofPtr;
	default:
		return 0;
	};
};

size_t ccGetTypeAlign(CC_Compiler *cc, CC_Type *type)
{
	switch (type->kind)
	{
	case CC_TYPE_CHAR:
	case CC_TYPE_SIGNED_CHAR:
	case CC_TYPE_UNSIGNED_CHAR:
		return 1;
	case CC_TYPE_SIGNED_SHORT:
	case CC_TYPE_UNSIGNED_SHORT:
		return cc->dataModel->sizeofShort;
	case CC_TYPE_SIGNED_INT:
	case CC_TYPE_UNSIGNED_INT:
		return cc->dataModel->sizeofInt;
	case CC_TYPE_SIGNED_LONG:
	case CC_TYPE_UNSIGNED_LONG:
		return cc->dataModel->sizeofLong;
	case CC_TYPE_SIGNED_LONG_LONG:
	case CC_TYPE_UNSIGNED_LONG_LONG:
		return cc->dataModel->sizeofLongLong;
	case CC_TYPE_FLOAT:
		return cc->dataModel->sizeofFloat;
	case CC_TYPE_DOUBLE:
		return cc->dataModel->sizeofDouble;
	case CC_TYPE_LONG_DOUBLE:
		return cc->dataModel->sizeofLongDouble;
	case CC_TYPE_BOOL:
		return cc->dataModel->sizeofBool;
	case CC_TYPE_FLOAT_COMPLEX:
		return cc->dataModel->sizeofFloatComplex;
	case CC_TYPE_DOUBLE_COMPLEX:
		return cc->dataModel->sizeofDoubleComplex;
	case CC_TYPE_LONG_DOUBLE_COMPLEX:
		return cc->dataModel->sizeofLongDoubleComplex;
	case CC_TYPE_STRUCT_OR_UNION:
		if (hashtabHasKey(cc->unit->structs, type->tag))
		{
			CC_StructDef *def = (CC_StructDef*) hashtabGet(cc->unit->structs, type->tag);
			return def->align;
		};
		return 0;
	case CC_TYPE_ARRAY:
		return ccGetTypeAlign(cc, type->base);
	case CC_TYPE_PTR:
		return cc->dataModel->sizeofPtr;
	default:
		return 0;
	};
};

int ccIsTypeCompatible(CC_Type *a, CC_Type *b)
{
	if (a->kind != b->kind) return 0;
	
	int i;
	switch (a->kind)
	{
	case CC_TYPE_ARRAY:
		if (a->numElements != b->numElements && a->numElements != 0 && b->numElements != 0)
		{
			return 0;
		};
		break;
	case CC_TYPE_PTR:
		return ccIsTypeCompatible(a->base, b->base);
	case CC_TYPE_STRUCT_OR_UNION:
		return strcmp(a->tag, b->tag) == 0;
	case CC_TYPE_FUNC:
		if (a->numArgs != b->numArgs) return 0;
		for (i=0; i<a->numArgs; i++)
		{
			if (!ccIsTypeCompatible(a->argTypes[i], b->argTypes[i])) return 0;
		};
		break;
	};
	
	return 1;
};

static const char* ccStorageClassName(int s)
{
	switch (s)
	{
	case CC_STOR_NONE:
		return "";
	case CC_STOR_TYPEDEF:
		return "typedef";
	case CC_STOR_EXTERN:
		return "extern";
	case CC_STOR_STATIC:
		return "static";
	case CC_STOR_THREAD_LOCAL:
		return "thread-local";
	case CC_STOR_AUTO:
		return "auto";
	case CC_STOR_REGISTER:
		return "register";
	case CC_STOR_EXTERN_THREAD_LOCAL:
		return "extern thread-local";
	case CC_STOR_STATIC_THREAD_LOCAL:
		return "static thread-local";
	default:
		INVALID_CODE_PATH;
		return "";
	};
};

static void ccDumpNode(int depth, CC_Node *node)
{
	char tabs[128];
	memset(tabs, 0, 128);
	memset(tabs, '\t', depth);
	
	printf("%s<%s>\n", tabs, nodeTypeNames[node->type]);
	if (node->str != NULL)
	{
		printf("\t%s<String>%s</String>\n", tabs, node->str);
	};
	if (node->cval.valType != NULL)
	{
		printf("\t%s<Type>", tabs);
		ccDumpType(node->cval.valType);
		printf("</Type>\n");
	};
	
	if (node->numChildren == 0 && node->str == NULL && node->cval.valType == NULL)
	{
		printf("\t%s<Value>%" PRIu64 "</Value>\n", tabs, node->cval.valU);
	};
	
	int i;
	for (i=0; i<node->numChildren; i++)
	{
		ccDumpNode(depth+1, node->children[i]);
	};
	printf("%s</%s>\n", tabs, nodeTypeNames[node->type]);
};

void ccDumpUnit(CC_Unit *unit)
{
	const char *key;
	
	printf("<Unit>\n");
	HASHTAB_FOREACH(unit->structs, CC_StructDef *, key, sdecl)
	{
		printf("\t<StructDecl>\n");
		printf("\t\t<Name>%s</Name>\n", key);
		printf("\t\t<Size>%d</Size>\n", (int) sdecl->size);
		printf("\t\t<Alignment>%d</Alignment>\n", (int) sdecl->align);
		
		CC_StructMember *member;
		for (member=sdecl->memberList; member!=NULL; member=member->next)
		{
			printf("\t\t<Member>`%s' @ %d [%d:%d] (size %d, alignment %d) as ", member->name, (int) member->offset, (int) member->bitOff, (int) member->bitLen, (int) member->size, (int) member->align);
			ccDumpType(member->type);
			printf("</Member>\n");
		};
		
		printf("\t</StructDecl>\n");
	};
	
	HASHTAB_FOREACH(unit->globals, CC_Decl *, key, decl)
	{
		printf("\t<Decl>%s `%s' as ", ccStorageClassName(decl->storageClass), decl->name);
		ccDumpType(decl->type);
		printf("</Decl>\n");
		if (decl->init != NULL) ccDumpNode(1, decl->init);
	};
	
	CC_Func *func;
	for (func=unit->funcs; func!=NULL; func=func->next)
	{
		printf("\t<Function %s>\n", func->name);
		
		ccDumpNode(2, func->body);
		
		printf("\t</Function %s>\n", func->name);
	};
	
	printf("</Unit>\n");
};
