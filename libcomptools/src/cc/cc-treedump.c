/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/cc/cc-treedump.h>

static void ccDumpTypeSpecifier(CC_TypeSpecifier *typeSpecifier, int depth);
static void ccDumpTypeQualifier(Token *typeQualifier, int depth);
static void ccDumpPointer(CC_Pointer *ptr, int depth);
static void ccDumpDeclarator(CC_Declarator *decl, int depth);
static void ccDumpExpression(CC_Expression *expr, int depth);
static void ccDumpCastExpression(CC_CastExpression *cast, int depth);
static void ccDumpAbstractDeclarator(CC_AbstractDeclarator *decl, int depth);
static void ccDumpDeclaratorTail(CC_DeclaratorTail *tail, int depth);
static void ccDumpAssignmentExpression(CC_AssignmentExpression *expr, int depth);
static void ccDumpInitializerList(CC_InitializerList *initList, int depth);
static void ccDumpInitializer(CC_Initializer *init, int depth);
static void ccDumpConstantExpression(CC_ConstantExpression *expr, int depth);
static void ccDumpCompoundStatement(CC_CompoundStatement *comp, int depth);
static void ccDumpStatement(CC_Statement *st, int depth);

static void ccDumpStaticAssert(CC_StaticAssert *staticAssert, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	char buffer[strlen(staticAssert->str->value)];
	lexParseString(staticAssert->str->value, buffer);
	
	printf("%s<StaticAssert>\n", tabs);
	ccDumpConstantExpression(staticAssert->expr, depth+1);
	printf("%s	<Message>%s</Message>\n", tabs, buffer);
	printf("%s</StaticAssert>\n", tabs);
};

static void ccDumpStorageClass(Token *storageClass, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<StorageClass>%s</StorageClass>\n", tabs, storageClass->value);
};

static void ccDumpSpecifierQualifierList(CC_SpecifierQualifierList *list, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<SpecifierQualifierList>\n", tabs);
	for (; list!=NULL; list=list->next)
	{
		if (list->typeSpecifier != NULL)
		{
			ccDumpTypeSpecifier(list->typeSpecifier, depth+1);
		}
		else if (list->typeQualifier != NULL)
		{
			ccDumpStorageClass(list->typeQualifier, depth+1);
		};
	};
	printf("%s</SpecifierQualifierList>\n", tabs);
};

static void ccDumpDirectAbstractDeclarator(CC_DirectAbstractDeclarator *direct, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<DirectAbstractDeclarator>\n", tabs);
	if (direct->decl != NULL) ccDumpAbstractDeclarator(direct->decl, depth+1);
	if (direct->tail != NULL) ccDumpDeclaratorTail(direct->tail, depth+1);
	printf("%s</DirectAbstractDeclarator>\n", tabs);
};

static void ccDumpAbstractDeclarator(CC_AbstractDeclarator *decl, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<AbstractDeclarator>\n", tabs);
	CC_Pointer *ptr;
	for (ptr=decl->ptr; ptr!=NULL; ptr=ptr->next)
	{
		ccDumpPointer(ptr, depth+1);
	};
	if (decl->direct != NULL) ccDumpDirectAbstractDeclarator(decl->direct, depth+1);
	printf("%s</AbstractDeclarator>\n", tabs);
};

static void ccDumpTypeName(CC_TypeName *typeName, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<TypeName>\n", tabs);
	ccDumpSpecifierQualifierList(typeName->specs, depth+1);
	if (typeName->decl != NULL) ccDumpAbstractDeclarator(typeName->decl, depth+1);
	printf("%s</TypeName>\n", tabs);
};

static void ccDumpAtomicTypeSpecifier(CC_AtomicTypeSpecifier *atomicSpecifier, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<AtomicTypeSpecifier>\n", tabs);
	ccDumpTypeName(atomicSpecifier->typeName, depth+1);
	printf("%s</AtomicTypeSpecifier>\n", tabs);
};

static void ccDumpEnumerator(CC_EnumeratorList *enumerator, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<Enumerator>\n", tabs);
	printf("%s	<Name>%s</Name>\n", tabs, enumerator->id->value);
	if (enumerator->expr != NULL) ccDumpConstantExpression(enumerator->expr, depth+1);
	printf("%s</Enumerator>\n", tabs);
};

static void ccDumpEnumSpecifier(CC_EnumSpecifier *enumSpecifier, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<EnumSpecifier>\n", tabs);
	if (enumSpecifier->id != NULL) printf("%s	<Name>%s</Name>\n", tabs, enumSpecifier->id->value);
	
	CC_EnumeratorList *list;
	for (list=enumSpecifier->list; list!=NULL; list=list->next)
	{
		ccDumpEnumerator(list, depth+1);
	};
	
	printf("%s</EnumSpecifier>\n", tabs);
};

static void ccDumpStructDeclaratorList(CC_StructDeclaratorList *decls, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	for (; decls!=NULL; decls=decls->next)
	{
		printf("%s<StructDeclarator>\n", tabs);
		ccDumpDeclarator(decls->decl, depth+1);
		if (decls->bits != NULL) ccDumpConstantExpression(decls->bits, depth+1);
		printf("%s</StructDeclarator>\n", tabs);
	};
};

static void ccDumpStructDeclaration(CC_StructDeclarationList *decl, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (decl->staticAssert != NULL)
	{
		ccDumpStaticAssert(decl->staticAssert, depth);
		return;
	};
	
	printf("%s<StructDeclaration>\n", tabs);
	ccDumpSpecifierQualifierList(decl->qualifiers, depth+1);
	ccDumpStructDeclaratorList(decl->decls, depth+1);
	printf("%s</StructDeclaration>\n", tabs);
};

static void ccDumpStructOrUnionSpecifier(CC_StructOrUnionSpecifier *comp, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<StructOrUnionSpecifier>\n", tabs);
	printf("%s	<Kind>%s</Kind>\n", tabs, comp->keyword->value);
	if (comp->id != NULL) printf("%s	<Name>%s</Name>\n", tabs, comp->id->value);
	
	CC_StructDeclarationList *list;
	for (list=comp->decls; list!=NULL; list=list->next)
	{
		ccDumpStructDeclaration(list, depth+1);
	};
	
	printf("%s</StructOrUnionSpecifier>\n", tabs);
};

static void ccDumpTypeSpecifier(CC_TypeSpecifier *typeSpecifier, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (typeSpecifier->token != NULL)
	{
		printf("%s<TypeSpecifier>%s</TypeSpecifier>\n", tabs, typeSpecifier->token->value);
	}
	else if (typeSpecifier->atomicSpecifier != NULL)
	{
		ccDumpAtomicTypeSpecifier(typeSpecifier->atomicSpecifier, depth);
	}
	else if (typeSpecifier->enumSpecifier != NULL)
	{
		ccDumpEnumSpecifier(typeSpecifier->enumSpecifier, depth);
	}
	else if (typeSpecifier->compSpecifier != NULL)
	{
		ccDumpStructOrUnionSpecifier(typeSpecifier->compSpecifier, depth);
	};
};

static void ccDumpTypeQualifier(Token *typeQualifier, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<TypeQualifier>%s</TypeQualifier>\n", tabs, typeQualifier->value);
};

static void ccDumpFunctionSpecifier(Token *funcSpecifier, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<FunctionSpecifier>%s</FunctionSpecifier>\n", tabs, funcSpecifier->value);
};

static void ccDumpGenericAssoc(CC_GenericAssocList *assoc, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<GenericAssoc>\n", tabs);
	if (assoc->typeName != NULL)
	{
		ccDumpTypeName(assoc->typeName, depth+1);
	}
	else
	{
		printf("%s	<TypeName>default</TypeName>\n", tabs);
	};
	ccDumpAssignmentExpression(assoc->expr, depth+1);
	printf("%s</GenericAssoc>\n", tabs);
};

static void ccDumpGenericSelection(CC_GenericSelection *generic, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<GenericSelection>\n", tabs);
	ccDumpAssignmentExpression(generic->expr, depth+1);
	
	CC_GenericAssocList *assoc;
	for (assoc=generic->assocs; assoc!=NULL; assoc=assoc->next)
	{
		ccDumpGenericAssoc(assoc, depth+1);
	};
	printf("%s</GenericSelection>\n", tabs);
};

static void ccDumpPrimaryExpression(CC_PrimaryExpression *primary, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (primary->tok != NULL)
	{
		printf("%s<PrimaryExpression>%s</PrimaryExpression>\n", tabs, primary->tok->value);
	}
	else if (primary->expr != NULL)
	{
		ccDumpExpression(primary->expr, depth);
	}
	else
	{
		ccDumpGenericSelection(primary->generic, depth);
	};
};

static void ccDumpSubscriptTail(CC_SubscriptTail *subscript, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<Subscript>\n", tabs);
	ccDumpExpression(subscript->expr, depth+1);
	printf("%s</Subscript>\n", tabs);
};

static void ccDumpCallTail(CC_CallTail *call, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<Call>\n", tabs);
	CC_ArgumentExpressionList *arg;
	for (arg=call->list; arg!=NULL; arg=arg->next)
	{
		ccDumpAssignmentExpression(arg->assign, depth+1);
	};
	printf("%s</Call>\n", tabs);
};

static void ccDumpMemberTail(CC_MemberTail *member, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<MemberAccess>\n", tabs);
	printf("%s	<MemberOperator>%s</MemberOperator>\n", tabs, member->op->value);
	printf("%s	<MemberName>%s</MemberName>\n", tabs, member->id->value);
	printf("%s</MemberAccess>\n", tabs);
};

static void ccDumpPostfixTail(CC_PostfixTail *tail, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);

	if (tail->subscript != NULL)
	{
		ccDumpSubscriptTail(tail->subscript, depth);
	}
	else if (tail->call != NULL)
	{
		ccDumpCallTail(tail->call, depth);
	}
	else if (tail->member != NULL)
	{
		ccDumpMemberTail(tail->member, depth);
	}
	else
	{
		printf("%s<PostfixOperator>%s</PostfixOperator>\n", tabs, tail->op->value);
	};
};

static void ccDumpPostfixExpression(CC_PostfixExpression *postfix, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (postfix->tail == NULL && postfix->typeName == NULL)
	{
		ccDumpPrimaryExpression(postfix->primary, depth);
	}
	else
	{
		printf("%s<PostfixExpression>\n", tabs);
		if (postfix->primary != NULL) ccDumpPrimaryExpression(postfix->primary, depth+1);
		else
		{
			ccDumpTypeName(postfix->typeName, depth+1);
			ccDumpInitializerList(postfix->initList, depth+1);
		};
		CC_PostfixTail *tail;
		for (tail=postfix->tail; tail!=NULL; tail=tail->next)
		{
			ccDumpPostfixTail(tail, depth+1);
		};
		printf("%s</PostfixExpression>\n", tabs);
	};
};

static void ccDumpUnaryExpression(CC_UnaryExpression *unary, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (unary->postfix != NULL)
	{
		ccDumpPostfixExpression(unary->postfix, depth);
	}
	else
	{
		printf("%s<UnaryExpression>\n", tabs);
		if (unary->op != NULL) printf("%s	<UnaryOperator>%s</UnaryOperator>\n", tabs, unary->op->value);
		if (unary->typeName != NULL) ccDumpTypeName(unary->typeName, depth+1);
		if (unary->cast != NULL) ccDumpCastExpression(unary->cast, depth+1);
		printf("%s</UnaryExpression>\n", tabs);
	};
};

static void ccDumpCastExpression(CC_CastExpression *cast, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (cast->unary != NULL)
	{
		ccDumpUnaryExpression(cast->unary, depth);
	}
	else
	{
		printf("%s<CastExpression>\n", tabs);
		ccDumpTypeName(cast->typeName, depth+1);
		ccDumpCastExpression(cast->cast, depth+1);
		printf("%s</CastExpression>\n", tabs);
	};
};

static void ccDumpMultiplicativeExpression(CC_MultiplicativeExpression *mul, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (mul->next == NULL)
	{
		ccDumpCastExpression(mul->cast, depth);
	}
	else
	{
		printf("%s<MultiplicativeExpression>\n", tabs);
		for (; mul!=NULL; mul=mul->next)
		{
			ccDumpCastExpression(mul->cast, depth+1);
			if (mul->op != NULL)
			{
				printf("%s	<MultiplicativeOperator>%s</MultiplicativeOperator>\n", tabs, mul->op->value);
			};
		};
		printf("%s</MultiplicativeExpression>\n", tabs);
	};
};

static void ccDumpAdditiveExpression(CC_AdditiveExpression *add, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (add->next == NULL)
	{
		ccDumpMultiplicativeExpression(add->mul, depth);
	}
	else
	{
		printf("%s<AdditiveExpression>\n", tabs);
		for (; add!=NULL; add=add->next)
		{
			ccDumpMultiplicativeExpression(add->mul, depth+1);
			if (add->op != NULL)
			{
				printf("%s	<AdditiveOperator>%s</AdditiveOperator>\n", tabs, add->op->value);
			};
		};
		printf("%s</AdditiveExpression>\n", tabs);
	};
};

static void ccDumpShiftExpression(CC_ShiftExpression *shift, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (shift->next == NULL)
	{
		ccDumpAdditiveExpression(shift->add, depth);
	}
	else
	{
		printf("%s<ShiftExpression>\n", tabs);
		for (; shift!=NULL; shift=shift->next)
		{
			ccDumpAdditiveExpression(shift->add, depth+1);
			if (shift->op != NULL)
			{
				printf("%s	<ShiftOperator>%s</ShiftOperator>\n", tabs, shift->op->value);
			};
		};
		printf("%s</ShiftExpression>\n", tabs);
	};
};

static void ccDumpRelationalExpression(CC_RelationalExpression *rel, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (rel->next == NULL)
	{
		ccDumpShiftExpression(rel->shift, depth);
	}
	else
	{
		printf("%s<RelationalExpression>\n", tabs);
		for (; rel!=NULL; rel=rel->next)
		{
			ccDumpShiftExpression(rel->shift, depth+1);
			if (rel->op != NULL)
			{
				printf("%s	<RelationalOperator>%s</RelationalOperator>\n", tabs, rel->op->value);
			};
		};
		printf("%s</RelationalExpression>\n", tabs);
	};
};

static void ccDumpEqualityExpression(CC_EqualityExpression *equality, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (equality->next == NULL)
	{
		ccDumpRelationalExpression(equality->rel, depth);
	}
	else
	{
		printf("%s<EqualityExpression>\n", tabs);
		for (; equality!=NULL; equality=equality->next)
		{
			ccDumpRelationalExpression(equality->rel, depth+1);
			if (equality->op != NULL)
			{
				printf("%s	<EqualityOperator>%s</EqualityOperator>\n", tabs, equality->op->value);
			};
		};
		printf("%s</EqualityExpression>\n", tabs);
	};
};

static void ccDumpBitwiseANDExpression(CC_BitwiseANDExpression *bitwiseAND, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (bitwiseAND->next == NULL)
	{
		ccDumpEqualityExpression(bitwiseAND->equality, depth);
	}
	else
	{
		printf("%s<BitwiseANDExpression>\n", tabs);
		for (; bitwiseAND!=NULL; bitwiseAND=bitwiseAND->next)
		{
			ccDumpEqualityExpression(bitwiseAND->equality, depth+1);
		};
		printf("%s</BitwiseANDExpression>\n", tabs);
	};
};

static void ccDumpBitwiseXORExpression(CC_BitwiseXORExpression *bitwiseXOR, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (bitwiseXOR->next == NULL)
	{
		ccDumpBitwiseANDExpression(bitwiseXOR->bitwiseAND, depth);
	}
	else
	{
		printf("%s<BitwiseXORExpression>\n", tabs);
		for (; bitwiseXOR!=NULL; bitwiseXOR=bitwiseXOR->next)
		{
			ccDumpBitwiseANDExpression(bitwiseXOR->bitwiseAND, depth+1);
		};
		printf("%s</BitwiseXORExpression>\n", tabs);
	};
};

static void ccDumpBitwiseORExpression(CC_BitwiseORExpression *bitwiseOR, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (bitwiseOR->next == NULL)
	{
		ccDumpBitwiseXORExpression(bitwiseOR->bitwiseXOR, depth);
	}
	else
	{
		printf("%s<BitwiseORExpression>\n", tabs);
		for (; bitwiseOR!=NULL; bitwiseOR=bitwiseOR->next)
		{
			ccDumpBitwiseXORExpression(bitwiseOR->bitwiseXOR, depth+1);
		};
		printf("%s</BitwiseORExpression>\n", tabs);
	};
};

static void ccDumpLogicalANDExpression(CC_LogicalANDExpression *logicalAND, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (logicalAND->next == NULL)
	{
		ccDumpBitwiseORExpression(logicalAND->bitwiseOR, depth);
	}
	else
	{
		printf("%s<LogicalANDExpression>\n", tabs);
		for (; logicalAND!=NULL; logicalAND=logicalAND->next)
		{
			ccDumpBitwiseORExpression(logicalAND->bitwiseOR, depth+1);
		};
		printf("%s</LogicalANDExpression>\n", tabs);
	};
};

static void ccDumpLogicalORExpression(CC_LogicalORExpression *logicalOR, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (logicalOR->next == NULL)
	{
		ccDumpLogicalANDExpression(logicalOR->logicalAND, depth);
	}
	else
	{
		printf("%s<LogicalORExpression>\n", tabs);
		for (; logicalOR!=NULL; logicalOR=logicalOR->next)
		{
			ccDumpLogicalANDExpression(logicalOR->logicalAND, depth+1);
		};
		printf("%s</LogicalORExpression>\n", tabs);
	};
};

static void ccDumpConditionalExpression(CC_ConditionalExpression *cond, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (cond->trueExpr == NULL)
	{
		ccDumpLogicalORExpression(cond->left, depth);
	}
	else
	{
		printf("%s<ConditionalExpression>\n", tabs);
		printf("%s	<Subject>\n", tabs);
		ccDumpLogicalORExpression(cond->left, depth+2);
		printf("%s	</Subject>\n", tabs);
		printf("%s	<True>\n", tabs);
		ccDumpExpression(cond->trueExpr, depth+2);
		printf("%s	</True>\n", tabs);
		printf("%s	<False>\n", tabs);
		ccDumpConditionalExpression(cond->falseExpr, depth+2);
		printf("%s	</False>\n", tabs);
		printf("%s</ConditionalExpression>\n", tabs);
	};
};

static void ccDumpConstantExpression(CC_ConstantExpression *expr, int depth)
{
	ccDumpConditionalExpression(expr->cond, depth);
};

static void ccDumpAlignmentSpecifier(CC_AlignmentSpecifier *spec, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<AlignmentSpecifier>\n", tabs);
	if (spec->typeName != NULL)
	{
		ccDumpTypeName(spec->typeName, depth+1);
	}
	else
	{
		ccDumpConstantExpression(spec->expr, depth+1);
	};
	printf("%s</AlignmentSpecifier>\n", tabs);
};

static void ccDumpAttributeSpecifier(CC_AttributeSpecifier *attrSpecifier, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<AttributeSpecifier>%s</AttributeSpecifier>\n", tabs, attrSpecifier->id->value);
};

static void ccDumpDeclarationSpecifiers(CC_DeclarationSpecifiers *specs, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<DeclarationSpecifiers>\n", tabs);
	for (; specs!=NULL; specs=specs->next)
	{
		if (specs->storageClass != NULL)
		{
			ccDumpStorageClass(specs->storageClass, depth+1);
		}
		else if (specs->typeSpecifier != NULL)
		{
			ccDumpTypeSpecifier(specs->typeSpecifier, depth+1);
		}
		else if (specs->typeQualifier != NULL)
		{
			ccDumpTypeQualifier(specs->typeQualifier, depth+1);
		}
		else if (specs->funcSpecifier != NULL)
		{
			ccDumpFunctionSpecifier(specs->funcSpecifier, depth+1);
		}
		else if (specs->alignSpecifier != NULL)
		{
			ccDumpAlignmentSpecifier(specs->alignSpecifier, depth+1);
		}
		else if (specs->attrSpecifier != NULL)
		{
			ccDumpAttributeSpecifier(specs->attrSpecifier, depth+1);
		};
	};
	printf("%s</DeclarationSpecifiers>\n", tabs);
};

static void ccDumpIdentifier(Token *id, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<Identifier>%s</Identifier>\n", tabs, id->value);
};

static void ccDumpAssignmentExpression(CC_AssignmentExpression *expr, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (expr->cond != NULL)
	{
		ccDumpConditionalExpression(expr->cond, depth);
	}
	else
	{
		printf("%s<AssignmentExpression>\n", tabs);
		ccDumpUnaryExpression(expr->left, depth+1);
		printf("%s	<AssignmentOperator>%s</AssignmentOperator>\n", tabs, expr->op->value);
		ccDumpAssignmentExpression(expr->right, depth+1);
		printf("%s</AssignmentExpression>\n", tabs);
	};
};

static void ccDumpExpression(CC_Expression *expr, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<Expression>\n", tabs);
	for (; expr!=NULL; expr=expr->next)
	{
		ccDumpAssignmentExpression(expr->assign, depth+1);
	};
	printf("%s</Expression>\n", tabs);
};

static void ccDumpParameterType(CC_ParameterTypeList *param, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (param->declSpecs == NULL && param->decl == NULL && param->absDecl == NULL)
	{
		printf("%s<Parameter>...</Parameter>\n", tabs);
	}
	else
	{
		printf("%s<Parameter>\n", tabs);
		if (param->declSpecs != NULL)
		{
			ccDumpDeclarationSpecifiers(param->declSpecs, depth+1);
		};
		if (param->decl != NULL)
		{
			ccDumpDeclarator(param->decl, depth+1);
		};
		if (param->absDecl != NULL)
		{
			ccDumpAbstractDeclarator(param->absDecl, depth+1);
		};
		printf("%s</Parameter>\n", tabs);
	};
};

static void ccDumpDeclaratorTail(CC_DeclaratorTail *tail, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (tail->array)
	{
		printf("%s<ArrayTail>\n", tabs);
		if (tail->isStatic)
		{
			ccDumpStorageClass(tail->isStatic, depth+1);
		};

		CC_TypeQualifierList *q;
		for (q=tail->qualifs; q!=NULL; q=q->next)
		{
			ccDumpTypeQualifier(q->qualifier, depth+1);
		};
		
		if (tail->expr != NULL) ccDumpAssignmentExpression(tail->expr, depth+1);
		printf("%s</ArrayTail>\n", tabs);
	}
	else
	{
		printf("%s<ArgumentTail>\n", tabs);
		CC_ParameterTypeList *list;
		for (list=tail->paramList; list!=NULL; list=list->next)
		{
			ccDumpParameterType(list, depth+1);
		};
		printf("%s</ArgumentTail>\n", tabs);
	};
};

static void ccDumpDirectDeclarator(CC_DirectDeclarator *direct, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<DirectDeclarator>\n", tabs);
	if (direct->id != NULL)
	{
		ccDumpIdentifier(direct->id, depth+1);
	}
	else if (direct->decl != NULL)
	{
		ccDumpDeclarator(direct->decl, depth+1);
	};
	
	CC_DeclaratorTail *tail;
	for (tail=direct->tail; tail!=NULL; tail=tail->next)
	{
		ccDumpDeclaratorTail(tail, depth+1);
	};
	
	printf("%s</DirectDeclarator>\n", tabs);
};

static void ccDumpPointer(CC_Pointer *ptr, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<Pointer>\n", tabs);
	CC_TypeQualifierList *q;
	for (q=ptr->qualifs; q!=NULL; q=q->next)
	{
		ccDumpTypeQualifier(q->qualifier, depth+1);
	};
	printf("%s</Pointer>\n", tabs);
};

static void ccDumpDeclarator(CC_Declarator *decl, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<Declarator>\n", tabs);
	CC_Pointer *ptr;
	for (ptr=decl->ptr; ptr!=NULL; ptr=ptr->next)
	{
		ccDumpPointer(ptr, depth+1);
	};
	ccDumpDirectDeclarator(decl->direct, depth+1);
	printf("%s</Declarator>\n", tabs);
};

static void ccDumpDesignator(CC_DesignatorList *desig, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (desig->id != NULL)
	{
		printf("%s<MemberDesignator>%s</MemberDesignator>\n", tabs, desig->id->value);
	}
	else
	{
		printf("%s<SubscriptDesignator>\n", tabs);
		ccDumpConstantExpression(desig->expr, depth+1);
		printf("%s</SubscriptDesignator>\n", tabs);
	};
};

static void ccDumpDesignation(CC_Designation *designation, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<Designation>\n", tabs);
	CC_DesignatorList *list;
	for (list=designation->list; list!=NULL; list=list->next)
	{
		ccDumpDesignator(list, depth+1);
	};
	printf("%s</Designation>\n", tabs);
};

static void ccDumpInitializerList(CC_InitializerList *initList, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<InitializerList>\n", tabs);
	for (; initList!=NULL; initList=initList->next)
	{
		if (initList->designation != NULL) ccDumpDesignation(initList->designation, depth+1);
		if (initList->init != NULL) ccDumpInitializer(initList->init, depth+1);
	};
	printf("%s</InitializerList>\n", tabs);
};

static void ccDumpInitializer(CC_Initializer *init, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<Initializer>\n", tabs);
	if (init->assign != NULL)
	{
		ccDumpAssignmentExpression(init->assign, depth+1);
	};
	if (init->initList != NULL)
	{
		ccDumpInitializerList(init->initList, depth+1);
	};
	printf("%s</Initializer>\n", tabs);
};

static void ccDumpInitDeclaratorList(CC_InitDeclaratorList *list, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<InitDeclaratorList>\n", tabs);
	for (; list!=NULL; list=list->next)
	{
		if (list->decl != NULL)
		{
			ccDumpDeclarator(list->decl, depth+1);
		};
		
		if (list->init != NULL)
		{
			ccDumpInitializer(list->init, depth+1);
		};
	};
	printf("%s</InitDeclaratorList>\n", tabs);
};

static void ccDumpDeclaration(CC_Declaration *decl, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (decl->staticAssert != NULL)
	{
		ccDumpStaticAssert(decl->staticAssert, depth);
		return;
	};
	
	printf("%s<Declaration>\n", tabs);
	ccDumpDeclarationSpecifiers(decl->declSpecs, depth+1);
	ccDumpInitDeclaratorList(decl->initDeclList, depth+1);
	printf("%s</Declaration>\n", tabs);
};

static void ccDumpLabel(CC_LabeledStatement *label, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	if (label->id != NULL)
	{
		printf("%s<Label>%s</Label>\n", tabs, label->id->value);
	}
	else if (label->expr != NULL)
	{
		printf("%s<Case>\n", tabs);
		ccDumpConstantExpression(label->expr, depth+1);
		printf("%s</Case>\n", tabs);
	}
	else
	{
		printf("%s<Default></Default>\n", tabs);
	};
};

static void ccDumpIfStatement(CC_IfStatement *ifs, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<IfStatement>\n", tabs);
	ccDumpExpression(ifs->expr, depth+1);
	printf("%s	<TrueAction>\n", tabs);
	ccDumpStatement(ifs->trueAction, depth+2);
	printf("%s	</TrueAction\n", tabs);
	if (ifs->falseAction != NULL)
	{
		printf("%s	<FalseAction>\n", tabs);
		ccDumpStatement(ifs->falseAction, depth+2);
		printf("%s	</FalseAction>\n", tabs);
	};
	printf("%s</IfStatement>\n", tabs);
};

static void ccDumpSwitchStatement(CC_SwitchStatement *sw, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<SwitchStatement>\n", tabs);
	ccDumpExpression(sw->expr, depth+1);
	ccDumpStatement(sw->st, depth+1);
	printf("%s</SwitchStatement>\n", tabs);
};

static void ccDumpWhileStatement(CC_WhileStatement *whiles, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<WhileStatement>\n", tabs);
	ccDumpExpression(whiles->expr, depth+1);
	ccDumpStatement(whiles->st, depth+1);
	printf("%s</WhileStatement>\n", tabs);
};

static void ccDumpDoWhileStatement(CC_DoWhileStatement *doWhile, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<DoWhileStatement>\n", tabs);
	ccDumpStatement(doWhile->st, depth+1);
	ccDumpExpression(doWhile->expr, depth+1);
	printf("%s</DoWhileStatement>\n", tabs);
};

static void ccDumpForStatement(CC_ForStatement *fors, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<ForStatement>\n", tabs);
	printf("%s	<Init>\n", tabs);
	
	if (fors->decl != NULL)
	{
		ccDumpDeclaration(fors->decl, depth+2);
	}
	else if (fors->init != NULL)
	{
		ccDumpExpression(fors->init, depth+2);
	};
	
	printf("%s	</Init>\n", tabs);
	printf("%s	<Condition>\n", tabs);
	if (fors->cond != NULL) ccDumpExpression(fors->cond, depth+2);
	printf("%s	</Condition>\n", tabs);
	printf("%s	<Iteration>\n", tabs);
	if (fors->iter != NULL) ccDumpExpression(fors->iter, depth+2);
	printf("%s	</Iteration>\n", tabs);
	ccDumpStatement(fors->st, depth+1);
	printf("%s</ForStatement>\n", tabs);
};

static void ccDumpJumpStatement(CC_JumpStatement *jmp, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<JumpStatement>\n", tabs);
	printf("%s	<Keyword>%s</Keyword>\n", tabs, jmp->keyword->value);
	if (jmp->id != NULL) ccDumpIdentifier(jmp->id, depth+1);
	if (jmp->expr != NULL) ccDumpExpression(jmp->expr, depth+1);
	printf("%s</JumpStatement>\n", tabs);
};

static void ccDumpStatement(CC_Statement *st, int depth)
{
	while (st->label != NULL)
	{
		ccDumpLabel(st->label, depth);
		st = st->label->st;
	};
	
	if (st->comp != NULL)
	{
		ccDumpCompoundStatement(st->comp, depth);
	}
	else if (st->expr != NULL)
	{
		ccDumpExpression(st->expr, depth);
	}
	else if (st->ifs != NULL)
	{
		ccDumpIfStatement(st->ifs, depth);
	}
	else if (st->sw != NULL)
	{
		ccDumpSwitchStatement(st->sw, depth);
	}
	else if (st->whiles != NULL)
	{
		ccDumpWhileStatement(st->whiles, depth);
	}
	else if (st->doWhile != NULL)
	{
		ccDumpDoWhileStatement(st->doWhile, depth);
	}
	else if (st->fors != NULL)
	{
		ccDumpForStatement(st->fors, depth);
	}
	else if (st->jmp != NULL)
	{
		ccDumpJumpStatement(st->jmp, depth);
	};
};

static void ccDumpBlockItemList(CC_BlockItemList *items, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<BlockItemList>\n", tabs);
	for (; items!=NULL; items=items->next)
	{
		if (items->decl != NULL)
		{
			ccDumpDeclaration(items->decl, depth+1);
		}
		else if (items->st != NULL)
		{
			ccDumpStatement(items->st, depth+1);
		};
	};
	printf("%s</BlockItemList>\n", tabs);
};

static void ccDumpCompoundStatement(CC_CompoundStatement *comp, int depth)
{
	ccDumpBlockItemList(comp->items, depth);
};

static void ccDumpFunctionDefinition(CC_FunctionDefinition *funcDef, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<FunctionDefinition>\n", tabs);
	ccDumpDeclarationSpecifiers(funcDef->specs, depth+1);
	ccDumpDeclarator(funcDef->decl, depth+1);
	ccDumpCompoundStatement(funcDef->body, depth+1);
	printf("%s</FunctionDefinition>\n", tabs);
};

static void ccDumpExternalDeclaration(CC_ExternalDeclaration *decl, int depth)
{
	char tabs[64];
	memset(tabs, 0, 64);
	memset(tabs, '\t', depth);
	
	printf("%s<ExternalDeclaration>\n", tabs);
	if (decl->decl != NULL)
	{
		ccDumpDeclaration(decl->decl, depth+1);
	}
	else if (decl->funcDef != NULL)
	{
		ccDumpFunctionDefinition(decl->funcDef, depth+1);
	};
	printf("%s</ExternalDeclaration>\n", tabs);
};

void ccDumpTree(CC_Context *tree)
{
	printf("<TranslationUnit>\n");
	
	CC_ExternalDeclaration *decl;
	for (decl=tree->declHead; decl!=NULL; decl=decl->next)
	{
		ccDumpExternalDeclaration(decl, 1);
	};
	
	printf("</TranslationUnit>\n");
};
