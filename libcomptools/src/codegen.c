/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <comptools/codegen.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <dlfcn.h>
#endif

static const char *nodeColors[64] = {
	"#2782ea",
	"#18c472",
	"#96479a",
	"#7a2d3f",
	"#454a61",
	"#7c614f",
	"#9cb6ba",
	"#c299de",
	"#f8c2df",
	"#3497d2",
	"#8a42a5",
	"#728b17",
	"#f79a97",
	"#dd4cdd",
	"#781120",
	"#43a3bb",
	"#1786ba",
	"#104c35",
	"#64de75",
	"#65d475",
	"#fe7f5a",
	"#4839c8",
	"#a5d063",
	"#ace01d",
	"#af3cac",
	"#fa7e52",
	"#0f82cf",
	"#1a7839",
	"#a0dfc2",
	"#a055d1",
	"#d33d03",
	"#50f11e",
	"#e9016d",
	"#4572a3",
	"#d222a7",
	"#8331aa",
	"#912c07",
	"#5d397c",
	"#c54d6d",
	"#b619fa",
	"#30af9c",
	"#aa2966",
	"#b1874e",
	"#f3e059",
	"#6250ff",
	"#3f288a",
	"#a15e9e",
	"#a7199a",
	"#923a2c",
	"#516561",
	"#7b50fc",
	"#f8f0c1",
	"#fa2506",
	"#991c84",
	"#dde083",
	"#0dc683",
	"#d1b343",
	"#45a3ca",
	"#d635d8",
	"#9a4953",
	"#8d31d5",
	"#b93d0c",
	"#071f1a",
	"#6a15e4"
};

CodeGenGraph* codegenNewGraph(int numBlocks)
{
	CodeGenGraph *graph = (CodeGenGraph*) calloc(1, sizeof(CodeGenGraph));
	graph->numBlocks = numBlocks;
	graph->blocks = (CodeGenBlock**) calloc(numBlocks, sizeof(void*));
	
	return graph;
};

CodeGenDriver* codegenGetDriver(const char *name)
{
	return NULL;
};

int codegenFormatAP(char **strp, const char *fmt, va_list ap)
{
	char buffer[256];
	int count = vsnprintf(buffer, 256, fmt, ap);
	
	if (count < 256)
	{
		*strp = strdup(buffer);
		return count;
	}
	else
	{
		*strp = (char*) malloc((size_t)count + 1);
		return vsnprintf(*strp, (size_t)count + 1, fmt, ap);
	};
};

int codegenFormat(char **strp, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	int x = codegenFormatAP(strp, fmt, ap);
	va_end(ap);
	return x;
};

char* codegenLabel(CodeGen *gen)
{
	char buffer[256];
	sprintf(buffer, "_.L%d", gen->nextLabel++);
	return strdup(buffer);
};

void codegenDumpBlock(CodeGenBlock *block)
{
	printf("digraph {\n");
	
	// first define the labels
	int i;
	for (i=0; i<block->numInsn; i++)
	{
		CodeGenInsn *insn = block->insn[i];
		printf("\t_%p [label=\"%s\"]\n", insn, insn->debugstr);
	};
	
	// now draw the dependency lines
	for (i=0; i<block->numInsn; i++)
	{
		CodeGenInsn *insn = block->insn[i];
		
		int j;
		for (j=0; j<insn->numDeps; j++)
		{
			CodeGenInsn *dep = insn->deps[j];
			printf("\t_%p -> _%p\n", dep, insn);
		};
	};
	
	printf("}\n");
};

void codegenDumpSched(CodeGenSched *sched)
{
	int i;
	for (i=0; i<sched->numInsn; i++)
	{
		printf("%s\n", sched->insn[i]->debugstr);
	};
};

void codegenDumpRegGraph(CodeGenDriver *drv, CodeGenRegInfo *regInfo, int count)
{
	printf("strict graph {\n");
	
	// first make labels for them all
	int i;
	for (i=0; i<count; i++)
	{
		const char *regName;
		const char *color;
		
		if (regInfo[i].cpureg == -1)
		{
			regName = "spilled";
			color = "#ffffff";
		}
		else
		{
			regName = drv->regNames[regInfo[i].cpureg];
			color = nodeColors[regInfo[i].cpureg];
		};
		
		if (regInfo[i].type != 0 && regInfo[i].type != CTIR_STACK) printf("\t_%p [label=\"$%d : %s @ %d\" style=\"filled\" color=\"%s\"]\n", &regInfo[i], i, regName, regInfo[i].location, color);
	};
	
	// now draw the overlaps
	for (i=0; i<count; i++)
	{
		if (regInfo[i].type != 0 && regInfo[i].type != CTIR_STACK)
		{
			int j;
			for (j=0; j<regInfo[i].numOverlaps; j++)
			{
				CodeGenRegInfo *overlap = regInfo[i].overlaps[j];
				printf("\t_%p -- _%p\n", &regInfo[i], overlap);
			};
		};
	};
	
	printf("}\n");
};

static int isInsnReady(CodeGenInsn *insn)
{
	int i;
	for (i=0; i<insn->numDeps; i++)
	{
		if (!insn->deps[i]->schedDone) return 0;
	};
	
	return 1;
};

static CodeGenSched* codegenSched(CodeGenBlock *block)
{
	CodeGenSched *sched = (CodeGenSched*) calloc(1, sizeof(CodeGenSched));
	sched->label = block->label;
	sched->numInsn = block->numInsn;
	sched->insn = (CodeGenInsn**) malloc(sizeof(void*) * block->numInsn);
	
	// array of how busy each CPU unit is
	int unitUsage[32];
	memset(unitUsage, 0, sizeof(unitUsage));
	
	int i;
	for (i=0; i<sched->numInsn; i++)
	{
		CodeGenInsn *choice = NULL;
		int choicePriority = -1;
		
		int j;
		for (j=0; j<block->numInsn; j++)
		{
			CodeGenInsn *insn = block->insn[j];
			if (isInsnReady(insn) && !insn->schedDone)
			{
				int priority = 0;
				int k;
				for (k=0; k<32; k++)
				{
					if (insn->units & (1 << k))
					{
						priority -= unitUsage[k];
					};
				};
				
				if (choice == NULL || choicePriority < priority)
				{
					choice = insn;
					choicePriority = priority;
				};
			};
		};
		
		sched->insn[i] = choice;
		for (j=0; j<32; j++)
		{
			if (choice->units & (1 << j))
			{
				unitUsage[j] += choice->cycles;
			};
		};
		
		choice->schedDone = 1;
		
		while (choice->schedNext != NULL)
		{
			i++;
			sched->insn[i] = choice->schedNext;
			choice = choice->schedNext;
			choice->schedDone = 1;
		};
	};
	
	return sched;
};

static int countBits(uint64_t field)
{
	int n = 0;
	while (field)
	{
		if (field & 1) n++;
		field >>= 1;
	};
	
	return n;
};

static void codegenAllocRegs(CodeGenDriver *drv, CTIR_Func *func, CodeGenRegInfo *regs, int count)
{
	// force a re-allocation of pre-colored registers which cannot get their color
	int i;
	for (i=0; i<count; i++)
	{
		if (regs[i].precolor != 0)
		{
			if ((regs[i].allowedRegs & (1UL << regs[i].precolor)) == 0)
			{
				regs[i].cpureg = 0;
			};
		};
	};
	
	while (1)
	{
		// find any unallocated register with less than its allowed set of
		// registers as neighbors
		int i;
		int lastCandidate = -1;
		for (i=0; i<count; i++)
		{
			if (regs[i].type != 0 && regs[i].type != CTIR_STACK && regs[i].cpureg == 0 && !regs[i].marked)
			{
				lastCandidate = i;
				int numNeighbors = 0;
				int numColors = countBits(regs[i].allowedRegs);
				
				int j;
				for (j=0; j<regs[i].numOverlaps; j++)
				{
					if (!regs[i].overlaps[j]->marked)
					{
						numNeighbors++;
					};
				};
				
				if (numColors > numNeighbors) break;
			};
		};
		
		if (i == count)
		{
			if (lastCandidate == -1)
			{
				// we've colored everything
				return;
			};
			
			// mark the last candidate instead (we might need to spill it)
			i = lastCandidate;
		};
		
		regs[i].marked = 1;
		codegenAllocRegs(drv, func, regs, count);
		
		// now try to find a different color than our neighbors!
		uint64_t possibleColors = regs[i].allowedRegs;
		int j;
		for (j=0; j<regs[i].numOverlaps; j++)
		{
			if (!regs[i].overlaps[j]->marked)
			{
				CodeGenRegInfo *other = regs[i].overlaps[j];
				if (other->cpureg > 0)
				{
					possibleColors &= ~(1UL << other->cpureg);
				};
			};
		};
		
		if (possibleColors == 0)
		{
			// cannot color in!
			regs[i].cpureg = -1;
			regs[i].location = drv->genspill(func);
		}
		else
		{
			// pass 1: try preffered registers
			for (j=0; j<64; j++)
			{
				if (possibleColors & (1UL << j))
				{
					if (regs[i].prefRegs & (1UL << j))
					{
						regs[i].cpureg = j;
						break;
					};
				};
			};
			
			// pass 2: if that didn't work, just try any
			if (j == 64)
			{
				for (j=0; j<64; j++)
				{
					if (possibleColors & (1UL << j))
					{
						regs[i].cpureg = j;
						break;
					};
				};
			};
		};
		
		// unmark
		regs[i].marked = 0;
	};
};

void codegenInlineConst(CodeGen *gen, const char *label, size_t align, const char *defline)
{
	CodeGenInlineConst *c = (CodeGenInlineConst*) calloc(1, sizeof(CodeGenInlineConst));
	c->align = align;
	c->defline = defline;
	
	hashtabSet(gen->inlineConst, label, c);
};

int codegen(CodeGenDriver *drv, As *as, CTIR_Unit *unit)
{
	CodeGen *gen = (CodeGen*) calloc(1, sizeof(CodeGen));
	gen->driver = drv;
	gen->as = as;
	gen->unit = unit;
	gen->inlineConst = hashtabNew();
	
	if (drv->init != NULL)
	{
		Error err = drv->init(gen, drv->opt);
		if (err != ERR_OK)
		{
			fprintf(stderr, "codegen: failed to initialize driver: 0x%04X\n", err);
			return -1;
		};
	};
	
	asLine(as, "// Assembly generated by a CompTools compiler");
	
	// spit out common variables
	CTIR_Comm *comm;
	for (comm=unit->comms; comm!=NULL; comm=comm->next)
	{
		char *line;
		codegenFormat(&line, ".comm %s %lu %lu", comm->name, (unsigned long) comm->align, (unsigned long) comm->size);
		asLine(as, line);
		free(line);
	};
	
	asLine(as, "");
	
	// spit out initialized/static variables
	CTIR_Glob *glob;
	for (glob=unit->globs; glob!=NULL; glob=glob->next)
	{
		char *line;
		codegenFormat(&line, ".section %s", glob->section);
		asLine(as, line);
		free(line);
		
		switch (glob->vis)
		{
		case CTIR_VIS_WEAK:
			codegenFormat(&line, ".weak %s", glob->name);
			asLine(as, line);
			free(line);
			break;
		case CTIR_VIS_GLOBAL:
			codegenFormat(&line, ".globl %s", glob->name);
			asLine(as, line);
			free(line);
			break;
		};
		
		codegenFormat(&line, ".align %d", (int) glob->align);
		asLine(as, line);
		free(line);
		
		if (glob->vis != CTIR_VIS_LOCAL)
		{
			codegenFormat(&line, ".object %s", glob->name);
			asLine(as, line);
			free(line);
		}
		else
		{
			codegenFormat(&line, "%s:", glob->name);
			asLine(as, line);
			free(line);
		};
		
		int i;
		for (i=0; i<glob->numData; i++)
		{
			drv->gendata(gen, &glob->data[i]);
		};
		
		if (glob->numData == 0 && glob->size != 0)
		{
			codegenFormat(&line, ".resv %" PRIu64, glob->size);
			asLine(as, line);
			free(line);
		};
		
		if (glob->vis != CTIR_VIS_LOCAL)
		{
			codegenFormat(&line, ".end %s", glob->name);
			asLine(as, line);
			free(line);
		};
		
		asLine(as, "");
	};
	
	// compile the functions
	CTIR_Func *func;
	for (func=unit->funcs; func!=NULL; func=func->next)
	{
		// assign labels to all blocks, and figure out how many registers we are
		// using.
		func->numRegsUsed = func->args.numSubs + 1;
		int i;
		for (i=0; i<func->numBlocks; i++)
		{
			CTIR_Block *block = func->blocks[i];
			block->label = codegenLabel(gen);
			
			int j;
			for (j=0; j<block->numCode; j++)
			{
				CTIR_Insn *insn = &block->code[j];
				if (insn->resultReg >= func->numRegsUsed)
				{
					func->numRegsUsed = insn->resultReg+1;
				};
			};
		};
		
		CodeGenGraph *graph = drv->genfunc(gen, func);
		if (graph == NULL) return -1;
		
		//codegenDumpBlock(graph->blocks[0]); exit(0);
		
		CodeGenSched **scheds = (CodeGenSched**) calloc(func->numBlocks, sizeof(void*));
		for (i=0; i<func->numBlocks; i++)
		{
			scheds[i] = codegenSched(graph->blocks[i]);
		};
		
		//codegenDumpSched(scheds[0]); exit(0);
		
		CodeGenRegInfo *regInfo = (CodeGenRegInfo*) calloc(func->numRegsUsed, sizeof(CodeGenRegInfo));
		for (i=0; i<func->numRegsUsed; i++)
		{
			regInfo[i].overlaps = (CodeGenRegInfo**) calloc(func->numRegsUsed, sizeof(void*));
		};
		drv->genregs(gen, func, func->numBlocks, scheds, regInfo);
		
		// generate the interference graph
		for (i=0; i<func->numRegsUsed; i++)
		{
			if (regInfo[i].type != 0 && regInfo[i].type != CTIR_STACK)
			{
				int j;
				for (j=i+1; j<func->numRegsUsed; j++)
				{
					if (regInfo[j].type != 0 && regInfo[j].type != CTIR_STACK)
					{
						if (regInfo[i].bornAt < regInfo[j].deadAt && regInfo[j].bornAt < regInfo[i].deadAt)
						{
							regInfo[i].overlaps[regInfo[i].numOverlaps++] = &regInfo[j];
							regInfo[j].overlaps[regInfo[j].numOverlaps++] = &regInfo[i];
						};
					};
				};
			};
		};
		
		codegenAllocRegs(drv, func, regInfo, func->numRegsUsed);
		//codegenDumpRegGraph(drv, regInfo, func->numRegsUsed); exit(0);
		
		asLine(as, ".section .text");
		
		char *line;
		switch (func->vis)
		{
		case CTIR_VIS_WEAK:
			codegenFormat(&line, ".weak %s", func->name);
			asLine(as, line);
			free(line);
			break;
		case CTIR_VIS_GLOBAL:
			codegenFormat(&line, ".globl %s", func->name);
			asLine(as, line);
			free(line);
			break;
		};
		
		codegenFormat(&line, ".func %s", func->name);
		asLine(as, line);
		free(line);

		drv->genprolog(gen, func, regInfo);
	
		for (i=0; i<func->numBlocks; i++)
		{
			CodeGenSched *sched = scheds[i];
			codegenFormat(&line, "%s:", sched->label);
			asLine(as, line);
			free(line);
			
			drv->genblock(gen, func, regInfo, sched, i);
		};
		
		drv->genepilog(gen, func, regInfo);
		
		codegenFormat(&line, ".end %s", func->name);
		asLine(as, line);
		free(line);
	};
	
	asLine(as, "");
	asLine(as, ".section .rodata");
	
	const char *label;
	HASHTAB_FOREACH(gen->inlineConst, CodeGenInlineConst*, label, c)
	{
		char *line;
		codegenFormat(&line, "%s:", label);
		asLine(as, line);
		free(line);
		
		codegenFormat(&line, ".align %d", (int) c->align);
		asLine(as, line);
		free(line);
		
		asLine(as, c->defline);
	};
	
	return 0;
};
