/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <comptools/console.h>

static int errorStatus = 0;
int conWerror;

int conGetError()
{
	return errorStatus;
};

void conClearError()
{
	errorStatus = 0;
};

void conDiagAP(const char *filename, int lineno, int col, int len, int level, const char *message, va_list ap)
{
	(void)len;
	static const char *levelNames[] = {"note", "warning", "error", "critical"};
	static const char *levelCols[] = {"\e[94m", "\e[93m", "\e[91m", "\e[91m"};
	static const char *normalColor = "\e[0m";
	
	if (level == CON_WARNING && conWerror)
	{
		level = CON_ERROR;
	};
	
	int printOk = 0;
	if (isatty(2))
	{
		FILE *fp = fopen(filename, "r");
		if (fp != NULL)
		{
			char linebuf[2048];
			char *line;
			int i;
			
			fprintf(stderr, " ---\nIn file `%s', at line %d:\n", filename, lineno);
			//while (count--) line = fgets(linebuf, 2048, fp);
			
			for (i=1;;i++)
			{
				line = fgets(linebuf, 2048, fp);
				if (line == NULL) break;
				
				char *endline = strchr(line, '\n');
				if (endline != NULL) *endline = 0;
				
				if (i < lineno-2 || i > lineno+2) continue;
				
				if (i == lineno) fprintf(stderr, "%s%5d%s| ", levelCols[level], i, normalColor);
				else fprintf(stderr, "%5d| ", i);
				
				if (i != lineno)
				{
					fprintf(stderr, "%s\n", line);
				}
				else
				{
					// print the part of the line before 'col'
					col--;
					if (col < 0) col = 0;
					if (col > strlen(line))
					{
						col = 0;
						len = 0;
					};
					
					if ((col+len) > strlen(line))
					{
						len = strlen(line) - col;
					};
					
					if (len == 0)
					{
						col = 0;
						len = strlen(line);
					};
					
					char buf2[2048];
					memcpy(buf2, line, col);
					buf2[col] = 0;
					
					fprintf(stderr, "%s%s", buf2, levelCols[level]);
					
					// now the "highlighted" part
					memcpy(buf2, &line[col], len);
					buf2[len] = 0;
					fprintf(stderr, "%s%s", buf2, normalColor);
					
					// finally the rest
					fprintf(stderr, "%s\n", &line[col+len]);
				};
			};

			fprintf(stderr, "%s%s:%s ", levelCols[level], levelNames[level], normalColor);
			vfprintf(stderr, message, ap);
			fprintf(stderr, "\n ---\n\n");
			
			printOk = 1;
			
			fclose(fp);
		};
	};
	
	if (!printOk)
	{
		fprintf(stderr, "%s:%d:%d: %s: ", filename, lineno, col, levelNames[level]);
		vfprintf(stderr, message, ap);
		fprintf(stderr, "\n");
	};
	
	if (level >= CON_ERROR)
	{
		errorStatus = 1;
	};
};

void conDiag(const char *filename, int lineno, int col, int len, int level, const char *message, ...)
{
	va_list ap;
	va_start(ap, message);
	conDiagAP(filename, lineno, col, len, level, message, ap);
	va_end(ap);
};
