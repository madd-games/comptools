/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <comptools/obj.h>
#include <comptools/opt.h>

const char *format = OBJDUMP_DEFAULT_OBJ;
int dumpHeader = 0;
int dumpSections = 0;
int dumpSymbols = 0;
int dumpRelocs = 0;

const char *getObjTypeName(int type)
{
	switch (type)
	{
	case OBJTYPE_EXEC:	return "OBJTYPE_EXEC";
	case OBJTYPE_RELOC:	return "OBJTYPE_RELOC";
	case OBJTYPE_SHARED:	return "OBJTYPE_SHARED";
	default:		return "??";
	};
};

const char *getSecTypeName(int type)
{
	switch (type)
	{
	case SECTYPE_PROGBITS:	return "PROGBITS";
	case SECTYPE_NOBITS:	return "NOBITS";
	default:		return "??";
	};
};

const char *getSymBindingName(int binding)
{
	switch (binding)
	{
	case SYMB_LOCAL:	return "LOCAL";
	case SYMB_GLOBAL:	return "GLOBAL";
	case SYMB_WEAK:		return "WEAK";
	default:		return "??";
	};
};

const char *getSymTypeName(int type)
{
	switch (type)
	{
	case SYMT_COMMON:	return "COMMON";
	case SYMT_FUNC:		return "FUNC";
	case SYMT_NONE:		return "NONE";
	case SYMT_OBJECT:	return "OBJECT";
	case SYMT_SECTION:	return "SECTION";
	case SYMT_UNDEF:	return "UNDEF";
	default:		return "??";
	};
};

int main(int argc, char *argv[])
{
	OptionList optlist;
	optInit(&optlist);
	optAdd(&optlist, "-format", "FORMAT", "Specify the object file format driver to use.",
		OPT_STRING, &format);
	optAdd(&optlist, "-h", "", "Show header information.", OPT_FLAG, &dumpHeader);
	optAdd(&optlist, "-S", "", "Show sections.", OPT_FLAG, &dumpSections);
	optAdd(&optlist, "-s", "", "Show symbols.", OPT_FLAG, &dumpSymbols);
	optAdd(&optlist, "-r", "", "Show relocations.", OPT_FLAG, &dumpRelocs);
	
	char **files = optParse(argc, argv, &optlist);
	if (files == NULL)
	{
		return 1;
	};
	
	if (files[0] == NULL)
	{
		fprintf(stderr, "%s: no input files\n", argv[0]);
		fprintf(stderr, "Use `%s --help' to get help.\n", argv[0]);
		return 1;
	};
	
	if (files[1] != NULL)
	{
		fprintf(stderr, "%s: expecting only one input file\n", argv[0]);
		return 1;
	};
	
	ObjDriver *driver = objGetDriver(format);
	if (driver == NULL)
	{
		fprintf(stderr, "%s: file format driver `%s' not found\n", argv[0], format);
		return 1;
	};
	
	Object *obj = objRead(driver, files[0]);
	if (obj == NULL)
	{
		fprintf(stderr, "%s: failed to read `%s'\n", argv[0], files[0]);
		return 1;
	};
	
	if (dumpHeader)
	{
		printf("Object: %s, type %s (%d)\n", format, getObjTypeName(obj->type), obj->type);
		const char *soname = obj->soname;
		if (soname == NULL) soname = "(none)";
		printf("Library name (soname): %s\n", soname);
		printf("\n");
	};
	
	if (dumpSections)
	{
		printf("Sections:\n");
		printf("%-16s%-9s%-19s%-19s%-7s%s\n", "SECTION", "TYPE", "ADDR", "SIZE", "ALIGN", "FLAGS");
		
		Section *sect;
		for (sect=obj->sects; sect!=NULL; sect=sect->next)
		{
			char flags[5];
			char *put = flags;
			if (sect->flags & SEC_READ) *put++ = 'R';
			if (sect->flags & SEC_WRITE) *put++ = 'W';
			if (sect->flags & SEC_EXEC) *put++ = 'X';
			*put = 0;
			
			printf("%-16s%-9s0x%016" PRIu64" 0x%016" PRIu64 " 0x%04" PRIu64 " %s\n",
				sect->name, getSecTypeName(sect->type),
				sect->addr, sect->size, sect->align, flags);
		};
		
		printf("\n");
	};
	
	if (dumpSymbols)
	{
		printf("Symbols:\n");
		printf("%-16s%-19s%-10s%-8s%-8s%s\n", "SECTION", "VALUE", "SIZE", "BINDING", "TYPE", "NAME");
		
		Symbol *sym;
		for (sym=obj->syms; sym!=NULL; sym=sym->next)
		{
			const char *sectname = "(none)";
			if (sym->sect != NULL) sectname = sym->sect->name;
			
			printf("%-16s0x%016" PRIu64 " %-10d%-8s%-8s%s\n", sectname, sym->value, (int) sym->size,
				getSymBindingName(sym->binding), getSymTypeName(sym->type),
				sym->name);
		};
		
		printf("\n");
	};
	
	if (dumpRelocs)
	{
		Section *sect;
		for (sect=obj->sects; sect!=NULL; sect=sect->next)
		{
			printf("Relocations for section `%s':\n", sect->name);
			printf("%-19s%-20s%-9s%s\n", "OFFSET", "TYPE", "PROP", "AGAINST");
			
			Reloc *reloc;
			for (reloc=sect->relocs; reloc!=NULL; reloc=reloc->next)
			{
				const char *prop = "";
				if (reloc->prop != NULL) prop = reloc->prop;
				
				printf("0x%016" PRIu64 " %-20s%-9s%s%+d\n",
					reloc->offset, reloc->type->name, prop, reloc->sym->name,
					(int) reloc->addend);
			};
			
			printf("\n");
		};
	};
	
	return 0;
};
