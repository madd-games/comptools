/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>
#include <errno.h>

#include <comptools/as.h>
#include <comptools/obj.h>
#include <comptools/opt.h>
#include <comptools/console.h>

const char *outFile = "a.out";
int verbose = 0;

int main(int argc, char *argv[])
{
	AsDriver *asdrv = asGetDriver(AS_DEFAULT_FAMILY);
	if (asdrv == NULL)
	{
		fprintf(stderr, "%s: CRITICAL: could not find assembler driver for `%s'\n",
			argv[0], AS_DEFAULT_FAMILY);
		return 1;
	};

	ObjDriver *objdrv = objGetDriver(AS_DEFAULT_OBJ);
	if (objdrv == NULL)
	{
		fprintf(stderr, "%s: CRITICAL: could not find object file format driver `%s'\n",
			argv[0], AS_DEFAULT_OBJ);
		return 1;
	};

	int dummy;
	
	OptionList optlist;
	optInit(&optlist);
	if (objdrv->drvOpts != NULL) objdrv->drvOpts(&optlist);
	optAdd(&optlist, "-v", "", "Be verbose: print additional debugging information.",
		OPT_FLAG, &verbose);
	optAdd(&optlist, "-o", "<output-file>", "Specify the output file name. Defaults to `a.out'.",
		OPT_SHORT_STRING, &outFile);
	optAdd(&optlist, "-c", "", "Does nothing; for compatibility reasons only.",
		OPT_FLAG, &dummy);
		
	char **files = optParse(argc, argv, &optlist);
	if (files == NULL)
	{
		return 1;
	};
	
	if (files[0] == NULL)
	{
		fprintf(stderr, "%s: no input files\n", argv[0]);
		fprintf(stderr, "Use `%s --help' to get help.\n", argv[0]);
		return 1;
	};
	
	if (files[1] != NULL)
	{
		fprintf(stderr, "%s: expecting only one input file\n", argv[0]);
		return 1;
	};
	
	FILE *fp = fopen(files[0], "r");
	if (fp == NULL)
	{
		fprintf(stderr, "%s: cannot open `%s': %s\n", argv[0], files[0], strerror(errno));
		return 1;
	};
	
	Object *obj = objNew(objdrv);
	if (obj == NULL)
	{
		fprintf(stderr, "%s: could not create object\n", argv[0]);
		return 1;
	};
	
	As *as = asNew(obj, asdrv);
	if (as == NULL)
	{
		fprintf(stderr, "%s: could not create assembler session\n", argv[0]);
		return 1;
	};
	
	char linebuf[2048];
	char *line;
	int lineno = 0;
	
	int status = 0;
	while ((line = fgets(linebuf, 2048, fp)) != NULL)
	{
		lineno++;
		
		char *error = asLine(as, line);
		if (error != NULL)
		{
			conDiag(files[0], lineno, 0, 0, CON_ERROR, "%s", error);
			status = 1;
		};
	};
	
	fclose(fp);
	
	if (status == 0)
	{
		Error err = objWrite(obj, outFile);
		if (err != ERR_OK)
		{
			fprintf(stderr, "%s: failed to write object `%s': error 0x%04X\n",
					argv[0], outFile, err);
			status = 1;
		};
	};
	
	return status;
};
