/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <string.h>

#include <comptools/elf64.h>

/**
 * Layout of sections:
 * 	Index			Name			Description
 * 	0			(null)			Null section.
 * 	1			.shstrtab		Section name string table.
 * 	2			.symtab			Symbol table.
 * 	3			.strtab			String table.
 *
 * Layout of file:
 * 		ELF64 Header
 * 		Program Headers
 * 		Section Headers
 * 		Symbol table
 * 		Section name string table data
 * 		Symbol string table data
 * 		Relocation tables
 * 		Section data
 */
#define	SHN_SHSTRTAB		1
#define	SHN_SYMTAB		2
#define	SHN_STRTAB		3

typedef struct
{
	char*			data;
	uint64_t		size;
} StringTable;

static uint64_t stralloc(StringTable *tab, const char *str)
{
	uint64_t result = tab->size;
	tab->data = (char*) realloc(tab->data, tab->size+strlen(str)+1);
	strcpy(&tab->data[tab->size], str);
	tab->size += strlen(str) + 1;
	return result;
};

void elf64_init(Object *obj, const void *opt)
{
	Elf64_DrvOptions *newopt = (Elf64_DrvOptions*) malloc(sizeof(Elf64_DrvOptions));
	memcpy(newopt, opt, sizeof(Elf64_DrvOptions));
	
	obj->drvdata = newopt;
};

Error elf64_write(Object *obj, FILE *fp)
{
	Elf64_DrvOptions *opt = (Elf64_DrvOptions*) obj->drvdata;
	
	StringTable shstrtab = {NULL, 0};
	StringTable genstrtab = {NULL, 0};

	uint64_t nameShstrtab = stralloc(&shstrtab, ".shstrtab");
	uint64_t nameSymtab = stralloc(&shstrtab, ".symtab");
	uint64_t nameStrtab = stralloc(&shstrtab, ".strtab");
	
	// calculate the number of program headers and section headers needed
	// for sections, we need: the null section (index 0), section header string table,
	// symbol table and symbol table string table (4 sections). Then for each Section
	// structure, we need 2 sections: the section itself, and a relocation table (IF
	// NEEDED).
	// for program headers: we need a PT_NULL/PT_PHDR header, PT_LOAD for program header table,
	// plus a PT_LOAD for each Section;
	// except for dynamic sections, which have 2 program headers IN TOTAL: a single
	// PT_LOAD which loads all dynamic data, followed by a PT_DYNAMIC for the ".dynamic"
	// section
	size_t numProgHeads = 2;
	size_t numSectHeads = 4;
	int haveDyn = 0;
	
	Section *sect;
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{	
		Elf64_SectAux *aux = (Elf64_SectAux*) malloc(sizeof(Elf64_SectAux));
		sect->aux = aux;
		
		memset(aux, 0, sizeof(Elf64_SectAux));
		aux->index = numSectHeads;
		aux->name = stralloc(&shstrtab, sect->name);

		char *relaname = (char*) malloc(strlen(sect->name) + 16);
		sprintf(relaname, ".rela%s", sect->name);
		aux->relaName = stralloc(&shstrtab, relaname);
		free(relaname);
		
		if (sect->flags & SEC_ELF_DYN)
		{
			haveDyn = 1;
		}
		else
		{
			numProgHeads++;
		};
		
		numSectHeads++;
		
		if (sect->relocs != NULL) numSectHeads++;
		if (strcmp(sect->name, ".interp") == 0) numProgHeads++;
	};
	
	// PT_LOAD + PT_DYNAMIC for dynamic sections if needed
	if (haveDyn) numProgHeads += 2;
	
	// create the symbol table (in memory for now)
	stralloc(&genstrtab, "");
	Elf64_Sym *symtab = (Elf64_Sym*) malloc(sizeof(Elf64_Sym));
	memset(symtab, 0, sizeof(Elf64_Sym));
	size_t numSymbols = 1;
	
	// first local symbols only
	Symbol *sym;
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		if (sym->binding == SYMB_LOCAL && sym->type != SYMT_UNDEF)
		{
			size_t index = numSymbols++;
			
			symtab = (Elf64_Sym*) realloc(symtab, sizeof(Elf64_Sym)*numSymbols);
			Elf64_Sym *elfsym = (Elf64_Sym*) &symtab[index];
			memset(elfsym, 0, sizeof(Elf64_Sym));

			unsigned char b, t;
			switch (sym->binding)
			{
			case SYMB_LOCAL:
				b = STB_LOCAL;
				break;
			case SYMB_GLOBAL:
				b = STB_GLOBAL;
				break;
			case SYMB_WEAK:
				b = STB_WEAK;
				break;
			};
			
			switch (sym->type)
			{
			case SYMT_NONE:
				t = STT_NOTYPE;
				break;
			case SYMT_OBJECT:
				t = STT_OBJECT;
				break;
			case SYMT_FUNC:
				t = STT_FUNC;
				break;
			case SYMT_SECTION:
				t = STT_SECTION;
				break;
			default:
				t = STT_NOTYPE;
				break;
			};
			
			opt->make(stralloc(&genstrtab, sym->name), &elfsym->st_name, sizeof(elfsym->st_name));
			elfsym->st_info = ELF_MAKE_STINFO(b, t);
			
			uint64_t value = sym->value;
			if (obj->type != OBJTYPE_RELOC)
			{
				if (sym->sect != NULL)
				{
					value += sym->sect->addr;
				};
			};
			
			if (sym->sect != NULL)
			{
				Elf64_SectAux *sectaux = (Elf64_SectAux*) sym->sect->aux;
				opt->make(sectaux->index, &elfsym->st_shndx, sizeof(elfsym->st_shndx));
			}
			else if (sym->type != SYMT_UNDEF)
			{
				opt->make(SHN_ABS, &elfsym->st_shndx, sizeof(elfsym->st_shndx));
			};
			
			if (sym->type == SYMT_COMMON)
			{
				opt->make(SHN_COMMON, &elfsym->st_shndx, sizeof(elfsym->st_shndx));
				opt->make(sym->align, &elfsym->st_value, sizeof(elfsym->st_value));
			}
			else
			{
				opt->make(value, &elfsym->st_value, sizeof(elfsym->st_value));
			};
			
			opt->make(sym->size, &elfsym->st_size, sizeof(elfsym->st_size));
			
			Elf64_SymAux *symaux = (Elf64_SymAux*) malloc(sizeof(Elf64_SymAux));
			symaux->index = index;
			sym->aux = symaux;
		};
	};

	// now the non-local symbols
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		if (sym->binding != SYMB_LOCAL || sym->type == SYMT_UNDEF)
		{
			size_t index = numSymbols++;
			
			symtab = (Elf64_Sym*) realloc(symtab, sizeof(Elf64_Sym)*numSymbols);
			Elf64_Sym *elfsym = (Elf64_Sym*) &symtab[index];
			memset(elfsym, 0, sizeof(Elf64_Sym));

			unsigned char b, t;
			switch (sym->binding)
			{
			case SYMB_LOCAL:
				b = STB_LOCAL;
				break;
			case SYMB_GLOBAL:
				b = STB_GLOBAL;
				break;
			case SYMB_WEAK:
				b = STB_WEAK;
				break;
			};
			
			if (sym->type == SYMT_UNDEF) b = STB_GLOBAL;

			switch (sym->type)
			{
			case SYMT_NONE:
				t = STT_NOTYPE;
				break;
			case SYMT_OBJECT:
				t = STT_OBJECT;
				break;
			case SYMT_FUNC:
				t = STT_FUNC;
				break;
			case SYMT_SECTION:
				t = STT_SECTION;
				break;
			default:
				t = STT_NOTYPE;
				break;
			};
			
			opt->make(stralloc(&genstrtab, sym->name), &elfsym->st_name, sizeof(elfsym->st_name));
			elfsym->st_info = ELF_MAKE_STINFO(b, t);
			
			if (sym->sect != NULL)
			{
				Elf64_SectAux *sectaux = (Elf64_SectAux*) sym->sect->aux;
				opt->make(sectaux->index, &elfsym->st_shndx, sizeof(elfsym->st_shndx));
			}
			else if (sym->type != SYMT_UNDEF)
			{
				opt->make(SHN_ABS, &elfsym->st_shndx, sizeof(elfsym->st_shndx));
			};

			uint64_t value = sym->value;
			if (obj->type != OBJTYPE_RELOC)
			{
				if (sym->sect != NULL)
				{
					value += sym->sect->addr;
				};
			};
		
			if (sym->type == SYMT_COMMON)
			{
				opt->make(SHN_COMMON, &elfsym->st_shndx, sizeof(elfsym->st_shndx));
				opt->make(sym->align, &elfsym->st_value, sizeof(elfsym->st_value));
			}
			else
			{
				opt->make(value, &elfsym->st_value, sizeof(elfsym->st_value));
			};
			
			opt->make(sym->size, &elfsym->st_size, sizeof(elfsym->st_size));
			
			Elf64_SymAux *symaux = (Elf64_SymAux*) malloc(sizeof(Elf64_SymAux));
			symaux->index = index;
			sym->aux = symaux;
		};
	};
	
	// allocate space for everything inside the file
	uint64_t place = sizeof(Elf64_Ehdr) + numProgHeads * sizeof(Elf64_Phdr)
				+ numSectHeads * sizeof(Elf64_Shdr)
				+ numSymbols * sizeof(Elf64_Sym)
				+ shstrtab.size + genstrtab.size;
	
	uint64_t posStrtab = place - genstrtab.size;
	uint64_t posShstrtab = posStrtab - shstrtab.size;
	uint64_t posSymtab = posShstrtab - (numSymbols * sizeof(Elf64_Sym));
	
	// write the relocation tables for all sections.
	fseek(fp, place, SEEK_SET);
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		Elf64_SectAux *aux = (Elf64_SectAux*) sect->aux;
		aux->relaOffset = ftell(fp);
		aux->relaSize = 0;
		
		Reloc *reloc;
		for (reloc=sect->relocs; reloc!=NULL; reloc=reloc->next)
		{
			int typeval = opt->relocCompToElf(reloc->type, reloc->prop);
			if (typeval == -1)
			{
				fprintf(stderr, "elf64: error: skipping unknown relocation `%s'\n",
						reloc->type->name);
				continue;
			};
			
			Elf64_Rela rela;
			memset(&rela, 0, sizeof(Elf64_Rela));
			
			Elf64_SymAux *symaux = (Elf64_SymAux*) reloc->sym->aux;
			opt->make(reloc->offset, &rela.r_offset, sizeof(rela.r_offset));
			uint64_t info = ELF64_R_INFO(symaux->index, typeval);
			opt->make(info, &rela.r_info, sizeof(rela.r_info));
			opt->make(reloc->addend, &rela.r_addend, sizeof(rela.r_addend));
			
			fwrite(&rela, sizeof(Elf64_Rela), 1, fp);
			aux->relaSize += sizeof(Elf64_Rela);
		};
	};
	
	// write non-dynamic section contents
	place = ftell(fp);
	uint64_t highestSectionAddr = 0;
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		if (sect->type == SECTYPE_PROGBITS && (sect->flags & SEC_ELF_DYN) == 0)
		{
			uint64_t mask = opt->pageSize - 1;
			while ((place & mask) != (sect->addr & mask)) place++;
			
			fseek(fp, place, SEEK_SET);
			fwrite(sect->data, 1, sect->size, fp);
			
			Elf64_SectAux *aux = (Elf64_SectAux*) sect->aux;
			aux->offset = place;
			place += sect->size;
		};
		
		uint64_t end = sect->addr + sect->size;
		if (end > highestSectionAddr) highestSectionAddr = end;
	};
	
	highestSectionAddr = (highestSectionAddr + 0xFFF) & ~0xFFF;
	
	// dynamic sections at the very end (to ensure they are in one place and correctly
	// group)
	uint64_t mask = opt->pageSize - 1;
	place = (place + mask) & ~mask;

	uint64_t dynBaseAddr = 0xFFFFFFFFFFFFFFFFUL;
	uint64_t dynEnd = 0;
	
	uint64_t dynSectVirt = 0;
	uint64_t dynSectOffset = 0;
	uint64_t dynSectSize = 0;
	
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		if (sect->flags & SEC_ELF_DYN)
		{
			if (sect->addr < dynBaseAddr)
			{
				dynBaseAddr = sect->addr;
			};
		};
	};
	
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		if (sect->flags & SEC_ELF_DYN)
		{
			if (strcmp(sect->name, ".dynsym") == 0)
			{
				// fix the section number in the symbol entries
				int i;
				for (i=0; i<sect->size/sizeof(Elf64_Sym); i++)
				{
					Elf64_Sym *elfsym = (Elf64_Sym*) sect->data;
					elfsym = &elfsym[i];
					
					uint64_t shndx = opt->get(&elfsym->st_shndx, sizeof(elfsym->st_shndx));
					
					if (shndx != 0)
					{
						uint64_t addr = opt->get(&elfsym->st_value, sizeof(elfsym->st_value));
						
						int index = SHN_ABS;
						
						Section *scan;
						for (scan=obj->sects; scan!=NULL; scan=scan->next)
						{
							if (addr >= scan->addr && addr < (scan->addr+scan->size))
							{
								Elf64_SectAux *scanaux = (Elf64_SectAux*) scan->aux;
								index = scanaux->index;
								break;
							};
						};
						
						opt->make(index, &elfsym->st_shndx, sizeof(elfsym->st_shndx));
					};
				};
			};

			uint64_t start = place + (sect->addr-dynBaseAddr);
			uint64_t end = start + sect->size;
			if (end > dynEnd) dynEnd = end;
			
			fseek(fp, start, SEEK_SET);
			fwrite(sect->data, 1, sect->size, fp);
			
			Elf64_SectAux *aux = (Elf64_SectAux*) sect->aux;
			aux->offset = start;
			
			if (strcmp(sect->name, ".dynamic") == 0)
			{
				dynSectOffset = start;
				dynSectVirt = sect->addr;
				dynSectSize = sect->size;
			};
		};
	};
	
	// create the ELF64 header
	Elf64_Ehdr ehdr;
	memset(&ehdr, 0, sizeof(Elf64_Ehdr));
	
	memcpy(ehdr.e_ident, "\x7F" "ELF", 4);
	ehdr.e_ident[EI_CLASS] = ELFCLASS64;
	
	if (opt->make == ctMakeLE)
	{
		ehdr.e_ident[EI_DATA] = ELFDATA2LSB;
	}
	else
	{
		ehdr.e_ident[EI_DATA] = ELFDATA2MSB;
	};
	
	ehdr.e_ident[EI_VERSION] = 1;
	ehdr.e_ident[EI_OSABI] = ELFOSABI_SYSV;
	ehdr.e_ident[EI_ABIVERSION] = 0;
	
	uint64_t type;
	switch (obj->type)
	{
	case OBJTYPE_EXEC:
		type = ET_EXEC;
		break;
	case OBJTYPE_SHARED:
		type = ET_DYN;
		break;
	default:
		type = ET_REL;
		break;
	};
	
	opt->make(type, &ehdr.e_type, sizeof(ehdr.e_type));
	opt->make(opt->machine, &ehdr.e_machine, sizeof(ehdr.e_machine));
	opt->make(1, &ehdr.e_version, sizeof(ehdr.e_version));
	
	uint64_t entry;
	if (obj->entry == NULL)
	{
		entry = 0;
	}
	else
	{
		switch (obj->entry->type)
		{
		case SYMT_NONE:
		case SYMT_OBJECT:
		case SYMT_FUNC:
		case SYMT_SECTION:
			entry = obj->entry->value;
			if (obj->entry->sect != NULL) entry += obj->entry->sect->addr;
			break;
		default:
			fprintf(stderr, "elf64: warning: failed to resolve entry symbol, defaulting to 0\n");
			entry = 0;
			break;
		};
	};
	
	opt->make(entry, &ehdr.e_entry, sizeof(ehdr.e_entry));
	opt->make(sizeof(Elf64_Ehdr), &ehdr.e_phoff, sizeof(ehdr.e_phoff));
	opt->make(sizeof(Elf64_Ehdr) + sizeof(Elf64_Phdr) * numProgHeads, &ehdr.e_shoff, sizeof(ehdr.e_shoff));
	opt->make(sizeof(Elf64_Ehdr), &ehdr.e_ehsize, sizeof(ehdr.e_ehsize));
	opt->make(sizeof(Elf64_Phdr), &ehdr.e_phentsize, sizeof(ehdr.e_phentsize));
	opt->make(numProgHeads, &ehdr.e_phnum, sizeof(ehdr.e_phnum));
	opt->make(sizeof(Elf64_Shdr), &ehdr.e_shentsize, sizeof(ehdr.e_shentsize));
	opt->make(numSectHeads, &ehdr.e_shnum, sizeof(ehdr.e_shnum));
	opt->make(SHN_SHSTRTAB, &ehdr.e_shstrndx, sizeof(ehdr.e_shstrndx));
	
	// write the ELF64 header
	fseek(fp, 0, SEEK_SET);
	fwrite(&ehdr, sizeof(Elf64_Ehdr), 1, fp);
	
	// write the PT_NULL/PT_PHDR program header
	Elf64_Phdr phdr;
	memset(&phdr, 0, sizeof(Elf64_Phdr));
	if (haveDyn)
	{
		opt->make(PT_PHDR, &phdr.p_type, sizeof(phdr.p_type));
		opt->make(PF_X | PF_R, &phdr.p_flags, sizeof(phdr.p_flags));
		opt->make(sizeof(Elf64_Ehdr), &phdr.p_offset, sizeof(phdr.p_offset));
		opt->make(highestSectionAddr + sizeof(Elf64_Ehdr), &phdr.p_vaddr, sizeof(phdr.p_vaddr));
		opt->make(highestSectionAddr + sizeof(Elf64_Ehdr), &phdr.p_paddr, sizeof(phdr.p_paddr));
		opt->make(sizeof(Elf64_Phdr) * numProgHeads, &phdr.p_filesz, sizeof(phdr.p_filesz));
		opt->make(sizeof(Elf64_Phdr) * numProgHeads, &phdr.p_memsz, sizeof(phdr.p_memsz));
		opt->make(8, &phdr.p_align, sizeof(phdr.p_align));
	};
	fwrite(&phdr, sizeof(Elf64_Phdr), 1, fp);
	
	// write the PT_LOAD for program header table
	memset(&phdr, 0, sizeof(Elf64_Phdr));
	opt->make(PT_LOAD, &phdr.p_type, sizeof(phdr.p_type));
	opt->make(PF_X | PF_R, &phdr.p_flags, sizeof(phdr.p_flags));
	opt->make(highestSectionAddr, &phdr.p_vaddr, sizeof(phdr.p_vaddr));
	opt->make(highestSectionAddr, &phdr.p_paddr, sizeof(phdr.p_paddr));
	opt->make(sizeof(Elf64_Phdr) * numProgHeads + sizeof(Elf64_Ehdr), &phdr.p_filesz, sizeof(phdr.p_filesz));
	opt->make(sizeof(Elf64_Phdr) * numProgHeads + sizeof(Elf64_Ehdr), &phdr.p_memsz, sizeof(phdr.p_memsz));
	opt->make(opt->pageSize, &phdr.p_align, sizeof(phdr.p_align));
	long phdrLoadPos = ftell(fp);
	fwrite(&phdr, sizeof(Elf64_Phdr), 1, fp);
	
	Elf64_Phdr phdrLoad;
	memcpy(&phdrLoad, &phdr, sizeof(Elf64_Phdr));
	
	// now write the useable program headers
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		if ((sect->flags & SEC_ELF_DYN) == 0 || strcmp(sect->name, ".interp") == 0)
		{
			Elf64_SectAux *aux = (Elf64_SectAux*) sect->aux;
				
			memset(&phdr, 0, sizeof(Elf64_Phdr));
			opt->make(PT_LOAD, &phdr.p_type, sizeof(phdr.p_type));
			
			if (strcmp(sect->name, ".interp") == 0)
			{
				opt->make(PT_INTERP, &phdr.p_type, sizeof(phdr.p_type));
			};
			
			uint64_t flags = 0;
			if (sect->flags & SEC_READ) flags |= PF_R;
			if (sect->flags & SEC_WRITE) flags |= PF_W;
			if (sect->flags & SEC_EXEC) flags |= PF_X;
			
			opt->make(flags, &phdr.p_flags, sizeof(phdr.p_flags));
			opt->make(aux->offset, &phdr.p_offset, sizeof(phdr.p_offset));
			opt->make(sect->addr, &phdr.p_vaddr, sizeof(phdr.p_vaddr));
			opt->make(sect->addr, &phdr.p_paddr, sizeof(phdr.p_paddr));
			if (sect->type == SECTYPE_PROGBITS) opt->make(sect->size, &phdr.p_filesz, sizeof(phdr.p_filesz));
			opt->make(sect->size, &phdr.p_memsz, sizeof(phdr.p_memsz));
			opt->make(opt->pageSize, &phdr.p_align, sizeof(phdr.p_align));
			
			if (strcmp(sect->name, ".interp") == 0)
			{
				// PT_INTERP must go first!
				// swap it with the initial PT_LOAD for program headers
				long thisPos = ftell(fp);
				fseek(fp, phdrLoadPos, SEEK_SET);
				fwrite(&phdr, sizeof(Elf64_Phdr), 1, fp);
				fseek(fp, thisPos, SEEK_SET);
				fwrite(&phdrLoad, sizeof(Elf64_Phdr), 1, fp);
			}
			else
			{	
				fwrite(&phdr, sizeof(Elf64_Phdr), 1, fp);
			};
		};
	};
	
	// if we have dynamic sections, create the program headers for them here
	if (haveDyn)
	{
		// first the PT_LOAD
		memset(&phdr, 0, sizeof(Elf64_Phdr));
		opt->make(PT_LOAD, &phdr.p_type, sizeof(phdr.p_type));
		opt->make(PF_R | PF_W | PF_X, &phdr.p_flags, sizeof(phdr.p_flags));
		opt->make(place, &phdr.p_offset, sizeof(phdr.p_offset));
		opt->make(dynBaseAddr, &phdr.p_vaddr, sizeof(phdr.p_vaddr));
		opt->make(dynBaseAddr, &phdr.p_paddr, sizeof(phdr.p_paddr));
		opt->make(dynEnd - place, &phdr.p_filesz, sizeof(phdr.p_filesz));
		opt->make(dynEnd - place, &phdr.p_memsz, sizeof(phdr.p_memsz));
		opt->make(opt->pageSize, &phdr.p_align, sizeof(phdr.p_align));
		fwrite(&phdr, sizeof(Elf64_Phdr), 1, fp);
		
		// now PT_DYNAMIC
		memset(&phdr, 0, sizeof(Elf64_Phdr));
		opt->make(PT_DYNAMIC, &phdr.p_type, sizeof(phdr.p_type));
		opt->make(PF_R | PF_W | PF_X, &phdr.p_flags, sizeof(phdr.p_flags));
		opt->make(dynSectOffset, &phdr.p_offset, sizeof(phdr.p_offset));
		opt->make(dynSectVirt, &phdr.p_vaddr, sizeof(phdr.p_vaddr));
		opt->make(dynSectVirt, &phdr.p_paddr, sizeof(phdr.p_paddr));
		opt->make(dynSectSize, &phdr.p_filesz, sizeof(phdr.p_filesz));
		opt->make(dynSectSize, &phdr.p_memsz, sizeof(phdr.p_memsz));
		opt->make(8, &phdr.p_align, sizeof(phdr.p_align));
		fwrite(&phdr, sizeof(Elf64_Phdr), 1, fp);
	};
	
	// section header time!
	// section 0: null section
	Elf64_Shdr shdr;
	memset(&shdr, 0, sizeof(Elf64_Shdr));
	fwrite(&shdr, sizeof(Elf64_Shdr), 1, fp);
	
	// section 1: section name string table
	memset(&shdr, 0, sizeof(Elf64_Shdr));
	opt->make(nameShstrtab, &shdr.sh_name, sizeof(shdr.sh_name));
	opt->make(SHT_STRTAB, &shdr.sh_type, sizeof(shdr.sh_type));
	opt->make(posShstrtab, &shdr.sh_offset, sizeof(shdr.sh_offset));
	opt->make(shstrtab.size, &shdr.sh_size, sizeof(shdr.sh_size));
	opt->make(1, &shdr.sh_addralign, sizeof(shdr.sh_addralign));
	fwrite(&shdr, sizeof(Elf64_Shdr), 1, fp);
	
	// section 2: symbol table
	uint64_t numLocalSyms = 0;
	while (numLocalSyms < numSymbols && ELF_STINFO_B(symtab[numLocalSyms].st_info) == STB_LOCAL)
	{
		numLocalSyms++;
	};
	
	memset(&shdr, 0, sizeof(Elf64_Shdr));
	opt->make(nameSymtab, &shdr.sh_name, sizeof(shdr.sh_name));
	opt->make(SHT_SYMTAB, &shdr.sh_type, sizeof(shdr.sh_type));
	opt->make(posSymtab, &shdr.sh_offset, sizeof(shdr.sh_offset));
	opt->make(numSymbols * sizeof(Elf64_Sym), &shdr.sh_size, sizeof(shdr.sh_size));
	opt->make(SHN_STRTAB, &shdr.sh_link, sizeof(shdr.sh_link));
	opt->make(numLocalSyms, &shdr.sh_info, sizeof(shdr.sh_info));
	opt->make(1, &shdr.sh_addralign, sizeof(shdr.sh_addralign));
	opt->make(sizeof(Elf64_Sym), &shdr.sh_entsize, sizeof(shdr.sh_entsize));
	fwrite(&shdr, sizeof(Elf64_Shdr), 1, fp);
	
	// section 3: symbol string table
	memset(&shdr, 0, sizeof(Elf64_Shdr));
	opt->make(nameStrtab, &shdr.sh_name, sizeof(shdr.sh_name));
	opt->make(SHT_STRTAB, &shdr.sh_type, sizeof(shdr.sh_type));
	opt->make(posStrtab, &shdr.sh_offset, sizeof(shdr.sh_offset));
	opt->make(genstrtab.size, &shdr.sh_size, sizeof(shdr.sh_size));
	opt->make(1, &shdr.sh_addralign, sizeof(shdr.sh_addralign));
	fwrite(&shdr, sizeof(Elf64_Shdr), 1, fp);
	
	// all other section headers
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		Elf64_SectAux *aux = (Elf64_SectAux*) sect->aux;
		
		// first the section itself
		memset(&shdr, 0, sizeof(Elf64_Shdr));
		opt->make(aux->name, &shdr.sh_name, sizeof(shdr.sh_name));
		if (sect->type == SECTYPE_PROGBITS)
			opt->make(SHT_PROGBITS, &shdr.sh_type, sizeof(shdr.sh_type));
		else
			opt->make(SHT_NOBITS, &shdr.sh_type, sizeof(shdr.sh_type));
		
		if (strcmp(sect->name, ".dynstr") == 0)
		{
			// dynamic string table
			opt->make(SHT_STRTAB, &shdr.sh_type, sizeof(shdr.sh_type));
		}
		else if (strcmp(sect->name, ".dynamic") == 0)
		{
			// dynamic table
			Section *dynstr = objGetSection(obj, ".dynstr");
			Elf64_SectAux *dynstraux = (Elf64_SectAux*) dynstr->aux;
			
			opt->make(SHT_DYNAMIC, &shdr.sh_type, sizeof(shdr.sh_type));
			opt->make(dynstraux->index, &shdr.sh_link, sizeof(shdr.sh_link));
			opt->make(sizeof(Elf64_Dyn), &shdr.sh_entsize, sizeof(shdr.sh_entsize));
		}
		else if (strcmp(sect->name, ".plt.rela") == 0)
		{
			// PLT relocation table
			Section *plt = objGetSection(obj, ".plt");
			Elf64_SectAux *pltaux = (Elf64_SectAux*) plt->aux;
			
			Section *dynsym = objGetSection(obj, ".dynsym");
			Elf64_SectAux *dynsymaux = (Elf64_SectAux*) dynsym->aux;
			
			opt->make(SHT_RELA, &shdr.sh_type, sizeof(shdr.sh_type));
			opt->make(pltaux->index, &shdr.sh_info, sizeof(shdr.sh_info));
			opt->make(dynsymaux->index, &shdr.sh_link, sizeof(shdr.sh_link));
			opt->make(sizeof(Elf64_Rela), &shdr.sh_entsize, sizeof(shdr.sh_entsize));
		}
		else if (strcmp(sect->name, ".got.rela") == 0)
		{
			// main GOT relocation table
			Section *dynsym = objGetSection(obj, ".dynsym");
			Elf64_SectAux *dynsymaux = (Elf64_SectAux*) dynsym->aux;
			
			opt->make(SHT_RELA, &shdr.sh_type, sizeof(shdr.sh_type));
			opt->make(dynsymaux->index, &shdr.sh_link, sizeof(shdr.sh_link));
			opt->make(sizeof(Elf64_Rela), &shdr.sh_entsize, sizeof(shdr.sh_entsize));
		}
		else if (strcmp(sect->name, ".dynsym") == 0)
		{
			// dynamic symbol table
			Section *dynstr = objGetSection(obj, ".dynstr");
			Elf64_SectAux *dynstraux = (Elf64_SectAux*) dynstr->aux;
			
			opt->make(SHT_DYNSYM, &shdr.sh_type, sizeof(shdr.sh_type));
			opt->make(dynstraux->index, &shdr.sh_link, sizeof(shdr.sh_link));
			opt->make(sizeof(Elf64_Sym), &shdr.sh_entsize, sizeof(shdr.sh_entsize));
			opt->make(1, &shdr.sh_info, sizeof(shdr.sh_info));
		};
			
		uint64_t flags = SHF_ALLOC;
		if (sect->flags & SEC_WRITE) flags |= SHF_WRITE;
		if (sect->flags & SEC_EXEC) flags |= SHF_EXECINSTR;
		opt->make(flags, &shdr.sh_flags, sizeof(shdr.sh_flags));
		
		opt->make(sect->addr, &shdr.sh_addr, sizeof(shdr.sh_addr));
		opt->make(aux->offset, &shdr.sh_offset, sizeof(shdr.sh_offset));
		opt->make(sect->size, &shdr.sh_size, sizeof(shdr.sh_size));
		opt->make(sect->align, &shdr.sh_addralign, sizeof(shdr.sh_addralign));
		fwrite(&shdr, sizeof(Elf64_Shdr), 1, fp);
		
		// now the relocation table section (contents were already written to file),
		// IF WE NEED IT
		if (sect->relocs != NULL)
		{
			memset(&shdr, 0, sizeof(Elf64_Shdr));
			opt->make(aux->relaName, &shdr.sh_name, sizeof(shdr.sh_name));
			opt->make(SHT_RELA, &shdr.sh_type, sizeof(shdr.sh_type));
			opt->make(aux->relaOffset, &shdr.sh_offset, sizeof(shdr.sh_offset));
			opt->make(aux->relaSize, &shdr.sh_size, sizeof(shdr.sh_size));
			opt->make(SHN_SYMTAB, &shdr.sh_link, sizeof(shdr.sh_link));
			opt->make(aux->index, &shdr.sh_info, sizeof(shdr.sh_info));
			opt->make(1, &shdr.sh_addralign, sizeof(shdr.sh_addralign));
			opt->make(sizeof(Elf64_Rela), &shdr.sh_entsize, sizeof(shdr.sh_entsize));
			fwrite(&shdr, sizeof(Elf64_Shdr), 1, fp);
		};
	};
	
	// symbol table
	fwrite(symtab, sizeof(Elf64_Sym), numSymbols, fp);
	
	// section name string table
	fwrite(shstrtab.data, 1, shstrtab.size, fp);
	
	// normal string table
	fwrite(genstrtab.data, 1, genstrtab.size, fp);
	
	// everything else already written, thanks
	return ERR_OK;
};
