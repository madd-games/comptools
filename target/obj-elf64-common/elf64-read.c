/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/elf64.h>

/**
 * Section data extracted from file.
 */
typedef struct
{
	/**
	 * For PROGBITS and NOBITS sections, the section itself.
	 */
	Section *sect;
	
	/**
	 * For SYMTAB sections, an array of symbols.
	 */
	Symbol **syms;
	
	/**
	 * For STRTAB sections, the string table.
	 */
	char *strtab;
} SectData;

Error elf64_read(Object *obj, FILE *fp)
{
	Elf64_DrvOptions *opt = (Elf64_DrvOptions*) obj->drvdata;
	
	// first read the ELF64 header
	Elf64_Ehdr ehdr;
	if (fread(&ehdr, sizeof(Elf64_Ehdr), 1, fp) != 1)
	{
		fprintf(stderr, "elf64: failed to read ELF64 header\n");
		return ERR_IO;
	};
	
	if (memcmp(ehdr.e_ident, "\x7F" "ELF", 4) != 0)
	{
		fprintf(stderr, "elf64: not an ELF64 file: header magic invalid\n");
		return ERR_INVALID_PARAM;
	};
	
	if (ehdr.e_ident[EI_CLASS] != ELFCLASS64)
	{
		fprintf(stderr, "elf64: not an ELF64 file: wrong class\n");
		return ERR_INVALID_PARAM;
	};
	
	unsigned char expectedData = ELFDATA2LSB;
	if (opt->make == ctMakeBE) expectedData = ELFDATA2MSB;
	
	if (ehdr.e_ident[EI_DATA] != expectedData)
	{
		fprintf(stderr, "elf64: unexpected data encoding\n");
		return ERR_INVALID_PARAM;
	};
	
	if (ehdr.e_ident[EI_VERSION] != 1)
	{
		fprintf(stderr, "elf64: unexpected file format version\n");
		return ERR_INVALID_PARAM;
	};
	
	if (ehdr.e_ident[EI_OSABI] != ELFOSABI_SYSV && ehdr.e_ident[EI_OSABI] != ELFOSABI_LINUX)
	{
		fprintf(stderr, "elf64: unexpected ABI (0x%02hhX)\n", ehdr.e_ident[EI_OSABI]);
		return ERR_INVALID_PARAM;
	};
	
	if (ehdr.e_ident[EI_ABIVERSION] != 0)
	{
		fprintf(stderr, "elf64: unexpected ABI version\n");
		return ERR_INVALID_PARAM;
	};
	
	uint64_t type = opt->get(&ehdr.e_type, sizeof(ehdr.e_type));
	switch (type)
	{
	case ET_REL:
		obj->type = OBJTYPE_RELOC;
		break;
	case ET_EXEC:
		obj->type = OBJTYPE_EXEC;
		break;
	case ET_DYN:
		obj->type = OBJTYPE_SHARED;
		break;
	default:
		fprintf(stderr, "elf64: unknown object type %d\n", (int) type);
		return ERR_INVALID_PARAM;
	};
	
	uint16_t machine = (uint16_t) opt->get(&ehdr.e_machine, sizeof(ehdr.e_machine));
	if (machine != opt->machine)
	{
		fprintf(stderr, "elf64: wrong machine type (expected %d, have %d)\n",
			(int) opt->machine, (int) machine);
		return ERR_INVALID_PARAM;
	};
	
	if (opt->get(&ehdr.e_version, sizeof(ehdr.e_version)) != 1)
	{
		fprintf(stderr, "elf64: invalid header version\n");
		return ERR_INVALID_PARAM;
	};
	
	if (opt->get(&ehdr.e_ehsize, sizeof(ehdr.e_ehsize)) != sizeof(Elf64_Ehdr))
	{
		fprintf(stderr, "elf64: wrong ELF64 header size\n");
		return ERR_INVALID_PARAM;
	};
	
	if (opt->get(&ehdr.e_shentsize, sizeof(ehdr.e_shentsize)) != sizeof(Elf64_Shdr))
	{
		fprintf(stderr, "elf64: wrong section header size\n");
		return ERR_INVALID_PARAM;
	};
	
	uint64_t shoff = opt->get(&ehdr.e_shoff, sizeof(ehdr.e_shoff));
	uint64_t shstrndx = opt->get(&ehdr.e_shstrndx, sizeof(ehdr.e_shstrndx));
	uint64_t shnum = opt->get(&ehdr.e_shnum, sizeof(ehdr.e_shnum));
	if (shnum < 2)
	{
		// no sections in this file
		return ERR_OK;
	};
	
	// read information about sections into memory
	SectData *secdata = (SectData*) malloc(sizeof(SectData) * shnum);
	memset(secdata, 0, sizeof(SectData));
	
	// first pass: load string tables
	int i;
	for (i=0; i<shnum; i++)
	{
		SectData *put = &secdata[i];
		fseek(fp, shoff + sizeof(Elf64_Shdr) * i, SEEK_SET);
		
		Elf64_Shdr shdr;
		if (fread(&shdr, sizeof(Elf64_Shdr), 1, fp) != 1)
		{
			fprintf(stderr, "elf64: failed to load section headers\n");
			free(secdata);
			return ERR_IO;
		};
		
		int type = (int) opt->get(&shdr.sh_type, sizeof(shdr.sh_type));
		if (type == SHT_STRTAB)
		{
			uint64_t size = opt->get(&shdr.sh_size, sizeof(shdr.sh_size));
			uint64_t offset = opt->get(&shdr.sh_offset, sizeof(shdr.sh_offset));
			char *strtab = (char*) malloc(size);
			
			fseek(fp, offset, SEEK_SET);
			if (fread(strtab, 1, size, fp) != size)
			{
				fprintf(stderr, "elf64: failed to read string table %d\n", i);
				free(secdata);
				return ERR_IO;
			};
			
			put->strtab = strtab;
		};
	};
	
	// second pass: load PROGBITS and NOBITS sections
	for (i=0; i<shnum; i++)
	{
		SectData *put = &secdata[i];
		fseek(fp, shoff + sizeof(Elf64_Shdr) * i, SEEK_SET);
		
		Elf64_Shdr shdr;
		if (fread(&shdr, sizeof(Elf64_Shdr), 1, fp) != 1)
		{
			fprintf(stderr, "elf64: failed to load section headers\n");
			free(secdata);
			return ERR_IO;
		};

		uint64_t elfFlags = opt->get(&shdr.sh_flags, sizeof(shdr.sh_flags));
		if ((elfFlags & SHF_ALLOC) == 0)
		{
			// section not allocated, ignore it
			continue;
		};

		int type = (int) opt->get(&shdr.sh_type, sizeof(shdr.sh_type));
		if (type == SHT_PROGBITS || type == SHT_NOBITS || type == SHT_DYNAMIC || type == SHT_STRTAB || type == SHT_DYNSYM)
		{
			uint64_t size = opt->get(&shdr.sh_size, sizeof(shdr.sh_size));
			uint64_t offset = opt->get(&shdr.sh_offset, sizeof(shdr.sh_offset));
			void *data = NULL;
			
			if (type != SHT_NOBITS)
			{
				data = malloc(size);
				
				fseek(fp, offset, SEEK_SET);
				if (fread(data, 1, size, fp) != size)
				{
					fprintf(stderr, "elf64: failed to read section %d\n", i);
					free(secdata);
					free(data);
					return ERR_IO;
				};
			};
			
			uint64_t nameoff = opt->get(&shdr.sh_name, sizeof(shdr.sh_name));
			const char *name = &secdata[shstrndx].strtab[nameoff];
			
			int comptype;
			if (type == SHT_NOBITS) comptype = SECTYPE_NOBITS;
			else comptype = SECTYPE_PROGBITS;
			
			int compflags = SEC_READ;
			if (elfFlags & SHF_WRITE) compflags |= SEC_WRITE;
			if (elfFlags & SHF_EXECINSTR) compflags |= SEC_EXEC;
			
			Section *sect = objCreateSection(obj, name, comptype, compflags);
			put->sect = sect;
			sect->data = data;
			sect->size = size;
			sect->align = opt->get(&shdr.sh_addralign, sizeof(shdr.sh_addralign));
			sect->addr = opt->get(&shdr.sh_addr, sizeof(shdr.sh_addr));
		};
	};
	
	// delete the section symbols created by the above passes
	// TODO: actually free()ing them would be a good idea
	obj->syms = NULL;
	
	// third pass: load symbol tables
	for (i=0; i<shnum; i++)
	{
		SectData *put = &secdata[i];
		fseek(fp, shoff + sizeof(Elf64_Shdr) * i, SEEK_SET);
		
		Elf64_Shdr shdr;
		if (fread(&shdr, sizeof(Elf64_Shdr), 1, fp) != 1)
		{
			fprintf(stderr, "elf64: failed to load section headers\n");
			free(secdata);
			return ERR_IO;
		};
		
		int type = (int) opt->get(&shdr.sh_type, sizeof(shdr.sh_type));
		if (type == SHT_SYMTAB || type == SHT_DYNSYM)
		{
			uint64_t size = opt->get(&shdr.sh_size, sizeof(shdr.sh_size));
			uint64_t offset = opt->get(&shdr.sh_offset, sizeof(shdr.sh_offset));
			Elf64_Sym *symtab = (Elf64_Sym*) malloc(size);
			
			fseek(fp, offset, SEEK_SET);
			if (fread(symtab, 1, size, fp) != size)
			{
				fprintf(stderr, "elf64: failed to read symbol table %d\n", i);
				free(secdata);
				return ERR_IO;
			};
			
			size_t numSyms = size / sizeof(Elf64_Sym);
			int idxStrtab = (int) opt->get(&shdr.sh_link, sizeof(shdr.sh_link));
			const char *strtab = secdata[idxStrtab].strtab;
			Symbol **symlist = (Symbol**) malloc(sizeof(void*) * numSyms);
			put->syms = symlist;
			
			// create a Symbol structure for every symbol
			int j;
			for (j=0; j<numSyms; j++)
			{
				Elf64_Sym *elfsym = &symtab[j];
				
				Symbol *sym = (Symbol*) malloc(sizeof(Symbol));
				memset(sym, 0, sizeof(Symbol));
				
				const char *symname = &strtab[opt->get(&elfsym->st_name, sizeof(elfsym->st_name))];
				sym->name = strdup(symname);
				
				if (symname[0] == 0)
				{
					// empty name: skip
					continue;
				};
				
				unsigned char b = ELF_STINFO_B(elfsym->st_info);
				unsigned char t = ELF_STINFO_T(elfsym->st_info);
				
				switch (b)
				{
				case STB_GLOBAL:
					sym->binding = SYMB_GLOBAL;
					break;
				case STB_WEAK:
					sym->binding = SYMB_WEAK;
					break;
				default:
					sym->binding = SYMB_LOCAL;
					break;
				};
				
				int okType = 1;
				switch (t)
				{
				case STT_NOTYPE:
					sym->type = SYMT_NONE;
					break;
				case STT_OBJECT:
					sym->type = SYMT_OBJECT;
					break;
				case STT_FUNC:
					sym->type = SYMT_FUNC;
					break;
				case STT_SECTION:
					sym->type = SYMT_SECTION;
					break;
				default:
					okType = 0;
					break;
				};
				
				// skip over unknown types
				if (!okType) continue;
				
				int sectIndex = (int) opt->get(&elfsym->st_shndx, sizeof(elfsym->st_shndx));
				switch (sectIndex)
				{
				case SHN_UNDEF:
					sym->type = SYMT_UNDEF;
					break;
				case SHN_ABS:
					sym->sect = NULL;
					sym->value = opt->get(&elfsym->st_value, sizeof(elfsym->st_value));
					break;
				case SHN_COMMON:
					sym->type = SYMT_COMMON;
					sym->align = opt->get(&elfsym->st_value, sizeof(elfsym->st_value));
					break;
				default:
					sym->sect = secdata[sectIndex].sect;
					sym->value = opt->get(&elfsym->st_value, sizeof(elfsym->st_value));
					break;
				};
				
				sym->next = obj->syms;
				obj->syms = sym;
			
				sym->size = opt->get(&elfsym->st_size, sizeof(elfsym->st_size));
				symlist[j] = sym;
			};
		};
	};
	
	// final pass: read relocation tables
	for (i=0; i<shnum; i++)
	{
		fseek(fp, shoff + sizeof(Elf64_Shdr) * i, SEEK_SET);
		
		Elf64_Shdr shdr;
		if (fread(&shdr, sizeof(Elf64_Shdr), 1, fp) != 1)
		{
			fprintf(stderr, "elf64: failed to load section headers\n");
			free(secdata);
			return ERR_IO;
		};
		
		int type = (int) opt->get(&shdr.sh_type, sizeof(shdr.sh_type));
		if (type == SHT_RELA)
		{
			uint64_t size = opt->get(&shdr.sh_size, sizeof(shdr.sh_size));
			uint64_t offset = opt->get(&shdr.sh_offset, sizeof(shdr.sh_offset));
			Elf64_Rela *relatab = (Elf64_Rela*) malloc(size);
			
			fseek(fp, offset, SEEK_SET);
			if (fread(relatab, 1, size, fp) != size)
			{
				fprintf(stderr, "elf64: failed to read string table %d\n", i);
				free(secdata);
				return ERR_IO;
			};
			
			size_t numRela = size / sizeof(Elf64_Rela);
			int sectSymtab = (int) opt->get(&shdr.sh_link, sizeof(shdr.sh_link));
			int sectTarget = (int) opt->get(&shdr.sh_info, sizeof(shdr.sh_info));
			
			Symbol **syms = secdata[sectSymtab].syms;
			Section *sect = secdata[sectTarget].sect;
			
			if (sect != NULL)
			{
				// translate the relocations
				int j;
				for (j=0; j<numRela; j++)
				{
					Elf64_Rela *rela = &relatab[j];
					uint64_t info = opt->get(&rela->r_info, sizeof(rela->r_info));
					
					int elftype = (int) ELF64_R_TYPE(info);
					int symidx = (int) ELF64_R_SYM(info);
					
					RelocType *type = opt->relocElfToComp(elftype);
					if (type == NULL)
					{
						fprintf(stderr, "elf64: unrecognised relocation type %d\n", elftype);
						return ERR_INVALID_PARAM;
					};
					
					Reloc *reloc = (Reloc*) malloc(sizeof(Reloc));
					memset(reloc, 0, sizeof(Reloc));
					
					reloc->type = type;
					reloc->sect = sect;
					reloc->offset = opt->get(&rela->r_offset, sizeof(rela->r_offset));
					reloc->sym = syms[symidx];
					reloc->addend = (int64_t) opt->get(&rela->r_addend, sizeof(rela->r_addend));
					
					reloc->next = sect->relocs;
					sect->relocs = reloc;
				};
			};
		};
	};
	
	// figure out the soname and stuff (if needed)
	Section *sectDynstr = objGetSection(obj, ".dynstr");
	if (sectDynstr != NULL)
	{
		const char *strtab = (const char*) sectDynstr->data;
		
		Section *sectDynamic = objGetSection(obj, ".dynamic");
		if (sectDynamic != NULL)
		{
			Elf64_Dyn *dyn;
			for (dyn=(Elf64_Dyn*) sectDynamic->data; dyn->d_tag.value!=0; dyn++)
			{
				if (opt->get(&dyn->d_tag, sizeof(dyn->d_tag)) == DT_SONAME)
				{
					uint64_t offset = opt->get(&dyn->d_un, sizeof(dyn->d_un));
					obj->soname = strdup(&strtab[offset]);
				};
			};
		};
	};
	
	return ERR_OK;
};
