/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_ELF64_H_
#define	COMPTOOLS_ELF64_H_

#include <comptools/types.h>
#include <comptools/obj.h>

#define	EI_MAG0			0
#define	EI_MAG1			1
#define	EI_MAG2			2
#define	EI_MAG3			3
#define	EI_CLASS		4
#define	EI_DATA			5
#define	EI_VERSION		6
#define	EI_OSABI		7
#define	EI_ABIVERSION		8
#define	EI_NIDENT		16

#define	ELFCLASS64		2

#define	ELFDATA2LSB		1
#define	ELFDATA2MSB		2

#define	ELFOSABI_SYSV		0
#define	ELFOSABI_HPUX		1
#define	ELFOSABI_NETBSD		2
#define	ELFOSABI_LINUX		3
#define	ELFOSABI_STANDALONE	255

#define	EV_CURRENT		1

#define	ET_NONE			0
#define	ET_REL			1
#define	ET_EXEC			2
#define	ET_DYN			3
#define	ET_CORE			4

#define	SHN_UNDEF		0
#define	SHN_ABS			0xFFF1
#define	SHN_COMMON		0xFFF2

#define	SHT_NULL		0
#define	SHT_PROGBITS		1
#define	SHT_SYMTAB		2
#define	SHT_STRTAB		3
#define	SHT_RELA		4
#define	SHT_HASH		5
#define	SHT_DYNAMIC		6
#define	SHT_NOTE		7
#define	SHT_NOBITS		8
#define	SHT_REL			9
#define	SHT_SHLIB		10
#define	SHT_DYNSYM		11

#define	SHF_WRITE		0x1
#define	SHF_ALLOC		0x2
#define	SHF_EXECINSTR		0x4

#define	STN_UNDEF		0

#define	STB_LOCAL		0
#define	STB_GLOBAL		1
#define	STB_WEAK		2

#define	STT_NOTYPE		0
#define	STT_OBJECT		1
#define	STT_FUNC		2
#define	STT_SECTION		3
#define	STT_FILE		4

#define	ELF_MAKE_STINFO(b, t)	(((b) << 4) | (t))
#define	ELF_STINFO_B(i)		(((i) >> 4) & 0xF)
#define	ELF_STINFO_T(i)		((i) & 0xF)

#define ELF64_R_SYM(i)		((i) >> 32)
#define ELF64_R_TYPE(i)		((i) & 0xffffffffL)
#define ELF64_R_INFO(s, t)	(((s) << 32) + ((t) & 0xffffffffL))

#define	PT_NULL			0
#define	PT_LOAD			1
#define	PT_DYNAMIC		2
#define	PT_INTERP		3
#define	PT_NOTE			4
#define	PT_SHLIB		5
#define	PT_PHDR			6

#define	PF_X			0x1
#define	PF_W			0x2
#define	PF_R			0x4

#define	DT_NULL			0
#define	DT_NEEDED		1
#define	DT_PLTRELSZ		2
#define	DT_PLTGOT		3
#define	DT_HASH			4
#define	DT_STRTAB		5
#define	DT_SYMTAB		6
#define	DT_RELA			7
#define	DT_RELASZ		8
#define	DT_RELAENT		9
#define	DT_STRSZ		10
#define	DT_SYMENT		11
#define	DT_INIT			12
#define	DT_FINI			13
#define	DT_SONAME		14
#define	DT_RPATH		15
#define	DT_SYMBOLIC		16
#define	DT_REL			17
#define	DT_RELSZ		18
#define	DT_RELENT		19
#define	DT_PLTREL		20
#define	DT_DEBUG		21
#define	DT_TEXTREL		22
#define	DT_JMPREL		23
#define	DT_BIND_NOW		24
#define	DT_INIT_ARRAY		25
#define	DT_FINI_ARRAY		26
#define	DT_INIT_ARRAYSZ		27
#define	DT_FINI_ARRAYSZ		28

/**
 * ELF64-specific section flag (for libcomptools sections) used to mark a section as
 * being related to dynamic linking (GOT, PLT, etc), needed to properly create the
 * relevant program headers.
 */
#define	SEC_ELF_DYN		(1 << 16)

/**
 * Fundamental ELF64 types. Define them using CT_NEWTYPE() so that they are distinct
 * from the host representation, and so that we can switch them between big and little
 * endian depending on the target.
 */
typedef	CT_NEWTYPE(uint64_t)	Elf64_U64;
typedef	Elf64_U64		Elf64_Addr;
typedef	Elf64_U64		Elf64_Off;
typedef CT_NEWTYPE(uint16_t)	Elf64_U16;
typedef	Elf64_U16		Elf64_Half;
typedef	CT_NEWTYPE(uint32_t)	Elf64_U32;
typedef	Elf64_U32		Elf64_Word;
typedef	CT_NEWTYPE(int32_t)	Elf64_S32;
typedef	Elf64_S32		Elf64_Sword;
typedef	Elf64_U64		Elf64_Xword;
typedef	CT_NEWTYPE(int64_t)	Elf64_S64;
typedef	Elf64_S64		Elf64_Sxword;

/**
 * Options for the ELF64 driver.
 */
typedef struct
{
	/**
	 * Size of a page on the target architecture.
	 */
	uint64_t pageSize;
	
	/**
	 * Machine (e_machine) value to use.
	 */
	uint16_t machine;
	
	/**
	 * Points to a function which translates the specified RelocType into an ELF64
	 * relocation enum. Return -1 if no match found.
	 */
	int (*relocCompToElf)(RelocType *type, const char *prop);
	
	/**
	 * Points to a function which translates the specified ELF64 relocation enum to
	 * a RelocType. Return NULL if no match found.
	 */
	RelocType* (*relocElfToComp)(int type);
	
	/**
	 * Make/get pointers for values in headers. These must be set to ct{Make/Get}[LE/BE]()
	 * depending on whether we're using big endian or little endian byte order in
	 * the ELF64 file.
	 */
	void (*make)(uint64_t value, void *buffer, int count);
	uint64_t (*get)(const void *buffer, int count);
} Elf64_DrvOptions;

typedef struct
{
	unsigned char		e_ident[EI_NIDENT];
	Elf64_Half		e_type;
	Elf64_Half		e_machine;
	Elf64_Word		e_version;
	Elf64_Addr		e_entry;
	Elf64_Off		e_phoff;
	Elf64_Off		e_shoff;
	Elf64_Word		e_flags;
	Elf64_Half		e_ehsize;
	Elf64_Half		e_phentsize;
	Elf64_Half		e_phnum;
	Elf64_Half		e_shentsize;
	Elf64_Half		e_shnum;
	Elf64_Half		e_shstrndx;
} Elf64_Ehdr;

typedef struct
{
	Elf64_Word		sh_name;
	Elf64_Word		sh_type;
	Elf64_Xword		sh_flags;
	Elf64_Addr		sh_addr;
	Elf64_Off		sh_offset;
	Elf64_Xword		sh_size;
	Elf64_Word		sh_link;
	Elf64_Word		sh_info;
	Elf64_Xword		sh_addralign;
	Elf64_Xword		sh_entsize;
} Elf64_Shdr;

typedef struct
{
	Elf64_Word		st_name;
	unsigned char		st_info;
	unsigned char		st_other;
	Elf64_Half		st_shndx;
	Elf64_Addr		st_value;
	Elf64_Xword		st_size;
} Elf64_Sym;

typedef struct
{
	Elf64_Addr		r_offset;
	Elf64_Xword		r_info;
} Elf64_Rel;

typedef struct
{
	Elf64_Addr		r_offset;
	Elf64_Xword		r_info;
	Elf64_Sxword		r_addend;
} Elf64_Rela;

typedef struct
{
	Elf64_Word		p_type;
	Elf64_Word		p_flags;
	Elf64_Off		p_offset;
	Elf64_Addr		p_vaddr;
	Elf64_Addr		p_paddr;
	Elf64_Xword		p_filesz;
	Elf64_Xword		p_memsz;
	Elf64_Xword		p_align;
} Elf64_Phdr;

typedef struct
{
	Elf64_Xword d_tag;
	union
	{
		Elf64_Xword	d_val;
		Elf64_Addr	d_ptr;
	} d_un;
} Elf64_Dyn;

/**
 * Auxilliary section data.
 */
typedef struct
{
	/**
	 * Section number allocated in the section header table.
	 */
	int			index;
	
	/**
	 * Allocated offset in the file.
	 */
	uint64_t		offset;
	
	/**
	 * Name of the section, as an offset into the shstrtab.
	 */
	uint64_t		name;
	
	/**
	 * Offset into file to the relocation table.
	 */
	uint64_t		relaOffset;
	uint64_t		relaSize;
	uint64_t		relaName;
} Elf64_SectAux;

/**
 * Symbol auxilliary data.
 */
typedef struct
{
	/**
	 * Index in the symbol table.
	 */
	uint64_t		index;

	/**
	 * Base address of the GOT.
	 */
	uint64_t		got;
	
	/**
	 * Offset into the GOT of this particular symbol.
	 */
	uint64_t		gotoff;
	
	/**
	 * Absolute address of the PLT entry for this symbol. 0 means that there
	 * isn't one (only function symbols have PLT entries).
	 */
	uint64_t		plt;
	
	/**
	 * Offset into the GOTPLT if this is a function; used while building the PLT
	 */
	uint64_t		gotpltoff;
	
	/**
	 * Index into the dynamic symbol table.
	 */
	int			dynsymidx;
	
	/**
	 * GOTPLT relocation index if applicable.
	 */
	int			pltidx;
} Elf64_SymAux;

/**
 * ELF64 implementation of driver init().
 */
void elf64_init(Object *obj, const void *opt);

/**
 * ELF64 implementation of driver write().
 */
Error elf64_write(Object *obj, FILE *fp);

/**
 * ELF64 implementation of driver read().
 */
Error elf64_read(Object *obj, FILE *fp);

/**
 * Calculate a hash value for a symbol name.
 */
unsigned long elf64_hash(const unsigned char *name);

#endif
