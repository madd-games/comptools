/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_GXO_COMMON_H_
#define COMPTOOLS_GXO_COMMON_H_

#include <comptools/types.h>
#include <comptools/obj.h>
#include <comptools/gxo.h>
#include <comptools/hashtab.h>

/**
 * Indirection entry.
 */
typedef struct
{
	uint64_t addr;
	uint64_t symaddr;
} GXO_Indir;

/**
 * GXO driver-specific data.
 */
typedef struct
{
	/**
	 * Name of the architecture.
	 */
	uint64_t arch;
	
	/**
	 * Name of the OS/ABI.
	 */
	uint64_t system;
	
	/**
	 * Make/get functions corresponding to the byte order.
	 */
	void (*make)(uint64_t value, void *buffer, int count);
	uint64_t (*get)(const void *buffer, int count);
	
	/**
	 * Page size (defaults to 4KB).
	 */
	uint64_t pageSize;
	
	/**
	 * Get arch-specific flags.
	 */
	uint64_t (*archflags)();
	
	/**
	 * Get system-specific flags.
	 */
	uint64_t (*sysflags)();
	
	/**
	 * Set relType and relParam based on the specified relocation. Return ERR_OK on success,
	 * error number on error.
	 */
	Error (*compToGxoRel)(Reloc *reloc, GXO_Reloc *outrel);
	
	/**
	 * Convert (relType, relParam) to a CompTools relocation type. Returns NULL if unrecognised.
	 */
	RelocType* (*gxoToCompRel)(uint64_t relType);
	
	/**
	 * Hash table mapping symbol names to their indirection entires (GXO_IndirEntry).
	 */
	HashTable *indirs;
} GXO_DrvData;

/**
 * Information about a section being processed.
 */
typedef struct
{
	union
	{
		uint64_t namenum;
		char name[9];
	};
	
	uint64_t vaddr;
	uint64_t offset;
	uint64_t size;
	uint64_t flags;
	Section *sect;
} GXO_SectionInfo;

/**
 * Symbol auxiliary data.
 */
typedef struct
{
	/**
	 * Address of the symbol table entry.
	 */
	uint64_t addr;
} GXO_SymAux;

/**
 * Initialize the GXO driver on the specified object.
 */
GXO_DrvData* gxoDrvInit(Object *obj, const char *arch, const char *system);

/**
 * Write a GXO file.
 */
Error gxoWrite(Object *obj, FILE *fp);

/**
 * Read a GXO file.
 */
Error gxoRead(Object *obj, FILE *fp);

/**
 * Prepare for relocations.
 */
Error gxoPreReloc(Object *obj);

/**
 * Convert a GXO generic relocation to CompTools.
 */
RelocType* gxoToCompRelGen(uint64_t reltype);

#endif
