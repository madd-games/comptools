/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <comptools/gxo.h>
#include <comptools/gxo-common.h>
#include <comptools/hashtab.h>

static uint64_t str2int(const char *str)
{
	union
	{
		uint64_t val;
		char data[8];
	} conv;
	
	conv.val = 0;
	memcpy(conv.data, str, strlen(str) > 8 ? 8 : strlen(str));
	
	return conv.val;
};

static Error resolve_genrel(void *ptr, uint64_t symval, int64_t addend, Reloc *reloc)
{
	return ERR_RELOC_TRUNC;
};

static RelocType rt_gen_s8 = {.resolve = resolve_genrel, .name = "RT_GEN_S8", .bytes = 1};
static RelocType rt_gen_u8 = {.resolve = resolve_genrel, .name = "RT_GEN_U8", .bytes = 1};
static RelocType rt_gen_s16 = {.resolve = resolve_genrel, .name = "RT_GEN_S16", .bytes = 2};
static RelocType rt_gen_u16 = {.resolve = resolve_genrel, .name = "RT_GEN_U16", .bytes = 2};
static RelocType rt_gen_s32 = {.resolve = resolve_genrel, .name = "RT_GEN_S32", .bytes = 4};
static RelocType rt_gen_u32 = {.resolve = resolve_genrel, .name = "RT_GEN_U32", .bytes = 4};
static RelocType rt_gen_s64 = {.resolve = resolve_genrel, .name = "RT_GEN_S64", .bytes = 8};
static RelocType rt_gen_u64 = {.resolve = resolve_genrel, .name = "RT_GEN_U64", .bytes = 8};

GXO_DrvData* gxoDrvInit(Object *obj, const char *arch, const char *system)
{
	GXO_DrvData *data = (GXO_DrvData*) calloc(1, sizeof(GXO_DrvData));
	obj->drvdata = data;

	data->arch = str2int(arch);
	data->system = str2int(system);
	data->pageSize = 0x1000;
	
	return data;
};

Error gxoWrite(Object *obj, FILE *fp)
{
	GXO_DrvData *data = (GXO_DrvData*) obj->drvdata;
	
	// first write the header
	GXO_Header ghead;
	memset(&ghead, 0, sizeof(GXO_Header));
	
	uint64_t flags;
	switch (obj->type)
	{
	case OBJTYPE_EXEC:
		flags = GXO_GEN_EXEC | GXO_GEN_PIC | GXO_GEN_DYNLD | GXO_GEN_TLS;
		break;
	case OBJTYPE_RELOC:
		flags = GXO_GEN_REL | GXO_GEN_TLS;
		break;
	case OBJTYPE_SHARED:
		flags = GXO_GEN_DYNLD | GXO_GEN_PIC | GXO_GEN_TLS;
		break;
	default:
		return ERR_INVALID_PARAM;
	};
	
	// if the '.symtab' section does not yet exist, set up the symbol table
	Section *sect = objGetSection(obj, ".symtab");
	if (sect == NULL)
	{
		// first create the '.symstr' section and append symbol names to it
		if (objGetSection(obj, ".symstr") != NULL)
		{
			return ERR_INVALID_PARAM;
		};
		
		sect = objCreateSection(obj, ".symstr", SECTYPE_PROGBITS, 0);
		if (sect == NULL) return ERR_INVALID_PARAM;
		
		uint64_t numSymbols = 0;
		Symbol *sym;
		for (sym=obj->syms; sym!=NULL; sym=sym->next)
		{
			numSymbols++;
		};
		
		// form the symbol table in memory
		GXO_Symbol *symtab = (GXO_Symbol*) calloc(numSymbols+1, sizeof(GXO_Symbol));
		GXO_Symbol *outsym = symtab;

		uint64_t addr = 0;
		for (sym=obj->syms; sym!=NULL; sym=sym->next)
		{
			uint64_t nameAddr = sect->addr + sect->size;
			objSectionAppend(sect, sym->name, strlen(sym->name)+1);
			
			data->make(nameAddr, &outsym->symName, sizeof(outsym->symName));
			
			uint64_t value = sym->value;
			if (sym->sect != NULL) value += sym->sect->addr;
			
			data->make(value, &outsym->symValue, sizeof(outsym->symValue));
			data->make(sym->size, &outsym->symSize, sizeof(outsym->symSize));
			
			switch (sym->binding)
			{
			case SYMB_GLOBAL:
				data->make(SYM_SCOPE_GLOBAL, &outsym->symScope, sizeof(outsym->symScope));
				break;
			case SYMB_LOCAL:
				data->make(SYM_SCOPE_LOCAL, &outsym->symScope, sizeof(outsym->symScope));
				break;
			case SYMB_WEAK:
				data->make(SYM_SCOPE_WEAK, &outsym->symScope, sizeof(outsym->symScope));
				break;
			};
			
			switch (sym->type)
			{
			case SYMT_UNDEF:
			case SYMT_COMMON:
				data->make(SYM_TYPE_UNDEF, &outsym->symType, sizeof(outsym->symType));
				break;
			case SYMT_FUNC:
				data->make(SYM_TYPE_FUNC, &outsym->symType, sizeof(outsym->symType));
				break;
			case SYMT_NONE:
			case SYMT_SECTION:
			default:
				data->make(SYM_TYPE_LABEL, &outsym->symType, sizeof(outsym->symType));
				break;
			case SYMT_OBJECT:
				data->make(SYM_TYPE_OBJECT, &outsym->symType, sizeof(outsym->symType));
				break;
			};
			
			if (sym->align == 0) sym->align = 1;
			uint8_t alignLog2;
			
			for (alignLog2=0; alignLog2!=255; alignLog2++)
			{
				if ((1UL << alignLog2) == sym->align) break;
			};
			
			data->make(alignLog2, &outsym->symAlignLog2, sizeof(outsym->symAlignLog2));
			
			GXO_SymAux *aux = (GXO_SymAux*) calloc(1, sizeof(GXO_SymAux));
			sym->aux = aux;
			aux->addr = addr;
			
			outsym++;
			addr += sizeof(GXO_Symbol);
		};
		
		// now write the symbol table
		sect = objCreateSection(obj, ".symtab", SECTYPE_PROGBITS, 0);
		if (sect == NULL) return ERR_INVALID_PARAM;
		
		objSectionAppend(sect, symtab, (numSymbols+1) * sizeof(GXO_Symbol));
		
		// re-scan the symbol table and specify addresses
		for (sym=obj->syms; sym!=NULL; sym=sym->next)
		{
			GXO_SymAux *aux = sym->aux;
			if (aux == NULL) continue;
			aux->addr += sect->addr;
		};
	};
	
	// if the '.reloc' section does not exist, then likewise create it
	sect = objGetSection(obj, ".reloc");
	if (sect == NULL)
	{
		Section *reltab = objCreateSection(obj, ".reloc", SECTYPE_PROGBITS, 0);
		if (reltab == NULL) return ERR_INVALID_PARAM;
		
		GXO_Reloc outrel;
		for (sect=obj->sects; sect!=NULL; sect=sect->next)
		{
			Reloc *rel;
			for (rel=sect->relocs; rel!=NULL; rel=rel->next)
			{
				memset(&outrel, 0, sizeof(GXO_Reloc));
				data->make(sect->addr + rel->offset, &outrel.relPosition, sizeof(outrel.relPosition));
				
				GXO_SymAux *symaux = (GXO_SymAux*) rel->sym->aux;
				data->make(symaux->addr, &outrel.relSymbol, sizeof(outrel.relSymbol));
				if (data->compToGxoRel(rel, &outrel) != ERR_OK) return ERR_INVALID_PARAM;
				
				objSectionAppend(reltab, &outrel, sizeof(GXO_Reloc));
			};
		};
		
		// list terminator
		memset(&outrel, 0, sizeof(GXO_Reloc));
		objSectionAppend(reltab, &outrel, sizeof(GXO_Reloc));
	};
	
	// if the '.annot' section odes not exist, then also create it
	sect = objGetSection(obj, ".annot");
	if (sect == NULL)
	{
		Section *antab = objCreateSection(obj, ".annot", SECTYPE_PROGBITS, 0);
		if (antab == NULL) return ERR_INVALID_PARAM;
		
		// <InsnPtr>0xFF</InsnPtr>
		char ipNode[9];
		data->make(0x000F000000000001ULL, ipNode, 9);
		ipNode[8] = 0xFF;
		objSectionAppend(antab, ipNode, 9);
		
		for (sect=obj->sects; sect!=NULL; sect=sect->next)
		{
			Annot *annot;
			for (annot=sect->annot; annot!=NULL; annot=annot->next)
			{
				// <Function>
				uint64_t funcHeader = 0x0001000000000000
						+ 8 + strlen(annot->name)	// <Name>name</Name>	
						+ 8 + 8				// <Address>?</Address>
						+ 8 + 8				// <Size>?</Size>
				;
				// </Function>
				
				AnnotChild *child;
				for (child=annot->children; child!=NULL; child=child->next)
				{
					funcHeader += 8 + child->size;
				};
				
				char hdr[8];
				data->make(funcHeader, hdr, 8);
				objSectionAppend(antab, hdr, 8);
				
				// <Name>*</Name>
				data->make(0x0004000000000000 + strlen(annot->name), hdr, 8);
				objSectionAppend(antab, hdr, 8);
				objSectionAppend(antab, annot->name, strlen(annot->name));
				
				// <Address>*</Address>
				data->make(0x0002000000000008, hdr, 8);
				objSectionAppend(antab, hdr, 8);
				data->make(sect->addr + annot->sectOffset, hdr, 8);
				objSectionAppend(antab, hdr, 8);
				
				// <Size>*</Size>
				data->make(0x0003000000000008, hdr, 8);
				objSectionAppend(antab, hdr, 8);
				data->make(annot->size, hdr, 8);
				objSectionAppend(antab, hdr, 8);
				
				for (child=annot->children; child!=NULL; child=child->next)
				{
					data->make(((uint64_t) child->type << 48) | child->size, hdr, 8);
					objSectionAppend(antab, hdr, 8);
					objSectionAppend(antab, child->data, child->size);
				};
			};
		};
	};
	
	uint64_t numSect = 0;
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		numSect++;
	};
	
	// figure out which part of the file sections will start being allocated to
	uint64_t allocNext = sizeof(GXO_Header) + numSect * sizeof(GXO_Section);
	
	data->make(GXO_MAGIC, &ghead.gxoMagic, sizeof(ghead.gxoMagic));
	ghead.gxoArch = data->arch;
	ghead.gxoSystem = data->system;
	if (data->archflags != NULL) data->make(data->archflags(), &ghead.gxoArchFlags, sizeof(ghead.gxoArchFlags));
	if (data->sysflags != NULL) data->make(data->sysflags(), &ghead.gxoSystemFlags, sizeof(ghead.gxoSystemFlags));
	data->make(flags, &ghead.gxoGenericFlags, sizeof(ghead.gxoGenericFlags));
	
	if (obj->entry != NULL)
	{
		uint64_t entry = obj->entry->value;
		if (obj->entry->sect != NULL) entry += obj->entry->sect->addr;
		data->make(entry, &ghead.gxoEntryAddr, sizeof(ghead.gxoEntryAddr));
	};
	
	data->make(sizeof(GXO_Header), &ghead.gxoHeaderSize, sizeof(ghead.gxoHeaderSize));
	data->make(sizeof(GXO_Header), &ghead.gxoSectionTableOffset, sizeof(ghead.gxoSectionTableOffset));
	data->make(numSect, &ghead.gxoSectionCount, sizeof(ghead.gxoSectionCount));
	data->make(sizeof(GXO_Section), &ghead.gxoSectionEntrySize, sizeof(ghead.gxoSectionEntrySize));
	fwrite(&ghead, 1, sizeof(GXO_Header), fp);
	
	// write the sections
	uint64_t nextEntPos = sizeof(GXO_Header);
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{	
		// create the section table entry
		GXO_Section shead;
		memset(&shead, 0, sizeof(GXO_Section));
		shead.sectName = str2int(sect->name);
		data->make(sect->addr, &shead.sectAddr, sizeof(shead.sectAddr));
		data->make(sect->size, &shead.sectSize, sizeof(shead.sectSize));
		
		if (sect->type == SECTYPE_PROGBITS)
		{
			// decide where in the file we're putting this
			allocNext = (allocNext + sect->align - 1) & ~(sect->align - 1);
			data->make(allocNext, &shead.sectOffset, sizeof(shead.sectOffset));
			
			// write the section data
			fseek(fp, allocNext, SEEK_SET);
			fwrite(sect->data, 1, sect->size, fp);
			
			// shift the allocator
			allocNext += sect->size;
		};
		
		uint8_t genflags = 0;
		if (sect->flags & SEC_READ) genflags |= SECT_GEN_READ;
		if (sect->flags & SEC_WRITE) genflags |= SECT_GEN_WRITE;
		if (sect->flags & SEC_EXEC) genflags |= SECT_GEN_EXEC;
		if (genflags == 0) genflags |= SECT_GEN_NOALLOC;
		if (strcmp(sect->name, ".indir") == 0) genflags |= SECT_GEN_RELRO;
		
		data->make(genflags, &shead.sectGenericFlags, sizeof(shead.sectGenericFlags));
		
		uint8_t alignLog2;
		for (alignLog2=0; alignLog2!=255; alignLog2+=1)
		{
			if ((1UL << alignLog2) == sect->align) break;
		};
		
		data->make(alignLog2, &shead.sectAlignLog2, sizeof(shead.sectAlignLog2));
		
		// write the section table entry
		fseek(fp, nextEntPos, SEEK_SET);
		fwrite(&shead, 1, sizeof(GXO_Section), fp);
		nextEntPos += sizeof(GXO_Section);
	};
	
	// done
	return ERR_OK;
};

Error gxoRead(Object *obj, FILE *fp)
{
	GXO_DrvData *data = (GXO_DrvData*) obj->drvdata;
	
	// read the header
	GXO_Header ghead;
	if (fread(&ghead, sizeof(GXO_Header), 1, fp) != 1)
	{
		return ERR_IO;
	};
	
	if (data->get(&ghead.gxoMagic, sizeof(ghead.gxoMagic)) != GXO_MAGIC)
	{
		return ERR_INVALID_PARAM;
	};
	
	if (ghead.gxoArch != data->arch)
	{
		return ERR_INVALID_PARAM;
	};
	
	if (ghead.gxoSystem != data->system)
	{
		return ERR_INVALID_PARAM;
	};
	
	if (data->get(&ghead.gxoHeaderSize, sizeof(ghead.gxoHeaderSize)) != sizeof(GXO_Header))
	{
		return ERR_INVALID_PARAM;
	};
	
	if (data->get(&ghead.gxoSectionEntrySize, sizeof(ghead.gxoSectionEntrySize)) != sizeof(GXO_Section))
	{
		return ERR_INVALID_PARAM;
	};
	
	uint64_t flags = data->get(&ghead.gxoGenericFlags, sizeof(ghead.gxoGenericFlags));
	if (flags & GXO_GEN_REL)
	{
		obj->type = OBJTYPE_RELOC;
	}
	else if (flags & GXO_GEN_EXEC)
	{
		obj->type = OBJTYPE_EXEC;
	}
	else
	{
		obj->type = OBJTYPE_SHARED;
	};
	
	// collect information about sections
	uint64_t numSect = data->get(&ghead.gxoSectionCount, sizeof(ghead.gxoSectionCount));
	GXO_SectionInfo sectInfo[numSect];
	memset(sectInfo, 0, sizeof(sectInfo));
	
	fseek(fp, data->get(&ghead.gxoSectionTableOffset, sizeof(ghead.gxoSectionTableOffset)), SEEK_SET);

	uint64_t i;
	for (i=0; i<numSect; i++)
	{
		GXO_Section sect;
		if (fread(&sect, sizeof(GXO_Section), 1, fp) != 1)
		{
			return ERR_IO;
		};
		
		sectInfo[i].namenum = sect.sectName;
		sectInfo[i].vaddr = data->get(&sect.sectAddr, sizeof(sect.sectAddr));
		sectInfo[i].offset = data->get(&sect.sectOffset, sizeof(sect.sectOffset));
		sectInfo[i].size = data->get(&sect.sectSize, sizeof(sect.sectSize));
		sectInfo[i].flags = data->get(&sect.sectGenericFlags, sizeof(sect.sectGenericFlags));
	};
	
	// pass 1: create CompTools sections
	for (i=0; i<numSect; i++)
	{
		if ((sectInfo[i].flags & SECT_GEN_NOALLOC) == 0)
		{
			int flags = 0;
			if (sectInfo[i].flags & SECT_GEN_READ) flags |= SEC_READ;
			if (sectInfo[i].flags & SECT_GEN_WRITE) flags |= SEC_WRITE;
			if (sectInfo[i].flags & SECT_GEN_EXEC) flags |= SEC_EXEC;
			
			if (sectInfo[i].offset != 0)
			{
				Section *sect = objCreateSection(obj, sectInfo[i].name, SECTYPE_PROGBITS, flags);
				sect->data = malloc(sectInfo[i].size);
				sect->size = sectInfo[i].size;
				
				fseek(fp, sectInfo[i].offset, SEEK_SET);
				if (fread(sect->data, 1, sect->size, fp) != sect->size)
				{
					return ERR_IO;
				};
				
				sectInfo[i].sect = sect;
			}
			else
			{
				Section *sect = objCreateSection(obj, sectInfo[i].name, SECTYPE_NOBITS, flags);
				objSectionResv(sect, sectInfo[i].size);
				sectInfo[i].sect = sect;
			};
		};
	};
	
	// pass 2: extract the symbol table
	HashTable *symlookup = hashtabNew();
	
	obj->syms = NULL;
	for (i=0; i<numSect; i++)
	{
		if (strcmp(sectInfo[i].name, ".symtab") == 0)
		{
			uint64_t offset = sectInfo[i].offset;
			
			GXO_Symbol sym;
			while (1)
			{
				fseek(fp, offset, SEEK_SET);
				if (fread(&sym, sizeof(GXO_Symbol), 1, fp) != 1)
				{
					return ERR_IO;
				};
				
				if (data->get(&sym.symName, sizeof(sym.symName)) == 0) break;
				
				uint64_t nameaddr = data->get(&sym.symName, sizeof(sym.symName));
				uint64_t nameoff = 0;
				
				uint64_t j;
				for (j=0; j<numSect; j++)
				{
					if (sectInfo[j].vaddr <= nameaddr && sectInfo[j].vaddr+sectInfo[j].size > nameaddr)
					{
						nameoff = nameaddr - sectInfo[j].vaddr + sectInfo[j].offset;
						break;
					};
				};
				
				if (nameoff == 0)
				{
					return ERR_INVALID_PARAM;
				};
				
				char namebuf[128];
				fseek(fp, nameoff, SEEK_SET);
				int c;
				char *put = namebuf;
				
				while ((c = fgetc(fp)) != 0)
				{
					if (c == EOF) return ERR_INVALID_PARAM;
					*put++ = c;
				};
				
				*put = 0;
				
				Symbol *outsym = (Symbol*) calloc(1, sizeof(Symbol));
				outsym->name = strdup(namebuf);
				
				uint64_t value = data->get(&sym.symValue, sizeof(sym.symValue));
				Section *sect = NULL;
				
				if (data->get(&sym.symType, sizeof(sym.symType)) != SYM_TYPE_ABS)
				{
					for (j=0; j<numSect; j++)
					{
						if (sectInfo[j].vaddr <= value && sectInfo[j].vaddr+sectInfo[j].size > value && sectInfo[j].sect != NULL)
						{
							value -= sectInfo[j].vaddr;
							sect = sectInfo[j].sect;
							break;
						};
					};
				};
				
				outsym->sect = sect;
				outsym->value = value;
				
				switch (data->get(&sym.symType, sizeof(sym.symType)))
				{
				case SYM_TYPE_ABS:
					outsym->type = SYMT_NONE;
					break;
				case SYM_TYPE_FUNC:
					outsym->type = SYMT_FUNC;
					break;
				case SYM_TYPE_LABEL:
					outsym->type = SYMT_NONE;
					if (value == 0 && sect != NULL)
					{
						if (strcmp(sect->name, namebuf) == 0)
						{
							outsym->type = SYMT_SECTION;
							sect->sym = outsym;
						};
					};
					break;
				case SYM_TYPE_OBJECT:
					outsym->type = SYMT_OBJECT;
					break;
				case SYM_TYPE_TLS:
					return ERR_INVALID_PARAM;			// TODO
					break;
				case SYM_TYPE_UNDEF:
					outsym->type = SYMT_UNDEF;
					if (data->get(&sym.symSize, sizeof(sym.symSize)) != 0)
					{
						outsym->type = SYMT_COMMON;
					};
					break;
				default:
					return ERR_INVALID_PARAM;
				};
				
				switch (data->get(&sym.symScope, sizeof(sym.symScope)))
				{
				case SYM_SCOPE_GLOBAL:
					outsym->binding = SYMB_GLOBAL;
					break;
				case SYM_SCOPE_LOCAL:
					outsym->binding = SYMB_LOCAL;
					break;
				case SYM_SCOPE_WEAK:
					outsym->binding = SYMB_WEAK;
					break;
				default:
					return ERR_INVALID_PARAM;
				};
				
				outsym->size = data->get(&sym.symSize, sizeof(sym.symSize));
				outsym->align = (1ULL << data->get(&sym.symAlignLog2, sizeof(sym.symAlignLog2)));
				
				outsym->next = obj->syms;
				obj->syms = outsym;
				
				char temp[17];
				sprintf(temp, "%016" PRIx64, offset - sectInfo[i].offset + sectInfo[i].vaddr);
				hashtabSet(symlookup, temp, outsym);
				
				offset += sizeof(GXO_Symbol);
			};
		};
	};
	
	// pass 3: the relocation table
	for (i=0; i<numSect; i++)
	{
		if (strcmp(sectInfo[i].name, ".reloc") == 0)
		{
			fseek(fp, sectInfo[i].offset, SEEK_SET);
			
			while (1)
			{
				GXO_Reloc reloc;
				if (fread(&reloc, sizeof(GXO_Reloc), 1, fp) != 1)
				{
					return ERR_IO;
				};
				
				uint64_t type = data->get(&reloc.relType, sizeof(reloc.relType));
				if (type == 0) break;
				
				uint64_t pos = data->get(&reloc.relPosition, sizeof(reloc.relPosition));
				
				Section *sect = NULL;
				uint64_t offset;
				
				uint64_t j;
				for (j=0; j<numSect; j++)
				{
					if (sectInfo[j].vaddr <= pos && sectInfo[j].vaddr+sectInfo[j].size > pos && sectInfo[j].sect != NULL)
					{
						offset = pos - sectInfo[j].vaddr;
						sect = sectInfo[j].sect;
						break;
					};
				};
				
				if (sect == NULL) return ERR_INVALID_PARAM;
				
				uint64_t param = data->get(&reloc.relParam, sizeof(reloc.relParam));
				
				char temp[17];
				sprintf(temp, "%016" PRIx64, data->get(&reloc.relSymbol, sizeof(reloc.relSymbol)));
				
				Symbol *sym = (Symbol*) hashtabGet(symlookup, temp);
				RelocType *rtype = data->gxoToCompRel(type);
				if (rtype == NULL) return ERR_INVALID_PARAM;
				
				Reloc *outrel = (Reloc*) malloc(sizeof(Reloc));
				memset(outrel, 0, sizeof(Reloc));
				
				outrel->type = rtype;
				outrel->sect = sect;
				outrel->offset = offset;
				outrel->sym = sym;
				outrel->addend = param;
				
				outrel->next = sect->relocs;
				sect->relocs = outrel;
			};
		};
	};
	
	// pass 4: the soname
	for (i=0; i<numSect; i++)
	{
		if (strcmp(sectInfo[i].name, ".resolve") == 0)
		{
			fseek(fp, sectInfo[i].offset, SEEK_SET);
			
			char buffer[8];
			if (fread(buffer, 8, 1, fp) != 1) return ERR_IO;
			
			uint64_t addr = data->get(buffer, 8);
			uint64_t j;
			for (j=0; j<numSect; j++)
			{
				if (sectInfo[j].vaddr <= addr && sectInfo[j].vaddr+sectInfo[j].size > addr)
				{
					char soname[128];
					fseek(fp, sectInfo[j].offset + addr - sectInfo[j].vaddr, SEEK_SET);
					
					char *put = soname;
					int c;
					while ((c = fgetc(fp)) != 0)
					{
						if (c == EOF) break;
						*put++ = c;
					};
					
					*put = 0;
					obj->soname = strdup(soname);
					break;
				};
			};
			
			break;
		};
	};
	
	// pass 5: annotations
	for (i=0; i<numSect; i++)
	{
		if (strcmp(sectInfo[i].name, ".annot") == 0)
		{
			fseek(fp, sectInfo[i].offset, SEEK_SET);
			size_t sizeLeft = sectInfo[i].size;
			
			while (sizeLeft >= 8)
			{
				char buf[8];
				fread(buf, 8, 1, fp);
				
				uint64_t header = data->get(buf, 8);
				
				uint16_t type = (uint16_t) (header >> 48);
				uint64_t size = (header & 0x0000FFFFFFFFFFFF);
				
				sizeLeft -= 8;
				sizeLeft -= size;
				
				if (type != ANNOT_Function)
				{
					fseek(fp, size, SEEK_CUR);
				}
				else
				{
					uint64_t fAddr = 0;
					uint64_t fSize = 0;
					char *fName = NULL;
					AnnotChild *fNodes = NULL;
					
					while (size != 0)
					{
						fread(buf, 8, 1, fp);
						header = data->get(buf, 8);
						
						uint16_t subtype = (uint16_t) (header >> 48);
						uint64_t subsize = (header & 0x0000FFFFFFFFFFFF);
						
						if (subtype == ANNOT_Name)
						{
							free(fName);
							fName = (char*) malloc(subsize+1);
							fread(fName, 1, subsize, fp);
							fName[subsize] = 0;
						}
						else if (subtype == ANNOT_Address)
						{
							fread(buf, 8, 1, fp);
							fAddr = data->get(buf, 8);
						}
						else if (subtype == ANNOT_Size)
						{
							fread(buf, 8, 1, fp);
							fSize = data->get(buf, 8);
						}
						else
						{
							AnnotChild *node = objCreateAnnotChild(NULL);
							node->next = fNodes;
							fNodes = node;
							
							node->type = subtype;
							node->size = subsize;
							node->data = malloc(subsize);
							
							fread(node->data, 1, subsize, fp);
						};
						
						size -= subsize;
						size -= 8;
					};
					
					Section *sect;
					for (sect=obj->sects; sect!=NULL; sect=sect->next)
					{
						if (sect->addr <= fAddr && sect->addr + sect->size > fAddr)
						{
							Annot *annot = objAddAnnot(sect, fName);
							annot->sectOffset = fAddr - sect->addr;
							annot->size = fSize;
							annot->children = fNodes;
							break;
						};
					};
				};
			};
		};
	};
	
	// TODO: entry point
	
	// cleanup
	hashtabDelete(symlookup);
	return ERR_OK;
};

typedef struct
{
	uint64_t straddr;
} GXO_Name;

static unsigned long gxoHash(const char *name_)
{
	const unsigned char *name = (const unsigned char*) name_;
	unsigned long h = 0;
	int c;
	
	while ((c = *name++))
	{
		h = c + (h << 6) + (h << 16) - h;
	};
	
	return h;
}

Error gxoPreReloc(Object *obj)
{
	GXO_DrvData *data = (GXO_DrvData*) obj->drvdata;
	data->indirs = hashtabNew();
	
	// force page alignment on all sections
	Section *sect;
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		if (sect->addr & (data->pageSize-1))
		{
			fprintf(stderr, "gxo: misaligned section `%s'\n", sect->name);
			return ERR_INVALID_PARAM;
		};
		
		if (sect->align < data->pageSize) sect->align = data->pageSize;
		
		if (sect->align > data->pageSize)
		{
			fprintf(stderr, "gxo: section `%s' is aligned too strictly\n", sect->name);
			return ERR_INVALID_PARAM;
		};
	};
	
	// generate an indirection table for all undefined symbols
	Section *indir = objGetSection(obj, ".indir");
	if (indir != NULL)
	{
		fprintf(stderr, "gxo: `.indir' section already exists\n");
		return ERR_INVALID_PARAM;
	};
	indir = objCreateSection(obj, ".indir", SECTYPE_NOBITS, SEC_READ | SEC_WRITE);
	if (indir == NULL)
	{
		fprintf(stderr, "gxo: could not create section `.indir'\n");
		return ERR_INVALID_PARAM;
	};
	indir->addr = (indir->addr + data->pageSize - 1) & ~(data->pageSize - 1);
	indir->align = data->pageSize;
	
	Symbol *sym;
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		if (sym->type == SYMT_UNDEF || sym->binding == SYMB_GLOBAL || sym->binding == SYMB_WEAK)
		{
			// allow references to this symbol
			if (sym->type == SYMT_UNDEF) sym->type = SYMT_NONE;
			
			if (!hashtabHasKey(data->indirs, sym->name))
			{
				GXO_Indir *ent = (GXO_Indir*) calloc(1, sizeof(GXO_Indir));
				ent->addr = indir->addr + indir->size;
				objSectionResv(indir, 8);
				hashtabSet(data->indirs, sym->name, ent);
			};
		};
	};
	
	// put all the strings we need in a ".dynstr" section
	Section *dynstr = objGetSection(obj, ".dynstr");
	if (dynstr != NULL)
	{
		fprintf(stderr, "gxo: `.dynstr' section already exists\n");
		return ERR_INVALID_PARAM;
	};
	dynstr = objCreateSection(obj, ".dynstr", SECTYPE_PROGBITS, SEC_READ);
	if (dynstr == NULL)
	{
		fprintf(stderr, "gxo: could not create section `.dynstr'\n");
		return ERR_INVALID_PARAM;
	};
	dynstr->addr = (dynstr->addr + data->pageSize - 1) & ~(data->pageSize - 1);
	dynstr->align = data->pageSize;

	HashTable *strtab = hashtabNew();
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		if (!hashtabHasKey(strtab, sym->name))
		{
			GXO_Name *ent = (GXO_Name*) malloc(sizeof(GXO_Name));
			ent->straddr = dynstr->addr + dynstr->size;
			objSectionAppend(dynstr, sym->name, strlen(sym->name)+1);
			hashtabSet(strtab, sym->name, ent);
		};
	};
	
	const char *soname = obj->soname;
	if (soname == NULL) soname = "";
	
	if (!hashtabHasKey(strtab, soname))
	{
		GXO_Name *ent = (GXO_Name*) malloc(sizeof(GXO_Name));
		ent->straddr = dynstr->addr + dynstr->size;
		objSectionAppend(dynstr, soname, strlen(soname)+1);
		hashtabSet(strtab, soname, ent);
	};
	
	int i;
	for (i=0; i<obj->numDepends; i++)
	{
		Object *dep = obj->depends[i];
		
		if (!hashtabHasKey(strtab, dep->soname))
		{
			GXO_Name *ent = (GXO_Name*) malloc(sizeof(GXO_Name));
			ent->straddr = dynstr->addr + dynstr->size;
			objSectionAppend(dynstr, dep->soname, strlen(dep->soname)+1);
			hashtabSet(strtab, dep->soname, ent);
		};
	};
	
	// create the dynamic symbol table
	Section *symtab = objGetSection(obj, ".symtab");
	if (symtab != NULL)
	{
		fprintf(stderr, "gxo: `.symtab' section already exists\n");
		return ERR_INVALID_PARAM;
	};
	symtab = objCreateSection(obj, ".symtab", SECTYPE_PROGBITS, SEC_READ);
	if (symtab == NULL)
	{
		fprintf(stderr, "gxo: could not create section `.symtab'\n");
		return ERR_INVALID_PARAM;
	};
	symtab->addr = (symtab->addr + data->pageSize - 1) & ~(data->pageSize - 1);
	symtab->align = data->pageSize;

	// generate undefined symbols for everything in the indirection table
	const char *name;
	{
		HASHTAB_FOREACH(data->indirs, GXO_Indir *, name, ent)
		{
			GXO_Name *nameinfo = (GXO_Name*) hashtabGet(strtab, name);
			
			GXO_Symbol out;
			memset(&out, 0, sizeof(GXO_Symbol));
			data->make(nameinfo->straddr, &out.symName, sizeof(out.symName));
			
			ent->symaddr = symtab->addr + symtab->size;
			objSectionAppend(symtab, &out, sizeof(GXO_Symbol));
		};
	};
	
	// export symbols, whilst creating the symbol hash table
	uint64_t numBuckets = data->pageSize / 8;
	uint64_t symhashtab[numBuckets];
	
	uint64_t h;
	for (h=0; h<numBuckets; h++)
	{
		symhashtab[h] = symtab->addr + symtab->size;
		
		for (sym=obj->syms; sym!=NULL; sym=sym->next)
		{
			if (sym->binding == SYMB_GLOBAL || sym->binding == SYMB_WEAK)
			{
				uint64_t bucket = gxoHash(sym->name) % numBuckets;
				if (bucket == h)
				{
					GXO_Name *nameinfo = (GXO_Name*) hashtabGet(strtab, sym->name);
					GXO_Symbol out;

					memset(&out, 0, sizeof(GXO_Symbol));
					data->make(nameinfo->straddr, &out.symName, sizeof(out.symName));

					uint64_t value = sym->value;
					if (sym->sect != NULL) value += sym->sect->addr;
					
					data->make(value, &out.symValue, sizeof(out.symValue));
					data->make(sym->size, &out.symSize, sizeof(out.symSize));
					
					switch (sym->type)
					{
					case SYMT_UNDEF:
					case SYMT_COMMON:
						data->make(SYM_TYPE_OBJECT, &out.symType, sizeof(out.symType));
						break;
					case SYMT_FUNC:
						data->make(SYM_TYPE_FUNC, &out.symType, sizeof(out.symType));
						break;
					case SYMT_NONE:
					case SYMT_SECTION:
					default:
						data->make(SYM_TYPE_LABEL, &out.symType, sizeof(out.symType));
						break;
					case SYMT_OBJECT:
						data->make(SYM_TYPE_OBJECT, &out.symType, sizeof(out.symType));
						break;
					};
					
					if (sym->binding == SYMB_WEAK || sym->type == SYMT_COMMON) data->make(SYM_SCOPE_WEAK, &out.symScope, sizeof(out.symScope));
					else data->make(SYM_SCOPE_GLOBAL, &out.symScope, sizeof(out.symScope));
					
					if (sym->align == 0) sym->align = 1;
					uint8_t alignLog2;
					
					for (alignLog2=0; alignLog2!=255; alignLog2++)
					{
						if ((1UL << alignLog2) == sym->align) break;
					};
					
					data->make(alignLog2, &out.symAlignLog2, sizeof(out.symAlignLog2));
					data->make(bucket, &out.symBucket, sizeof(out.symBucket));
					
					objSectionAppend(symtab, &out, sizeof(GXO_Symbol));
				};
			};
		};
	};
	
	// terminate the symbol table
	GXO_Symbol endsym;
	memset(&endsym, 0, sizeof(GXO_Symbol));
	objSectionAppend(symtab, &endsym, sizeof(GXO_Symbol));
	
	// write the symbol hash table
	Section *symhash = objGetSection(obj, ".symhash");
	if (symhash != NULL)
	{
		fprintf(stderr, "gxo: `.symhash' section already exists\n");
		return ERR_INVALID_PARAM;
	};
	symhash = objCreateSection(obj, ".symhash", SECTYPE_PROGBITS, SEC_READ);
	if (symhash == NULL)
	{
		fprintf(stderr, "gxo: could not create section `.symhash'\n");
		return ERR_INVALID_PARAM;
	};
	symhash->addr = (symhash->addr + data->pageSize - 1) & ~(data->pageSize - 1);
	symhash->align = data->pageSize;
	objSectionAppend(symhash, symhashtab, sizeof(symhashtab));
	
	// create the dynamic relocation table
	Section *reloc = objGetSection(obj, ".reloc");
	if (reloc != NULL)
	{
		fprintf(stderr, "gxo: `.reloc' section already exists\n");
		return ERR_INVALID_PARAM;
	};
	reloc = objCreateSection(obj, ".reloc", SECTYPE_PROGBITS, SEC_READ);
	if (reloc == NULL)
	{
		fprintf(stderr, "gxo: could not create section `.reloc'\n");
		return ERR_INVALID_PARAM;
	};
	reloc->addr = (reloc->addr + data->pageSize - 1) & ~(data->pageSize - 1);
	reloc->align = data->pageSize;
	
	// emit relocation entries for the indirection table
	{
		HASHTAB_FOREACH(data->indirs, GXO_Indir *, name, ent)
		{
			GXO_Reloc rel;
			memset(&rel, 0, sizeof(GXO_Reloc));
			data->make(ent->addr, &rel.relPosition, sizeof(rel.relPosition));
			data->make(ent->symaddr, &rel.relSymbol, sizeof(rel.relSymbol));
			data->make(RT_GEN_U64, &rel.relType, sizeof(rel.relType));
			objSectionAppend(reloc, &rel, sizeof(GXO_Reloc));
		};
	};
	
	// terminate the relocation table
	GXO_Reloc endrel;
	memset(&endrel, 0, sizeof(GXO_Reloc));
	objSectionAppend(reloc, &endrel, sizeof(GXO_Reloc));

	// now the resolution table
	Section *resolve = objGetSection(obj, ".resolve");
	if (resolve != NULL)
	{
		fprintf(stderr, "gxo: `.resolve' section already exists\n");
		return ERR_INVALID_PARAM;
	};
	resolve = objCreateSection(obj, ".resolve", SECTYPE_PROGBITS, SEC_READ);
	if (reloc == NULL)
	{
		fprintf(stderr, "gxo: could not create section `.resolve'\n");
		return ERR_INVALID_PARAM;
	};
	resolve->addr = (resolve->addr + data->pageSize - 1) & ~(data->pageSize - 1);
	resolve->align = data->pageSize;
	
	GXO_Name *pname = (GXO_Name*) hashtabGet(strtab, soname);
	char buffer[8];
	ctMakeLE(pname->straddr, buffer, 8);
	objSectionAppend(resolve, buffer, 8);
	
	for (i=0; i<obj->numDepends; i++)
	{
		Object *dep = obj->depends[i];
		pname = (GXO_Name*) hashtabGet(strtab, dep->soname);
		ctMakeLE(pname->straddr, buffer, 8);
		objSectionAppend(resolve, buffer, 8);
	};
	
	memset(buffer, 0, 8);
	objSectionAppend(resolve, buffer, 8);
	
	return ERR_OK;
};

RelocType* gxoToCompRelGen(uint64_t reltype)
{
	switch (reltype)
	{
	case RT_GEN_S8:			return &rt_gen_s8;
	case RT_GEN_U8:			return &rt_gen_u8;
	case RT_GEN_S16:		return &rt_gen_s16;
	case RT_GEN_U16:		return &rt_gen_u16;
	case RT_GEN_S32:		return &rt_gen_s32;
	case RT_GEN_U32:		return &rt_gen_u32;
	case RT_GEN_S64:		return &rt_gen_s64;
	case RT_GEN_U64:		return &rt_gen_u64;
	default:			return NULL;
	};
};
