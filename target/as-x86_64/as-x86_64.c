/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <comptools/as-x86.h>
#include <comptools/types.h>

#include <string.h>

typedef struct
{
	const char *name;
	int num;
} GxoRegMapping;

static GxoRegMapping regMap[] = {
	{"rax",			0x00},
	{"rcx",			0x01},
	{"rdx",			0x02},
	{"rbx",			0x03},
	{"rsp",			0x04},
	{"rbp",			0x05},
	{"rsi",			0x06},
	{"rdi",			0x07},
	{"r8",			0x08},
	{"r9",			0x09},
	{"r10",			0x0A},
	{"r11",			0x0B},
	{"r12",			0x0C},
	{"r13",			0x0D},
	{"r14",			0x0E},
	{"r15",			0x0F},
	
	{"xmm0",		0x40},
	{"xmm1",		0x41},
	{"xmm2",		0x42},
	{"xmm3",		0x43},
	{"xmm4",		0x44},
	{"xmm5",		0x45},
	{"xmm6",		0x46},
	{"xmm7",		0x47},
	{"xmm8",		0x48},
	{"xmm9",		0x49},
	{"xmm10",		0x4A},
	{"xmm11",		0x4B},
	{"xmm12",		0x4C},
	{"xmm13",		0x4D},
	{"xmm14",		0x4E},
	{"xmm15",		0x4F},
	
	{"rip",			0xFF},
	
	// LIST TERMINATOR
	{NULL, -1}
};

static int getGxoRegNum(As *as, const char *name)
{
	GxoRegMapping *map;
	for (map=regMap; map->name!=NULL; map++)
	{
		if (strcmp(map->name, name) == 0)
		{
			return map->num;
		};
	};
	
	return -1;
};

AsDriver asDriver_x86_64 = {
	.name = "x86_64",
	.opt = (void*) 64,
	.comment = ";",
	.init = x86asInit,
	.line = x86asLine,
	.getGxoRegNum = getGxoRegNum,
	.make = ctMakeLE
};
