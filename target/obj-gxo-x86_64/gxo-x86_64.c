/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>

#include <comptools/types.h>
#include <comptools/gxo-x86_64.h>
#include <comptools/as-x86.h>
#include <comptools/obj.h>
#include <comptools/gxo.h>

static Error resolve_x86_64_abs8(void *ptr, uint64_t symval, int64_t addend, Reloc *reloc)
{
	uint64_t trueval = symval+addend;
	uint8_t val = (uint8_t) trueval;	
	if (trueval != (uint64_t) val) return ERR_RELOC_TRUNC;
	ctMakeLE(val, ptr, 1);
	return ERR_OK;
};

static Error resolve_x86_64_abs16(void *ptr, uint64_t symval, int64_t addend, Reloc *reloc)
{
	uint64_t trueval = symval+addend;
	uint16_t val = (uint16_t) trueval;	
	if (trueval != (uint64_t) val) return ERR_RELOC_TRUNC;
	ctMakeLE(val, ptr, 2);
	return ERR_OK;
};

static Error resolve_x86_64_abs32(void *ptr, uint64_t symval, int64_t addend, Reloc *reloc)
{
	int64_t trueval = symval+addend;
	int32_t val = (int32_t) trueval;	
	if (trueval != (int64_t) val) return ERR_RELOC_TRUNC;
	ctMakeLE(val, ptr, 4);
	return ERR_OK;
};

static Error resolve_x86_64_abs64(void *ptr, uint64_t symval, int64_t addend, Reloc *reloc)
{
	uint64_t trueval = symval+addend;
	ctMakeLE(trueval, ptr, 8);
	return ERR_OK;
};

static Error resolve_x86_64_rel16(void *ptr, uint64_t symval, int64_t addend, Reloc *reloc)
{
	int64_t trueval = (symval + addend - reloc->sect->addr - reloc->offset);
	int16_t val = (int16_t) trueval;
	if (trueval != (int64_t)val) return ERR_RELOC_TRUNC;
	ctMakeLE(val, ptr, 2);
	return ERR_OK;
};

static Error resolve_x86_64_rel32(void *ptr, uint64_t symval, int64_t addend, Reloc *reloc)
{
	int64_t trueval = (symval + addend - reloc->sect->addr - reloc->offset);
	int32_t val = (int32_t) trueval;
	if (trueval != (int64_t)val) return ERR_RELOC_TRUNC;
	ctMakeLE(val, ptr, 4);
	return ERR_OK;
};

static Error resolve_x86_64_rel32_indir(void *ptr, uint64_t symval, int64_t addend, Reloc *reloc)
{
	GXO_DrvData *data = (GXO_DrvData*) reloc->sect->obj->drvdata;
	const char *name = reloc->sym->name;
	
	if (!hashtabHasKey(data->indirs, name))
	{
		return ERR_RELOC_TRUNC;
	};
	
	GXO_Indir *indir = (GXO_Indir*) hashtabGet(data->indirs, name);

	int64_t trueval = (indir->addr + addend - reloc->sect->addr - reloc->offset);
	int32_t val = (int32_t) trueval;
	if (trueval != (int64_t)val) return ERR_RELOC_TRUNC;
	ctMakeLE(val, ptr, 4);
	return ERR_OK;
};

static RelocType rt_x86_64_abs8 = {.resolve = resolve_x86_64_abs8, .name = "RT_X86_64_ABS8", .bytes = 1};
static RelocType rt_x86_64_abs16 = {.resolve = resolve_x86_64_abs16, .name = "RT_X86_64_ABS16", .bytes = 2};
static RelocType rt_x86_64_abs32 = {.resolve = resolve_x86_64_abs32, .name = "RT_X86_64_ABS32", .bytes = 4};
static RelocType rt_x86_64_abs64 = {.resolve = resolve_x86_64_abs64, .name = "RT_X86_64_ABS64", .bytes = 8};
static RelocType rt_x86_64_rel16 = {.resolve = resolve_x86_64_rel16, .name = "RT_X86_64_REL16", .bytes = 2};
static RelocType rt_x86_64_rel32 = {.resolve = resolve_x86_64_rel32, .name = "RT_X86_64_REL32", .bytes = 4};
static RelocType rt_x86_64_rel32_indir = {.resolve = resolve_x86_64_rel32_indir, .name = "RT_X86_64_REL32_INDIR", .bytes = 4};

static Error gxoCompToGxoRel_x86_64(Reloc *rel, GXO_Reloc *outrel)
{
	if (rel->type == &rt_x86_64_abs8)
	{
		ctMakeLE(RT_X86_64_ABS8, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rt_x86_64_abs16)
	{
		ctMakeLE(RT_X86_64_ABS16, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rt_x86_64_abs32)
	{
		ctMakeLE(RT_X86_64_ABS32, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rt_x86_64_abs64)
	{
		ctMakeLE(RT_X86_64_ABS64, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rt_x86_64_rel16)
	{
		ctMakeLE(RT_X86_64_REL16, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rt_x86_64_rel32)
	{
		ctMakeLE(RT_X86_64_REL32, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rt_x86_64_rel32_indir)
	{
		ctMakeLE(RT_X86_64_REL32_INDIR, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rel_x86_abs8 && rel->prop == NULL)
	{
		ctMakeLE(RT_X86_64_ABS8, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rel_x86_abs16 && rel->prop == NULL)
	{
		ctMakeLE(RT_X86_64_ABS16, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rel_x86_abs32 && rel->prop == NULL)
	{
		ctMakeLE(RT_X86_64_ABS32, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rel_x86_abs64 && rel->prop == NULL)
	{
		ctMakeLE(RT_X86_64_ABS64, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rel_x86_rel16 && rel->prop == NULL)
	{
		ctMakeLE(RT_X86_64_REL16, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rel_x86_rel32 && rel->prop == NULL)
	{
		ctMakeLE(RT_X86_64_REL32, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else if (rel->type == &rel_x86_rel32 && rel->prop != NULL && strcmp(rel->prop, "INDIR") == 0)
	{
		ctMakeLE(RT_X86_64_REL32_INDIR, &outrel->relType, sizeof(outrel->relType));
		ctMakeLE(rel->addend, &outrel->relParam, sizeof(outrel->relParam));
	}
	else
	{
		return ERR_INVALID_PARAM;
	};
	
	return ERR_OK;
};

static RelocType* gxoToCompRel_x86_64(uint64_t type)
{
	switch (type)
	{
	case RT_X86_64_ABS8:		return &rt_x86_64_abs8;
	case RT_X86_64_ABS16:		return &rt_x86_64_abs16;
	case RT_X86_64_ABS32:		return &rt_x86_64_abs32;
	case RT_X86_64_ABS64:		return &rt_x86_64_abs64;
	case RT_X86_64_REL16:		return &rt_x86_64_rel16;
	case RT_X86_64_REL32:		return &rt_x86_64_rel32;
	case RT_X86_64_REL32_INDIR:	return &rt_x86_64_rel32_indir;
	default:			return gxoToCompRelGen(type);
	};
};

GXO_DrvData* gxoDrvInit_x86_64(Object *obj, const char *system)
{
	GXO_DrvData *data = gxoDrvInit(obj, "_x86_64_", system);
	data->make = ctMakeLE;
	data->get = ctGetLE;
	data->compToGxoRel = gxoCompToGxoRel_x86_64;
	data->gxoToCompRel = gxoToCompRel_x86_64;
	return data;
};
