/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <comptools/codegen-x86_64.h>

static const char *getCmpForType(int type)
{
	switch (type)
	{
	case CTIR_I8:
	case CTIR_U8:
	case CTIR_I16:
	case CTIR_U16:
	case CTIR_I32:
	case CTIR_U32:
	case CTIR_I64:
	case CTIR_U64:
		return "cmp";
	case CTIR_F32:
		return "ucomiss";
	case CTIR_F64:
		return "ucomisd";
	default:
		fprintf(stderr, "x86_64: cannot determine comparison instruction\n");
		abort();
		return NULL;
	};
};

static const char *getSetterFor(int opcode, int type)
{
	switch (opcode)
	{
	case CTIR_eq:
		return "sete";
	case CTIR_neq:
		return "setne";
	case CTIR_less:
		if (type == CTIR_I8 || type == CTIR_I16 || type == CTIR_I32 || type == CTIR_I64) return "setl";
		else return "setb";
	case CTIR_less_eq:
		if (type == CTIR_I8 || type == CTIR_I16 || type == CTIR_I32 || type == CTIR_I64) return "setle";
		else return "setbe";
	case CTIR_greater:
		if (type == CTIR_I8 || type == CTIR_I16 || type == CTIR_I32 || type == CTIR_I64) return "setg";
		else return "seta";
	case CTIR_greater_eq:
		if (type == CTIR_I8 || type == CTIR_I16 || type == CTIR_I32 || type == CTIR_I64) return "setge";
		else return "setae";
	default:
		fprintf(stderr, "x86_64: cannot determine setter instruction\n");
		abort();
		return NULL;
	};
};

void x86_64_gen_cmp(CodeGen *codegen, CTIR_Func *func, CTIR_Block *block, CodeGenBlock *out, x86_64_FuncData *funcData, CTIR_Insn *insn)
{
	int type = funcData->regInfo[insn->ops[0].regNo].type;

	// zero out the destination register
	x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
	x86->mnem = "xor";
	
	x86->ops[0].kind = OK_CTIR_REG;
	x86->ops[0].regNo = insn->resultReg;
	x86->ops[0].aliasType = CTIR_U32;
	
	x86->ops[1] = x86->ops[0];
	CodeGenInsn *xor = pushInsn(out, x86, strdup("cmp"), 1, U_ALU, 2, funcData->regInfo[insn->ops[0].regNo].definer, funcData->regInfo[insn->ops[1].regNo].definer);
	
	// now emit the comparison instruction
	x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
	x86->mnem = getCmpForType(type);
	
	x86->ops[0].kind = OK_CTIR_REG;
	x86->ops[0].regNo = insn->ops[0].regNo;
	
	x86->ops[1].kind = OK_CTIR_REG;
	x86->ops[1].regNo = insn->ops[1].regNo;
	
	CodeGenInsn *cmp = pushInsn(out, x86, strdup("cmp"), 1, U_ALU, 1, xor);
	xor->schedNext = cmp;
	
	// now emit the setter instruction
	x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
	x86->mnem = getSetterFor(insn->opcode, type);
	
	x86->ops[0].kind = OK_CTIR_REG;
	x86->ops[0].regNo = insn->resultReg;
	x86->ops[0].aliasType = CTIR_U8;
	
	CodeGenInsn *set = pushInsn(out, x86, strdup("cmp"), 1, U_ALU, 1, cmp);
	cmp->schedNext = set;
	
	funcData->regInfo[insn->resultReg].type = CTIR_I32;
	funcData->regInfo[insn->resultReg].definer = set;
};
