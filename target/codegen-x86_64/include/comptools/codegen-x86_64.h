/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef COMPTOOLS_CODEGEN_X86_64_H
#define COMPTOOLS_CODEGEN_X86_64_H

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/codegen.h>
#include <comptools/error.h>
#include <comptools/console.h>

/**
 * Flags indicating which units an instruction uses.
 */
#define	U_ALU				(1 << 0)
#define	U_MEM_LOAD			(1 << 1)

/**
 * Register names.
 */
#define	X86_64_REG_NAMES {"(none)", "rbx", "rcx", "rdx", "rsi", "rdi", "rbp", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7", "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14"}

/**
 * Figure out which registers must be preserved across function calls.
 */
#if defined(CODEGEN_X86_64_SYSV) || defined(CODEGEN_X86_64_GLIDIX)
#	define X86_64_PRESV_REGS	((1 << R_RBX) | (1 << R_RBP) | (1 << R_R12) | (1 << R_R13) | (1 << R_R14) | (1 << R_R15))
#else
#	error "Unknown ABI!"
#endif

enum
{
	// do not include RAX: we use it as a "temporary register",
	// and might need it to be available. same applies to XMM15.
	R_RBX = 1,
	R_RCX,
	R_RDX,
	R_RSI,
	R_RDI,
	R_RBP,
	R_R8,
	R_R9,
	R_R10,
	R_R11,
	R_R12,
	R_R13,
	R_R14,
	R_R15,
	
	R_XMM0,
	R_XMM1,
	R_XMM2,
	R_XMM3,
	R_XMM4,
	R_XMM5,
	R_XMM6,
	R_XMM7,
	R_XMM8,
	R_XMM9,
	R_XMM10,
	R_XMM11,
	R_XMM12,
	R_XMM13,
	R_XMM14,
};

/**
 * Operand kinds.
 */
enum
{
	OK_NONE,		/* no operand */
	OK_CTIR_REG,		/* CTIR register (more specifically, whatever it was mapped to) */
	OK_CPU_REG,		/* CPU register */
	OK_MEMREF,		/* memory reference (offset[regNo]) */
	OK_MEMREF_CTIR,		/* memory reference (offset[ctirReg]) */
	OK_CONST,		/* constant */
	OK_VAR_AREA,		/* reference to variable area [variable_area + offset] */
};

/**
 * x86_64 operand description.
 */
typedef struct
{
	/**
	 * Kind of operand.
	 */
	int kind;
	
	int regNo;		/* register number (CTIR) */
	const char *regName;	/* x86_64 register name */
	const char *sizeName;	/* size name (??? PTR) */
	off_t offset;		/* offset */
	uint64_t constVal;	/* constant value */
	int aliasType;		/* CTIR type to pretend that the CTIR register is (VOID = don't pretend) */
	const char *label;	/* if not NULL, label to use instead of 'offset' for MEMREF */
} x86_64_Operand;

/**
 * x86_64 instruction/pseudo-instruction description.
 */
typedef struct
{
	/**
	 * The instruction mnemonic.
	 */
	const char *mnem;
	
	/**
	 * Operands.
	 */
	x86_64_Operand ops[2];
	
	/**
	 * Set of registers clobbered by this instruction.
	 */
	uint64_t clobbers;
	
	/**
	 * If not NULL, specifies a suffix to append to the instruction. For example,
	 * some instructions take a third immediate operand.
	 */
	const char *suffix;
	
	/**
	 * If these two values are nonzero, it indicates that this instruction is only needed
	 * if the CTIR registers given here are allocated to different registers.
	 */
	int ctirDepRegA;
	int ctirDepRegB;
	
	/**
	 * If 'ctirDepFixed' is nonzero, it indicates that if CTIR register 'ctirDepFixed' was
	 * allocated to CPU register 'prefReg', then this instruction is not needed.
	 */
	int ctirDepFixed;
	int prefReg;
} x86_64_Insn;

/**
 * x86_64 CTIR register information.
 */
typedef struct
{
	/**
	 * CTIR type assigned to this register.
	 */
	int type;
	
	/**
	 * If CTIR_TYPE_STACK, (positive) offset from the stack pointer.
	 */
	size_t offsetFromVarArea;
	
	/**
	 * The instruction which wrote to this register. It must be scheduled before
	 * any instruction which reads the value.
	 */
	CodeGenInsn *definer;
	
	/**
	 * The instruction which last accessed this stack area, for CTIR_STACK
	 * type registers; must be scheduler before any instruction which reads
	 * the new value.
	 */
	CodeGenInsn *lastAccessor;
} x86_64_RegInfo;

/**
 * x86_64 function information.
 */
typedef struct
{
	/**
	 * Size of the stack frame.
	 */
	size_t frameSize;
	
	/**
	 * Information about every CTIR register.
	 */
	x86_64_RegInfo *regInfo;
	
	/**
	 * Epilog label.
	 */
	char *epilogLabel;
	
	/**
	 * Last spill location. Grows DOWNWARDS, with 'rbp' or hypothetical base pointer
	 * being value '0'.
	 */
	int lastSpillLocation;
	
	/**
	 * Saved registers. Set during prolog generation.
	 */
	uint64_t saveRegs;
	
	/**
	 * Offset from RBP (or "hypothetical RBP") to the beginning of the variable area.
	 */
	int varAreaFromRBP;
	
	/**
	 * Number of bytes from RSP to RBP, or hypothetical RBP. That is:
	 * RSP + frameOffset = RBP
	 * If -1, the value is not known, and RBP is the base pointer.
	 */
	int frameOffset;
	
	/**
	 * Set to 1 if we need the stack to be aligned to 16 bytes.
	 */
	int needStackAlign16;
} x86_64_FuncData;

extern const char* regTable[][CTIR_NUM_TYPES];

CodeGenInsn* pushInsn(CodeGenBlock *block, x86_64_Insn *x86, char *debugstr, int cycles, uint32_t units, int numDeps, ...);

/**
 * Implementations of CTIR instructions.
 */
void x86_64_gen_cmp(CodeGen *codegen, CTIR_Func *func, CTIR_Block *block, CodeGenBlock *out, x86_64_FuncData *funcData, CTIR_Insn *insn);
void x86_64_gen_shift(CodeGen *codegen, CTIR_Func *func, CTIR_Block *block, CodeGenBlock *out, x86_64_FuncData *funcData, CTIR_Insn *insn);

#endif
