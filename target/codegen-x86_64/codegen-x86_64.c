/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <comptools/codegen.h>
#include <comptools/error.h>
#include <comptools/console.h>
#include <comptools/codegen-x86_64.h>

static const char *cpuRegNames[] = X86_64_REG_NAMES;

/**
 * Options.
 */
#if defined(CODEGEN_X86_64_SYSV)
int mnoRedZone;
#endif

/**
 * Array mapping CPU register numbers to arrays mapping CTIR types to the required alias
 * of the register.
 */
const char* regTable[][CTIR_NUM_TYPES] = {
//	VOID, U8, U16, U32, U64, I8, I16, I32, I64, F32, F64
	{NULL},
	{NULL, "bl", "bx", "ebx", "rbx", "bl", "bx", "ebx", "rbx"},
	{NULL, "cl", "cx", "ecx", "rcx", "cl", "cx", "ecx", "rcx"},
	{NULL, "dl", "dx", "edx", "rdx", "dl", "dx", "edx", "rdx"},
	{NULL, "sil", "si", "esi", "rsi", "sil", "si", "esi", "rsi"},
	{NULL, "dil", "di", "edi", "rdi", "dil", "di", "edi", "rdi"},
	{NULL, "bpl", "bp", "ebp", "rbp", "bpl", "bp", "ebp", "rbp"},
	{NULL, "r8l", "r8w", "r8d", "r8", "r8l", "r8w", "r8d", "r8"},
	{NULL, "r9l", "r9w", "r9d", "r9", "r9l", "r9w", "r9d", "r9"},
	{NULL, "r10l", "r10w", "r10d", "r10", "r10l", "r10w", "r10d", "r10"},
	{NULL, "r11l", "r11w", "r11d", "r11", "r11l", "r11w", "r11d", "r11"},
	{NULL, "r12l", "r12w", "r12d", "r12", "r12l", "r12w", "r12d", "r12"},
	{NULL, "r13l", "r13w", "r13d", "r13", "r13l", "r13w", "r13d", "r13"},
	{NULL, "r14l", "r14w", "r14d", "r14", "r14l", "r14w", "r14d", "r14"},
	{NULL, "r15l", "r15w", "r15d", "r15", "r15l", "r15w", "r15d", "r15"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm0", "xmm0"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm1", "xmm1"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm2", "xmm2"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm3", "xmm3"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm4", "xmm4"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm5", "xmm5"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm6", "xmm6"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm7", "xmm7"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm8", "xmm8"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm9", "xmm9"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm10", "xmm10"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm11", "xmm11"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm12", "xmm12"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm13", "xmm13"},
	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "xmm14", "xmm14"},
};

static const char *getSizeNameForType(int ctirType)
{
	switch (ctirType)
	{
	case CTIR_I8:
	case CTIR_U8:
		return "BYTE";
	case CTIR_I16:
	case CTIR_U16:
		return "WORD";
	case CTIR_I32:
	case CTIR_U32:
		return "DWORD";
	case CTIR_I64:
	case CTIR_U64:
		return "QWORD";
	case CTIR_F32:
	case CTIR_F64:
		// the CompTools assembler expects XMMWORD in these cases, and it's the suffix
		// to the instruction (e.g. "ss", "sd", etc) which determines the actual size
		return "XMMWORD";
	default:
		return NULL;
	};
};

CodeGenInsn* pushInsn(CodeGenBlock *block, x86_64_Insn *x86, char *debugstr, int cycles, uint32_t units, int numDeps, ...)
{
	CodeGenInsn *insn = (CodeGenInsn*) calloc(1, sizeof(CodeGenInsn));
	insn->drvdata = x86;
	insn->cycles = cycles;
	insn->units = units;
	insn->deps = (CodeGenInsn**) malloc(sizeof(void*) * numDeps);
	
	va_list ap;
	va_start(ap, numDeps);
	
	int i;
	for (i=0; i<numDeps; i++)
	{
		insn->deps[i] = va_arg(ap, CodeGenInsn*);
		if (insn->deps[i] == NULL)
		{
			i--;
			numDeps--;
		};
	};
	
	insn->numDeps = numDeps;
	insn->debugstr = debugstr;
	va_end(ap);
	
	int index = block->numInsn++;
	block->insn = (CodeGenInsn**) realloc(block->insn, sizeof(void*) * block->numInsn);
	block->insn[index] = insn;
	
	return insn;
};

static int isIntegerType(int type)
{
	switch (type)
	{
	case CTIR_I8:
	case CTIR_I16:
	case CTIR_I32:
	case CTIR_I64:
	case CTIR_U8:
	case CTIR_U16:
	case CTIR_U32:
	case CTIR_U64:
		return 1;
	default:
		return 0;
	};
};

static const char* getMovForType(int type)
{
	switch (type)
	{
	case CTIR_I8:
	case CTIR_I16:
	case CTIR_I32:
	case CTIR_I64:
	case CTIR_U8:
	case CTIR_U16:
	case CTIR_U32:
	case CTIR_U64:
		return "mov";
	case CTIR_F32:
		return "movss";
	case CTIR_F64:
		return "movsd";
	default:
		fprintf(stderr, "x86_64: i don't know how to move type `%d'\n", type);
		abort();
		return NULL;
	};
};

static void pushConst(x86_64_FuncData *funcData, CodeGenBlock *block, int resultReg, uint64_t constVal, int type)
{
	// mov <resultReg>, <constValue>
	x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
	x86->mnem = "mov";
		
	x86->ops[0].kind = OK_CTIR_REG;
	x86->ops[0].regNo = resultReg;
	
	x86->ops[1].kind = OK_CONST;
	x86->ops[1].constVal = constVal;
	
	char *debugstr;
	codegenFormat(&debugstr, "mov ${%d}, %lu", resultReg, (unsigned long) constVal);
	
	if (constVal == 0 && isIntegerType(type))
	{
		// moving zero into a register can be done as: xor <reg>, <reg>
		free(debugstr);
		x86->ops[1].kind = OK_CTIR_REG;
		x86->ops[1].regNo = resultReg;
		x86->mnem = "xor";
		codegenFormat(&debugstr, "xor ${%d}, ${%d}", resultReg, resultReg);
	};
	
	x86_64_RegInfo *reg = &funcData->regInfo[resultReg];
	reg->type = type;
	reg->definer = pushInsn(block, x86, debugstr, 1, U_ALU, 0);
};

static int getRelativeSize(int type)
{
	switch (type)
	{
	case CTIR_U8:
	case CTIR_U16:
	case CTIR_U32:
	case CTIR_U64:
		return type-CTIR_U8;
	case CTIR_I8:
	case CTIR_I16:
	case CTIR_I32:
	case CTIR_I64:
		return type-CTIR_I8;
	case CTIR_F32:
	case CTIR_F64:
		return type-CTIR_F32+3;
	default:
		return 0;
	};
};

static void pushConversion(x86_64_FuncData *funcData, CodeGenBlock *block, int resultReg, int srcReg, int type, int isSigned)
{
	// pretend we are unsigned if converting from larger type to smaller
	x86_64_RegInfo *reg = &funcData->regInfo[resultReg];
	x86_64_RegInfo *src = &funcData->regInfo[srcReg];
	
	if (!isIntegerType(src->type))
	{	
		// convert into integer
		x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
		switch (src->type)
		{
		case CTIR_F32:
			x86->mnem = "cvttss2si";
			if (type == CTIR_U64 || type == CTIR_I64) x86->mnem = "cvttss2sq";
			break;
		case CTIR_F64:
			x86->mnem = "cvttsd2si";
			if (type == CTIR_U64 || type == CTIR_I64) x86->mnem = "cvttsd2sq";
			break;
		default:
			fprintf(stderr, "x86_64: i don't know how to convert %d to integer\n", src->type);
			abort();
			break;
		};
		
		x86->ops[0].kind = OK_CTIR_REG;
		x86->ops[0].regNo = resultReg;
		x86->ops[0].aliasType = CTIR_U32;
		if (type == CTIR_U64 || type == CTIR_I64) x86->ops[0].aliasType = CTIR_U64;
		
		x86->ops[1].kind = OK_CTIR_REG;
		x86->ops[1].regNo = srcReg;
		
		char *debugstr;
		codegenFormat(&debugstr, "%s ${%d}, xmm0 ; clobber xmm0", x86->mnem, resultReg);
		
		CodeGenInsn *dep = pushInsn(block, x86, debugstr, 1, U_ALU, 1, src->definer);
		
		reg->type = type;
		reg->definer = dep;
		return;
	};
	
	int alias = 0;
	
	// mov(sx)(d) <resultReg>, <sourceReg>
	x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
	if (src->type == CTIR_I8 || src->type == CTIR_I16 || src->type == CTIR_I32 || src->type == CTIR_I64)
	{
		if (src->type == CTIR_I32 && type == CTIR_I64) x86->mnem = "movsxd";
		else x86->mnem = "movsx";
	}
	else
	{
		x86->mnem = "movzx";
		if ((src->type == CTIR_I32 && type == CTIR_I64) || (getRelativeSize(src->type) > getRelativeSize(type)))
		{
			x86->mnem = "mov";
			alias = 1;
		};
	};
	
	// if the types do not change, we don't do zero or sign extesion
	if (getRelativeSize(type) <= getRelativeSize(src->type))
	{
		x86->mnem = "mov";
		alias = 1;
	};
	
	x86->ops[0].kind = OK_CTIR_REG;
	x86->ops[0].regNo = resultReg;
	
	x86->ops[1].kind = OK_CTIR_REG;
	x86->ops[1].regNo = srcReg;
	if (alias) x86->ops[1].aliasType = type;
	
	char *debugstr;
	codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, resultReg, srcReg);
	
	reg->type = type;
	reg->definer = pushInsn(block, x86, debugstr, 1, U_ALU, 1, src->definer);
};

static const char* getFloatConversionMnem(int dest, int src)
{
	if (dest == CTIR_F32 && src == CTIR_F64)
	{
		return "cvtsd2ss";
	}
	else if (dest == CTIR_F64 && src == CTIR_F32)
	{
		return "cvtss2sd";
	}
	else if (dest == CTIR_F64 && isIntegerType(src))
	{
		if (src == CTIR_U64 || src == CTIR_I64) return "cvtsq2sd";
		return "cvtsi2sd";
	}
	else if (dest == CTIR_F32 && isIntegerType(src))
	{
		if (src == CTIR_U64 || src == CTIR_I64) return "cvtsq2ss";
		return "cvtsi2ss";
	}
	else
	{
		fprintf(stderr, "x86_64: i don't know how to convert from %d to %d\n", src, dest);
		abort();
		return NULL;
	};
};

static void pushFloatConversion(x86_64_FuncData *funcData, CodeGenBlock *block, int resultReg, int srcReg, int type)
{
	// pretend we are unsigned if converting from larger type to smaller
	x86_64_RegInfo *reg = &funcData->regInfo[resultReg];
	x86_64_RegInfo *src = &funcData->regInfo[srcReg];

	reg->type = type;
	if (type == src->type) return;
	
	int aliasAs = 0;
	CodeGenInsn *dep = src->definer;
	if (src->type == CTIR_U16 || src->type == CTIR_U8 || src->type == CTIR_I16 || src->type == CTIR_I8)
	{
		// sign-extend or zero-extend first
		// we can do it in-place (on the register itself)
		// because it doesn't change the 16-bit value
		x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
		x86->mnem = "movzx";
		if (src->type == CTIR_I16 || src->type == CTIR_I8) x86->mnem = "movsx";
		
		x86->ops[0].kind = OK_CTIR_REG;
		x86->ops[0].regNo = srcReg;
		x86->ops[0].aliasType = CTIR_I32;
		
		x86->ops[1].kind = OK_CTIR_REG;
		x86->ops[1].regNo = srcReg;
		
		char *debugstr;
		codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, srcReg, srcReg);
		
		dep = pushInsn(block, x86, debugstr, 1, U_ALU, 1, src->definer);
		aliasAs = CTIR_I32;
	};
	
	// <conversionInsn> <resultReg>, <sourceReg>
	x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
	x86->mnem = getFloatConversionMnem(type, src->type);
	assert(x86->mnem != NULL);
	
	x86->ops[0].kind = OK_CTIR_REG;
	x86->ops[0].regNo = resultReg;
	
	x86->ops[1].kind = OK_CTIR_REG;
	x86->ops[1].regNo = srcReg;
	if (aliasAs) x86->ops[1].aliasType = CTIR_U32;
	
	char *debugstr;
	codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, resultReg, srcReg);
	
	reg->definer = pushInsn(block, x86, debugstr, 1, U_ALU, 1, dep);
	if (dep != src->definer) dep->schedNext = reg->definer;
};

static const char* getReturnReg(int type)
{
	switch (type)
	{
	case CTIR_I8:
	case CTIR_U8:
		return "al";
	case CTIR_I16:
	case CTIR_U16:
		return "ax";
	case CTIR_I32:
	case CTIR_U32:
		return "eax";
	case CTIR_I64:
	case CTIR_U64:
		return "rax";
	case CTIR_F32:
	case CTIR_F64:
		return "xmm0";
	default:
		fprintf(stderr, "x86_64: cannot determine return register\n");
		abort();
		return NULL;
	};
};

static const char* getAddMnemForType(int type)
{
	switch (type)
	{
	case CTIR_F32:
		return "addss";
	case CTIR_F64:
		return "addsd";
	default:
		return "add";
	};
};

static const char* getSubMnemForType(int type)
{
	switch (type)
	{
	case CTIR_F32:
		return "subss";
	case CTIR_F64:
		return "subsd";
	default:
		return "sub";
	};
};

static const char* getMulMnemForType(int type)
{
	switch (type)
	{
	case CTIR_F32:
		return "mulss";
	case CTIR_F64:
		return "mulsd";
	default:
		return "imul";
	};
};

static CodeGenInsn* genPhiControl(CodeGenInsn *curr, CodeGenBlock *block, CTIR_Func *func, CTIR_Block *src, CTIR_Block *dest)
{
	// figure out which regs we are defining
	int defRegs[func->numRegsUsed];
	memset(defRegs, 0, sizeof(defRegs));
	
	int i;
	for (i=0; i<src->numCode; i++)
	{
		defRegs[src->code[i].resultReg] = 1;
	};
	
	if (src->index == 0)
	{
		for (i=0; i<func->args.numSubs; i++)
		{
			defRegs[i] = 1;
		};
	};
	
	// scan through every 'phi' spec in the destination, and emit a 'mov' to its destination
	for (i=0; i<dest->numCode; i++)
	{
		CTIR_Insn *insn = &dest->code[i];
		if (insn->opcode == CTIR_phi)
		{
			x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
			x86->mnem = "mov"; // TODO: ??
			
			x86->ops[0].kind = OK_CTIR_REG;
			x86->ops[0].regNo = insn->resultReg;
			
			x86->ops[1].kind = OK_CTIR_REG;
			
			x86->ctirDepRegA = insn->resultReg;
			
			int j;
			for (j=0; j<insn->numOps; j++)
			{
				if (defRegs[insn->ops[j].regNo])
				{
					x86->ops[1].regNo = insn->ops[j].regNo;
					x86->ctirDepRegB = insn->ops[j].regNo;
					break;
				};
			};
			
			CodeGenInsn *next = pushInsn(block, x86, strdup("nah"), 1, U_ALU, 1, curr);
			curr->schedNext = next;
			curr = next;
		};
	};
	
	return curr;
};

static CodeGenBlock* genBlock(CodeGen *codegen, CTIR_Func *func, CTIR_Block *block)
{
	CodeGenBlock *out = (CodeGenBlock*) calloc(1, sizeof(CodeGenBlock));
	out->label = block->label;
	
	x86_64_FuncData *funcData = (x86_64_FuncData*) func->cgdata;
	
	int haveFence = 0;
	CodeGenInsn *lastFence = NULL;
	
	int i;
	for (i=0; i<block->numCode; i++)
	{
		CTIR_Insn *insn = &block->code[i];
		
		switch (insn->opcode)
		{
		case CTIR_nop:
			// do nothing
			break;
		case CTIR_alloc:
			{
				size_t align = insn->ops[0].constVal;
				size_t size = insn->ops[1].constVal;
				
				if (align > 16)
				{
					lexDiag(insn->tok, CON_ERROR, "local variables cannot have an alignment stricter than 16 bytes (have %lu)\n", (unsigned long) align);
					return NULL;
				};
				
				if (align > 8)
				{
					funcData->needStackAlign16 = 1;
				};
				
				// align the stack
				funcData->frameSize = (funcData->frameSize + align - 1) & ~(align - 1);
				
				// define the register
				x86_64_RegInfo *reg = &funcData->regInfo[insn->resultReg];
				reg->type = CTIR_STACK;
				reg->offsetFromVarArea = funcData->frameSize;
				
				// increase the frame size
				funcData->frameSize += size;
			};
			break;
		case CTIR_store:
			{
				if (insn->ops[0].kind == CTIR_KIND_SYMREF)
				{
#if defined(CODEGEN_X86_64_SYSV) || defined(CODEGEN_X86_64_GLIDIX)
					// global stores are a full barrier
					CodeGenInsn **deps = (CodeGenInsn**) malloc(sizeof(void*) * out->numInsn);
					memcpy(deps, out->insn, sizeof(void*) * out->numInsn);
					int numDeps = out->numInsn;
					
					x86_64_RegInfo *src = &funcData->regInfo[insn->ops[1].regNo];
					int type = src->type;
					
					if (!insn->ops[0].isGlobal)
					{
						x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = getMovForType(type);
						
						x86->ops[0].kind = OK_MEMREF;
						x86->ops[0].sizeName = getSizeNameForType(type);
						assert(x86->ops[0].sizeName != NULL);
						x86->ops[0].regName = "rip";
						x86->ops[0].label = insn->ops[0].symbolName;

						x86->ops[1].kind = OK_CTIR_REG;
						x86->ops[1].regNo = insn->ops[1].regNo;
						
						char *debugstr;
						codegenFormat(&debugstr, "mov %s PTR %s[rip], ${%d}", x86->ops[0].sizeName, x86->ops[0].label, insn->ops[1].regNo);
						
						CodeGenInsn *insn = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, 1+haveFence, src->definer, lastFence);
						insn->deps = deps;
						insn->numDeps = numDeps;
					}
					else
					{
						// load GOT/INDIR entry into RAX
						char *gotLabel;
#ifdef CODEGEN_X86_64_SYSV
						codegenFormat(&gotLabel, "%s@GOTPCREL", insn->ops[0].symbolName);
#else
						codegenFormat(&gotLabel, "%s@INDIR", insn->ops[0].symbolName);
#endif
						x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = "mov";
						
						x86->ops[0].kind = OK_CPU_REG;
						x86->ops[0].regName = "rax";
						
						x86->ops[1].kind = OK_MEMREF;
						x86->ops[1].sizeName = "QWORD";
						x86->ops[1].regName = "rip";
						x86->ops[1].label = gotLabel;
						
						char *debugstr;
						codegenFormat(&debugstr, "mov rax, %s PTR %s[rip]", x86->ops[1].sizeName, x86->ops[1].label);
						
						CodeGenInsn *dep = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, 1+haveFence, src->definer, lastFence);
						dep->deps = deps;
						dep->numDeps = numDeps;
						
						// dereference RAX
						x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = getMovForType(type);
						
						x86->ops[0].kind = OK_MEMREF;
						x86->ops[0].sizeName = getSizeNameForType(type);
						assert(x86->ops[0].sizeName != NULL);
						x86->ops[0].regName = "rax";
						x86->ops[0].offset = 0;

						x86->ops[1].kind = OK_CTIR_REG;
						x86->ops[1].regNo = insn->ops[1].regNo;

						codegenFormat(&debugstr, "mov %s PTR [rax], ${%d}", x86->ops[0].sizeName, insn->ops[1].regNo);
						
						CodeGenInsn *insn = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, 1, dep);
						dep->schedNext = insn;
					};
#else
#error "I don't know how to load global variables!"
#endif
				}
				else if (insn->ops[0].kind == CTIR_KIND_REG)
				{
					x86_64_RegInfo *reg = &funcData->regInfo[insn->ops[0].regNo];
					x86_64_RegInfo *src = &funcData->regInfo[insn->ops[1].regNo];
					
					if (reg->type == CTIR_STACK)
					{
						// mov <sizeName> PTR <offset>[rsp], <source>
						x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = getMovForType(src->type);
						
						x86->ops[0].kind = OK_VAR_AREA;
						x86->ops[0].sizeName = getSizeNameForType(src->type);
						if (x86->ops[0].sizeName == NULL)
						{
							lexDiag(insn->tok, CON_ERROR, "cannot store value of the specified type");
							return NULL;
						};
						x86->ops[0].offset = reg->offsetFromVarArea;
						
						x86->ops[1].kind = OK_CTIR_REG;
						x86->ops[1].regNo = insn->ops[1].regNo;
						
						char *debugstr;
						codegenFormat(&debugstr, "mov %s PTR %lu[var_area], ${%d}", x86->ops[0].sizeName, (unsigned long) reg->offsetFromVarArea, insn->ops[1].regNo);
						reg->lastAccessor = pushInsn(out, x86, debugstr, 1, U_MEM_LOAD | U_ALU, 2, reg->lastAccessor, src->definer);
					}
					else if (reg->type == CTIR_U64)
					{
						// mov <sizeName> PTR [dest], <source>
						// mov <sizeName> PTR <offset>[rsp], <source>
						x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = getMovForType(src->type);
						
						x86->ops[0].kind = OK_MEMREF_CTIR;
						x86->ops[0].sizeName = getSizeNameForType(src->type);
						if (x86->ops[0].sizeName == NULL)
						{
							lexDiag(insn->tok, CON_ERROR, "cannot store value of the specified type");
							return NULL;
						};
						x86->ops[0].offset = 0;
						x86->ops[0].regNo = insn->ops[0].regNo;
						
						x86->ops[1].kind = OK_CTIR_REG;
						x86->ops[1].regNo = insn->ops[1].regNo;
						
						char *debugstr;
						codegenFormat(&debugstr, "mov %s PTR %lu[var_area], ${%d}", x86->ops[0].sizeName, (unsigned long) reg->offsetFromVarArea, insn->ops[1].regNo);
						reg->lastAccessor = pushInsn(out, x86, debugstr, 1, U_MEM_LOAD | U_ALU, 2, reg->lastAccessor, src->definer);
					}
					else
					{
						lexDiag(insn->tok, CON_ERROR, "register operand to `store' has invalid type");
						return NULL;
					};
				}
				else
				{
					lexDiag(insn->tok, CON_ERROR, "invalid location operand to `store'");
					return NULL;
				};
			};
			break;
		case CTIR_debug_var:
			// TODO (ignore for now)
			break;
		case CTIR_i8:
			if (insn->ops[0].kind == CTIR_KIND_CONST)
			{
				pushConst(funcData, out, insn->resultReg, insn->ops[0].constVal, CTIR_I8);
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				pushConversion(funcData, out, insn->resultReg, insn->ops[0].regNo, CTIR_I8, 1);
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "cannot convert source type into `i8'");
				return NULL;
			};
			break;
		case CTIR_i16:
			if (insn->ops[0].kind == CTIR_KIND_CONST)
			{
				pushConst(funcData, out, insn->resultReg, insn->ops[0].constVal, CTIR_I16);
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				pushConversion(funcData, out, insn->resultReg, insn->ops[0].regNo, CTIR_I16, 1);
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "cannot convert source type into `i16'");
				return NULL;
			};
			break;
		case CTIR_i32:
			if (insn->ops[0].kind == CTIR_KIND_CONST)
			{
				pushConst(funcData, out, insn->resultReg, insn->ops[0].constVal, CTIR_I32);
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				pushConversion(funcData, out, insn->resultReg, insn->ops[0].regNo, CTIR_I32, 1);
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "cannot convert source type into `i32'");
				return NULL;
			};
			break;
		case CTIR_i64:
			if (insn->ops[0].kind == CTIR_KIND_CONST)
			{
				pushConst(funcData, out, insn->resultReg, insn->ops[0].constVal, CTIR_I64);
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				pushConversion(funcData, out, insn->resultReg, insn->ops[0].regNo, CTIR_I64, 1);
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "cannot convert source type into `i64'");
				return NULL;
			};
			break;
		case CTIR_u8:
			if (insn->ops[0].kind == CTIR_KIND_CONST)
			{
				pushConst(funcData, out, insn->resultReg, insn->ops[0].constVal, CTIR_U8);
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				pushConversion(funcData, out, insn->resultReg, insn->ops[0].regNo, CTIR_U8, 0);
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "cannot convert source type into `u8'");
				return NULL;
			};
			break;
		case CTIR_u16:
			if (insn->ops[0].kind == CTIR_KIND_CONST)
			{
				pushConst(funcData, out, insn->resultReg, insn->ops[0].constVal, CTIR_U16);
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				pushConversion(funcData, out, insn->resultReg, insn->ops[0].regNo, CTIR_U16, 0);
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "cannot convert source type into `u16'");
				return NULL;
			};
			break;
		case CTIR_u32:
			if (insn->ops[0].kind == CTIR_KIND_CONST)
			{
				pushConst(funcData, out, insn->resultReg, insn->ops[0].constVal, CTIR_U32);
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				pushConversion(funcData, out, insn->resultReg, insn->ops[0].regNo, CTIR_U32, 0);
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "cannot convert source type into `u32'");
				return NULL;
			};
			break;
		case CTIR_u64:
			if (insn->ops[0].kind == CTIR_KIND_CONST)
			{
				pushConst(funcData, out, insn->resultReg, insn->ops[0].constVal, CTIR_U64);
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				pushConversion(funcData, out, insn->resultReg, insn->ops[0].regNo, CTIR_U64, 0);
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "cannot convert source type into `u64'");
				return NULL;
			};
			break;
		case CTIR_f64:
			if (insn->ops[0].kind == CTIR_KIND_CONST)
			{
				// spill the value to 'tempLabel' and then:
				// mov <resultReg>, QWORD PTR <tempLabel>[rip]
				const char *tempLabel = codegenLabel(codegen);
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = "movsd";
					
				x86->ops[0].kind = OK_CTIR_REG;
				x86->ops[0].regNo = insn->resultReg;
				
				x86->ops[1].kind = OK_MEMREF;
				x86->ops[1].regName = "rip";
				x86->ops[1].sizeName = "XMMWORD";
				x86->ops[1].label = tempLabel;
				
				char *debugstr;
				codegenFormat(&debugstr, "movsd ${%d}, XMMWORD PTR %s[rip]", insn->resultReg, tempLabel);
				
				x86_64_RegInfo *reg = &funcData->regInfo[insn->resultReg];
				reg->type = CTIR_F64;
				reg->definer = pushInsn(out, x86, debugstr, 1, U_ALU, 0);
				
				char *defline;
				codegenFormat(&defline, "\tdq %" PRIu64, insn->ops[0].constVal);
				codegenInlineConst(codegen, tempLabel, 8, defline);
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				pushFloatConversion(funcData, out, insn->resultReg, insn->ops[0].regNo, CTIR_F64);
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "cannot convert source type into `f64'");
				return NULL;
			};
			break;
		case CTIR_f32:
			if (insn->ops[0].kind == CTIR_KIND_CONST)
			{
				// spill the value to 'tempLabel' and then:
				// mov <resultReg>, QWORD PTR <tempLabel>[rip]
				const char *tempLabel = codegenLabel(codegen);
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = "movss";
					
				x86->ops[0].kind = OK_CTIR_REG;
				x86->ops[0].regNo = insn->resultReg;
				
				x86->ops[1].kind = OK_MEMREF;
				x86->ops[1].regName = "rip";
				x86->ops[1].sizeName = "XMMWORD";
				x86->ops[1].label = tempLabel;
				
				char *debugstr;
				codegenFormat(&debugstr, "movss ${%d}, XMMWORD PTR %s[rip]", insn->resultReg, tempLabel);
				
				x86_64_RegInfo *reg = &funcData->regInfo[insn->resultReg];
				reg->type = CTIR_F32;
				reg->definer = pushInsn(out, x86, debugstr, 1, U_ALU, 0);

				char *defline;
				codegenFormat(&defline, "\tdd %" PRIu32, (uint32_t) insn->ops[0].constVal);
				hashtabSet(codegen->inlineConst, tempLabel, defline);
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				pushFloatConversion(funcData, out, insn->resultReg, insn->ops[0].regNo, CTIR_F32);
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "cannot convert source type into `f32'");
				return NULL;
			};
			break;
		case CTIR_ret:
			{
				x86_64_RegInfo *reg = &funcData->regInfo[insn->ops[0].regNo];
				
				if (func->ret.kind == CTIR_KIND_TUPLE)
				{
					// TODO
					lexDiag(insn->tok, CON_ERROR, "tuple returns are not yet implemented");
					return NULL;
				}
				else if (func->ret.typeSpec != reg->type)
				{
					lexDiag(insn->tok, CON_ERROR, "mismatch between register type and function return type");
					return NULL;
				}
				else
				{
					// mov <returnReg>, <returnValue>
					// jmp <epilog>
					x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = getMovForType(reg->type);
					
					x86->ops[0].kind = OK_CPU_REG;
					x86->ops[0].regName = getReturnReg(reg->type);
					
					x86->ops[1].kind = OK_CTIR_REG;
					x86->ops[1].regNo = insn->ops[0].regNo;
					
					char *debugstr;
					codegenFormat(&debugstr, "mov %s, ${%d} ; jmp %s", x86->ops[0].regName, insn->ops[0].regNo, funcData->epilogLabel);
					
					// 'ret' depends on ALL instructions to complete: so make its
					// dependency list be the entire current list of instructions
					CodeGenInsn **deps = (CodeGenInsn**) malloc(sizeof(void*) * out->numInsn);
					memcpy(deps, out->insn, sizeof(void*) * out->numInsn);
					int numDeps = out->numInsn;
					CodeGenInsn *reti = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, 0);
					free(reti->deps);
					reti->deps = deps;
					reti->numDeps = numDeps;
				};
			};
			break;
		case CTIR_soft_fence:
		case CTIR_hard_fence:
			{
				// (mfence)
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				if (insn->opcode == CTIR_soft_fence) x86->mnem = "";
				else x86->mnem = "mfence";
				
				char *debugstr;
				codegenFormat(&debugstr, "%s ; fence", x86->mnem);
				
				// the point of a fence is that it depends on all instructions which came before it
				CodeGenInsn **deps = (CodeGenInsn**) malloc(sizeof(void*) * out->numInsn);
				memcpy(deps, out->insn, sizeof(void*) * out->numInsn);
				int numDeps = out->numInsn;
				CodeGenInsn *fence = pushInsn(out, x86, debugstr, 0, 0, 0);
				free(fence->deps);
				fence->deps = deps;
				fence->numDeps = numDeps;
				haveFence = 1;
				lastFence = fence;
				
				// make sure no memory-accessing instructions can be scheduled before us,
				// by making us the last accessor and definer of all current registers
				int j;
				for (j=0; j<func->numRegsUsed; j++)
				{
					funcData->regInfo[j].definer = fence;
					funcData->regInfo[j].lastAccessor = fence;
				};
			};
			break;
		case CTIR_addrof:
			if (insn->ops[0].kind == CTIR_KIND_SYMREF)
			{
#if defined(CODEGEN_X86_64_SYSV) || defined(CODEGEN_X86_64_GLIDIX)
				x86_64_RegInfo *dest = &funcData->regInfo[insn->resultReg];
				dest->type = CTIR_U64;
				
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				char *debugstr;
				
				if (!insn->ops[0].isGlobal)
				{
					x86->mnem = "lea";
					
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = insn->resultReg;
					
					x86->ops[1].kind = OK_MEMREF;
					x86->ops[1].sizeName = "QWORD";
					x86->ops[1].regName = "rip";
					x86->ops[1].label = insn->ops[0].symbolName;
					
					codegenFormat(&debugstr, "lea ${%d}, %s PTR %s[rip]", insn->resultReg, x86->ops[1].sizeName, x86->ops[1].label);
				}
				else
				{
					// load it from the GOT
					char *gotLabel;
#ifdef CODEGEN_X86_64_SYSV
					codegenFormat(&gotLabel, "%s@GOTPCREL", insn->ops[0].symbolName);
#else
					codegenFormat(&gotLabel, "%s@INDIR", insn->ops[0].symbolName);
#endif
					x86->mnem = "mov";
					
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = insn->resultReg;
					
					x86->ops[1].kind = OK_MEMREF;
					x86->ops[1].sizeName = "QWORD";
					x86->ops[1].regName = "rip";
					x86->ops[1].label = gotLabel;

					codegenFormat(&debugstr, "mov ${%d}, %s PTR %s[rip]", insn->resultReg, x86->ops[1].sizeName, x86->ops[1].label);
				};
				
				CodeGenInsn *insn = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, 0);
				dest->definer = insn;
#else
#error "I don't know how to calculate addresses of global variables!"
#endif
			}
			else if (insn->ops[0].kind == CTIR_KIND_REG)
			{
				x86_64_RegInfo *reg = &funcData->regInfo[insn->ops[0].regNo];
				if (reg->type != CTIR_STACK)
				{
					lexDiag(insn->tok, CON_ERROR, "register location operand to `addrof' does not refer to a `stack' type");
					return NULL;
				};
				
				int type = CTIR_U64;
				x86_64_RegInfo *dest = &funcData->regInfo[insn->resultReg];
				dest->type = type;
				
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = "lea";
				
				x86->ops[0].kind = OK_CTIR_REG;
				x86->ops[0].regNo = insn->resultReg;
				
				x86->ops[1].kind = OK_VAR_AREA;
				x86->ops[1].sizeName = "QWORD";
				assert(x86->ops[1].sizeName != NULL);
				x86->ops[1].offset = reg->offsetFromVarArea;
				
				char *debugstr;
				codegenFormat(&debugstr, "lea ${%d}, [var_area + %d]", insn->resultReg, reg->offsetFromVarArea);
				
				CodeGenInsn *insn = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, 1, reg->lastAccessor);
				dest->definer = insn;
			}
			else
			{
				lexDiag(insn->tok, CON_ERROR, "x86_64: i don't know how to calculate the address of this");
				return NULL;
			};
			break;
		case CTIR_load:
			{
				if (insn->ops[0].kind == CTIR_KIND_SYMREF)
				{
#if defined(CODEGEN_X86_64_SYSV) || defined(CODEGEN_X86_64_GLIDIX)
					// global loads are a full barrier
					CodeGenInsn **deps = (CodeGenInsn**) malloc(sizeof(void*) * out->numInsn);
					memcpy(deps, out->insn, sizeof(void*) * out->numInsn);
					int numDeps = out->numInsn;
					
					int type = insn->ops[1].typeSpec;
					x86_64_RegInfo *dest = &funcData->regInfo[insn->resultReg];
					dest->type = type;
					
					if (!insn->ops[0].isGlobal)
					{
						x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = getMovForType(type);
						
						x86->ops[0].kind = OK_CTIR_REG;
						x86->ops[0].regNo = insn->resultReg;
						
						x86->ops[1].kind = OK_MEMREF;
						x86->ops[1].sizeName = getSizeNameForType(type);
						assert(x86->ops[1].sizeName != NULL);
						x86->ops[1].regName = "rip";
						x86->ops[1].label = insn->ops[0].symbolName;
						
						char *debugstr;
						codegenFormat(&debugstr, "mov ${%d}, %s PTR %s[rip]", insn->resultReg, x86->ops[1].sizeName, x86->ops[1].label);
						
						CodeGenInsn *insn = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, haveFence, lastFence);
						dest->definer = insn;
						insn->deps = deps;
						insn->numDeps = numDeps;
					}
					else
					{
						// load GOT entry into RAX
						char *gotLabel;
#ifdef CODEGEN_X86_64_SYSV
						codegenFormat(&gotLabel, "%s@GOTPCREL", insn->ops[0].symbolName);
#else
						codegenFormat(&gotLabel, "%s@INDIR", insn->ops[0].symbolName);
#endif
						
						x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = "mov";
						
						x86->ops[0].kind = OK_CPU_REG;
						x86->ops[0].regName = "rax";
						
						x86->ops[1].kind = OK_MEMREF;
						x86->ops[1].sizeName = "QWORD";
						x86->ops[1].regName = "rip";
						x86->ops[1].label = gotLabel;
						
						char *debugstr;
						codegenFormat(&debugstr, "mov rax, %s PTR %s[rip]", x86->ops[1].sizeName, x86->ops[1].label);
						
						CodeGenInsn *dep = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, haveFence, lastFence);
						dep->deps = deps;
						dep->numDeps = numDeps;
						
						// dereference RAX
						x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = getMovForType(type);
						
						x86->ops[0].kind = OK_CTIR_REG;
						x86->ops[0].regNo = insn->resultReg;
						
						x86->ops[1].kind = OK_MEMREF;
						x86->ops[1].sizeName = getSizeNameForType(type);
						assert(x86->ops[1].sizeName != NULL);
						x86->ops[1].regName = "rax";
						x86->ops[1].offset = 0;
						
						codegenFormat(&debugstr, "mov ${%d}, %s PTR [rax]", insn->resultReg, x86->ops[1].sizeName);
						
						CodeGenInsn *insn = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, 1, dep);
						dep->schedNext = insn;
						dest->definer = insn;
					};
#else
#error "I don't know how to load global variables!"
#endif
				}
				else if (insn->ops[0].kind == CTIR_KIND_REG)
				{
					x86_64_RegInfo *reg = &funcData->regInfo[insn->ops[0].regNo];
					if (reg->type == CTIR_STACK)
					{
						int type = insn->ops[1].typeSpec;
						x86_64_RegInfo *dest = &funcData->regInfo[insn->resultReg];
						dest->type = type;
						
						x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = getMovForType(type);
						
						x86->ops[0].kind = OK_CTIR_REG;
						x86->ops[0].regNo = insn->resultReg;
						
						x86->ops[1].kind = OK_VAR_AREA;
						x86->ops[1].sizeName = getSizeNameForType(type);
						assert(x86->ops[1].sizeName != NULL);
						x86->ops[1].offset = reg->offsetFromVarArea;
						
						char *debugstr;
						codegenFormat(&debugstr, "mov ${%d}, [var_area + %d]", insn->resultReg, reg->offsetFromVarArea);
						
						CodeGenInsn *insn = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, 1, reg->lastAccessor);
						dest->definer = insn;
						reg->lastAccessor = insn;
					}
					else if (reg->type == CTIR_U64)
					{
						int type = insn->ops[1].typeSpec;
						x86_64_RegInfo *dest = &funcData->regInfo[insn->resultReg];
						dest->type = type;
						
						x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = getMovForType(type);
						
						x86->ops[0].kind = OK_CTIR_REG;
						x86->ops[0].regNo = insn->resultReg;
						
						x86->ops[1].kind = OK_MEMREF_CTIR;
						x86->ops[1].sizeName = getSizeNameForType(type);
						assert(x86->ops[1].sizeName != NULL);
						x86->ops[1].offset = 0;
						x86->ops[1].regNo = insn->ops[0].regNo;
						
						char *debugstr;
						codegenFormat(&debugstr, "mov ${%d}, [var_area + %d]", insn->resultReg, reg->offsetFromVarArea);
						
						CodeGenInsn *insn = pushInsn(out, x86, debugstr, 8, U_MEM_LOAD | U_ALU, 1, reg->lastAccessor);
						dest->definer = insn;
						reg->lastAccessor = insn;
					}
					else
					{
						lexDiag(insn->tok, CON_ERROR, "register operand to `load' has invalid type");
						return NULL;
					};
				}
				else
				{
					lexDiag(insn->tok, CON_ERROR, "invalid subject of a load");
					return NULL;
				};
			};
			break;
		case CTIR_add:
			{
				if (insn->ops[0].kind != CTIR_KIND_REG || insn->ops[1].kind != CTIR_KIND_REG)
				{
					lexDiag(insn->tok, CON_ERROR, "`add' expects registers as both operands");
					return NULL;
				};
				
				if (insn->resultReg == 0)
				{
					lexDiag(insn->tok, CON_ERROR, "`add' with discarded result");
					return NULL;
				};
				
				x86_64_RegInfo *left = &funcData->regInfo[insn->ops[0].regNo];
				x86_64_RegInfo *right = &funcData->regInfo[insn->ops[1].regNo];
				x86_64_RegInfo *dest = &funcData->regInfo[insn->resultReg];
				
				if (left->type != right->type)
				{
					lexDiag(insn->tok, CON_ERROR, "`add' between mismatched types");
					return NULL;
				};
				
				dest->type = left->type;
				
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = getMovForType(left->type);
				
				x86->ops[0].kind = OK_CTIR_REG;
				x86->ops[0].regNo = insn->resultReg;
				
				x86->ops[1].kind = OK_CTIR_REG;
				x86->ops[1].regNo = insn->ops[0].regNo;
				
				x86->ctirDepRegA = insn->resultReg;
				x86->ctirDepRegB = insn->ops[0].regNo;

				char *debugstr;
				codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, insn->resultReg, insn->ops[1].regNo);

				CodeGenInsn *mov = pushInsn(out, x86, debugstr, 1, U_ALU, 2, left->definer, right->definer);

				x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = getAddMnemForType(left->type);
				
				x86->ops[0].kind = OK_CTIR_REG;
				x86->ops[0].regNo = insn->resultReg;
			
				x86->ops[1].kind = OK_CTIR_REG;
				x86->ops[1].regNo = insn->ops[1].regNo;
				
				codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, insn->resultReg, insn->ops[1].regNo);
				
				dest->definer = pushInsn(out, x86, debugstr, 1, U_ALU, 1, mov);
			};
			break;
		case CTIR_sub:
			{
				if (insn->ops[0].kind != CTIR_KIND_REG || insn->ops[1].kind != CTIR_KIND_REG)
				{
					lexDiag(insn->tok, CON_ERROR, "`sub' expects registers as both operands");
					return NULL;
				};
				
				if (insn->resultReg == 0)
				{
					lexDiag(insn->tok, CON_ERROR, "`sub' with discarded result");
					return NULL;
				};
				
				x86_64_RegInfo *left = &funcData->regInfo[insn->ops[0].regNo];
				x86_64_RegInfo *right = &funcData->regInfo[insn->ops[1].regNo];
				x86_64_RegInfo *dest = &funcData->regInfo[insn->resultReg];
				
				if (left->type != right->type)
				{
					lexDiag(insn->tok, CON_ERROR, "`sub' between mismatched types");
					return NULL;
				};
				
				dest->type = left->type;
				
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = getMovForType(left->type);
				
				x86->ops[0].kind = OK_CTIR_REG;
				x86->ops[0].regNo = insn->resultReg;
				
				x86->ops[1].kind = OK_CTIR_REG;
				x86->ops[1].regNo = insn->ops[0].regNo;
				
				x86->ctirDepRegA = insn->resultReg;
				x86->ctirDepRegB = insn->ops[0].regNo;

				char *debugstr;
				codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, insn->resultReg, insn->ops[1].regNo);

				CodeGenInsn *mov = pushInsn(out, x86, debugstr, 1, U_ALU, 2, left->definer, right->definer);

				x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = getSubMnemForType(left->type);
				
				x86->ops[0].kind = OK_CTIR_REG;
				x86->ops[0].regNo = insn->resultReg;
			
				x86->ops[1].kind = OK_CTIR_REG;
				x86->ops[1].regNo = insn->ops[1].regNo;
				
				codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, insn->resultReg, insn->ops[1].regNo);
				
				dest->definer = pushInsn(out, x86, debugstr, 1, U_ALU, 1, mov);
			};
			break;
		case CTIR_mul:
			{
				if (insn->ops[0].kind != CTIR_KIND_REG || insn->ops[1].kind != CTIR_KIND_REG)
				{
					lexDiag(insn->tok, CON_ERROR, "`mul' expects registers as both operands");
					return NULL;
				};
				
				if (insn->resultReg == 0)
				{
					lexDiag(insn->tok, CON_ERROR, "`mul' with discarded result");
					return NULL;
				};
				
				x86_64_RegInfo *left = &funcData->regInfo[insn->ops[0].regNo];
				x86_64_RegInfo *right = &funcData->regInfo[insn->ops[1].regNo];
				x86_64_RegInfo *dest = &funcData->regInfo[insn->resultReg];
				
				if (left->type != right->type)
				{
					lexDiag(insn->tok, CON_ERROR, "`mul' between mismatched types");
					return NULL;
				};
				
				dest->type = left->type;
				
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = getMovForType(left->type);
				
				x86->ops[0].kind = OK_CTIR_REG;
				x86->ops[0].regNo = insn->resultReg;
				
				x86->ops[1].kind = OK_CTIR_REG;
				x86->ops[1].regNo = insn->ops[0].regNo;
				
				x86->ctirDepRegA = insn->resultReg;
				x86->ctirDepRegB = insn->ops[0].regNo;

				char *debugstr;
				codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, insn->resultReg, insn->ops[1].regNo);

				CodeGenInsn *mov = pushInsn(out, x86, debugstr, 1, U_ALU, 2, left->definer, right->definer);

				x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = getMulMnemForType(left->type);
				
				x86->ops[0].kind = OK_CTIR_REG;
				x86->ops[0].regNo = insn->resultReg;
			
				x86->ops[1].kind = OK_CTIR_REG;
				x86->ops[1].regNo = insn->ops[1].regNo;
				
				codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, insn->resultReg, insn->ops[1].regNo);
				
				dest->definer = pushInsn(out, x86, debugstr, 1, U_ALU, 1, mov);
			};
			break;
		case CTIR_div:
		case CTIR_mod:
			{
				if (insn->ops[0].kind != CTIR_KIND_REG || insn->ops[1].kind != CTIR_KIND_REG)
				{
					lexDiag(insn->tok, CON_ERROR, "`div' expects registers as both operands");
					return NULL;
				};
				
				if (insn->resultReg == 0)
				{
					lexDiag(insn->tok, CON_ERROR, "`div' with discarded result");
					return NULL;
				};
				
				x86_64_RegInfo *left = &funcData->regInfo[insn->ops[0].regNo];
				x86_64_RegInfo *right = &funcData->regInfo[insn->ops[1].regNo];
				x86_64_RegInfo *dest = &funcData->regInfo[insn->resultReg];
				
				if (left->type != right->type)
				{
					lexDiag(insn->tok, CON_ERROR, "`div' between mismatched types");
					return NULL;
				};
				
				dest->type = left->type;
				
				if (dest->type == CTIR_F32 || dest->type == CTIR_F64)
				{
					x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = getMovForType(dest->type);
					
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = insn->resultReg;
					
					x86->ops[1].kind = OK_CTIR_REG;
					x86->ops[1].regNo = insn->ops[0].regNo;
					
					x86->ctirDepRegA = insn->resultReg;
					x86->ctirDepRegB = insn->ops[0].regNo;

					char *debugstr;
					codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, insn->resultReg, insn->ops[1].regNo);

					CodeGenInsn *mov = pushInsn(out, x86, debugstr, 1, U_ALU, 2, left->definer, right->definer);

					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "divss";
					if (dest->type == CTIR_F64) x86->mnem = "divsd";
					
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = insn->resultReg;
				
					x86->ops[1].kind = OK_CTIR_REG;
					x86->ops[1].regNo = insn->ops[1].regNo;
					
					codegenFormat(&debugstr, "%s ${%d}, ${%d}", x86->mnem, insn->resultReg, insn->ops[1].regNo);
					
					dest->definer = pushInsn(out, x86, debugstr, 1, U_ALU, 1, mov);
				}
				else if (dest->type == CTIR_I32 || dest->type == CTIR_I64)
				{
					uint64_t clobbers = (1 << R_RDX);
					
					// mov eax, <a>
					x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "mov";
					
					x86->ops[0].kind = OK_CPU_REG;
					x86->ops[0].regName = "eax";
					if (dest->type == CTIR_I64) x86->ops[0].regName = "rax";
					
					x86->ops[1].kind = OK_CTIR_REG;
					x86->ops[1].regNo = insn->ops[0].regNo;
					
					x86->clobbers = clobbers;
					
					char *debugstr;
					codegenFormat(&debugstr, "mov %s, ${%d}", x86->ops[0].regName, x86->ops[1].regNo);
					
					CodeGenInsn *mov = pushInsn(out, x86, debugstr, 1, U_ALU, 2, left->definer, right->definer);
					
					// extend into RDX:RAX / EDX:EAX
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "cdq";
					if (dest->type == CTIR_I64) x86->mnem = "cqo";
					
					x86->clobbers = clobbers;
					
					CodeGenInsn *ext = pushInsn(out, x86, strdup(x86->mnem), 1, U_ALU, 1, mov);
					mov->schedNext = ext;
					
					// divide by <b>
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "idiv";
					
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = insn->ops[1].regNo;
					
					x86->clobbers = clobbers;
					
					codegenFormat(&debugstr, "idiv ${%d}", x86->ops[0].regNo);
					CodeGenInsn *div = pushInsn(out, x86, debugstr, 1, U_ALU, 1, ext);
					ext->schedNext = div;
					
					// mov the result (EAX/RAX) into destination
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "mov";
					
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = insn->resultReg;

					x86->ops[1].kind = OK_CPU_REG;
					x86->ops[1].regName = "eax";
					if (dest->type == CTIR_I64) x86->ops[0].regName = "rax";
					
					if (insn->opcode == CTIR_mod)
					{
						x86->ops[1].regName = "edx";
						if (dest->type == CTIR_I64) x86->ops[1].regName = "rdx";
					};
					
					x86->clobbers = clobbers;
					
					codegenFormat(&debugstr, "mov ${%d}, %s", x86->ops[0].regNo, x86->ops[1].regName);
					mov = pushInsn(out, x86, debugstr, 1, U_ALU, 1, div);
					div->schedNext = mov;
					
					dest->definer = mov;
				}
				else if (dest->type == CTIR_U32 || dest->type == CTIR_U64)
				{
					uint64_t clobbers = (1 << R_RDX);
					
					// mov eax, <a>
					x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "mov";
					
					x86->ops[0].kind = OK_CPU_REG;
					x86->ops[0].regName = "eax";
					if (dest->type == CTIR_I64) x86->ops[0].regName = "rax";
					
					x86->ops[1].kind = OK_CTIR_REG;
					x86->ops[1].regNo = insn->ops[0].regNo;
					
					x86->clobbers = clobbers;
					
					char *debugstr;
					codegenFormat(&debugstr, "mov %s, ${%d}", x86->ops[0].regName, x86->ops[1].regNo);
					
					CodeGenInsn *mov = pushInsn(out, x86, debugstr, 1, U_ALU, 2, left->definer, right->definer);
					
					// zero-extend into RDX:RAX / EDX:EAX (by zeroing out EDX/RDX)
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "xor edx, edx";
					
					x86->clobbers = clobbers;
					
					CodeGenInsn *ext = pushInsn(out, x86, strdup(x86->mnem), 1, U_ALU, 1, mov);
					mov->schedNext = ext;
					
					// divide by <b>
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "div";
					
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = insn->ops[1].regNo;
					
					x86->clobbers = clobbers;
					
					codegenFormat(&debugstr, "div ${%d}", x86->ops[0].regNo);
					CodeGenInsn *div = pushInsn(out, x86, debugstr, 1, U_ALU, 1, ext);
					ext->schedNext = div;
					
					// mov the result (EAX/RAX) into destination
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "mov";
					
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = insn->resultReg;

					x86->ops[1].kind = OK_CPU_REG;
					x86->ops[1].regName = "eax";
					if (dest->type == CTIR_U64) x86->ops[1].regName = "rax";

					if (insn->opcode == CTIR_mod)
					{
						x86->ops[1].regName = "edx";
						if (dest->type == CTIR_U64) x86->ops[1].regName = "rdx";
					};

					x86->clobbers = clobbers;
					
					codegenFormat(&debugstr, "mov ${%d}, %s", x86->ops[0].regNo, x86->ops[1].regName);
					mov = pushInsn(out, x86, debugstr, 1, U_ALU, 1, div);
					div->schedNext = mov;
					
					dest->definer = mov;
				}
				else
				{
					lexDiag(insn->tok, CON_ERROR, "x86_64: division on unsupported type");
					return NULL;
				};
			};
			break;
		case CTIR_call:
			{
				// calls must be followed by all instructions preceding them
				CodeGenInsn **deps = (CodeGenInsn**) malloc(sizeof(void*) * out->numInsn);
				memcpy(deps, out->insn, sizeof(void*) * out->numInsn);
				int numDeps = out->numInsn;
				
#if defined(CODEGEN_X86_64_SYSV) || defined(CODEGEN_X86_64_GLIDIX)
				funcData->needStackAlign16 = 1;
				
				// allocate the arguments into registers and/or stack as necessary
				int intRegs[6];
				int numIntRegs = 0;
				int floatRegs[8];
				int numFloatRegs = 0;
				int stackRegs[insn->numOps];		// (upper limit)
				int numStackRegs = 0;
				
				int j;
				for (j=2; j<insn->numOps; j++)
				{
					if (insn->ops[j].kind != CTIR_KIND_REG)
					{
						lexDiag(insn->tok, CON_ERROR, "i don't know how to pass non-CTIR-reg arguments yet");
						return NULL;
					};
					
					int regNo = insn->ops[j].regNo;
					x86_64_RegInfo *info = &funcData->regInfo[regNo];
					
					if (isIntegerType(info->type))
					{
						if (numIntRegs == 6)
						{
							stackRegs[numStackRegs++] = regNo;
						}
						else
						{
							intRegs[numIntRegs++] = regNo;
						};
					}
					else
					{
						if (numFloatRegs == 8)
						{
							stackRegs[numStackRegs++] = regNo;
						}
						else
						{
							floatRegs[numFloatRegs++] = regNo;
						};
					};
				};
				
				uint64_t clobbers = 0;
				CodeGenInsn *lastInsn = NULL;
				
				// first, push the stack arguments right-to-left
				// TODO: instead of forcing the program to address via RBP, make a mechanism
				// for stack adjustment!
				int stackAdjust = 0;
				if (numStackRegs != 0)
				{
					funcData->frameOffset = -1;
					
					stackAdjust = 8 * numStackRegs;
					if (stackAdjust % 16) stackAdjust += 8;		// make sure the stack remains 16-byte-aligned
					
					x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "sub";
					
					char *suffix;
					codegenFormat(&suffix, " rsp, %d", stackAdjust);
					x86->suffix = suffix;
					
					x86->clobbers = clobbers;
					
					CodeGenInsn *push = pushInsn(out, x86, strdup("can't be bothered"), 1, U_ALU, 1, lastInsn);
					if (lastInsn == NULL)
					{
						free(push->deps);
						push->deps = deps;
						push->numDeps = numDeps;
					}
					else
					{
						lastInsn->schedNext = push;
					};

					lastInsn = push;
					
					for (j=0; j<numStackRegs; j++)
					{
						int regNo = stackRegs[j];
						x86_64_RegInfo *info = &funcData->regInfo[regNo];
						
						x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
						x86->mnem = getMovForType(info->type);
						
						x86->ops[0].kind = OK_MEMREF;
						x86->ops[0].sizeName = getSizeNameForType(info->type);
						x86->ops[0].offset = j * 8;
						x86->ops[0].regName = "rsp";
						
						x86->ops[1].kind = OK_CTIR_REG;
						x86->ops[1].regNo = regNo;
						
						CodeGenInsn *insn = pushInsn(out, x86, strdup("can't be bothered"), 1, U_ALU, 1, lastInsn);
						lastInsn->schedNext = insn;
						lastInsn = insn;
					};
				};
				
				// now the int arguments
				while (numIntRegs)
				{
					static int cpuregs[6] = {R_RDI, R_RSI, R_RDX, R_RCX, R_R8, R_R9};
					int regNo = intRegs[--numIntRegs];
					x86_64_RegInfo *info = &funcData->regInfo[regNo];
					
					clobbers |= (1UL << cpuregs[numIntRegs]);
					
					x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "mov";
					
					x86->ops[0].kind = OK_CPU_REG;
					x86->ops[0].regName = regTable[cpuregs[numIntRegs]][info->type];
					
					x86->ops[1].kind = OK_CTIR_REG;
					x86->ops[1].regNo = regNo;
					
					x86->ctirDepFixed = regNo;
					x86->prefReg = cpuregs[numIntRegs];
					
					x86->clobbers = clobbers;
					
					CodeGenInsn *insn = pushInsn(out, x86, strdup("can't be bothered"), 1, U_ALU, 1, lastInsn);
					if (lastInsn == NULL)
					{
						free(insn->deps);
						insn->deps = deps;
						insn->numDeps = numDeps;
					}
					else
					{
						lastInsn->schedNext = insn;
					};
					
					lastInsn = insn;
				};
				
				// now the float arguments
				int eax = numFloatRegs;
				while (numFloatRegs)
				{
					int regNo = floatRegs[--numFloatRegs];
					x86_64_RegInfo *info = &funcData->regInfo[regNo];
					
					int cpureg = R_XMM0 + numFloatRegs;
					clobbers |= (1UL << cpureg);
					
					x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = getMovForType(info->type);
					
					x86->ops[0].kind = OK_CPU_REG;
					x86->ops[0].regName = regTable[cpureg][info->type];
					
					x86->ops[1].kind = OK_CTIR_REG;
					x86->ops[1].regNo = regNo;
					
					x86->ctirDepFixed = regNo;
					x86->prefReg = cpureg;
					
					x86->clobbers = clobbers;
					
					CodeGenInsn *insn = pushInsn(out, x86, strdup("can't be bothered"), 1, U_ALU, 1, lastInsn);
					if (lastInsn == NULL)
					{
						free(insn->deps);
						insn->deps = deps;
						insn->numDeps = numDeps;
					}
					else
					{
						lastInsn->schedNext = insn;
					};
					
					lastInsn = insn;
				};
				
				// number of float arguments must be stored in eax
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				if (eax == 0)
				{
					x86->mnem = "xor";
					x86->suffix = " eax, eax";
				}
				else
				{
					x86->mnem = "mov";
					char *suffix;
					codegenFormat(&suffix, " eax, %d", eax);
					x86->suffix = suffix;
				};
				
				CodeGenInsn *seteax = pushInsn(out, x86, strdup("no."), 1, U_ALU, 1, lastInsn);
				if (lastInsn == NULL)
				{
					free(seteax->deps);
					seteax->deps = deps;
					seteax->numDeps = numDeps;
				}
				else
				{
					lastInsn->schedNext = seteax;
				};
				
				// perform the actual call
				x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = "call";
				
				if (insn->ops[1].kind == CTIR_KIND_SYMREF)
				{
					char *suffix;
					
					if (insn->ops[1].isGlobal)
					{
#ifdef CODEGEN_X86_64_SYSV
						codegenFormat(&suffix, " DWORD OFFSET %s@PLT", insn->ops[1].symbolName);
#else
						codegenFormat(&suffix, " QWORD PTR %s@INDIR[rip]", insn->ops[1].symbolName);
#endif
					}
					else
					{
						codegenFormat(&suffix, " DWORD OFFSET %s", insn->ops[1].symbolName);
					};
					
					x86->suffix = suffix;
				}
				else
				{
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = insn->ops[1].regNo;
				};
				
				clobbers |= (~X86_64_PRESV_REGS);
				x86->clobbers = clobbers;
				
				CodeGenInsn *call = pushInsn(out, x86, strdup("no."), 1, U_ALU, 1, seteax);
				seteax->schedNext = call;
				lastInsn = call;
				
				if (stackAdjust != 0)
				{
					// pop off the arguments we've pushed onto the stack
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "add";
					
					char *suffix;
					codegenFormat(&suffix, " rsp, %d", stackAdjust);
					x86->suffix = suffix;
					
					CodeGenInsn *add = pushInsn(out, x86, strdup("no."), 1, U_ALU, 1, lastInsn);
					lastInsn->schedNext = add;
					lastInsn = add;
				};
				
				if (insn->resultReg != 0)
				{
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = getMovForType(insn->ops[0].typeSpec);
					
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = insn->resultReg;
					
					x86->ops[1].kind = OK_CPU_REG;
					x86->ops[1].regName = getReturnReg(insn->ops[0].typeSpec);
					
					x86->clobbers = clobbers;
					
					CodeGenInsn *mov = pushInsn(out, x86, strdup("no"), 1, U_ALU, 1, lastInsn);
					lastInsn->schedNext = mov;
					
					x86_64_RegInfo *dest = &funcData->regInfo[insn->resultReg];
					dest->definer = mov;
					dest->type = insn->ops[0].typeSpec;
				};
#else
#	error "I don't know the calling convention!"
#endif
			};
			break;
		case CTIR_jmp:
			{
				int blockref = insn->ops[0].blockIndex;
				CTIR_Block *target = func->blocks[blockref];
				
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = "";
				
				CodeGenInsn **deps = (CodeGenInsn**) malloc(sizeof(void*) * out->numInsn);
				memcpy(deps, out->insn, sizeof(void*) * out->numInsn);
				int numDeps = out->numInsn;
				CodeGenInsn *dummy = pushInsn(out, x86, strdup("dummy"), 0, 0, 0);
				free(dummy->deps);
				dummy->deps = deps;
				dummy->numDeps = numDeps;

				dummy = genPhiControl(dummy, out, func, block, target);
				
				if (blockref == 0 || func->blocks[blockref-1] != block)
				{
					x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "jmp";
					
					char *suffix;
					codegenFormat(&suffix, " DWORD OFFSET %s", target->label);
					x86->suffix = suffix;

					CodeGenInsn *jmp = pushInsn(out, x86, strdup("jmp"), 8, U_MEM_LOAD | U_ALU, 1, dummy);
					dummy->schedNext = jmp;
				};
			};
			break;
		case CTIR_select:
			{
				CTIR_Block *destTrue = func->blocks[insn->ops[1].blockIndex];
				CTIR_Block *destFalse = func->blocks[insn->ops[2].blockIndex];
				
				// first emit a "test" instruction for the subject
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = "test";
				
				x86->ops[0].kind = OK_CTIR_REG;
				x86->ops[0].regNo = insn->ops[0].regNo;
				x86->ops[1] = x86->ops[0];
				
				CodeGenInsn **deps = (CodeGenInsn**) malloc(sizeof(void*) * out->numInsn);
				memcpy(deps, out->insn, sizeof(void*) * out->numInsn);
				int numDeps = out->numInsn;
				CodeGenInsn *test = pushInsn(out, x86, strdup("test"), 1, U_ALU, 0);
				free(test->deps);
				test->deps = deps;
				test->numDeps = numDeps;
				
				CodeGenInsn *last = genPhiControl(test, out, func, block, destTrue);

				x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = "jnz";
				
				char *suffix;
				codegenFormat(&suffix, " DWORD OFFSET %s", destTrue->label);
				x86->suffix = suffix;

				CodeGenInsn *jmp = pushInsn(out, x86, strdup("jnz"), 8, U_MEM_LOAD | U_ALU, 1, last);
				last->schedNext = jmp;
				last = jmp;
				
				last = genPhiControl(last, out, func, block, destFalse);

				if (insn->ops[2].blockIndex == 0 || func->blocks[insn->ops[2].blockIndex-1] != block)
				{
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "jmp";
					
					codegenFormat(&suffix, " DWORD OFFSET %s", destFalse->label);
					x86->suffix = suffix;

					jmp = pushInsn(out, x86, strdup("jmp"), 8, U_MEM_LOAD | U_ALU, 1, last);
					last->schedNext = jmp;
					last = jmp;
				};
			};
			break;
		case CTIR_phi:
			{
				x86_64_Insn *dummy = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				dummy->mnem = "";
				
				int i;
				for (i=0; i<insn->numOps; i++)
				{
					if (funcData->regInfo[insn->ops[i].regNo].type != 0)
					{
						funcData->regInfo[insn->resultReg].type = funcData->regInfo[insn->ops[i].regNo].type;
						break;
					};
				};
				
				if (i == insn->numOps)
				{
					lexDiag(insn->tok, CON_ERROR, "cannot determine register type");
					return NULL;
				};
				
				funcData->regInfo[insn->resultReg].definer = pushInsn(out, dummy, strdup("dummy"), 0, 0, 0);
			};
			break;
		case CTIR_crash:
			{

				x86_64_Insn *dummy = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				dummy->mnem = "ud2";
				
				CodeGenInsn *crash = pushInsn(out, dummy, strdup("dummy"), 0, 0, 0);
				if (insn->resultReg != 0)
				{
					funcData->regInfo[insn->resultReg].type = insn->ops[0].typeSpec;
					funcData->regInfo[insn->resultReg].definer = crash;
				};
			};
			break;
		case CTIR_switch:
			{
				x86_64_Insn *x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
				x86->mnem = "";
				
				CodeGenInsn **deps = (CodeGenInsn**) malloc(sizeof(void*) * out->numInsn);
				memcpy(deps, out->insn, sizeof(void*) * out->numInsn);
				int numDeps = out->numInsn;
				CodeGenInsn *dummy = pushInsn(out, x86, strdup("dummy"), 1, U_ALU, 0);
				free(dummy->deps);
				dummy->deps = deps;
				dummy->numDeps = numDeps;
				
				int subject = insn->ops[0].regNo;
				CodeGenInsn *last = dummy;
				
				char *suffix;
				
				int i;
				for (i=2; i<insn->numOps; i+=2)
				{
					codegenFormat(&suffix, ", %" PRIi64, insn->ops[i].constVal);
					
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "cmp";
					
					x86->ops[0].kind = OK_CTIR_REG;
					x86->ops[0].regNo = subject;
					
					x86->suffix = suffix;
					last->schedNext = pushInsn(out, x86, strdup("cmp"), 1, U_ALU, 1, last);
					last = last->schedNext;
					
					CTIR_Block *dest = func->blocks[insn->ops[i+1].blockIndex];
					last = genPhiControl(last, out, func, block, dest);
					
					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "je";

					codegenFormat(&suffix, " DWORD OFFSET %s", dest->label);
					x86->suffix = suffix;
					
					last->schedNext = pushInsn(out, x86, strdup("je"), 1, U_ALU, 1, last);
					last = last->schedNext;
				};
				
				if (insn->ops[1].blockIndex == 0 || func->blocks[insn->ops[1].blockIndex-1] != block)
				{
					CTIR_Block *dest = func->blocks[insn->ops[1].blockIndex];
					last = genPhiControl(last, out, func, block, dest);

					x86 = (x86_64_Insn*) calloc(1, sizeof(x86_64_Insn));
					x86->mnem = "jmp";

					codegenFormat(&suffix, " DWORD OFFSET %s", dest->label);
					x86->suffix = suffix;
					
					last->schedNext = pushInsn(out, x86, strdup("jmp"), 1, U_ALU, 1, last);
				};
			};
			break;
		case CTIR_eq:
		case CTIR_neq:
		case CTIR_less:
		case CTIR_less_eq:
		case CTIR_greater:
		case CTIR_greater_eq:
			x86_64_gen_cmp(codegen, func, block, out, funcData, insn);
			break;
		case CTIR_shl:
		case CTIR_shr:
		case CTIR_sar:
			x86_64_gen_shift(codegen, func, block, out, funcData, insn);
			break;
		default:
			lexDiag(insn->tok, CON_ERROR, "cannot generate code: unrecognised CTIR opcode `%s'", ctirGetOpcodeName(insn->opcode));
			return NULL;
		};
	};
	
	return out;
};

static CodeGenGraph* codegen_x86_64_genfunc(CodeGen *codegen, CTIR_Func *func)
{
	CodeGenGraph *graph = codegenNewGraph(func->numBlocks);
	graph->presvRegs = X86_64_PRESV_REGS;
	
	x86_64_FuncData *funcData = (x86_64_FuncData*) calloc(1, sizeof(x86_64_FuncData));
	func->cgdata = funcData;
	
	funcData->frameSize = 0;
	funcData->regInfo = (x86_64_RegInfo*) calloc(func->numRegsUsed, sizeof(x86_64_RegInfo));
	funcData->epilogLabel = codegenLabel(codegen);
	
	int i;
	for (i=0; i<func->args.numSubs; i++)
	{
		if (func->args.subs[i].kind == CTIR_KIND_TYPESPEC)
		{
			funcData->regInfo[i+1].type = func->args.subs[i].typeSpec;
		}
		else
		{
			funcData->regInfo[i+1].type = CTIR_STACK;
		};
	};
	
	for (i=0; i<func->numBlocks; i++)
	{
		graph->blocks[i] = genBlock(codegen, func, func->blocks[i]);
		if (graph->blocks[i] == NULL) return NULL;
	};
	
	return graph;
};

static uint64_t getAllowedRegs(int type)
{
	switch (type)
	{
	case CTIR_I8:
	case CTIR_U8:
	case CTIR_I16:
	case CTIR_U16:
	case CTIR_I32:
	case CTIR_U32:
	case CTIR_I64:
	case CTIR_U64:
		return ((1 << R_RBX) | (1 << R_RCX) | (1 << R_RDX) | (1 << R_RSI) | (1 << R_RDI) /*| (1 << R_RBP)*/ | (1 << R_R8) | (1 << R_R9) | (1 << R_R10) | (1 << R_R11) | (1 << R_R12) | (1 << R_R13) | (1 << R_R14) | (1 << R_R15));
	case CTIR_F32:
	case CTIR_F64:
		return ((1 << R_XMM0) | (1 << R_XMM1) | (1 << R_XMM2) | (1 << R_XMM3) | (1 << R_XMM4) | (1 << R_XMM5) | (1 << R_XMM6) | (1 << R_XMM7) | (1 << R_XMM8) | (1 << R_XMM9) | (1 << R_XMM10) | (1 << R_XMM11) | (1 << R_XMM12) | (1 << R_XMM13) | (1 << R_XMM14));
	default:
		return 0;
	}
}

static void codegen_x86_64_genregs(CodeGen *codegen, CTIR_Func *func, int numScheds, CodeGenSched **scheds, CodeGenRegInfo *regs)
{
	x86_64_FuncData *funcData = (x86_64_FuncData*) func->cgdata;
	
	int i;
	for (i=0; i<func->numRegsUsed; i++)
	{
		regs[i].type = funcData->regInfo[i].type;
		regs[i].allowedRegs = getAllowedRegs(regs[i].type);
		regs[i].prefRegs = regs[i].allowedRegs & ~(X86_64_PRESV_REGS);
	};
	
	// specify argument locations
#if defined(CODEGEN_X86_64_SYSV) || defined(CODEGEN_X86_64_GLIDIX)
	static const int intRegs[6] = {R_RDI, R_RSI, R_RDX, R_RCX, R_R8, R_R9};
	int currentFrameOffset = 8;
	int numIntRegs = 0;
	int numFloatRegs = 0;
	
	for (i=0; i<func->args.numSubs; i++)
	{
		regs[i+1].bornAt = 1;	// argument registers are born at the beginning of the function
		regs[i+1].deadAt = 1;
		
		if (func->args.subs[i].kind == CTIR_KIND_TYPESPEC)
		{
			switch (func->args.subs[i].typeSpec)
			{
			case CTIR_I8:
			case CTIR_I16:
			case CTIR_I32:
			case CTIR_I64:
			case CTIR_U8:
			case CTIR_U16:
			case CTIR_U32:
			case CTIR_U64:
				if (numIntRegs < 6)
				{
					regs[i+1].cpureg = intRegs[numIntRegs++];
					regs[i+1].precolor = regs[i+1].cpureg;
				}
				else
				{
					regs[i+1].cpureg = -1;
					regs[i+1].location = currentFrameOffset;
					currentFrameOffset += 8;
				};
				break;
			case CTIR_F32:
			case CTIR_F64:
				if (numFloatRegs < 8)
				{
					regs[i+1].cpureg = R_XMM0 + (numFloatRegs++);
					regs[i+1].precolor = regs[i+1].cpureg;
				}
				else
				{
					regs[i+1].cpureg = -1;
					regs[i+1].location = currentFrameOffset;
					currentFrameOffset += 8;
				};
				break;
			default:
				fprintf(stderr, "x86_64: i don't know which register to put this in!");
				abort();
				break;
			}
		}
		else
		{
			fprintf(stderr, "x86_64: i haven't implemented this yet!");
			abort();
		}
	}
#else
#	error "I don't know the calling convention!"
#endif
	
	for (i=0; i<numScheds; i++)
	{
		CodeGenSched *sched = scheds[i];
		
		int j;
		for (j=0; j<sched->numInsn; j++)
		{
			uint64_t point = ((uint64_t) (i+1) << 32) | (uint64_t) j;
			CodeGenInsn *insn = sched->insn[j];
			x86_64_Insn *x86 = (x86_64_Insn*) insn->drvdata;
			
			int k;
			for (k=0; k<2; k++)
			{
				if (x86->ops[k].kind == OK_CTIR_REG)
				{
					int regNo = x86->ops[k].regNo;
					if (regs[regNo].bornAt == 0) regs[regNo].bornAt = point;
					regs[regNo].deadAt = point;
				};
			};
		};
	};
	
	// disallow clobbered registers
	for (i=0; i<numScheds; i++)
	{
		CodeGenSched *sched = scheds[i];
		
		int j;
		for (j=0; j<sched->numInsn; j++)
		{
			uint64_t point = ((uint64_t) (i+1) << 32) | (uint64_t) j;
			CodeGenInsn *insn = sched->insn[j];
			x86_64_Insn *x86 = (x86_64_Insn*) insn->drvdata;
			
			if (x86->clobbers != 0)
			{
				int k;
				for (k=0; k<func->numRegsUsed; k++)
				{
					if (regs[k].allowedRegs != 0 && regs[k].bornAt <= point && regs[k].deadAt > point)
					{
						regs[k].allowedRegs &= ~(x86->clobbers);
						regs[k].prefRegs &= ~(x86->clobbers);
					};
				};
			};
		};
	};
};

static int codegen_x86_64_genspill(CTIR_Func *func)
{
	x86_64_FuncData *funcData = (x86_64_FuncData*) func->cgdata;
	funcData->lastSpillLocation -= 8;
	return funcData->lastSpillLocation;
};

static void codegen_x86_64_genprolog(CodeGen *codegen, CTIR_Func *func, CodeGenRegInfo *regs)
{
	As *as = codegen->as;
	x86_64_FuncData *funcData = (x86_64_FuncData*) func->cgdata;
	
	if (funcData->frameOffset == -1)
	{
		asLine(as, "\tpush rbp");
		asLine(as, "\tmov rbp, rsp");
	};
	
	char *line;
	if (funcData->lastSpillLocation != 0)
	{
		codegenFormat(&line, "\tsub rsp, %d", -funcData->lastSpillLocation);
		asLine(as, line);
		free(line);
	};
	
	uint64_t usedRegs = 0;
	int i;
	for (i=0; i<func->numRegsUsed; i++)
	{
		if (regs[i].cpureg > 0)
		{
			usedRegs |= (1UL << regs[i].cpureg);
		};
	};
	
	int varAreaFromRBP = -funcData->lastSpillLocation;
	uint64_t saveRegs = X86_64_PRESV_REGS & usedRegs;
	for (i=0; i<64; i++)
	{
		if (saveRegs & (1UL << i))
		{
			codegenFormat(&line, "\tpush %s", cpuRegNames[i]);
			asLine(as, line);
			free(line);
			varAreaFromRBP -= 8;
		};
	};
	
	if (funcData->needStackAlign16)
	{
		// make sure the stack becomes 16-byte-aligned after creating the variable area
		uint64_t toAdd = 16 - (((-varAreaFromRBP) + funcData->frameSize) % 16);
		if (funcData->frameOffset != -1)
		{
			// we didn't do a "push rbp" at the beginning, so we must add 8 more
			// bytes in order to align the stack
			if (toAdd > 8)
			{
				toAdd -= 8;
			}
			else
			{
				toAdd += 8;
			};
		};
		funcData->frameSize += (toAdd % 16);
	}
	else
	{
		// we only need 8 bytes of alignment
		funcData->frameSize = (funcData->frameSize + 7) & ~7;
	};
	
	if (funcData->frameSize != 0)
	{
		codegenFormat(&line, "\tsub rsp, %lu", (unsigned long) funcData->frameSize);
		asLine(as, line);
		free(line);
		varAreaFromRBP -= funcData->frameSize;
	};
	
	if (funcData->frameOffset != -1) funcData->frameOffset = -varAreaFromRBP;
	funcData->saveRegs = saveRegs;
	funcData->varAreaFromRBP = varAreaFromRBP;
	
	// relocate pre-colored registers if needed
	for (i=1; i<func->numRegsUsed; i++)
	{
		if (regs[i].precolor != 0 && regs[i].precolor != regs[i].cpureg)
		{
			if (regs[i].cpureg != -1)
			{
				codegenFormat(&line, "\tmov %s, %s", regTable[regs[i].cpureg][funcData->regInfo[i].type], regTable[regs[i].precolor][funcData->regInfo[i].type]);
				asLine(as, line);
				free(line);
			}
			else
			{
				const char *sizeName;
				const char *mnem;
				int faketype;
				if (isIntegerType(regs[i].type))
				{
					sizeName = "QWORD";
					mnem = "mov";
					faketype = CTIR_U64;
				}
				else
				{
					sizeName = "XMMWORD";
					mnem = getMovForType(regs[i].type);
					faketype = regs[i].type;
				};
				
				const char *refreg;
				int offset;
				if (funcData->frameOffset == -1)
				{
					refreg = "rbp";
					offset = regs[i].location;
				}
				else
				{
					refreg = "rsp";
					offset = regs[i].location + funcData->frameOffset;
				};
				
				codegenFormat(&line, "\t%s %s PTR %d[%s], %s", mnem, sizeName, offset, refreg, regTable[regs[i].precolor][faketype]);
				asLine(as, line);
				free(line);
			};
		};
	};
};

static void codegen_x86_64_genepilog(CodeGen *codegen, CTIR_Func *func, CodeGenRegInfo *regs)
{
	As *as = codegen->as;
	x86_64_FuncData *funcData = (x86_64_FuncData*) func->cgdata;
	
	char *line;
	codegenFormat(&line, "%s:", funcData->epilogLabel);
	asLine(as, line);
	free(line);
	
	if (func->attr & CTIR_FA_NORETURN)
	{
		// we don't need an epilog if the function never returns. just to increase the chance of a crash
		// and not some other adverse effect if undefined behaviour is invoked, just add an undefined
		// instruction to the epilog.
		asLine(as, "\tud2");
		return;
	};
	
	if (funcData->frameSize != 0)
	{
		codegenFormat(&line, "\tadd rsp, %lu", (unsigned long) funcData->frameSize);
		asLine(as, line);
		free(line);
	};
	
	int i;
	uint64_t saveRegs = funcData->saveRegs;
	for (i=63; i>=0; i--)
	{
		if (saveRegs & (1UL << i))
		{
			codegenFormat(&line, "\tpop %s", cpuRegNames[i]);
			asLine(as, line);
			free(line);
		};
	};
	
	if (funcData->frameOffset == -1)
	{
		asLine(as, "\tmov rsp, rbp");
		asLine(as, "\tpop rbp");
	}
	else
	{
		if (funcData->lastSpillLocation != 0)
		{
			codegenFormat(&line, "\tadd rsp, %lu", (unsigned long) (-funcData->lastSpillLocation));
			asLine(as, line);
			free(line);
		};
	};
	
	asLine(as, "\tret");
};

static char* formatOperand(x86_64_Operand *op)
{
	char *result;
	switch (op->kind)
	{
	case OK_NONE:
		result = NULL;
		break;
	case OK_CPU_REG:
		result = strdup(op->regName);
		break;
	case OK_MEMREF:
		if (op->label == NULL) codegenFormat(&result, "%s PTR %ld[%s]", op->sizeName, (long) op->offset, op->regName);
		else codegenFormat(&result, "%s PTR %s[%s]", op->sizeName, op->label, op->regName);
		break;
	case OK_CONST:
		codegenFormat(&result, "%lu", (unsigned long) op->constVal);
		break;
	default:
		fprintf(stderr, "x86_64: failed to format operand\n");
		abort();
		break;
	};
	
	return result;
};

static const char* getRaxForType(int type)
{
	switch (type)
	{
	case CTIR_U8:
	case CTIR_I8:
		return "al";
	case CTIR_U16:
	case CTIR_I16:
		return "ax";
	case CTIR_I32:
	case CTIR_U32:
		return "eax";
	case CTIR_I64:
	case CTIR_U64:
		return "rax";
	default:
		abort();
		return "(invalid)";
	};
};

static void codegen_x86_64_genblock(CodeGen *codegen, CTIR_Func *func, CodeGenRegInfo *regs, CodeGenSched *sched, int blockIndex)
{
	As *as = codegen->as;
	x86_64_FuncData *funcData = (x86_64_FuncData*) func->cgdata;
	
	char *line;
	
	int i;
	for (i=0; i<sched->numInsn; i++)
	{
		CodeGenInsn *insn = sched->insn[i];
		x86_64_Insn *x86 = (x86_64_Insn*) insn->drvdata;
		
		if (x86->ctirDepRegA != 0 && x86->ctirDepRegB != 0)
		{
			if (regs[x86->ctirDepRegA].cpureg == regs[x86->ctirDepRegB].cpureg)
			{
				if (regs[x86->ctirDepRegA].cpureg != -1 || (regs[x86->ctirDepRegA].location == regs[x86->ctirDepRegB].location))
				{
					// instruction can be skipped
					continue;
				};
			};
		};
		
		if (x86->ctirDepFixed != 0)
		{
			if (regs[x86->ctirDepFixed].cpureg == x86->prefReg)
			{
				// instruction can be skipped
				continue;
			};
		};
		
		// first adjust each operand
		char *comment = strdup("\t;");
		
		int j;
		for (j=0; j<2; j++)
		{
			x86_64_Operand *op = &x86->ops[j];
			
			if (op->kind == OK_CTIR_REG)
			{
				CodeGenRegInfo *info = &regs[op->regNo];
				int type = info->type;
				if (op->aliasType != 0) type = op->aliasType;
				
				if (info->cpureg == -1)
				{
					if (x86->ops[!j].kind == OK_MEMREF)
					{
						const char *refreg = "rbp";
						int offset = info->location;
						
						if (funcData->frameOffset != -1)
						{
							refreg = "rsp";
							offset += funcData->frameOffset;
						};
						
						if (isIntegerType(info->type))
						{
							codegenFormat(&line, "\tmov rax, QWORD PTR %d[%s]", offset, refreg);
							asLine(as, line);
							free(line);
							
							op->kind = OK_CPU_REG;
							op->regName = getRaxForType(info->type);
						}
						else
						{
							codegenFormat(&line, "\t%s xmm15, XMMWORD PTR %d[%s]", getMovForType(info->type), offset, refreg);
							asLine(as, line);
							free(line);
							
							op->kind = OK_CPU_REG;
							op->regName = "xmm15";
						};
					}
					else
					{
						op->kind = OK_MEMREF;
						op->sizeName = getSizeNameForType(type);
						assert(op->sizeName != NULL);
						op->regName = "rbp";
						op->offset = info->location;
						
						if (funcData->frameOffset != -1)
						{
							op->regName = "rsp";
							op->offset += funcData->frameOffset;
						};
					};
				}
				else
				{
					op->kind = OK_CPU_REG;
					op->regName = regTable[info->cpureg][type];
				};

				char *newComment;
				codegenFormat(&newComment, "%s\t${%d}", comment, op->regNo);
				free(comment);
				comment = newComment;
			}
			else if (op->kind == OK_MEMREF_CTIR)
			{
				CodeGenRegInfo *info = &regs[op->regNo];
				if (info->cpureg == -1)
				{
					const char *refreg = "rbp";
					int offset = info->location;
					
					if (funcData->frameOffset != -1)
					{
						refreg = "rsp";
						offset += funcData->frameOffset;
					};
					
					codegenFormat(&line, "\tmov rax, QWORD PTR %d[%s]", offset, refreg);
					asLine(as, line);
					free(line);
					
					op->kind = OK_MEMREF;
					op->regName = "rax";
				}
				else
				{
					op->kind = OK_MEMREF;
					op->regName = regTable[info->cpureg][CTIR_U64];
				};
			}
			else if (op->kind == OK_VAR_AREA)
			{
				char *newComment;
				codegenFormat(&newComment, "%s\t[var_area+%lu]", comment, (unsigned long) op->offset);
				free(comment);
				comment = newComment;

				if (funcData->frameOffset == -1)
				{
					op->kind = OK_MEMREF;
					op->regName = "rbp";
					op->offset += funcData->varAreaFromRBP;
				}
				else
				{
					op->kind = OK_MEMREF;
					op->regName = "rsp";
					op->offset += funcData->varAreaFromRBP + funcData->frameOffset;
				};
			};
		};
		
		char *a = formatOperand(&x86->ops[0]);
		char *b = formatOperand(&x86->ops[1]);
		
		free(comment);
		comment = strdup("");
		
		const char *suffix = (x86->suffix != NULL) ? x86->suffix : "";
		
		if (a == NULL && b == NULL)
		{
			codegenFormat(&line, "\t%s%s %s", x86->mnem, suffix, comment);
		}
		else if (a != NULL && b == NULL)
		{
			codegenFormat(&line, "\t%s %s%s %s", x86->mnem, a, suffix, comment);
		}
		else
		{
			codegenFormat(&line, "\t%s %s, %s%s %s", x86->mnem, a, b, suffix, comment);
		};
		
		asLine(as, line);
		free(a);
		free(b);
		free(line);
		free(comment);
	};
	
	int fallThrough = 0;
	if (blockIndex == func->numBlocks-1)
	{
		fallThrough = 1;
	}
	else if (func->blocks[blockIndex]->numCode != 0)
	{
		CTIR_Insn *last = &func->blocks[blockIndex]->code[func->blocks[blockIndex]->numCode-1];
		int i;
		for (i=0; i<last->numOps; i++)
		{
			if (last->ops[i].kind == CTIR_KIND_BLOCKREF)
			{
				// if there is a blockref, then the code already generated must jump somewhere;
				// so we don't need to emit a jump ourselves
				fallThrough = 1;
				break;
			};
		};
	};
	
	// CTIR functions return when the end of a block is reached without a branch
	// the last instruction might be RET. for x86_64, the 'ret' instruction does
	// nothing other than store the result in the correct register. thus, in either
	// case, we must simply append a jump to the epilog at the end of each block;
	// except that the last block falls through to the epilog, so we don't need it
	if (!fallThrough)
	{
		codegenFormat(&line, "\tjmp DWORD OFFSET %s", funcData->epilogLabel);
		asLine(as, line);
		free(line);
	};
};

static void codegen_x86_64_gendata(CodeGen *codegen, CTIR_Data *data)
{
	const char *mnem;
	
	switch (data->type)
	{
	case CTIR_U8:
	case CTIR_I8:
		mnem = "db";
		break;
	case CTIR_U16:
	case CTIR_I16:
		mnem = "dw";
		break;
	case CTIR_U32:
	case CTIR_I32:
	case CTIR_F32:
		mnem = "dd";
		break;
	case CTIR_U64:
	case CTIR_I64:
	case CTIR_F64:
		mnem = "dq";
		break;
	default:
		mnem = NULL;
		break;
	};
	
	assert(mnem != NULL);
	
	char *line;
	codegenFormat(&line, "\t%s %" PRIu64, mnem, data->val);
	asLine(codegen->as, line);
	free(line);
};

static void codegen_x86_64_genoptions(OptionList *list)
{
#if defined(CODEGEN_X86_64_SYSV)
	optAdd(list, "-mno-red-zone", "",
		"Forbid the use of the red zone.",
		OPT_FLAG, &mnoRedZone);
#endif
};

static Error codegen_x86_64_init(CodeGen *codegen, const void *opt)
{
	return ERR_OK;
};

CodeGenDriver codegenDriver_x86_64 = {
	.name = "x86_64",
	.regNames = X86_64_REG_NAMES,
	.init = codegen_x86_64_init,
	.genfunc = codegen_x86_64_genfunc,
	.genregs = codegen_x86_64_genregs,
	.genspill = codegen_x86_64_genspill,
	.genprolog = codegen_x86_64_genprolog,
	.genepilog = codegen_x86_64_genepilog,
	.genblock = codegen_x86_64_genblock,
	.gendata = codegen_x86_64_gendata,
	.genoptions = codegen_x86_64_genoptions,
};
