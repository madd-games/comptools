/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>
#include <stdlib.h>

#include <comptools/elf64.h>
#include <comptools/as-x86.h>
#include <comptools/elf64-x86_64.h>

/**
 * ELF interpreter.
 */
#ifndef ELF64_INTERP
#define	ELF64_INTERP			"/lib64/ld-linux-x86-64.so.2"
#endif

/**
 * Machine number.
 */
#define	EM_X86_64			62

/**
 * Relocation types for x86_64.
 */
#define	R_X86_64_NONE			0
#define	R_X86_64_64			1
#define	R_X86_64_PC32			2
#define	R_X86_64_GOT32			3
#define	R_X86_64_PLT32			4
#define	R_X86_64_COPY			5
#define	R_X86_64_GLOB_DAT		6
#define	R_X86_64_JUMP_SLOT		7
#define	R_X86_64_RELATIVE		8
#define	R_X86_64_GOTPCREL		9
#define	R_X86_64_32			10
#define	R_X86_64_32S			11
#define	R_X86_64_16			12
#define	R_X86_64_PC16			13
#define	R_X86_64_8			14
#define	R_X86_64_PC8			15
#define	R_X86_64_PC64			24
#define	R_X86_64_GOTOFF64		25
#define	R_X86_64_GOTPC32		26
#define	R_X86_64_SIZE32			32
#define	R_X86_64_SIZE64			33
#define	R_X86_64_IRELATIVE		37
#define	R_X86_64_GOTPCRELX		41
#define	R_X86_64_REX_GOTPCRELX		42

typedef struct
{
	char*			data;
	uint64_t		size;
} StringTable;

static uint64_t stralloc(StringTable *tab, const char *str)
{
	uint64_t result = tab->size;
	tab->data = (char*) realloc(tab->data, tab->size+strlen(str)+1);
	strcpy(&tab->data[tab->size], str);
	tab->size += strlen(str) + 1;
	return result;
};

static Error resolve_x86_64_none(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	return ERR_OK;
};

static Error resolve_x86_64_64(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{	
	ule64_t result = ule64(symval+addend);
	memcpy(put, &result, 8);
	return ERR_OK;
};

static Error resolve_x86_64_pc32(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{	
	int64_t fullval = symval + addend - (reloc->offset + reloc->sect->addr);
	int32_t val = (int32_t) fullval;
	
	if ((int64_t)val != fullval)
	{
		return ERR_RELOC_TRUNC;
	};
	
	sle32_t sleval = sle32(val);
	memcpy(put, &sleval, 4);
	return ERR_OK;
};

static Error resolve_x86_64_got32(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	Elf64_SymAux *aux = (Elf64_SymAux*) reloc->sym->aux;
	if (aux == NULL) return ERR_RELOC_TRUNC;
	
	int64_t fulladdr = aux->got + addend;
	int32_t value = (int32_t) fulladdr;
	
	if ((int64_t)value != fulladdr)
	{
		return ERR_RELOC_TRUNC;
	};
	
	sle32_t leval = sle32(value);
	memcpy(put, &leval, 4);
	return ERR_OK;
};

static Error resolve_x86_64_plt32(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	Elf64_SymAux *aux = (Elf64_SymAux*) reloc->sym->aux;
	if (aux == NULL) return ERR_RELOC_TRUNC;
	if (aux->plt == 0) return ERR_RELOC_TRUNC;
	
	int64_t fulladdr = aux->plt + addend - (reloc->offset + reloc->sect->addr);
	int32_t addr32 = (int32_t) fulladdr;
	
	if ((int64_t)addr32 != fulladdr)
	{
		return ERR_RELOC_TRUNC;
	};
	
	sle32_t leval = sle32(addr32);
	memcpy(put, &leval, 4);
	return ERR_OK;
};

static Error resolve_x86_64_copy(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	// this should not appear in standard object relocations
	return ERR_RELOC_TRUNC;
};

static Error resolve_x86_64_glob_dat(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	// this should not appear in standard object relocations
	return ERR_RELOC_TRUNC;
};

static Error resolve_x86_64_jump_slot(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	// this should not appear in standard object relocations
	return ERR_RELOC_TRUNC;
};

static Error resolve_x86_64_relative(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	// this should not appear in standard object relocations
	return ERR_RELOC_TRUNC;
};

static Error resolve_x86_64_gotpcrel(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	Elf64_SymAux *aux = (Elf64_SymAux*) reloc->sym->aux;
	if (aux == NULL) return ERR_RELOC_TRUNC;
	
	int64_t fulladdr = aux->got + aux->gotoff + addend - (reloc->offset + reloc->sect->addr);
	int32_t addr32 = (int32_t) fulladdr;
	
	if ((int64_t)addr32 != fulladdr)
	{
		return ERR_RELOC_TRUNC;
	};
	
	sle32_t leval = sle32(addr32);
	memcpy(put, &leval, 4);
	return ERR_OK;
};

static Error resolve_x86_64_32(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{	
	uint64_t fulladdr = symval + addend;
	if ((fulladdr & 0xFFFFFFFF) != fulladdr)
	{
		return ERR_RELOC_TRUNC;
	};
	
	ule32_t val = ule32(fulladdr);
	memcpy(put, &val, 4);
	return ERR_OK;
};

static Error resolve_x86_64_32s(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{	
	int64_t fulladdr = symval + addend;
	int32_t addr32 = (int32_t) fulladdr;
	
	if ((int64_t)addr32 != fulladdr)
	{
		return ERR_RELOC_TRUNC;
	};
	
	sle32_t val = sle32(addr32);
	memcpy(put, &val, 4);
	return ERR_OK;
};

static Error resolve_x86_64_16(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	uint64_t fulladdr = symval + addend;
	if ((fulladdr & 0xFFFF) != fulladdr)
	{
		return ERR_RELOC_TRUNC;
	};
	
	ule16_t val = ule16(fulladdr);
	memcpy(put, &val, 2);
	return ERR_OK;
};

static Error resolve_x86_64_pc16(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	int64_t fullval = symval + addend - (reloc->offset + reloc->sect->addr);
	int16_t val = (int16_t) fullval;
	
	if ((int64_t)val != fullval)
	{
		return ERR_RELOC_TRUNC;
	};
	
	sle16_t sleval = sle16(val);
	memcpy(put, &sleval, 2);
	return ERR_OK;
};

static Error resolve_x86_64_8(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	uint64_t fulladdr = symval + addend;
	if ((fulladdr & 0xFF) != fulladdr)
	{
		return ERR_RELOC_TRUNC;
	};
	
	uint8_t val = (uint8_t) fulladdr;
	memcpy(put, &val, 1);
	return ERR_OK;
};

static Error resolve_x86_64_pc8(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	int64_t fullval = symval + addend - (reloc->offset + reloc->sect->addr);
	int8_t val = (int8_t) fullval;
	
	if ((int64_t)val != fullval)
	{
		return ERR_RELOC_TRUNC;
	};
	
	memcpy(put, &val, 1);
	return ERR_OK;
};

static Error resolve_x86_64_pc64(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	ule64_t result = ule64(symval+addend-(reloc->sect->addr+reloc->offset));
	memcpy(put, &result, 8);
	return ERR_OK;
};

static Error resolve_x86_64_gotoff64(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	Elf64_SymAux *aux = (Elf64_SymAux*) reloc->sym->aux;
	if (aux == NULL) return ERR_RELOC_TRUNC;

	ule64_t result = ule64(symval+addend-aux->got);
	memcpy(put, &result, 8);
	return ERR_OK;
};

static Error resolve_x86_64_gotpc32(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	Elf64_SymAux *aux = (Elf64_SymAux*) reloc->sym->aux;
	if (aux == NULL) return ERR_RELOC_TRUNC;
	
	int64_t fulladdr = aux->got + addend - (reloc->sect->addr + reloc->offset);
	int32_t addr32 = (int32_t) fulladdr;
	
	if ((int64_t)addr32 != fulladdr)
	{
		return ERR_RELOC_TRUNC;
	};
	
	sle32_t val = sle32(addr32);
	memcpy(put, &val, 4);
	return ERR_OK;
};

static Error resolve_x86_64_size32(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{	
	int64_t fulladdr = reloc->sym->size;
	int32_t addr32 = (int32_t) fulladdr;
	
	if ((int64_t)addr32 != fulladdr)
	{
		return ERR_RELOC_TRUNC;
	};
	
	sle32_t val = sle32(addr32);
	memcpy(put, &val, 4);
	return ERR_OK;
};

static Error resolve_x86_64_size64(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	ule64_t val = ule64(reloc->sym->size);
	memcpy(put, &val, 8);
	return ERR_OK;
};

static Error resolve_x86_64_irelative(void *put, uint64_t symval, int64_t addend, Reloc *reloc)
{
	return ERR_RELOC_TRUNC;
};

RelocType rel_x86_64_none = {
	.resolve = resolve_x86_64_none,
	.name = "R_X86_64_NONE",
	.bytes = 0,
};

RelocType rel_x86_64_64 = {
	.resolve = resolve_x86_64_64,
	.name = "R_X86_64_64",
	.bytes = 8,
};

RelocType rel_x86_64_pc32 = {
	.resolve = resolve_x86_64_pc32,
	.name = "R_X86_64_PC32",
	.bytes = 4,
};

RelocType rel_x86_64_got32 = {
	.resolve = resolve_x86_64_got32,
	.name = "R_X86_64_GOT32",
	.bytes = 4,
};

RelocType rel_x86_64_plt32 = {
	.resolve = resolve_x86_64_plt32,
	.name = "R_X86_64_PLT32",
	.bytes = 4,
};

RelocType rel_x86_64_copy = {
	.resolve = resolve_x86_64_copy,
	.name = "R_X86_64_COPY",
	.bytes = 0,		/* N/A */
};

RelocType rel_x86_64_glob_dat = {
	.resolve = resolve_x86_64_glob_dat,
	.name = "R_X86_64_GLOB_DAT",
	.bytes = 8,
};

RelocType rel_x86_64_jump_slot = {
	.resolve = resolve_x86_64_jump_slot,
	.name = "R_X86_64_JUMP_SLOT",
	.bytes = 8,
};

RelocType rel_x86_64_relative = {
	.resolve = resolve_x86_64_relative,
	.name = "R_X86_64_RELATIVE",
	.bytes = 8,
};

RelocType rel_x86_64_gotpcrel = {
	.resolve = resolve_x86_64_gotpcrel,
	.name = "R_X86_64_GOTPCREL",
	.bytes = 4,
};

RelocType rel_x86_64_32 = {
	.resolve = resolve_x86_64_32,
	.name = "R_X86_64_32",
	.bytes = 4,
};

RelocType rel_x86_64_32s = {
	.resolve = resolve_x86_64_32s,
	.name = "R_X86_64_32S",
	.bytes = 4,
};

RelocType rel_x86_64_16 = {
	.resolve = resolve_x86_64_16,
	.name = "R_X86_64_16",
	.bytes = 2,
};

RelocType rel_x86_64_pc16 = {
	.resolve = resolve_x86_64_pc16,
	.name = "R_X86_64_PC16",
	.bytes = 2,
};

RelocType rel_x86_64_8 = {
	.resolve = resolve_x86_64_8,
	.name = "R_X86_64_8",
	.bytes = 1,
};

RelocType rel_x86_64_pc8 = {
	.resolve = resolve_x86_64_pc8,
	.name = "R_X86_64_PC8",
	.bytes = 1,
};

RelocType rel_x86_64_pc64 = {
	.resolve = resolve_x86_64_pc64,
	.name = "R_X86_64_PC64",
	.bytes = 8,
};

RelocType rel_x86_64_gotoff64 = {
	.resolve = resolve_x86_64_gotoff64,
	.name = "R_X86_64_GOTOFF64",
	.bytes = 8,
};

RelocType rel_x86_64_gotpc32 = {
	.resolve = resolve_x86_64_gotpc32,
	.name = "R_X86_64_GOTPC32",
	.bytes = 4,
};

RelocType rel_x86_64_size32 = {
	.resolve = resolve_x86_64_size32,
	.name = "R_X86_64_SIZE32",
	.bytes = 4,
};

RelocType rel_x86_64_size64 = {
	.resolve = resolve_x86_64_size64,
	.name = "R_X86_64_SIZE64",
	.bytes = 8,
};

RelocType rel_x86_64_irelative = {
	.resolve = resolve_x86_64_irelative,
	.name = "R_X86_64_IRELATIVE",
	.bytes = 8
};

static int elf64_x86_64_relocCompToElf_gen(RelocType *type)
{
	if (type == REL_X86_ABS8) return R_X86_64_8;
	else if (type == REL_X86_ABS16) return R_X86_64_16;
	else if (type == REL_X86_ABS32) return R_X86_64_32S;
	else if (type == REL_X86_ABS64) return R_X86_64_64;
	else if (type == REL_X86_REL16) return R_X86_64_PC16;
	else if (type == REL_X86_REL32) return R_X86_64_PC32;
	else if (type == &rel_x86_64_none) return R_X86_64_NONE;
	else if (type == &rel_x86_64_64) return R_X86_64_64;
	else if (type == &rel_x86_64_pc32) return R_X86_64_PC32;
	else if (type == &rel_x86_64_got32) return R_X86_64_GOT32;
	else if (type == &rel_x86_64_plt32) return R_X86_64_PLT32;
	else if (type == &rel_x86_64_copy) return R_X86_64_COPY;
	else if (type == &rel_x86_64_glob_dat) return R_X86_64_GLOB_DAT;
	else if (type == &rel_x86_64_jump_slot) return R_X86_64_JUMP_SLOT;
	else if (type == &rel_x86_64_relative) return R_X86_64_RELATIVE;
	else if (type == &rel_x86_64_gotpcrel) return R_X86_64_GOTPCREL;
	else if (type == &rel_x86_64_32) return R_X86_64_32;
	else if (type == &rel_x86_64_32s) return R_X86_64_32S;
	else if (type == &rel_x86_64_16) return R_X86_64_16;
	else if (type == &rel_x86_64_pc16) return R_X86_64_PC16;
	else if (type == &rel_x86_64_8) return R_X86_64_8;
	else if (type == &rel_x86_64_pc8) return R_X86_64_PC8;
	else if (type == &rel_x86_64_pc64) return R_X86_64_PC64;
	else if (type == &rel_x86_64_gotoff64) return R_X86_64_GOTOFF64;
	else if (type == &rel_x86_64_gotpc32) return R_X86_64_GOTPC32;
	else if (type == &rel_x86_64_size32) return R_X86_64_SIZE32;
	else if (type == &rel_x86_64_size64) return R_X86_64_SIZE64;
	else if (type == &rel_x86_64_irelative) return R_X86_64_IRELATIVE;
	else return -1;
};

static int elf64_x86_64_relocCompToElf_GOTPCREL(RelocType *type)
{
	if (type == REL_X86_REL32) return R_X86_64_GOTPCREL;
	else return -1;
};

static int elf64_x86_64_relocCompToElf_PLT(RelocType *type)
{
	if (type == REL_X86_REL32) return R_X86_64_PLT32;
	else return -1;
};

static int elf64_x86_64_relocCompToElf(RelocType *type, const char *prop)
{
	if (prop == NULL || prop[0] == 0)
	{
		return elf64_x86_64_relocCompToElf_gen(type);
	}
	else if (strcmp(prop, "GOTPCREL") == 0)
	{
		return elf64_x86_64_relocCompToElf_GOTPCREL(type);
	}
	else if (strcmp(prop, "PLT") == 0)
	{
		return elf64_x86_64_relocCompToElf_PLT(type);
	}
	else
	{
		return -1;
	};
};

static RelocType* elf64_x86_64_relocElfToComp(int type)
{
	switch (type)
	{
	case R_X86_64_NONE:		return &rel_x86_64_none;
	case R_X86_64_64:		return &rel_x86_64_64;
	case R_X86_64_PC32:		return &rel_x86_64_pc32;
	case R_X86_64_GOT32:		return &rel_x86_64_got32;
	case R_X86_64_PLT32:		return &rel_x86_64_plt32;
	case R_X86_64_COPY:		return &rel_x86_64_copy;
	case R_X86_64_GLOB_DAT:		return &rel_x86_64_glob_dat;
	case R_X86_64_JUMP_SLOT:	return &rel_x86_64_jump_slot;
	case R_X86_64_RELATIVE:		return &rel_x86_64_relative;
	case R_X86_64_GOTPCRELX:
	case R_X86_64_REX_GOTPCRELX:
	case R_X86_64_GOTPCREL:		return &rel_x86_64_gotpcrel;
	case R_X86_64_32:		return &rel_x86_64_32;
	case R_X86_64_32S:		return &rel_x86_64_32s;
	case R_X86_64_16:		return &rel_x86_64_16;
	case R_X86_64_PC16:		return &rel_x86_64_pc16;
	case R_X86_64_8:		return &rel_x86_64_8;
	case R_X86_64_PC8:		return &rel_x86_64_pc8;
	case R_X86_64_PC64:		return &rel_x86_64_pc64;
	case R_X86_64_GOTOFF64:		return &rel_x86_64_gotoff64;
	case R_X86_64_GOTPC32:		return &rel_x86_64_gotpc32;
	case R_X86_64_SIZE32:		return &rel_x86_64_size32;
	case R_X86_64_SIZE64:		return &rel_x86_64_size64;
	case R_X86_64_IRELATIVE:	return &rel_x86_64_irelative;
	default:			return NULL;
	}
};

static void elf64_addDyn(Section *sect, uint64_t tag, uint64_t value)
{
	Elf64_Dyn dyn;
	memset(&dyn, 0, sizeof(Elf64_Dyn));
	
	ctMakeLE(tag, &dyn.d_tag, sizeof(dyn.d_tag));
	ctMakeLE(value, &dyn.d_un, sizeof(dyn.d_un));
	
	objSectionAppend(sect, &dyn, sizeof(Elf64_Dyn));
};

static Error elf64_x86_64_preReloc(Object *obj)
{
	Elf64_DrvOptions *opt = (Elf64_DrvOptions*) obj->drvdata;
	
	// figure out if we need a GOT/PLT and whether we are PIC
	int pic = 0;
	int haveInterp = 0;
	if (obj->type == OBJTYPE_SHARED)
	{
		pic = 1;
		
		if (obj->entry != NULL)
		{
			Section *interp = objGetSection(obj, ".interp");
			if (interp != NULL)
			{
				fprintf(stderr, "elf64: .interp section already exists\n");
				return ERR_INVALID_PARAM;
			};
			interp = objCreateSection(obj, ".interp", SECTYPE_PROGBITS, SEC_READ | SEC_WRITE | SEC_ELF_DYN);
			interp->addr = (interp->addr + 0xFFF) & ~0xFFF;
			objSectionAppend(interp, ELF64_INTERP, strlen(ELF64_INTERP)+1);
			pic = 0;
			haveInterp = 1;
		};
	}
	else if (obj->type == OBJTYPE_EXEC && obj->numDepends != 0)
	{
		Section *interp = objGetSection(obj, ".interp");
		if (interp != NULL)
		{
			fprintf(stderr, "elf64: .interp section already exists\n");
			return ERR_INVALID_PARAM;
		};
		interp = objCreateSection(obj, ".interp", SECTYPE_PROGBITS, SEC_READ | SEC_WRITE | SEC_ELF_DYN);
		interp->addr = (interp->addr + 0xFFF) & ~0xFFF;
		objSectionAppend(interp, ELF64_INTERP, strlen(ELF64_INTERP)+1);
		pic = 0;
		haveInterp = 1;
	}
	else
	{
		// no need for GOT/PLT
		return ERR_OK;
	};
	(void)pic;
	
	// we might need zeroes for temporary padding and stuff
	char zeroes[256];
	memset(zeroes, 0, 256);
	
	// first create the .got section (which will be separate from .got.plt)
	Section *got = objGetSection(obj, ".got");
	if (got != NULL)
	{
		fprintf(stderr, "elf64: .got section already exists\n");
		return ERR_INVALID_PARAM;
	};
	got = objCreateSection(obj, ".got", SECTYPE_PROGBITS, SEC_READ | SEC_WRITE | SEC_ELF_DYN);
	if (!haveInterp) got->addr = (got->addr + 0xFFF) & ~0xFFF;
	objSectionAppend(got, zeroes, 8);
	
	// allocate symbol auxilliary structure to all symbols
	Symbol *sym;
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		Elf64_SymAux *aux = (Elf64_SymAux*) malloc(sizeof(Elf64_SymAux));
		memset(aux, 0, sizeof(Elf64_SymAux));
		sym->aux = aux;
		
		aux->got = got->addr;
	};
	
	// allocate GOT entries for symbols referenced by R_X86_64_GOTPCREL relocations
	Section *sect;
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		Reloc *reloc;
		for (reloc=sect->relocs; reloc!=NULL; reloc=reloc->next)
		{
			if (reloc->type == &rel_x86_64_gotpcrel)
			{
				Elf64_SymAux *aux = (Elf64_SymAux*) reloc->sym->aux;
				aux->gotoff = got->size;
				objSectionAppend(got, zeroes, 8);
				
				if (obj->type == OBJTYPE_SHARED && reloc->sym->type == SYMT_UNDEF)
				{
					// allow references to undefined symbols via the GOT
					reloc->sym->type = SYMT_NONE;
					reloc->sym->binding = SYMB_GLOBAL;
				};
			};
		};
	};
	
	// now create the .got.plt section. note that the first 3 entries are reserved as follows:
	// GOTPLT[0] contains the position of the .dynamic section.
	// GOTPLT[1] is for use by the dynamic linker and we must set it to zero
	// GOTPLT[2] is the dynamic linker "resolve" callback, and we must initialize it to zero
	// we will fill in GOTPLT[0] later.
	Section *gotplt = objGetSection(obj, ".got.plt");
	if (gotplt != NULL)
	{
		fprintf(stderr, "elf64: .got.plt section already exists\n");
		return ERR_INVALID_PARAM;
	};
	gotplt = objCreateSection(obj, ".got.plt", SECTYPE_PROGBITS, SEC_READ | SEC_WRITE | SEC_ELF_DYN);
	objSectionAppend(gotplt, zeroes, 8*3);
	
	// if we are a non-PIC executable, translate all R_X86_64_PC32 relocations against functions
	// in library into R_X86_64_PLT32
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		Reloc *reloc;
		for (reloc=sect->relocs; reloc!=NULL; reloc=reloc->next)
		{
			if (reloc->type == &rel_x86_64_pc32)
			{
				const char *symname = reloc->sym->name;
				
				// try to find the symbol inside libraries
				int depidx;
				for (depidx=0; depidx<obj->numDepends; depidx++)
				{
					Symbol *sym = objGetSymbol(obj->depends[depidx], symname, 0);
					if (sym != NULL)
					{
						if (sym->type == SYMT_FUNC)
						{
							reloc->type = &rel_x86_64_plt32;
						};
					};
				};
			};
		};
	};

	// allocate space in the GOTPLT for every symbol referenced by R_X86_64_PLT32 relocations
	for (sect=obj->sects; sect!=NULL; sect=sect->next)
	{
		Reloc *reloc;
		for (reloc=sect->relocs; reloc!=NULL; reloc=reloc->next)
		{
			if (reloc->type == &rel_x86_64_plt32)
			{
				Elf64_SymAux *aux = (Elf64_SymAux*) reloc->sym->aux;
				aux->gotpltoff = gotplt->size;
				objSectionAppend(gotplt, zeroes, 8);
				
				if (reloc->sym->type == SYMT_UNDEF)
				{
					// allow references to undefined symbols via the GOTPLT
					reloc->sym->type = SYMT_NONE;
					reloc->sym->binding = SYMB_GLOBAL;
				};
			};
		};
	};
	
	// create the dynamic symbol table
	StringTable strtab = {NULL, 0};
	stralloc(&strtab, "");
	
	Section *dynsym = objGetSection(obj, ".dynsym");
	if (dynsym != NULL)
	{
		fprintf(stderr, "elf64: .dynsym section already exists");
		return ERR_INVALID_PARAM;
	};
	dynsym = objCreateSection(obj, ".dynsym", SECTYPE_PROGBITS, SEC_READ | SEC_WRITE | SEC_ELF_DYN);
	
	Elf64_Sym elfsym;
	memset(&elfsym, 0, sizeof(Elf64_Sym));
	objSectionAppend(dynsym, &elfsym, sizeof(Elf64_Sym));
	
	// the number of buckets can be arbitrary, let's default to 32
#define	NBUCKET 32
	uint32_t buckets[NBUCKET];
	memset(buckets, 0, sizeof(buckets));
	uint32_t *chains = (uint32_t*) malloc(4);
	chains[0] = 0;
	
	int symidx = 1;
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		Elf64_SymAux *aux = (Elf64_SymAux*) sym->aux;
		int dyn = 0;
		switch (sym->type)
		{
		case SYMT_NONE:
		case SYMT_OBJECT:
		case SYMT_FUNC:
		case SYMT_COMMON:
			dyn = 1;
			break;
		case SYMT_UNDEF:
			if (aux != NULL && (aux->gotoff != 0 || aux->gotpltoff != 0))
			{
				dyn = 1;
			};
			break;
		};
		
		if (!dyn) continue;
		
		int type;
		if (sym->type == SYMT_FUNC)
		{
			type = STT_FUNC;
		}
		else
		{
			type = STT_OBJECT;
		};
		
		if (aux != NULL && aux->gotpltoff != 0)
		{
			type = STT_FUNC;
		};
		
		memset(&elfsym, 0, sizeof(Elf64_Sym));
		opt->make(stralloc(&strtab, sym->name), &elfsym.st_name, sizeof(elfsym.st_name));
		elfsym.st_info = ELF_MAKE_STINFO(STB_GLOBAL, type);
		
		uint64_t value = sym->value;
		if (sym->sect != NULL)
		{
			opt->make(1, &elfsym.st_shndx, sizeof(elfsym.st_shndx));
			value += sym->sect->addr;
		};
		
		opt->make(value, &elfsym.st_value, sizeof(elfsym.st_value));
		opt->make(sym->size, &elfsym.st_size, sizeof(elfsym.st_size));
		
		objSectionAppend(dynsym, &elfsym, sizeof(Elf64_Sym));
		aux->dynsymidx = symidx++;
		
		chains = (uint32_t*) realloc(chains, 4 * symidx);
		chains[aux->dynsymidx] = 0;
		
		if (sym->sect != NULL)
		{
			unsigned long x = elf64_hash((const unsigned char*)sym->name);
			
			chains[aux->dynsymidx] = buckets[x % NBUCKET];
			buckets[x % NBUCKET] = aux->dynsymidx;
		};
	};
	
	// create the hash table
	Section *hash = objGetSection(obj, ".hash");
	if (hash != NULL)
	{
		fprintf(stderr, "elf64: .hash section already exists\n");
		return ERR_INVALID_PARAM;
	};
	hash = objCreateSection(obj, ".hash", SECTYPE_PROGBITS, SEC_READ | SEC_WRITE | SEC_ELF_DYN);
	
	char buf[4];
	opt->make(NBUCKET, buf, 4);
	objSectionAppend(hash, buf, 4);
	
	opt->make(symidx, buf, 4);
	objSectionAppend(hash, buf, 4);
	
	int bucket;
	for (bucket=0; bucket<NBUCKET; bucket++)
	{
		opt->make(buckets[bucket], buf, 4);
		objSectionAppend(hash, buf, 4);
	};
	
	int chain;
	for (chain=0; chain<symidx; chain++)
	{
		opt->make(buckets[bucket], buf, 4);
		objSectionAppend(hash, buf, 4);
	};
	
	// ensure 8-byte alignment
	if (hash->size & 7)
	{
		objSectionAppend(hash, zeroes, 4);
	};
	
	// create the main GOT relocation table
	Section *gotrel = objGetSection(obj, ".got.rela");
	if (gotrel != NULL)
	{
		fprintf(stderr, "elf64: .got.rela section already exists\n");
		return ERR_INVALID_PARAM;
	};
	gotrel = objCreateSection(obj, ".got.rela", SECTYPE_PROGBITS, SEC_READ | SEC_WRITE | SEC_ELF_DYN);
	
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		Elf64_SymAux *aux = (Elf64_SymAux*) sym->aux;
		
		if (aux != NULL && aux->gotoff != 0)
		{
			Elf64_Rela rela;
			memset(&rela, 0, sizeof(Elf64_Rela));
			opt->make(aux->got + aux->gotoff, &rela.r_offset, sizeof(rela.r_offset));
			uint64_t info = ELF64_R_INFO((uint64_t)aux->dynsymidx, R_X86_64_GLOB_DAT);
			opt->make(info, &rela.r_info, sizeof(rela.r_info));
			objSectionAppend(gotrel, &rela, sizeof(Elf64_Rela));
		};
		
		if (sym->type == SYMT_COMMON && obj->type == OBJTYPE_EXEC)
		{
			uint64_t symaddr = sym->value;
			if (sym->sect != NULL) symaddr += sym->sect->addr;
			
			// TODO: emitting R_X86_64_COPY relocations causes undefined
			// references for some reason (?) - don't know the reason but
			// maybe let's just leave it commented out for now, especially
			// since we'll mostly be using PIE.
			/*
			Elf64_Rela rela;
			memset(&rela, 0, sizeof(Elf64_Rela));
			opt->make(symaddr, &rela.r_offset, sizeof(rela.r_offset));
			uint64_t info = ELF64_R_INFO((uint64_t)aux->dynsymidx, R_X86_64_COPY);
			opt->make(info, &rela.r_info, sizeof(rela.r_info));
			objSectionAppend(gotrel, &rela, sizeof(Elf64_Rela));
			*/
		};
	};
	
	// now the GOTPLT relocation table
	Section *pltrel = objGetSection(obj, ".plt.rela");
	if (pltrel != NULL)
	{
		fprintf(stderr, "elf64: .plt.rela section already exists\n");
		return ERR_INVALID_PARAM;
	};
	pltrel = objCreateSection(obj, ".plt.rela", SECTYPE_PROGBITS, SEC_READ | SEC_WRITE | SEC_ELF_DYN);

	int pltidx = 0;
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		Elf64_SymAux *aux = (Elf64_SymAux*) sym->aux;
		
		if (aux != NULL && aux->gotpltoff != 0)
		{
			Elf64_Rela rela;
			memset(&rela, 0, sizeof(Elf64_Rela));
			opt->make(gotplt->addr + aux->gotpltoff, &rela.r_offset, sizeof(rela.r_offset));
			uint64_t info = ELF64_R_INFO((uint64_t)aux->dynsymidx, R_X86_64_JUMP_SLOT);
			opt->make(info, &rela.r_info, sizeof(rela.r_info));
			objSectionAppend(pltrel, &rela, sizeof(Elf64_Rela));
			
			aux->pltidx = pltidx++;
		};
	};
	
	// create the PLT and fill in the appropriate entries in the GOTPLT
	Section *plt = objGetSection(obj, ".plt");
	if (plt != NULL)
	{
		fprintf(stderr, "elf64: .plt section already exists\n");
		return ERR_INVALID_PARAM;
	};
	plt = objCreateSection(obj, ".plt", SECTYPE_PROGBITS, SEC_READ | SEC_EXEC | SEC_ELF_DYN);
	
	// first the PLT prolog
	objSectionAppend(plt, "\xFF\x35", 2);	// push QWORD PTR ?[rip]
	opt->make(gotplt->addr + 8 - 4 - (plt->addr + plt->size), buf, 4);
	objSectionAppend(plt, buf, 4);		// ? = GOTPLT+8
	objSectionAppend(plt, "\xFF\x25", 2);	// jmp QWORD PTR ?[rip]
	opt->make(gotplt->addr + 16 - 4 - (plt->addr + plt->size), buf, 4);
	objSectionAppend(plt, buf, 4);		// ? = GOTPLT+16
	objSectionAppend(plt, "\x90\x90\x90\x90", 4); // pad to 8-byte boundary with NOPs
	
	// emit PLT entries for every symbol that's in the GOTPLT
	for (sym=obj->syms; sym!=NULL; sym=sym->next)
	{
		Elf64_SymAux *aux = (Elf64_SymAux*) sym->aux;
		
		if (aux != NULL && aux->gotpltoff != 0)
		{
			aux->plt = plt->addr + plt->size;
			if (obj->type == OBJTYPE_EXEC)
			{
				// redirect normal functions to PLT for non-PIC executables
				sym->sect = plt;
				sym->value = plt->size;
			};
			
			objSectionAppend(plt, "\xFF\x25", 2);	// jmp QWORD PTR ?[rip]
			opt->make(gotplt->addr + aux->gotpltoff - 4 - (plt->addr + plt->size), buf, 4);
			objSectionAppend(plt, buf, 4);		// ? = sym@GOTPLT
			
			// set the initial value of the GOTPLT entry to point
			// to right after the JMP
			char *put = (char*) gotplt->data + aux->gotpltoff;
			opt->make(plt->addr + plt->size, put, 8);
			
			// now the resolution part
			objSectionAppend(plt, "\x68", 1);	// push ?
			opt->make(aux->pltidx, buf, 4);
			objSectionAppend(plt, buf, 4);		// ? = relocation index
			
			objSectionAppend(plt, "\xE9", 1);	// jmp relative
			opt->make(-plt->size-4, buf, 4);
			objSectionAppend(plt, buf, 4);		// to .PLT0
		};
	};
	
	// finally time for the .dynamic section
	Section *dynamic = objGetSection(obj, ".dynamic");
	if (dynamic != NULL)
	{
		fprintf(stderr, "elf64: .dynamic section already exists\n");
		return ERR_INVALID_PARAM;
	};
	dynamic = objCreateSection(obj, ".dynamic", SECTYPE_PROGBITS, SEC_READ | SEC_WRITE | SEC_ELF_DYN);
	
	// first entry MUST be DT_STRTAB: it will be filled in later when we know the address of the string table
	// then also we need the size of it
	elf64_addDyn(dynamic, DT_STRTAB, 0);
	elf64_addDyn(dynamic, DT_STRSZ, 0);
	
	// the soname
	if (obj->soname != NULL) elf64_addDyn(dynamic, DT_SONAME, stralloc(&strtab, obj->soname));

	// dependencies
	int depidx;
	for (depidx=0; depidx<obj->numDepends; depidx++)
	{
		Object *dep = obj->depends[depidx];
		elf64_addDyn(dynamic, DT_NEEDED, stralloc(&strtab, dep->soname));
	};
	
	// hash table
	elf64_addDyn(dynamic, DT_HASH, hash->addr);
	
	// PLT properties
	elf64_addDyn(dynamic, DT_PLTGOT, gotplt->addr);
	elf64_addDyn(dynamic, DT_PLTRELSZ, pltrel->size);
	elf64_addDyn(dynamic, DT_PLTREL, DT_RELA);
	elf64_addDyn(dynamic, DT_JMPREL, pltrel->addr);
	
	// symbol table and GOT relocations
	elf64_addDyn(dynamic, DT_RELA, gotrel->addr);
	elf64_addDyn(dynamic, DT_RELASZ, gotrel->size);
	elf64_addDyn(dynamic, DT_RELAENT, sizeof(Elf64_Rela));
	elf64_addDyn(dynamic, DT_SYMTAB, dynsym->addr);
	elf64_addDyn(dynamic, DT_SYMENT, sizeof(Elf64_Sym));
	
	// finally mark the end of the dynamic array
	elf64_addDyn(dynamic, DT_NULL, 0);
	
	// now write the string table
	Section *dynstr = objGetSection(obj, ".dynstr");
	if (dynstr != NULL)
	{
		fprintf(stderr, "elf64: .dynstr section already exists\n");
		return ERR_INVALID_PARAM;
	};
	dynstr = objCreateSection(obj, ".dynstr", SECTYPE_PROGBITS, SEC_READ | SEC_WRITE | SEC_ELF_DYN);
	objSectionAppend(dynstr, strtab.data, strtab.size);
	
	// update the STRTAB and STRSZ dynamic tags
	Elf64_Dyn *dyns = (Elf64_Dyn*) dynamic->data;
	opt->make(dynstr->addr, &dyns[0].d_un, sizeof(dyns[0].d_un));		// DT_STRTAB
	opt->make(dynstr->size, &dyns[1].d_un, sizeof(dyns[0].d_un));		// DT_STRSZ
	
	// set the address of GOTPLT[0] to the dynamic section
	opt->make(dynamic->addr, gotplt->data, 8);
	
	// thanks
	return ERR_OK;
};

static Elf64_DrvOptions opt_x86_64 = {
	.pageSize = 0x1000,
	.machine = EM_X86_64,
	.make = ctMakeLE,
	.get = ctGetLE,
	.relocCompToElf = elf64_x86_64_relocCompToElf,
	.relocElfToComp = elf64_x86_64_relocElfToComp,
};

ObjDriver objDriver_elf64__x86_64 = {
	.name = "elf64-x86_64",
	.opt = &opt_x86_64,
	.init = elf64_init,
	.write = elf64_write,
	.read = elf64_read,
	.preReloc = elf64_x86_64_preReloc,
};
