/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>
#include <stdlib.h>

#include <comptools/obj.h>
#include <comptools/gxo-x86_64.h>

#define	GXO_GLIDIX_KERNEL			(1 << 0)
#define	GXO_GLIDIX_MODULE			(1 << 1)
#define	GXO_GLIDIX_GUI				(1 << 2)

static int mgui;
static int mkernel;
static int module;

static uint64_t glidix_sysflags()
{
	uint64_t flags = 0;
	if (mgui) flags |= GXO_GLIDIX_GUI;
	if (module) flags |= GXO_GLIDIX_MODULE;
	if (mkernel) flags |= GXO_GLIDIX_KERNEL;
	return flags;
};

static void glidix_init(Object *obj, const void *opt)
{
	GXO_DrvData *data = gxoDrvInit_x86_64(obj, "_glidix_");
	data->sysflags = glidix_sysflags;
};

static void glidix_opts(OptionList *optlist)
{
	optAdd(optlist, "-mgui", "", "Mark the output as a GUI application.", OPT_FLAG, &mgui);
	optAdd(optlist, "-mkernel", "", "Mark the output as a Glidix kernel image.", OPT_FLAG, &mkernel);
	optAdd(optlist, "-module", "", "Mark the output as a Glidix kernel module.", OPT_FLAG, &module);
};

ObjDriver objDriver_gxo__x86_64__glidix = {
	.name = "gxo-x86_64-glidix",
	.init = glidix_init,
	.write = gxoWrite,
	.read = gxoRead,
	.preReloc = gxoPreReloc,
	.drvOpts = glidix_opts,
};
