/**
 * ldsParse.c
 * Test ldsParse().
 */

#include <assert.h>
#include <stdio.h>
#include <comptools/ld-script.h>
#include <string.h>

const char *scriptSource = ".test = 7 - MY_SYM + 6;\n\
OTHER_SYMBOL = 5 - .test;\n\
align 0x1000;\n\
section .text {test = .; load .text;};\n\
section .privdata :x {};\n\
entry __demo;";

int main()
{
	LDS_Script *script = ldsParse(scriptSource, "<internal>");
	
	printf("SCRIPT SOURCE:\n%s\n\n", scriptSource);
	printf("RESULTING PARSE TREE:\n");
	ldsDump(script);
	
	assert(script != NULL);
	return 0;
};
