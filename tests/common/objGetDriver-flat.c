/**
 * objGetDriver-flat.c
 * Test objGetDriver() for the "flat" driver.
 */

#include <assert.h>
#include <stdio.h>
#include <comptools/obj.h>
#include <string.h>

int main()
{
	ObjDriver *drv = objGetDriver("flat");
	if (drv == NULL)
	{
		printf("ERROR: objGetDriver() returned NULL\n");
		return 1;
	};
	
	if (strcmp(drv->name, "flat") != 0)
	{
		printf("ERROR: objGetDriver() returned a driver named `%s'\n", drv->name);
		return 1;
	};
	
	return 0;
};
