/**
 * hashtab.c
 * Test the hash table.
 */

#include <assert.h>
#include <stdio.h>
#include <comptools/hashtab.h>
#include <string.h>

int main()
{
	void *a = (void*) 1;
	void *b = (void*) 2;
	void *c = (void*) 3;
	
	HashTable *tab = hashtabNew();
	assert(tab != NULL);
	
	hashtabSet(tab, "ValueA", a);
	hashtabSet(tab, "ValueB", b);
	
	assert(hashtabHasKey(tab, "ValueA"));
	assert(hashtabHasKey(tab, "ValueB"));
	assert(!hashtabHasKey(tab, "ValueC"));
	
	assert(hashtabGet(tab, "ValueA") == a);
	assert(hashtabGet(tab, "ValueB") == b);
	
	hashtabUnset(tab, "ValueA");
	
	assert(!hashtabHasKey(tab, "ValueA"));
	assert(hashtabHasKey(tab, "ValueB"));
	assert(!hashtabHasKey(tab, "ValueC"));
	
	hashtabSet(tab, "ValueB", c);
	assert(hashtabHasKey(tab, "ValueB"));
	assert(hashtabGet(tab, "ValueB") == c);
	
	hashtabDelete(tab);
	return 0;
};
