/**
 * lexCompileRegex.c
 * Test lexCompileRegex();
 */

#include <assert.h>
#include <comptools/lex.h>

#define ASSERT_VALID_REGEX(expr) regex = lexCompileRegex(expr); assert(regex != NULL)
#define ASSERT_INVALID_REGEX(expr) regex = lexCompileRegex(expr); assert(regex == NULL)

int main()
{
	Regex *regex;
	
	// test valid expressions
	ASSERT_VALID_REGEX("hello");
	ASSERT_VALID_REGEX("he\\llo");
	ASSERT_VALID_REGEX("h+e");
	ASSERT_VALID_REGEX("h*e");
	ASSERT_VALID_REGEX("h?e");
	ASSERT_VALID_REGEX("h+?e");
	ASSERT_VALID_REGEX("h*?e");
	ASSERT_VALID_REGEX("\\?");
	ASSERT_VALID_REGEX("\\+");
	ASSERT_VALID_REGEX("[abc]");
	ASSERT_VALID_REGEX("[\\]]");
	ASSERT_VALID_REGEX("[a-z]");
	ASSERT_VALID_REGEX("[-a-zA-Z]");
	ASSERT_VALID_REGEX("he(ll)o");
	ASSERT_VALID_REGEX("he(ll|pp)o");
	ASSERT_VALID_REGEX("h(ell(o|p)a)de");
	ASSERT_VALID_REGEX(".");
	
	// test invalid expressions
	ASSERT_INVALID_REGEX("hello\\");
	ASSERT_INVALID_REGEX("+e");
	ASSERT_INVALID_REGEX("*e");
	ASSERT_INVALID_REGEX("?e");
	ASSERT_INVALID_REGEX("h+*");
	ASSERT_INVALID_REGEX("h?+");
	ASSERT_INVALID_REGEX("h?*");
	ASSERT_INVALID_REGEX("[a");
	ASSERT_INVALID_REGEX("(test");
	ASSERT_INVALID_REGEX("test)");
	
	return 0;
};
