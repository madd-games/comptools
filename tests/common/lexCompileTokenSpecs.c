/**
 * lexCompileTokenSpecs.c
 * Test lexCompileTokenSpecs().
 */

#include <comptools/lex.h>

TokenSpec testSpecs[] = {
	{TOK_USER_BASE+0,		"[_a-zA-Z0-9]+"},
	{TOK_USER_BASE+1,		"$+"},
	{TOK_USER_BASE+2,		"hello"},
	
	// LIST TERMINATOR
	{TOK_ENDLIST}
};

int main()
{
	lexCompileTokenSpecs(testSpecs);
	return 0;
};
