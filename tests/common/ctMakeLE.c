/**
 * ctMakeLE.c
 * Test ctMakeLE().
 */

#include <comptools/types.h>
#include <string.h>

int main()
{
	uint64_t testVal = 0x0123456789ABCDEF;
	uint8_t correct[8] = {0xEF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01};
	uint8_t actual[8];
	ctMakeLE(testVal, actual, 8);
	return memcmp(correct, actual, 8);
};
