/**
 * cpp.c
 * Test the C pre-processor.
 */

#include <assert.h>
#include <stdio.h>
#include <comptools/cc/cpp.h>
#include <comptools/console.h>
#include <string.h>

const char *sampleHeader = "THIS_IS_FROM_A_HEADER";

const char *sampleCode = "#define test 5 test hi\n\
#ifndef __STDC_VERSION__\n\
// this is a comment\n\
#define	__STDC_VERSION__ test\n\
__DO_NOT_INCLUDE_ME\n\
#ifdef test\n\
__DO_NOT_INCLUDE_ME_EITHER\n\
#endif\n\
#endif\n\
#ifdef test\n\
__PLEASE_INCLUDE_ME\n\
#endif\n\
#ifndef test\n\
__BUT_NOT_ME\n\
#endif\n\
test\n\
__STDC_VERSION__\n\
#ifdef test\n\
__INCLUDE_THIS /* but this is a comment! */\n\
#else\n\
__DO_NOT_INCLUDE_THIS\n\
#endif\n\
#include <sample.h>\n\
#define TEST_VA(...) __VA_ARGS__\n\
TEST_VA(one, two)\n\
#define	MAKE_STRING(x) #x\n\
MAKE_STRING(hello world)";

Error testIncluder(const char *headerName, const char **data, const char **filename)
{
	if (strcmp(headerName, "<sample.h>") == 0)
	{
		*data = sampleHeader;
		*filename = "sample.h";
		return ERR_OK;
	}
	else
	{
		return ERR_IO;
	};
};

int main()
{
	CPP *cpp = cppNew();
	assert(cpp != NULL);
	assert(cpp->includer == NULL);
	cpp->includer = testIncluder;
	
	assert(conGetError() == 0);
	cppDefine(cpp, "__COMPTOOLS__", "");
	assert(conGetError() == 0);
	cppDefine(cpp, "__STDC_VERSION__", "201710L");
	assert(conGetError() == 0);
	cppDefine(cpp, "__COMPTOOLS__", "5");
	assert(conGetError() != 0);
	
	conClearError();
	assert(conGetError() == 0);
	
	Token *tokens = cppRun(cpp, sampleCode, "sample");
	printf("RESULTING TOKEN LIST:\n");
	lexDumpTokenList(tokens);
	
	Token *token = tokens;
	assert(token->type == TOK_CC_ID);
	assert(strcmp(token->value, "__PLEASE_INCLUDE_ME") == 0);
	
	token = token->next;
	assert(token->type == TOK_CC_ICONST);
	assert(strcmp(token->value, "5") == 0);
	
	token = token->next;
	assert(token->type == TOK_CC_ID);
	assert(strcmp(token->value, "test") == 0);
	
	token = token->next;
	assert(token->type == TOK_CC_ID);
	assert(strcmp(token->value, "hi") == 0);
	
	token = token->next;
	assert(token->type == TOK_CC_ICONST);
	assert(strcmp(token->value, "201710L") == 0);
	
	token = token->next;
	assert(token->type == TOK_CC_ID);
	assert(strcmp(token->value, "__INCLUDE_THIS") == 0);
	
	token = token->next;
	assert(token->type == TOK_CC_ID);
	assert(strcmp(token->value, "THIS_IS_FROM_A_HEADER") == 0);
	
	token = token->next;
	assert(token->type == TOK_CC_ID);
	assert(strcmp(token->value, "one") == 0);
	
	token = token->next;
	assert(token->type == TOK_CC_COMMA);
	
	token = token->next;
	assert(token->type == TOK_CC_ID);
	assert(strcmp(token->value, "two") == 0);

	token = token->next;
	assert(token->type == TOK_CC_STRING);
	assert(strcmp(token->value, "\"hello world\"") == 0);
	
	token = token->next;
	assert(token->type == TOK_END);
	return 0;
};
