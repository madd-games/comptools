/**
 * ctMakeBE-32bits.c
 * Test ctMakeBE() on 32-bit values.
 */

#include <comptools/types.h>
#include <string.h>

int main()
{
	uint32_t testVal = 0xDEADBEEF;
	uint8_t correct[4] = {0xDE, 0xAD, 0xBE, 0xEF};
	uint8_t actual[4];
	ctMakeBE(testVal, actual, 4);
	return memcmp(correct, actual, 4);
};
