/**
 * lexTokenize.c
 * Test lexTokenize().
 */

#include <assert.h>
#include <string.h>
#include <comptools/lex.h>

#define	TOK_ID				(TOK_USER_BASE+0)
#define	TOK_HELLO			(TOK_USER_BASE+1)

TokenSpec testSpecs[] = {
	{TOK_HELLO,			"hello"},
	{TOK_ID,			"[_a-zA-Z][_0-9a-zA-Z]*"},
	{TOK_WHITESPACE,		"$+"},
	
	// LIST TERMINATOR
	{TOK_ENDLIST}
};

int main()
{
	lexCompileTokenSpecs(testSpecs);
	
	Token *list = lexTokenize(testSpecs, "hello tokenized world\nhellotest", "sample.test");
	
	assert(strcmp(list->value, "hello") == 0);
	assert(list->type == TOK_HELLO);
	assert(strcmp(list->filename, "sample.test") == 0);
	assert(list->lineno == 1);
	assert(list->col == 1);
	
	list = list->next;
	assert(strcmp(list->value, "tokenized") == 0);
	assert(list->type == TOK_ID);
	assert(strcmp(list->filename, "sample.test") == 0);
	assert(list->lineno == 1);
	assert(list->col == 7);

	list = list->next;
	assert(strcmp(list->value, "world") == 0);
	assert(list->type == TOK_ID);
	assert(strcmp(list->filename, "sample.test") == 0);
	assert(list->lineno == 1);
	assert(list->col == 17);

	list = list->next;
	assert(strcmp(list->value, "hellotest") == 0);
	assert(list->type == TOK_ID);
	assert(strcmp(list->filename, "sample.test") == 0);
	assert(list->lineno == 2);
	assert(list->col == 1);

	return 0;
};
