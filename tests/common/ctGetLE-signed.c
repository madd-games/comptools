/**
 * ctGetLE-signed.c
 * Test ctGetLE() with signed values.
 */

#include <comptools/types.h>
#include <string.h>
#include <stdio.h>

int main()
{
	uint8_t testbytes[4] = {0xFE, 0xFF, 0xFF, 0xFF};
	uint64_t val = ctGetLE(testbytes, 4);
	int32_t realval = (int32_t) val;
	printf("Expecting -2, got %d\n", realval);
	return realval != -2;
};
