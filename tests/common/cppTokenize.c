/**
 * cppTokenize.c
 * Test cppTokenize().
 */

#include <assert.h>
#include <stdio.h>
#include <comptools/cc/cpp.h>
#include <string.h>

const char *source = "#include <stdio.h>\n\
#define TEST_MACRO test\n\
\"hello world\" 0xDEADBEEFUL 1.56e+5";

int main()
{
	Token *toklist = cppTokenize(source, "testfile");
	assert(toklist != NULL);
	
	assert(toklist->type == TOK_CC_INCLUDE);
	assert(strcmp(toklist->value, "#include <stdio.h>") == 0);
	
	toklist = toklist->next;
	assert(toklist->type == TOK_CC_NEWLINE);
	
	toklist = toklist->next;
	assert(toklist->type == TOK_CC_DIR);
	assert(strcmp(toklist->value, "#define") == 0);
	
	toklist = toklist->next;
	assert(toklist->type == TOK_CC_ID);
	assert(strcmp(toklist->value, "TEST_MACRO") == 0);
	
	toklist = toklist->next;
	assert(toklist->type == TOK_CC_ID);
	assert(strcmp(toklist->value, "test") == 0);

	toklist = toklist->next;
	assert(toklist->type == TOK_CC_NEWLINE);
	
	toklist = toklist->next;
	assert(toklist->type == TOK_CC_STRING);
	assert(strcmp(toklist->value, "\"hello world\"") == 0);
	
	toklist = toklist->next;
	assert(toklist->type == TOK_CC_ICONST);
	assert(strcmp(toklist->value, "0xDEADBEEFUL") == 0);
	
	toklist = toklist->next;
	assert(toklist->type == TOK_CC_FCONST);
	assert(strcmp(toklist->value, "1.56e+5") == 0);
	
	toklist = toklist->next;
	assert(toklist->type == TOK_END);
	
	return 0;
};
