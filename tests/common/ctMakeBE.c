/**
 * ctMakeBE.c
 * Test ctMakeBE().
 */

#include <comptools/types.h>
#include <string.h>

int main()
{
	uint64_t testVal = 0x0123456789ABCDEF;
	uint8_t correct[8] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF};
	uint8_t actual[8];
	ctMakeBE(testVal, actual, 8);
	return memcmp(correct, actual, 8);
};
