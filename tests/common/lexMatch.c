/**
 * lexMatch.c
 * Test lexMatch().
 */

#include <assert.h>
#include <stdio.h>
#include <comptools/lex.h>

void assertMatch(const char *expr, const char *str, ssize_t expectedReturn)
{
	printf("MATCH '%s' AGAINST '%s', EXPECTING %d\n", expr, str, (int) expectedReturn);
	Regex *regex = lexCompileRegex(expr);
	assert(regex != NULL);
	ssize_t matchSize = lexMatch(regex, str);
	printf("->RESULT: %d\n", (int) matchSize);
	assert(matchSize == expectedReturn);
	lexDeleteRegex(regex);
};

int main()
{
	// test some valid matches
	assertMatch("hello", "hello", 5);
	assertMatch("hello", "hello world", 5);
	assertMatch("h+ello", "hello world", 5);
	assertMatch("h+ello", "hhhello world", 7);
	assertMatch("h*ello", "hello world", 5);
	assertMatch("h*ello", "hhhello world", 7);
	assertMatch("h*ello", "ello", 4);
	assertMatch("h?ello", "hello", 5);
	assertMatch("h?ello", "ello", 4);
	assertMatch("h\\?ello", "h?ello", 6);
	assertMatch("\\?hello", "?hello", 6);
	assertMatch("[abc]", "abc", 1);
	assertMatch("[abc]", "b", 1);
	assertMatch("[abc]+", "abc", 3);
	assertMatch("[a-zA-Z]+", "abcdefABCDEF", 12);
	assertMatch("(happy|sad)", "happy", 5);
	assertMatch("(happy|sad)", "sad", 3);
	assertMatch(".*", "test!", 5);
	assertMatch("$*", " \t ", 3);
	assertMatch(".*?", "test!", 5);
	assertMatch(".*?s", "test", 3);
	assertMatch(".*?sb?t", "test", 4);
	assertMatch(".*?sb?", "test", 3);
	
	// test invalid matches
	assertMatch("hello", "world", -1);
	assertMatch("h+ello", "ello", -1);
	assertMatch("h\\+ello", "hello", -1);
	assertMatch("[abc]", "z", -1);

	return 0;
};
