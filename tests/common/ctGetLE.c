/**
 * ctGetLE.c
 * Test ctGetLE().
 */

#include <comptools/types.h>
#include <string.h>
#include <stdio.h>

int main()
{
	uint8_t testbytes[4] = {0xDE, 0xAD, 0xBE, 0xEF};
	uint64_t val = ctGetLE(testbytes, 4);
	printf("Expecting 0xEFBEADDE, have 0x%lX\n", val);
	return val != 0xEFBEADDE;
};
