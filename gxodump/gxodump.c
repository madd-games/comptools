/*
	Madd Compiler Tools

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define __USE_MINGW_ANSI_STDIO 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <ctype.h>
#include <assert.h>

#include <comptools/gxo.h>
#include <comptools/types.h>
#include <comptools/opt.h>
#include <comptools/obj.h>

int dumpHeader;
int dumpSections;
int dumpSymbols;
int dumpRelocs;
int dumpAnnot;
const char *hexdumpSection;

uint64_t (*get)(const void *buffer, int count);

typedef struct
{
	uint64_t vaddr;
	uint64_t size;
	uint32_t offset;
	char name[9];
} SectionInfo;

void printArchFlags(uint64_t archnum, uint64_t flags)
{
	if (flags != 0) printf("(invalid)");
	return;
};

void printSystemFlags(uint64_t sysnum, uint64_t flags)
{
	union
	{
		uint64_t val;
		char name[9];
	} conv;
	memset(&conv, 0, sizeof(conv));
	conv.val = sysnum;
	
	if (strcmp(conv.name, "_glidix_") == 0)
	{
		uint64_t allowed = 0x7;
		if ((flags & (~allowed)) != 0)
		{
			printf("(invalid)");
			return;
		};
		
		if (flags & (1 << 0)) printf("kernel ");
		if (flags & (1 << 1)) printf("module ");
		if (flags & (1 << 2)) printf("gui ");
	}
	else
	{
		printf("?");
	};
};

void printGenericFlags(uint64_t flags)
{
	uint64_t allowed = GXO_GEN_DYNLD | GXO_GEN_EXEC | GXO_GEN_PIC | GXO_GEN_REL | GXO_GEN_TLS;
	if ((flags & (~allowed)) != 0)
	{
		printf("(invalid)");
		return;
	};
	
	if (flags & GXO_GEN_DYNLD) printf("dynld ");
	if (flags & GXO_GEN_EXEC) printf("exec ");
	if (flags & GXO_GEN_PIC) printf("pic ");
	if (flags & GXO_GEN_REL) printf("rel ");
	if (flags & GXO_GEN_TLS) printf("tls ");
};

void hexdump(const char *data, size_t size, uint64_t vaddr)
{
	uint64_t offset = 0;
	while (size != 0)
	{
		uint64_t chunk = size;
		if (chunk > 10) chunk = 10;
		
		uint64_t i;
		printf("%016" PRIx64 "   ", offset+vaddr);
		
		for (i=0; i<10; i++)
		{
			if (i < chunk) printf("%02hhX ", data[offset+i]);
			else printf("   ");
		};
		
		printf("  ");
		for (i=0; i<chunk; i++)
		{
			char c = data[offset+i];
			if (isprint(c))
			{
				printf("%c", c);
			}
			else
			{
				printf(".");
			};
		};
		
		printf("\n");
		
		size -= chunk;
		offset += chunk;
	};
};

const char* getSymScopeName(uint64_t scope)
{
	switch (scope)
	{
	case SYM_SCOPE_GLOBAL:		return "GLOBAL";
	case SYM_SCOPE_LOCAL:		return "LOCAL";
	case SYM_SCOPE_WEAK:		return "WEAK";
	default:			return "?";
	};
};

const char* getSymTypeName(uint64_t type)
{
	switch (type)
	{
	case SYM_TYPE_ABS:		return "ABS";
	case SYM_TYPE_FUNC:		return "FUNC";
	case SYM_TYPE_LABEL:		return "LABEL";
	case SYM_TYPE_OBJECT:		return "OBJECT";
	case SYM_TYPE_TLS:		return "TLS";
	case SYM_TYPE_UNDEF:		return "UNDEF";
	default:			return "?";
	};
};

uint64_t resolveAddr(SectionInfo *info, uint64_t count, uint64_t vaddr)
{
	uint64_t i;
	for (i=0; i<count; i++)
	{
		if (info[i].vaddr <= vaddr && info[i].vaddr+info[i].size > vaddr)
		{
			return vaddr - info[i].vaddr + info[i].offset;
		};
	};
	
	return 0;
};

const char *getRelocTypeName(uint64_t archnum, uint64_t sysnum, uint64_t type)
{
	struct
	{
		union
		{
			uint64_t archnum;
			char archname[9];
		};
		
		union
		{
			uint64_t sysnum;
			char sysname[9];
		};
	} conv;
	memset(&conv, 0, sizeof(conv));
	conv.archnum = archnum;
	conv.sysnum = sysnum;
	
	// first convert the generic relocation types
	switch (type)
	{
	case RT_GEN_U8:			return "RT_GEN_U8";
	case RT_GEN_S8:			return "RT_GEN_S8";
	case RT_GEN_U16:		return "RT_GEN_U16";
	case RT_GEN_S16:		return "RT_GEN_S16";
	case RT_GEN_U32:		return "RT_GEN_U32";
	case RT_GEN_S32:		return "RT_GEN_S32";
	case RT_GEN_U64:		return "RT_GEN_U64";
	case RT_GEN_S64:		return "RT_GEN_S64";
	};
	
	// now consider target-specific relocations
	if (strcmp(conv.archname, "_x86_64_") == 0)
	{
		switch (type)
		{
		case 0x1001:		return "RT_X86_64_ABS8";
		case 0x1002:		return "RT_X86_64_ABS16";
		case 0x1003:		return "RT_X86_64_ABS32";
		case 0x1004:		return "RT_X86_64_ABS64";
		case 0x1005:		return "RT_X86_64_REL16";
		case 0x1006:		return "RT_X86_64_REL32";
		case 0x1007:		return "RT_X86_64_REL32_INDIR";
		};
	};
	
	// all options exhausted: give up
	return "?";
};

void atfDumpString(int level, const char *tagName, char *data, size_t size)
{
	char indent[256];
	memset(indent, 0, 256);
	memset(indent, ' ', level*2);
	
	printf("%s<%s>", indent, tagName);
	fwrite(data, 1, size, stdout);
	printf("</%s>\n", tagName);
};

void atfDumpValue64(int level, const char *tagName, char *data)
{
	char indent[256];
	memset(indent, 0, 256);
	memset(indent, ' ', level*2);
	
	uint64_t value = get(data, 8);
	printf("%s<%s>0x%016" PRIx64 "</%s>\n", indent, tagName, value, tagName);
};

void atfDumpValueI32(int level, const char *tagName, char *data)
{
	char indent[256];
	memset(indent, 0, 256);
	memset(indent, ' ', level*2);
	
	int value = (int) get(data, 4);
	printf("%s<%s>%d</%s>\n", indent, tagName, value, tagName);
};

void atfDumpValue8(int level, const char *tagName, char *data)
{
	char indent[256];
	memset(indent, 0, 256);
	memset(indent, ' ', level*2);
	
	printf("%s<%s>0x%02hhx</%s>\n", indent, tagName, *data, tagName);
};

void atfDumpEmpty(int level, const char *tagName)
{
	char indent[256];
	memset(indent, 0, 256);
	memset(indent, ' ', level*2);
	
	printf("%s<%s/>\n", indent, tagName);
};

void atfDumpInvalid(int level, uint16_t type)
{
	char indent[256];
	memset(indent, 0, 256);
	memset(indent, ' ', level*2);
	
	printf("%s<!-- UNKNOWN NODES HERE (0x%04" PRIx16 ") -->\n", indent, type);
};

void atfDumpNode(int level, const char *tagName, char *data, size_t size)
{
	char indent[256];
	memset(indent, 0, 256);
	memset(indent, ' ', level*2);
	
	level++;
	printf("%s<%s>\n", indent, tagName);
	
	char *end = data + size;
	while (data < end)
	{
		uint64_t header = get(data, 8);
		data += 8;
		
		uint16_t type = (uint16_t) (header >> 48);
		uint64_t subsize = (header & 0x0000FFFFFFFFFFFF);
		
		switch (type)
		{
		case ANNOT_Function:
			atfDumpNode(level, "Function", data, subsize);
			break;
		case ANNOT_Address:
			atfDumpValue64(level, "Address", data);
			break;
		case ANNOT_Size:
			atfDumpValue64(level, "Size", data);
			break;
		case ANNOT_Name:
			atfDumpString(level, "Name", data, subsize);
			break;
		case ANNOT_Code:
			atfDumpNode(level, "Code", data, subsize);
			break;
		case ANNOT_Unwind:
			atfDumpNode(level, "Unwind", data, subsize);
			break;
		case ANNOT_Offset:
			atfDumpValue64(level, "Offset", data);
			break;
		case ANNOT_AcmPop:
			atfDumpEmpty(level, "AcmPop");
			break;
		case ANNOT_AcmPushConst:
			atfDumpValue64(level, "AcmPushConst", data);
			break;
		case ANNOT_AcmPushStateEntry:
			atfDumpValue8(level, "AcmPushStateEntry", data);
			break;
		case ANNOT_AcmAdd:
			atfDumpEmpty(level, "AcmAdd");
			break;
		case ANNOT_AcmSub:
			atfDumpEmpty(level, "AcmSub");
			break;
		case ANNOT_AcmMemoryFetch:
			atfDumpEmpty(level, "AcmMemoryFetch");
			break;
		case ANNOT_StateEntry:
			atfDumpValue8(level, "StateEntry", data);
			break;
		case ANNOT_InsnPtr:
			atfDumpValue8(level, "InsnPtr", data);
			break;
		case ANNOT_AcmUndefined:
			atfDumpEmpty(level, "AcmUndefined");
			break;
		case ANNOT_LineInfo:
			atfDumpNode(level, "LineInfo", data, subsize);
			break;
		case ANNOT_File:
			atfDumpString(level, "File", data, subsize);
			break;
		case ANNOT_Line:
			atfDumpValueI32(level, "Line", data);
			break;
		default:
			atfDumpInvalid(level, type);
			break;
		};
		
		data += subsize;
	};
	
	printf("%s</%s>\n", indent, tagName);
};

int main(int argc, char *argv[])
{
	OptionList optlist;
	optInit(&optlist);
	optAdd(&optlist, "-h", "", "Show header information.", OPT_FLAG, &dumpHeader);
	optAdd(&optlist, "-S", "", "Show sections.", OPT_FLAG, &dumpSections);
	optAdd(&optlist, "-s", "", "Show symbols.", OPT_FLAG, &dumpSymbols);
	optAdd(&optlist, "-r", "", "Show relocations.", OPT_FLAG, &dumpRelocs);
	optAdd(&optlist, "-x", "SECTION", "Show a hex dump of the specified section in the file.", OPT_SHORT_STRING, &hexdumpSection);
	optAdd(&optlist, "-a", "", "Show annotations.", OPT_FLAG, &dumpAnnot);
	
	char **files = optParse(argc, argv, &optlist);
	if (files == NULL)
	{
		return 1;
	};
	
	if (files[0] == NULL)
	{
		fprintf(stderr, "%s: no input files\n", argv[0]);
		fprintf(stderr, "Use `%s --help' to get help.\n", argv[0]);
		return 1;
	};
	
	if (files[1] != NULL)
	{
		fprintf(stderr, "%s: expecting only one input file\n", argv[0]);
		return 1;
	};
	
	FILE *fp = fopen(files[0], "rb");
	if (fp == NULL)
	{
		fprintf(stderr, "%s: cannot open `%s': %s\n", argv[0], files[0], strerror(errno));
		return 1;
	};
	
	GXO_Header ghead;
	if (fread(&ghead, sizeof(GXO_Header), 1, fp) != 1)
	{
		fprintf(stderr, "%s: failed to read GXO header\n", argv[0]);
		return 1;
	};
	
	const char *byteOrder;
	if (ctGetLE(&ghead.gxoMagic, sizeof(ghead.gxoMagic)) == GXO_MAGIC)
	{
		byteOrder = "Little-endian";
		get = ctGetLE;
	}
	else if (ctGetBE(&ghead.gxoMagic, sizeof(ghead.gxoMagic)) == GXO_MAGIC)
	{
		byteOrder = "Big-endian";
		get = ctGetBE;
	}
	else
	{
		fprintf(stderr, "`%s' is not a valid GXO file", files[0]);
		return 1;
	};

	union
	{
		uint64_t val;
		char str[9];
	} strdecode;
	strdecode.str[8] = 0;

	if (dumpHeader)
	{	
		printf("GXO Header in `%s':\n", files[0]);
		printf("Byte order:			%s\n", byteOrder);
		strdecode.val = ghead.gxoArch;
		printf("Arch string:			%s\n", strdecode.str);
		strdecode.val = ghead.gxoSystem;
		printf("System string:			%s\n", strdecode.str);
		printf("Arch flags:			0x%016" PRIx64 " ", get(&ghead.gxoArchFlags, sizeof(ghead.gxoArchFlags)));
		printf("[ "); printArchFlags(ghead.gxoArch, get(&ghead.gxoArchFlags, sizeof(ghead.gxoArchFlags))); printf("]\n");
		printf("System flags:			0x%016" PRIx64 " ", get(&ghead.gxoSystemFlags, sizeof(ghead.gxoSystemFlags)));
		printf("[ "); printSystemFlags(ghead.gxoSystem, get(&ghead.gxoSystemFlags, sizeof(ghead.gxoSystemFlags))); printf("]\n");
		printf("Generic flags:			0x%016" PRIx64 " ", get(&ghead.gxoGenericFlags, sizeof(ghead.gxoGenericFlags)));
		printf("[ "); printGenericFlags(get(&ghead.gxoGenericFlags, sizeof(ghead.gxoGenericFlags))); printf("]\n");
		printf("Entry point:			0x%" PRIx64 "\n", get(&ghead.gxoEntryAddr, sizeof(ghead.gxoEntryAddr)));
		printf("Header size:			%" PRIu64 " bytes (expecting %d bytes)\n", get(&ghead.gxoHeaderSize, sizeof(ghead.gxoHeaderSize)), (int) sizeof(GXO_Header));
		printf("Section table offset:		%" PRIu64 " bytes into the file\n", get(&ghead.gxoSectionTableOffset, sizeof(ghead.gxoSectionTableOffset)));
		printf("Number of sections:		%" PRIu64 "\n", get(&ghead.gxoSectionCount, sizeof(ghead.gxoSectionCount)));
		printf("Section table entry size:	%" PRIu64 " bytes (expecting %d bytes)\n", get(&ghead.gxoSectionEntrySize, sizeof(ghead.gxoSectionEntrySize)), (int) sizeof(GXO_Section));
		printf("\n");
	};

	uint64_t offset = get(&ghead.gxoSectionTableOffset, sizeof(ghead.gxoSectionTableOffset));
	fseek(fp, offset, SEEK_SET);
	
	uint64_t sectCount = get(&ghead.gxoSectionCount, sizeof(ghead.gxoSectionCount));
	SectionInfo info[sectCount];
	
	uint64_t i;
	for (i=0; i<sectCount; i++)
	{
		GXO_Section sect;
		if (fread(&sect, sizeof(GXO_Section), 1, fp) != 1)
		{
			printf("Failed to read section table entry.\n");
			return 1;
		};
		
		strdecode.val = sect.sectName;
		
		strcpy(info[i].name, strdecode.str);
		info[i].offset = (uint32_t) get(&sect.sectOffset, sizeof(sect.sectOffset));
		info[i].vaddr = get(&sect.sectAddr, sizeof(sect.sectAddr));
		info[i].size = get(&sect.sectSize, sizeof(sect.sectSize));
	};
	
	if (dumpSections)
	{
		uint64_t offset = get(&ghead.gxoSectionTableOffset, sizeof(ghead.gxoSectionTableOffset));
		fseek(fp, offset, SEEK_SET);
		
		printf("Section table in `%s':\n", files[0]);
		printf("%-9s%-19s%-19s%-11s%-9s%-9s\n", "Name", "Address", "Size", "Offset", "Align", "Flags");
		
		uint64_t count = get(&ghead.gxoSectionCount, sizeof(ghead.gxoSectionCount));
		while (count--)
		{
			GXO_Section sect;
			if (fread(&sect, sizeof(GXO_Section), 1, fp) != 1)
			{
				printf("Failed to read section table entry.\n");
				return 1;
			};
			
			uint64_t flags = get(&sect.sectGenericFlags, sizeof(sect.sectGenericFlags));
			strdecode.val = sect.sectName;
			printf("%-9s0x%016" PRIx64 " 0x%016" PRIx64 " 0x%08" PRIx32 " 0x%06" PRIx64 " %c%c%c%c%c\n",
				strdecode.str,
				get(&sect.sectAddr, sizeof(sect.sectAddr)),
				get(&sect.sectSize, sizeof(sect.sectSize)),
				(uint32_t) get(&sect.sectOffset, sizeof(sect.sectOffset)),
				(uint64_t) (1ULL << get(&sect.sectAlignLog2, sizeof(sect.sectAlignLog2))),
				(flags & SECT_GEN_READ) ? 'R' : ' ',
				(flags & SECT_GEN_WRITE) ? 'W' : ' ',
				(flags & SECT_GEN_EXEC) ? 'X' : ' ',
				(flags & SECT_GEN_NOALLOC) ? 'N' : ' ',
				(flags & SECT_GEN_RELRO) ? 'P' : ' '
			);
		};
		
		printf("\n");
	};
	
	if (hexdumpSection != NULL)
	{
		uint64_t offset = get(&ghead.gxoSectionTableOffset, sizeof(ghead.gxoSectionTableOffset));
		fseek(fp, offset, SEEK_SET);
		
		GXO_Section sect;
		int found = 0;
		
		uint64_t count = get(&ghead.gxoSectionCount, sizeof(ghead.gxoSectionCount));
		while (count--)
		{
			if (fread(&sect, sizeof(GXO_Section), 1, fp) != 1)
			{
				printf("Failed to read section table entry.\n");
				return 1;
			};
			
			strdecode.val = sect.sectName;
			if (strcmp(strdecode.str, hexdumpSection) == 0)
			{
				found = 1;
				break;
			};
		};
		
		if (!found)
		{
			fprintf(stderr, "Could not find section named `%s'\n", hexdumpSection);
			return 1;
		};
		
		offset = get(&sect.sectOffset, sizeof(sect.sectOffset));
		if (offset == 0)
		{
			fprintf(stderr, "Section `%s' is not file-backed\n", hexdumpSection);
			return 1;
		};
		
		uint64_t size = get(&sect.sectSize, sizeof(sect.sectSize));
		
		char *data = (char*) malloc(size);
		assert(data != NULL);
		
		fseek(fp, offset, SEEK_SET);
		if (fread(data, size, 1, fp) != 1)
		{
			printf("Failed to read section data.\n");
			return 1;
		};
		
		printf("Hexdump of section `%s':\n", hexdumpSection);
		hexdump(data, size, get(&sect.sectAddr, sizeof(sect.sectAddr)));
		printf("\n");
	};
	
	if (dumpSymbols)
	{
		printf("Symbol table in file `%s':\n", files[0]);
		// find the .symtab section
		for (i=0; i<sectCount; i++)
		{
			if (strcmp(info[i].name, ".symtab") == 0) break;
		};
		
		if (i != sectCount)
		{
			printf("%-17s%-19s%-6s%-7s%-7s%-7s%-7s\n", "Name", "Value", "Size", "Scope", "Type", "Align", "Bucket");
			uint32_t offset = info[i].offset;
			
			while (1)
			{
				GXO_Symbol sym;
				fseek(fp, offset, SEEK_SET);
				
				if (fread(&sym, sizeof(GXO_Symbol), 1, fp) != 1)
				{
					printf("Failed to read symbol table entry.\n");
					return 1;
				};
				
				if (get(&sym.symName, sizeof(sym.symName)) == 0) break;
				
				char namebuf[16];
				fseek(fp, resolveAddr(info, sectCount, get(&sym.symName, sizeof(sym.symName))), SEEK_SET);
				fread(namebuf, 1, 16, fp);
				namebuf[15] = 0;
				
				printf("%-17s0x%016" PRIx64 " %-5d %-7s%-7s0x%04" PRIx64 " %02hhX\n",
					namebuf,
					get(&sym.symValue, sizeof(sym.symValue)),
					(int) get(&sym.symSize, sizeof(sym.symSize)),
					getSymScopeName(get(&sym.symScope, sizeof(sym.symScope))),
					getSymTypeName(get(&sym.symType, sizeof(sym.symType))),
					(uint64_t) (1ULL << get(&sym.symAlignLog2, sizeof(sym.symAlignLog2))),
					(uint8_t) get(&sym.symBucket, sizeof(sym.symBucket))
				);
				
				offset += sizeof(GXO_Symbol);
			};
			
			printf("\n");
		}
		else
		{
			printf("(no symbol table)\n");
		};
	};
	
	if (dumpRelocs)
	{
		printf("Relocation table in file `%s':\n", files[0]);

		for (i=0; i<sectCount; i++)
		{
			if (strcmp(info[i].name, ".reloc") == 0) break;
		};
		
		if (i != sectCount)
		{
			printf("%-19s%-25s%-17s%-19s\n", "Position", "Type", "Against", "Param");
			uint32_t offset = info[i].offset;
			
			while (1)
			{
				GXO_Reloc rel;
				fseek(fp, offset, SEEK_SET);
				
				if (fread(&rel, sizeof(GXO_Reloc), 1, fp) != 1)
				{
					printf("Failed to read relocation table entry.\n");
					return 1;
				};
				
				if (get(&rel.relType, sizeof(rel.relType)) == 0) break;

				GXO_Symbol sym;
				fseek(fp, resolveAddr(info, sectCount, get(&rel.relSymbol, sizeof(rel.relSymbol))), SEEK_SET);
				
				if (fread(&sym, sizeof(GXO_Symbol), 1, fp) != 1)
				{
					printf("Failed to read symbol table entry.\n");
					return 1;
				};

				char namebuf[16];
				fseek(fp, resolveAddr(info, sectCount, get(&sym.symName, sizeof(sym.symName))), SEEK_SET);
				fread(namebuf, 1, 16, fp);
				namebuf[15] = 0;
				
				printf("0x%016" PRIx64 " %-25s%-17s%" PRIi64 "\n",
					get(&rel.relPosition, sizeof(rel.relPosition)),
					getRelocTypeName(ghead.gxoArch, ghead.gxoSystem, get(&rel.relType, sizeof(rel.relType))),
					namebuf,
					(int64_t) get(&rel.relParam, sizeof(rel.relParam))
				);
				
				offset += sizeof(GXO_Reloc);
			};
			
			printf("\n");
		}
		else
		{
			printf("(no relocation table)\n");
		};
	};
	
	if (dumpAnnot)
	{
		printf("Annotation table in file `%s':\n", files[0]);

		for (i=0; i<sectCount; i++)
		{
			if (strcmp(info[i].name, ".annot") == 0) break;
		};
		
		if (i != sectCount)
		{
			fseek(fp, info[i].offset, SEEK_SET);
			char *buffer = (char*) malloc(info[i].size);
			fread(buffer, 1, info[i].size, fp);
			
			atfDumpNode(0, "GxoAnnot", buffer, info[i].size);
			free(buffer);
		};
	};
	
	fclose(fp);
	return 0;
};
