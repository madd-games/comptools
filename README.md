# CompTools

This is _Madd Compiler Tools_ or _CompTools_. It is a package whose purpose is to provide tools needed for compilation, linking, and otherwise managing code for a variety of targets using a portable interface. The whole project centers around `libcomptools`, a library which provides the following features, at various stages of completion:

|          Feature         |                                                                                                                     Description                                                                                                                     |   Progress  |
|:------------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-----------:|
|        C compiler        |                                                                         An API for pre-processing, parsing, analysing and compiling C code, available in subdirectory `cc`.                                                                         | In progress |
|   Object file handling   |                                                                            An API for reading/writing object files, and manipulating them in various ways, e.g. linking.                                                                            | Implemented |
|         Assembler        |                                                                                 Translation of a target's assembly language into `Object`s, used by the object API.                                                                                 | Implemented |
|       Data handling      |                                                                 Managing data types such as integers of specific sizes and byte orderings, to make targets independent of the host.                                                                 | Implemented |
| Just-in-time compilation | An API to load an `Object` directly into memory to be executed (e.g. via function calls), whether it be imported from a file, generated from a language like C, or any other way which can produce an `Object` useable by the CompTools object API. |   Planned   |
| Lexer                    | Manipulate regular expressions, and divide input strings into tokens based on them.                                                                                                                                                                 | Implemented |

## Command line interface

For every command listed below, _CMD_, the package produces a binary with the name `comptools-TARGET-CMD`, and nothing else. Typically, after installation, links with the name `comptools-CMD` and `CMD` itself would be created. For example, if the target is `x86_64-glidix`, the binary for `cc` would be called `comptools-x86_64-glidix-cc`, and the links `x86_64-glidix-cc` and `cc` itself would point to it.

* `as` - the assembler; takes assembly language source code as input and produces object files.
* `ld` - the linker; takes one or more object files as inputs, and produces executables and shared libraries.
* `objdump` - dumps information about object files, in the context of the CompTools object API (i.e. what the API sees).
* `cc` - C compiler; translates C source into object files, or into assembly, CTIR, etc.
* `tree2dot` - converts an XML tree (as produced by some CompTools commands/APIs) into a `dot` file which can then be converted into a diagram of a graph for simpler viewing.
